/*
 Copyright (c) 2020 Fondazione Ugo Bordoni.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This code is derived from: speedtest_worker.js by Federico Dossena
 Licensed under the GNU General Public License version 3,
 Original version:
   -> https://github.com/adolfintel/speedtest/
*/

// data reported to main thread
var testStatus = -1; // -1=not started, 0=starting, 1=download test, 2=ping+jitter test, 3=upload test, 4=finished, 5=abort/error
var dlStatus = ""; // download speed in megabit/s with 2 decimal digits
var isGraceTime = true;
var dlAvg = 0;
var totDownloaded = 0; //total number of downloaded bytes
var downloadDuration = 0; //total duration of download test in milliseconds
var numDownloadTestDone = 0; //Total number of measures done
var numDownloadTestFailed = 0; //Number of measures done
var ulStatus = ""; // upload speed in megabit/s with 2 decimal digits
var ulAvg = 0;
var totUploaded = 0; //total number of uploaded bytes
var uploadDuration = 0; //total duration of upload test in milliseconds
var numUploadTestDone = 0; //Total number of measures done
var numUploadTestFailed = 0; //Number of measures done
var pingHistory = []; // array that contains the historical values of ping
var pingAvg = 0;
var jitterAvg = 0;
var clientIp = ""; // client's IP address as reported by getIP.php
var dlProgress = 0; //progress of download test 0-1
var ulProgress = 0; //progress of upload test 0-1
var pingProgress = 0; //progress of ping+jitter test 0-1
var serverSelectionProgress = 0; //progress of choosing server 0-1
var countRetryServerSelection = 0; //number of times the choose server procedure has been reiterated (max settings.chooseServerRetries)
var testId = null; //test ID (sent back by telemetry if used, null otherwise)
var misuraNonValida = false;
var erroreStartMisura = false;
var start = new Date().getTime(); //start time
var serverName = "";
var stop; //stop time
var softwareVersion = "1.0"; //TODO to be updated
var log = ""; //telemetry log
function tlog(s) {
  if (settings.telemetry_level >= 2) {
    log += Date(Date.now()).toString() + ": " + s + "\n";
    console.log(s);
  }
}

function tverb(s) {
  if (settings.telemetry_level >= 3) {
    log += Date(Date.now()).toString() + ": " + s + "\n";
    console.log(s);
  }
}

function twarn(s) {
  if (settings.telemetry_level >= 1) {
    log += Date(Date.now()).toString() + " WARN: " + s + "\n";
    console.warn(s);
  }
}

// test settings. can be overridden by sending specific values with the start command
var settings = {
  test_order: "IP_D_U", //order in which tests will be performed as a string. D=Download, U=Upload, P=Ping+Jitter, I=IP, _=1 second delay
  time_ul_max: 10, // max duration of upload test in seconds
  time_dl_max: 10, // max duration of download test in seconds
  time_auto: false, // if set to true, tests will take less time on faster connections
  time_ulGraceTime: 2, //time to wait in seconds before actually measuring ul speed (wait for buffers to fill)
  time_dlGraceTime: 2, //time to wait in seconds before actually measuring dl speed (wait for TCP window to increase)
  count_ping: 11, // number of pings to perform in ping test
  url_dl: "garbage", // path to a large file or garbage.php, used for download test. must be relative to this js file
  url_ul: "empty", // path to an empty file, used for upload test. must be relative to this js file
  url_ping: "empty", // path to an empty file, used for ping test. must be relative to this js file
  url_getIp: "getIP", // path to getIP.php relative to this js file, or a similar thing that outputs the client's ip
  getIp_ispInfo: true, //if set to true, the server will include ISP info with the IP address
  getIp_ispInfo_distance: "km", //km or mi=estimate distance from server in km/mi; set to false to disable distance estimation. getIp_ispInfo must be enabled in order for this to work
  xhr_dlMultistream: 6, // number of download streams to use (can be different if enable_quirks is active)
  xhr_ulMultistream: 3, // number of upload streams to use (can be different if enable_quirks is active)
  xhr_multistreamDelay: 300, //how much concurrent requests should be delayed
  xhr_ignoreErrors: 1, // 0=fail on errors, 1=attempt to restart a stream if it fails, 2=ignore all errors
  xhr_dlUseBlob: false, // if set to true, it reduces ram usage but uses the hard drive (useful with large garbagePhp_chunkSize and/or high xhr_dlMultistream)
  xhr_ul_blob_megabytes: 20, //size in megabytes of the upload blobs sent in the upload test (forced to 4 on chrome mobile)
  garbagePhp_chunkSize: 100, // size of chunks sent by garbage.php (can be different if enable_quirks is active)
  enable_quirks: true, // enable quirks for specific browsers. currently it overrides settings to optimize for specific browsers, unless they are already being overridden with the start command
  ping_allowPerformanceApi: true, // if enabled, the ping test will attempt to calculate the ping more precisely using the Performance API. Currently works perfectly in Chrome, badly in Edge, and not at all in Firefox. If Performance API is not supported or the result is obviously wrong, a fallback is provided.
  overheadCompensationFactor: 1.0, //can be changed to compensatie for transport overhead. (see doc.md for some other values)
  useMebibits: false, //if set to true, speed will be reported in mebibits/s instead of megabits/s
  telemetry_level: 0, // 0=disabled, 1=basic (results only), 2=full (results and timing) 3=debug (results+log)
  url_telemetry: "/speedtest_measures/", // path to the script that adds telemetry data to the database
  telemetry_extra: "", //extra data that can be passed to the telemetry through the settings
  //	ip_misura: "193.104.137.135",     ip address of the server to use for measures
  //ip_misura: "http://192.168.80.62:8080",      ip address of the server HAWK4 to use for measures
  ip_misura: "hawk4.misurainternet.it",
  port_misura: "8080",
  //ip_db: "",          localhost
  //port_db: "8080",
  dl_threshold_error: 1.0, //threshold for the download error
  ul_threshold_error: 1.0, //threshold for the upload error
  csrftoken: "", //csrftoken

  chooseServerRetries: 1, // number of retries to find an active server (used only if xhr_ignoreErrors > 0 )
  count_ping_choose_server: 4, // number of pings performed for any server during chooseServer procedure
  alpha_choose_server_probability: 0.25,//0.2,//0.15, //alpha parameter for assigning server probabilities in chooseServer procedure
  timeout_ping_choose_server: 500,//500, //ping timeouts in chooseServer procedure
  timeout_threshold_choose_server: 180,//200, //timeout in chooseServer procedure after which all pending pings are terminated
  tolerance_timeout_choose_server: 4,//minimum initial number of completed servers to trigger the cancelPromise function when timeout expires
  measurement_servers: {

    eagle3: {
      url: "eagle3.misurainternet.it",
      port: "8080"
    },
    eagle4: {
      url: "eagle4.misurainternet.it",
      port: "8080"
    },
    hawk5: {
      url: "hawk5.misurainternet.it",
      port: "8080"
    },
    hawk6: {
      url: "hawk6.misurainternet.it",
      port: "8080"
    },
    hawk4: {
      url: "hawk4.misurainternet.it",
      port: "8080"
    }
  }
};

const server_list = Object.keys(settings.measurement_servers);
let current_tolerance_timeout = settings.tolerance_timeout_choose_server;
let timeout_threshold = null;

var xhr = null; // array of currently active xhr requests
var interval = null; // timer used in tests
var test_pointer = 0; //pointer to the next test to run inside settings.test_order

/*
  this function is used on URLs passed in the settings to determine whether we need a ? or an & as a separator
*/
function url_sep(url) {
  return url.match(/\?/) ? "&" : "?";
}

/*
	listener for commands from main thread to this worker.
	commands:
	-status: returns the current status as a JSON string containing testStatus, dlStatus, ulStatus, pingStatus, clientIp, jitterStatus, dlProgress, ulProgress, pingProgress
	-abort: aborts the current test
	-start: starts the test. optionally, settings can be passed as JSON.
		example: start {"time_ul_max":"10", "time_dl_max":"10", "count_ping":"50"}
*/
this.addEventListener("message", function(e) {
  var params = e.data.split("|");
  if (params[0] === "status") {
    // return status
    postMessage(
        JSON.stringify({
          misuraNonValida: misuraNonValida,
          erroreStartMisura: erroreStartMisura,
          isGraceTime: isGraceTime,
          testState: testStatus,
          dlStatus: dlStatus,
          dlAvg: dlAvg,
          ulStatus: ulStatus,
          ulAvg: ulAvg,
          pingHistory: pingHistory,
          pingAvg: pingAvg,
          clientIp: clientIp,
          ispInfo: ispInfo,
          jitterAvg: jitterAvg,
          dlProgress: dlProgress,
          ulProgress: ulProgress,
          pingProgress: pingProgress,
          testId: testId
        })
    );
  }
  if (params[0] === "start" && testStatus === -1) {
    // start new test
    testStatus = 0;
    try {
      // parse settings, if presentconsole.log("pingAvg:"+pingAvg);
      var s = {};
      try {
        s = JSON.parse(params[1]);
      } catch (e) {
        twarn(
            "Error parsing custom settings JSON. Please check your syntax: " + e
        );
      }
      //copy custom settings
      for (var key in s) {
        if (typeof settings[key] !== "undefined") {
          settings[key] = s[key];
        } else {
          twarn("Unknown setting ignored: " + key);
        }
      }
      // quirks for specific browsers. apply only if not overridden. more may be added in future releases
      if (
          settings.enable_quirks ||
          (typeof s.enable_quirks !== "undefined" && s.enable_quirks)
      ) {
        var ua = navigator.userAgent;
        if (/Firefox.(\d+\.\d+)/i.test(ua)) {
          if (typeof s.xhr_ulMultistream === "undefined") {
            // ff more precise with 1 upload stream
            settings.xhr_ulMultistream = 1;
          }
          if (typeof s.xhr_ulMultistream === "undefined") {
            // ff performance API sucks
            settings.ping_allowPerformanceApi = false;
          }
        }
        if (/Edge.(\d+\.\d+)/i.test(ua)) {
          if (typeof s.xhr_dlMultistream === "undefined") {
            // edge more precise with 3 download streams
            settings.xhr_dlMultistream = 3;
          }
        }
        if (/Chrome.(\d+)/i.test(ua) && !!self.fetch) {
          if (typeof s.xhr_dlMultistream === "undefined") {
            // chrome more precise with 5 streams
            settings.xhr_dlMultistream = 5;
          }
        }
      }
      if (/Edge.(\d+\.\d+)/i.test(ua)) {
        //Edge 15 introduced a bug that causes onprogress events to not get fired, we have to use the "small chunks" workaround that reduces accuracy
        settings.forceIE11Workaround = true;
      }
      if (/PlayStation 4.(\d+\.\d+)/i.test(ua)) {
        //PS4 browser has the same bug as IE11/Edge
        settings.forceIE11Workaround = true;
      }
      if (
          /Chrome.(\d+)/i.test(ua) &&
          /Android|iPhone|iPad|iPod|Windows Phone/i.test(ua)
      ) {
        //cheap af
        //Chrome mobile introduced a limitation somewhere around version 65, we have to limit XHR upload size to 4 megabytes
        settings.xhr_ul_blob_megabytes = 4;
      }
      //telemetry_level has to be parsed and not just copied
      if (typeof s.telemetry_level !== "undefined")
        settings.telemetry_level =
            s.telemetry_level === "basic"
                ? 1
                : s.telemetry_level === "full"
                ? 2
                : s.telemetry_level === "debug"
                    ? 3
                    : 0; // telemetry level
      //transform test_order to uppercase, just in case
      settings.test_order = settings.test_order.toUpperCase();
    } catch (e) {
      twarn(
          "Possible error in custom test settings. Some settings may not be applied. Exception: " +
          e
      );
    }
    // run the tests
    tverb(JSON.stringify(settings));
    test_pointer = 0;
    var iRun = false,
        dRun = false,
        uRun = false,
        pRun = false;
    var runNextTest = function() {
      if (testStatus == 5) return;
      if (test_pointer >= settings.test_order.length) {
        //test is finished
        if (settings.telemetry_level > 0)
          sendTelemetry(function(id) {
            testStatus = 4;
            //return dell'ID della misura salvata
            if (id != null) testId = id;
          });
        else testStatus = 4;
        return;
      }
      switch (settings.test_order.charAt(test_pointer)) {
        case "I":
        {
          test_pointer++;
          if (iRun) {
            runNextTest();
            return;
          } else iRun = true;
          getIp(runNextTest);
        }
          break;
        case "D":
        {
          test_pointer++;
          if (dRun) {
            runNextTest();
            return;
          } else dRun = true;
          testStatus = 1;
          dlTest(runNextTest);
        }
          break;
        case "U":
        {
          test_pointer++;
          if (uRun) {
            runNextTest();
            return;
          } else uRun = true;
          testStatus = 3;
          ulTest(runNextTest);
        }
          break;
        case "P":
        {
          test_pointer++;
          if (pRun) {
            runNextTest();
            return;
          } else pRun = true;
          testStatus = 2;
          pingTest(runNextTest);
        }
          break;
        case "_":
        {
          test_pointer++;
          setTimeout(runNextTest, 1000);
        }
          break;
        default:
          test_pointer++;
      }
    };
    chooseServer(runNextTest);
  }
  if (params[0] === "abort") {
    // abort command
    tlog("manually aborted");
    clearRequests(); // stop all xhr activity
    runNextTest = null;
    if (interval) clearInterval(interval); // clear timer if present
    if (settings.telemetry_level > 1) sendTelemetry(function() {});
    testStatus = 5;
    dlStatus = "";
    //dlHistory = [];
    dlAvg = 0;
    ulStatus = "";
    //ulHistory = [];
    ulAvg = 0;
    //pingStatus = "";
    //jitterStatus = "";  set test as aborted
    pingHistory = [];
    pingAvg = 0;
    jitterAvg = 0;
  }

  if (params[0] === "feedback") {
    //load parameters
    var s = {};
    try {
      s = JSON.parse(params[1]);
    } catch (e) {
      twarn("Error parsing custom JSON. Please check your syntax: " + e);
    }
    tverb(JSON.stringify(s));

    //send feedback command
    tlog("Sending feedback to db server");
    sendFeedback(s.csrftoken, s.feedback_value, s.idMisura, function() {});
  }
});
// stops all XHR activity, aggressively
function clearRequests() {
  tverb("stopping pending XHRs");
  if (xhr) {
    for (var i = 0; i < xhr.length; i++) {
      try {
        xhr[i].onprogress = null;
        xhr[i].onload = null;
        xhr[i].onerror = null;
      } catch (e) {}
      try {
        xhr[i].upload.onprogress = null;
        xhr[i].upload.onload = null;
        xhr[i].upload.onerror = null;
      } catch (e) {}
      try {
        xhr[i].abort();
      } catch (e) {}
      try {
        delete xhr[i];
      } catch (e) {}
    }
    xhr = null;
  }
}
// gets client's IP using url_getIp, then calls the done function
var ipCalled = false; // used to prevent multiple accidental calls to getIp
var ispInfo = ""; //used for telemetry
function getIp(done) {
  tverb("getIp");
  if (ipCalled) return;
  else ipCalled = true; // getIp already called?
  var startT = new Date().getTime();
  xhr = new XMLHttpRequest();
  xhr.onload = function() {
    tlog(
        "IP: " +
        xhr.responseText +
        ", took " +
        (new Date().getTime() - startT) +
        "ms"
    );
    try {
      var data = JSON.parse(xhr.responseText);
      clientIp = data.clientIp;
      ispInfo = data.ispInfo;
      serverName = data.serverName;
    } catch (e) {
      clientIp = "";
      serverName = "";
      ispInfo = "";
    }
    done();
  };
  xhr.onerror = function() {
    tlog("getIp failed, took " + (new Date().getTime() - startT) + "ms");
    done();
  };
  xhr.ontimeout = function() {
    erroreStartMisura = true;

    testStatus = 5;
    pingAvg = "NA";
    jitterAvg = "NA";
    clearRequests();
    tlog(
        "getIp test failed, on timeout " + (new Date().getTime() - startT) + "ms"
    );
    pingProgress = 1;
    done();
  };
  var misura_address =
      "https://" +
      settings.ip_misura +
      ":" +
      settings.port_misura +
      "/" +
      settings.url_getIp +
      url_sep(settings.url_getIp) +
      (settings.getIp_ispInfo
          ? "isp=true" +
          (settings.getIp_ispInfo_distance
              ? "&distance=" + settings.getIp_ispInfo_distance + "&"
              : "&")
          : "&") +
      "r=" +
      Math.random();

  xhr.open("GET", misura_address, true);
  xhr.timeout = 5000;
  xhr.send();
}
// download test, calls done function when it's over
var dlCalled = false; // used to prevent multiple accidental calls to dlTest
function dlTest(done) {
  tverb("dlTest");
  if (dlCalled) return;
  else dlCalled = true; // dlTest already called?
  var totLoaded = 0.0, // total number of loaded bytes
      previousTotLoaded = 0.0, // total number of loaded bytes in the previous update
      startT = new Date().getTime(), // timestamp when test was started
      bonusT = 0, //how many milliseconds the test has been shortened by (higher on faster connections)
      graceTimeDone = false, //set to true after the grace time is past
      failed = false, // set to true if a stream fails
      xhr = [];
  // function to create a download stream. streams are slightly delayed so that they will not end at the same time
  var testStream = function(i, delay) {
    setTimeout(
        function() {
          if (testStatus !== 1) return; // delayed stream ended up starting after the end of the download test
          //tverb("dl test stream started " + i + " " + delay);
          var prevLoaded = 0; // number of bytes loaded last time onprogress was called
          var x = new XMLHttpRequest();
          xhr[i] = x;
          xhr[i].onprogress = function(event) {
            //tverb("dl stream progress event " + i + " " + event.loaded);
            if (testStatus !== 1) {
              try {
                x.abort();
              } catch (e) {}
            } // just in case this XHR is still running after the download test
            // progress event, add number of new loaded bytes to totLoaded
            var loadDiff = event.loaded <= 0 ? 0 : event.loaded - prevLoaded;
            if (isNaN(loadDiff) || !isFinite(loadDiff) || loadDiff < 0) return; // just in case
            totLoaded += loadDiff;
            prevLoaded = event.loaded;
          }.bind(this);
          xhr[i].onload = function() {
            // the large file has been loaded entirely, start again
            //tverb("dl stream finished " + i);
            try {
              xhr[i].abort();
            } catch (e) {} // reset the stream data to empty ram
            testStream(i, 0);
          }.bind(this);
          xhr[i].onerror = function() {
            // error
            tverb("dl stream failed " + i);
            if (settings.xhr_ignoreErrors === 0) failed = true; //abort
            try {
              xhr[i].abort();
            } catch (e) {}
            delete xhr[i];
            if (settings.xhr_ignoreErrors === 1) testStream(i, 0); //restart stream
          }.bind(this);
          // send xhr
          try {
            if (settings.xhr_dlUseBlob) xhr[i].responseType = "blob";
            else xhr[i].responseType = "arraybuffer";
          } catch (e) {}
          var misura_address =
              "https://" +
              settings.ip_misura +
              ":" +
              settings.port_misura +
              "/" +
              settings.url_dl +
              url_sep(settings.url_dl) +
              "r=" +
              Math.random() +
              "&ckSize=" +
              settings.garbagePhp_chunkSize;
          xhr[i].open("GET", misura_address, true); // random string to prevent caching
          xhr[i].send();
        }.bind(this),
        1 + delay
    );
  }.bind(this);
  // open streams
  for (var i = 0; i < settings.xhr_dlMultistream; i++) {
    testStream(i, settings.xhr_multistreamDelay * i);
  }
  // every 100ms, update dlStatus
  interval = setInterval(
      function() {
        //tverb("DL: " + dlStatus + (graceTimeDone ? "" : " (in grace time)"));
        var t = new Date().getTime() - startT; //differenza di tempo tra questo momento e l'inizio del test
        if (graceTimeDone)
          dlProgress = (t + bonusT) / (settings.time_dl_max * 1000);
        if (t < 100) return;
        if (!graceTimeDone) {
          //Siamo ancora nel periodo iniziale di graceTime, quindi scartiamo questi valori
          if (t > 1000 * settings.time_dlGraceTime) {
            if (totLoaded > 0) {
              // if the connection is so slow that we didn't get a single chunk yet, do not reset
              startT = new Date().getTime();
              bonusT = 0;
              totLoaded = 0.0;
              previousTotLoaded = 0.0;
            }
            graceTimeDone = true;
            isGraceTime = false;
          }
        } else {
          //controllo il numero di byte scambiati e aggiorno i controlli per l'errore e previousTotLoaded
          if (totLoaded <= previousTotLoaded) {
            numDownloadTestFailed += 1; //counts the number of testStream failed
          }
          numDownloadTestDone += 1;
          previousTotLoaded = totLoaded;
          totDownloaded = totLoaded;
          var speed = totLoaded / (t / 1000.0);

          if (settings.time_auto) {
            //decide how much to shorten the test. Every 100ms, the test is shortened by the bonusT calculated here
            var bonus = (6.4 * speed) / 100000;
            bonusT += bonus > 800 ? 800 : bonus;
          }
          //update status
          //here there is a trick beacause toFixed function does not round correctly
          dlStatus = (
              Math.round(
                  ((speed * 8 * settings.overheadCompensationFactor) /
                      (settings.useMebibits ? 1048576 : 1000000)) *
                  10
              ) / 10
          ).toFixed(1);
          //dlStatus = ((speed * 8 * settings.overheadCompensationFactor) / (settings.useMebibits ? 1048576 : 1000000)).toFixed(1);
          // speed is multiplied by 8 to go from bytes to bits, overhead compensation is applied, then everything is divided by 1048576 or 1000000 to go to megabits/mebibits
          //Si può passare direttamente dlStatus...
          dlAvg = dlStatus;
          tverb("Average DL: " + dlAvg);
          if ((t + bonusT) / 1000.0 > settings.time_dl_max || failed) {
            // test is over, stop streams and timer
            if (failed || isNaN(dlStatus)) dlStatus = "Fail";
            clearRequests();
            clearInterval(interval);
            dlProgress = 1;
            downloadDuration = new Date().getTime() - startT;
            tlog ("Download test - Average DL bitrate: " + dlAvg + " Mbps, took " + downloadDuration + " ms");
            tlog(
                "DOWNLOAD: thresholdError: " +
                settings.dl_threshold_error +
                " numDownloadTestFailed: " +
                numDownloadTestFailed +
                " numDownloadTestDone: " +
                numDownloadTestDone +
                " percentageErrors: " +
                parseFloat(numDownloadTestFailed) /
                parseFloat(numDownloadTestDone)
            );
            //Controllo che il numero di misurazioni != da 0 siano più della threshold di errore
            if (
                parseFloat(numDownloadTestFailed) /
                parseFloat(numDownloadTestDone) >
                settings.dl_threshold_error
            ) {
              misuraNonValida = true;
            }
            //reinizializzo la variabile globale relativa al grace time
            isGraceTime = true;
            done();
          }
        }
      }.bind(this),
      100
  );
}

// upload test, calls done function whent it's over
var ulCalled = false; // used to prevent multiple accidental calls to ulTest
function ulTest(done) {
  tverb("ulTest");
  if (ulCalled) return;
  else ulCalled = true; // ulTest already called?
  // garbage data for upload test
  var r = new ArrayBuffer(1048576);
  var maxInt = Math.pow(2, 32) - 1;
  try {
    r = new Uint32Array(r);
    for (var i = 0; i < r.length; i++) r[i] = Math.random() * maxInt;
  } catch (e) {}
  var req = [];
  var reqsmall = [];
  for (var i = 0; i < settings.xhr_ul_blob_megabytes; i++) req.push(r);
  req = new Blob(req);
  r = new ArrayBuffer(262144);
  try {
    r = new Uint32Array(r);
    for (var i = 0; i < r.length; i++) r[i] = Math.random() * maxInt;
  } catch (e) {}
  reqsmall.push(r);
  reqsmall = new Blob(reqsmall);
  var totLoaded = 0.0, // total number of transmitted bytes
      previousTotLoaded = 0.0, // total number of loaded bytes in the previous update
      startT = new Date().getTime(), // timestamp when test was started
      bonusT = 0, //how many milliseconds the test has been shortened by (higher on faster connections)
      graceTimeDone = false, //set to true after the grace time is past
      failed = false, // set to true if a stream fails
      //thresholdError = 0.5;   threshold per considerare il test di upload non valido
      //numTestDone = 0.0,  counts the number of testStream done
      //numTestFailed = 0.0;  counts the number of testStream failed
      xhr = [];
  // function to create an upload stream. streams are slightly delayed so that they will not end at the same time
  var testStream = function(i, delay) {
    setTimeout(
        function() {
          if (testStatus !== 3) return; // delayed stream ended up starting after the end of the upload test
          //tverb("ul test stream started " + i + " " + delay);
          var prevLoaded = 0; // number of bytes transmitted last time onprogress was called
          var x = new XMLHttpRequest();
          xhr[i] = x;
          var ie11workaround;
          if (settings.forceIE11Workaround) ie11workaround = true;
          else {
            try {
              xhr[i].upload.onprogress;
              ie11workaround = false;
            } catch (e) {
              ie11workaround = true;
            }
          }
          if (ie11workaround) {
            tverb("ul stream progress event (ie11wa)");
            // IE11 workarond: xhr.upload does not work properly, therefore we send a bunch of small 256k requests and use the onload event as progress. This is not precise, especially on fast connections
            xhr[i].onload = xhr[i].onerror = function() {
              //tverb("ul stream progress event (ie11wa)");
              totLoaded += reqsmall.size;
              testStream(i, 0);
            };
            var misura_address =
                "https://" +
                settings.ip_misura +
                ":" +
                settings.port_misura +
                "/" +
                settings.url_ul +
                url_sep(settings.url_ul) +
                "r=" +
                Math.random();
            xhr[i].open("POST", misura_address, true); // random string to prevent caching
            try {
              xhr[i].setRequestHeader("Content-Encoding", "identity"); // disable compression (some browsers may refuse it, but data is incompressible anyway)
            } catch (e) {}
            try {
              xhr[i].setRequestHeader("Content-Type", "application/octet-stream"); //force content-type to application/octet-stream in case the server misinterprets it
            } catch (e) {}
            xhr[i].send(reqsmall);
          } else {
            // REGULAR version, no workaround
            xhr[i].upload.onprogress = function(event) {
              //tverb("ul stream progress event " + i + " " + event.loaded);
              if (testStatus !== 3) {
                try {
                  x.abort();
                } catch (e) {}
              } // just in case this XHR is still running after the upload test
              // progress event, add number of new loaded bytes to totLoaded
              var loadDiff = event.loaded <= 0 ? 0 : event.loaded - prevLoaded;
              if (isNaN(loadDiff) || !isFinite(loadDiff) || loadDiff < 0) return; // just in case
              totLoaded += loadDiff;
              prevLoaded = event.loaded;
            }.bind(this);
            xhr[i].upload.onload = function() {
              // this stream sent all the garbage data, start again
              //tverb("ul stream finished " + i);
              testStream(i, 0);
            }.bind(this);
            xhr[i].upload.onerror = function() {
              tverb("ul stream failed " + i);
              if (settings.xhr_ignoreErrors === 0) failed = true; //abort
              try {
                xhr[i].abort();
              } catch (e) {}
              delete xhr[i];
              if (settings.xhr_ignoreErrors === 1) testStream(i, 0); //restart stream
            }.bind(this);
            // send xhr
            var misura_address =
                "https://" +
                settings.ip_misura +
                ":" +
                settings.port_misura +
                "/" +
                settings.url_ul +
                url_sep(settings.url_ul) +
                "r=" +
                Math.random();
            xhr[i].open("POST", misura_address, true); // random string to prevent caching
            try {
              xhr[i].setRequestHeader("Content-Encoding", "identity"); // disable compression (some browsers may refuse it, but data is incompressible anyway)
            } catch (e) {}
            try {
              xhr[i].setRequestHeader("Content-Type", "application/octet-stream"); //force content-type to application/octet-stream in case the server misinterprets it
            } catch (e) {}
            xhr[i].send(req);
          }
        }.bind(this),
        1
    );
  }.bind(this);
  // open streams
  for (var i = 0; i < settings.xhr_ulMultistream; i++) {
    testStream(i, settings.xhr_multistreamDelay * i);
  }
  // every 100ms, update ulStatus
  interval = setInterval(
      function() {
        //tverb("UL: " + ulStatus + (graceTimeDone ? "" : " (in grace time)"));
        var t = new Date().getTime() - startT; //differenza di tempo tra questo momento e l'inizio del test
        if (graceTimeDone)
          ulProgress = (t + bonusT) / (settings.time_ul_max * 1000);
        if (t < 100) return;
        if (!graceTimeDone) {
          //Siamo ancora nel periodo iniziale di graceTime, quindi scartiamo questi valori
          if (t > 1000 * settings.time_ulGraceTime) {
            if (totLoaded > 0) {
              // if the connection is so slow that we didn't get a single chunk yet, do not reset
              startT = new Date().getTime();
              bonusT = 0;
              totLoaded = 0.0;
              previousTotLoaded = 0.0;
            }
            graceTimeDone = true;
            isGraceTime = false;
          }
        } else {
          //controllo il numero di byte scambiati e aggiorno i controlli per l'errore e previousTotLoaded
          if (totLoaded <= previousTotLoaded) {
            numUploadTestFailed += 1; //counts the number of testStream failed
          }
          numUploadTestDone += 1;
          previousTotLoaded = totLoaded;
          totUploaded = totLoaded;
          var speed = totLoaded / (t / 1000.0); //velocità media dall'inizio del test ad adesso

          if (settings.time_auto) {
            //decide how much to shorten the test. Every 100ms, the test is shortened by the bonusT calculated here
            var bonus = (6.4 * speed) / 100000;
            bonusT += bonus > 800 ? 800 : bonus;
          }
          //update status
          //here there is a trick beacause toFixed function does not round correctly
          ulStatus = (
              Math.round(
                  ((speed * 8 * settings.overheadCompensationFactor) /
                      (settings.useMebibits ? 1048576 : 1000000)) *
                  10
              ) / 10
          ).toFixed(1);
          //ulStatus = ((speed * 8 * settings.overheadCompensationFactor) / (settings.useMebibits ? 1048576 : 1000000)).toFixed(1);
          // speed is multiplied by 8 to go from bytes to bits, overhead compensation is applied, then everything is divided by 1048576 or 1000000 to go to megabits/mebibits
          //ulAvg=temp.toFixed(2);
          ulAvg = ulStatus;
          tverb("Average UL: " + ulAvg);
          if ((t + bonusT) / 1000.0 > settings.time_ul_max || failed) {
            // test is over, stop streams and timer
            if (failed || isNaN(ulStatus)) ulStatus = "Fail";
            clearRequests();
            clearInterval(interval);
            ulProgress = 1;
            uploadDuration = new Date().getTime() - startT;
            tlog ("Upload test - Average UL bitrate: " + ulAvg + " Mbps, took " + uploadDuration + " ms");

            tlog(
                "UPLOAD: thresholdError: " +
                settings.ul_threshold_error +
                " numUploadTestFailed: " +
                numUploadTestFailed +
                " numUploadTestDone: " +
                numUploadTestDone +
                " percentageErrors: " +
                parseFloat(numUploadTestFailed) / parseFloat(numUploadTestDone)
            );
            //Controllo che il numero di misurazioni != da 0 siano più della threshold di errore
            if (
                parseFloat(numUploadTestFailed) / parseFloat(numUploadTestDone) >
                settings.ul_threshold_error
            ) {
              misuraNonValida = true;
            }
            //reinizializzo la variabile globale relativa al grace time
            isGraceTime = true;
            done();
          }
        }
      }.bind(this),
      100
  );
}
// ping+jitter test, function done is called when it's over
var ptCalled = false; // used to prevent multiple accidental calls to pingTest
function pingTest(done) {
  tverb("pingTest");
  if (ptCalled) return;
  else ptCalled = true; // pingTest already called?
  var startT = new Date().getTime(); //when the test was started
  var prevT = null; // last time a pong was received
  var ping = 0.0; // current ping value
  var jitter = 0.0; // current jitter value
  var i = 0; // counter of pongs received
  var prevInstspd = 0; // last ping time, used for jitter calculation
  var countErrorPing = 0;
  xhr = [];
  // ping function
  var doPing = function() {
    //tverb("ping");
    pingProgress = i / settings.count_ping;
    //console.log("pingProgress: " + pingProgress);     la variabile i viene incrementata
    prevT = new Date().getTime();
    xhr[0] = new XMLHttpRequest();
    xhr[0].onload = function() {
      // pong
      //tverb("pong");
      if (i === 0) {
        prevT = new Date().getTime(); // first pong
      } else {
        var instspd = new Date().getTime() - prevT;
        //Valore di default di settings.ping_allowPerformanceApi = True (tranne che su Firefox). API non lavorano bene su tutti i browser. Disattivare?
        if (settings.ping_allowPerformanceApi) {
          try {
            //try to get accurate performance timing using performance api
            var p = performance.getEntries();
            p = p[p.length - 1];
            var d = p.responseStart - p.requestStart;
            if (d <= 0) d = p.duration;

            //Controllare. Se il valore calcolato dalle performance API è minore di instspd allora prendo quel valore.
            if (d > 0 && d < instspd) instspd = d;
          } catch (e) {
            //if not possible, keep the estimate
            tverb("Performance API not supported, using estimate");
          }
        }
        //noticed that some browsers randomly have 0ms ping
        if (instspd < 1) instspd = prevInstspd;
        if (instspd < 1) instspd = 1;

        ping = instspd;
        prevInstspd = instspd;
        //console.log("instspd: " + instspd + " prevInstspd: " + prevInstspd + " ping: " + ping);
      }
      //console.log("prevT: " + prevT); prevT viene incrementato

      //Approssimazione del valore di ping a 2 cifre decimali
      //pingStatus = ping.toFixed(2);

      var total = 0;
      //Invece del pingStatus approssimato, io salverei il ping non approssimato per il calcolo di pingAvg e jitter.
      //Approssimo solo alla fine per la visualizzazione
      pingHistory.push(ping);
      for (var j = 0; j < pingHistory.length; j++) {
        total += parseFloat(pingHistory[j]);
      }
      var temp = total / parseFloat(pingHistory.length);
      pingAvg = temp.toFixed(1);
      i++;
      //tverb("ping: " + pingStatus + ' Average ping: ' + pingAvg + ' calculated from ' + pingHistory + " jitter: " + jitterStatus +' jitter: ' + jitter);
      tverb(
          "Average ping: " +
          pingAvg +
          " calculated from " +
          pingHistory +
          " jitter: " +
          jitter
      );
      if (i < settings.count_ping) {
        doPing();
      } else {
        // more pings to do?
        pingProgress = 1;
        pingHistory.shift();
        //console.log("FINAL pingHistory.length: " + pingHistory.length);
        tlog(" CLEANED PING HISTORY " + pingHistory);
        //Calcolo della media dei ping
        var total = 0;
        for (var j = 0; j < pingHistory.length; j++) {
          total += parseFloat(pingHistory[j]);
        }

        var pingAvgExact = total / parseFloat(pingHistory.length);
        //Trasformazione del ping da RTT a RTT/2
        pingAvg = (pingAvgExact / 2).toFixed(1);

        //Math.sqrt((sum_i=1^(pingHistory.length)(pingHistory[i] - pingAvg)^2)/pingHistory.length)
        //Calcolo del jitter

        var total = 0;
        for (var j = 0; j < pingHistory.length; j++) {
          var quadraticError = Math.pow(
              pingAvgExact - parseFloat(pingHistory[j]),
              2
          );
          total += quadraticError;
          //console.log('Total quadraticError: ' + total + ' and quadraticError: ' + quadraticError );
        }

        var jitterExact = Math.sqrt(
            parseFloat(total) / parseFloat(pingHistory.length)
        );
        jitterAvg = jitterExact.toFixed(1);
        tlog(
            "Average ping: " +
            pingAvg +
            " jitter: " +
            jitterAvg +
            ", took " +
            (new Date().getTime() - startT) +
            "ms"
        );
        done();
      }
    }.bind(this);
    xhr[0].onerror = function() {
      // a ping failed, cancel test
      tverb("ping failed");
      //console.log("errore ping");
      if (settings.xhr_ignoreErrors === 0) {
        //abort
        //pingStatus = "Fail";
        pingAvg = "NA";
        //jitterStatus = "Fail";
        jitterAvg = "NA";
        clearRequests();
        // tlog(
        //     "ping test failed, took " + (new Date().getTime() - startT) + "ms"
        // );
        tlog(
            "Ping test - ping test failed, took " + (new Date().getTime() - startT) + "ms"
        );
        pingProgress = 1;
        done();
      }
      if (settings.xhr_ignoreErrors === 1) {
        if (countErrorPing < 5) {
          countErrorPing++;
          tlog("ping test failed " + countErrorPing + "times");
          doPing(); //retry ping
        } else {
          erroreStartMisura = true;

          testStatus = 5;
          //abort
          //pingStatus = "Fail";
          pingAvg = "NA";
          //jitterStatus = "Fail";
          jitterAvg = "NA";
          clearRequests();
          // tlog(
          //     "ping test failed, took " + (new Date().getTime() - startT) + "ms"
          // );
          tlog(
              "Ping test - ping test failed, took " + (new Date().getTime() - startT) + "ms"
          );
          pingProgress = 1;
          done();
        }
      }
      if (settings.xhr_ignoreErrors === 2) {
        //ignore failed ping
        i++;
        if (i < settings.count_ping) doPing();
        else {
          // more pings to do?
          pingProgress = 1;
          tlog(
              "ping: " +
              pingAvg +
              " jitter: " +
              jitterAvg +
              ", took " +
              (new Date().getTime() - startT) +
              "ms"
          );

          done();
        }
      }
    }.bind(this);
    // send xhr
    var misura_address =
        "https://" +
        settings.ip_misura +
        ":" +
        settings.port_misura +
        "/" +
        settings.url_ping +
        url_sep(settings.url_ping) +
        "r=" +
        Math.random();
    xhr[0].open("GET", misura_address, true); // random string to prevent caching
    xhr[0].send();
  }.bind(this);
  doPing(); // start first ping
}
// telemetry
function sendTelemetry(done) {
  if (settings.telemetry_level < 1) return;
  xhr = new XMLHttpRequest();
  xhr.onload = function() {
    try {
      var jsonResponse = JSON.parse(xhr.responseText);
      var message = jsonResponse.result;
      var misuraID = jsonResponse.id;
      //Controllo che misuraID sia un numero
      if (!isNaN(misuraID)) {
        done(misuraID);
      } else {
        tlog("Id measure is not a number!");
        done(null);
      }
    } catch (e) {
      done(null);
    }
  };
  xhr.onerror = function() {
    tlog("TELEMETRY ERROR " + xhr.status);
    done(null);
  };
  //var db_address = "http://" + settings.ip_db + ":" + settings.port_db + "/" + settings.url_telemetry + url_sep(settings.url_telemetry) + "r=" + Math.random() + "/";
  //var db_address = "http://" + settings.ip_db + ":" + settings.port_db + "/" + settings.url_telemetry;
  var db_address = settings.url_telemetry;
  xhr.open("POST", db_address, true);
  xhr.setRequestHeader("X-csrftoken", settings.csrftoken);
  xhr.setRequestHeader("Content-Type", "application/json");

  //Seconda versione - tabella misura_speedtest
  stop = new Date().getTime();
  var ua = navigator.userAgent;
  var ip = clientIp;
  var byte_down = parseInt(totDownloaded);
  var time_down = parseInt(downloadDuration);
  var byte_up = parseInt(totUploaded);
  var time_up = parseInt(uploadDuration);
  var ping = parseFloat(pingAvg); //Ping in RTT/2
  var jitter = parseFloat(jitterAvg);
  var version = softwareVersion;

  //Formattazione della data per salvataggio su DB - da millisecondi a dateFormat
  var dateFormat = "Y-m-d H:M:S";
  var startDate = new Date(parseInt(start));
  var stopDate = new Date(parseInt(stop));
  tlog("startDate: " + startDate + " stopDate: " + stopDate);
  var tzoffset = startDate.getTimezoneOffset() * 60000; // offset in milliseconds: CET=-60, CEST=-120
  var startString = new Date(startDate - tzoffset).toISOString(); // Get a fake UTC time in order to send correct timezoned time
  var stopString = new Date(stopDate - tzoffset).toISOString();
  var feedback = -1; //-1 se non ancora espresso dall'utente. 0 se l'utente NON è soddisfatto della misura. 1 se l'utente è soddisfatto.

  //Aggiunte informazioni sul numero di test fatti e falliti per dl/ul
  var num_test_done_down = parseInt(numDownloadTestDone);
  var num_test_failed_down = parseInt(numDownloadTestFailed);
  var num_test_done_up = parseInt(numUploadTestDone);
  var num_test_failed_up = parseInt(numUploadTestFailed);

  //Trasformo i dati in un oggetto JSON
  var obj = new Object();
  //obj.id = id;                id della misura se si vuole fare riferimento ad una misura precedente (se valorizzato, verrà cercata la misura, se trovata si modificano tutti i dati, altrimenti viene restituito errore
  //obj.sessionid = sessionid;  sessionid: <id_di_sessione_rilasciato_da_django>,
  obj.server = serverName;
  obj.ip = ip; //<ip_del_client>
  obj.ispinfo = ispInfo; //<stringa_ispinfo>
  obj.ua = ua; //<stringa_user_agent>
  obj.version = version; //<stringa_versione_software>
  obj.start = startString;
  obj.stop = stopString;
  obj.feedback = feedback; //<valore_del_feedback>

  var tests = [];

  //download
  var down = new Object();
  down.type = "download"; //tipo_di_test (down, up, ping, jitter)>
  down.value = byte_down; //il numero di byte
  down.time = time_down; //la durata del test
  down.num_done = num_test_done_down; //numero di test totali effettuati per il download
  down.num_failed = num_test_failed_down; //numero di test falliti per il download
  down.compensation_factor = settings.overheadCompensationFactor; //compensation factor utilizzato per il calcolo di upload e download. null per ping e jitter
  tests.push(down);

  //upload
  var up = new Object();
  up.type = "upload"; //tipo_di_test (down, up, ping, jitter)>
  up.value = byte_up; //il numero di byte
  up.time = time_up; //la durata del test
  up.num_done = num_test_done_up; //numero di test totali effettuati per l'upload
  up.num_failed = num_test_failed_up; //numero di test falliti per l'upload
  up.compensation_factor = settings.overheadCompensationFactor; //compensation factor utilizzato per il calcolo di upload e download. null per ping e jitter
  tests.push(up);

  //ping
  var ping_obj = new Object();
  ping_obj.type = "ping"; //tipo_di_test (down, up, ping, jitter)>
  ping_obj.value = null; //per ping null
  ping_obj.time = ping; //valore di ping
  ping_obj.num_done = null; //numero di test totali effettuati per il download
  ping_obj.num_failed = null; //numero di test falliti per il download
  ping_obj.compensation_factor = null; //compensation factor utilizzato per il calcolo di upload e download. null per ping e jitter
  tests.push(ping_obj);

  //jitter
  var jitter_obj = new Object();
  jitter_obj.type = "jitter"; //tipo_di_test (down, up, ping, jitter)>
  jitter_obj.value = jitter; //valore di jitter
  jitter_obj.time = null; //per jitter null
  jitter_obj.num_done = null;
  jitter_obj.num_failed = null;
  jitter_obj.compensation_factor = null; //compensation factor utilizzato per il calcolo di upload e download. null per ping e jitter
  tests.push(jitter_obj);

  obj.tests = tests;

  var jsonString = JSON.stringify(obj);
  tlog("Sending JSON informations");
  xhr.send(jsonString);
  tlog("JSON informations sent");
}

function sendFeedback(csrftoken, feedback_value, id_misura, done) {
  tverb("Saving feedback into DB server");
  xhr = new XMLHttpRequest();
  xhr.onload = function() {
    try {
      var jsonResponse = JSON.parse(xhr.responseText);
      var message = jsonResponse.result;
      tlog("message: " + message);
      done(null);
    } catch (e) {
      done(null);
    }
  };

  xhr.onerror = function() {
    tlog("SEND FEEDBACK ERROR " + xhr.status);
    done(null);
  };

  //var db_address = "http://" + settings.ip_db + ":" + settings.port_db + "/" + settings.url_telemetry + id_misura + "/";
  var db_address = settings.url_telemetry + id_misura + "/";
  xhr.open("PATCH", db_address, true);
  xhr.setRequestHeader("X-csrftoken", csrftoken);
  xhr.setRequestHeader("Content-Type", "application/json");

  //Trasformo i dati in un oggetto JSON
  var obj = new Object();
  //aggiungo il valore del feedback ai precedenti dati e li invio per il salvataggio su db
  obj.feedback = parseInt(feedback_value); //-1 se non ancora espresso dall'utente. 0 se l'utente NON è soddisfatto della misura. 1 se l'utente è soddisfatto.

  var jsonString = JSON.stringify(obj);
  tlog("Sending JSON feedback");
  xhr.send(jsonString);
  tlog("JSON feedback sent");
}


var cScalled = false;
async function chooseServer(done) {

  tverb("choosing server");
  if (cScalled)
    return;
  else
    cScalled = true; // server chosen
  let pingAvgs = {};
  let jitterAvgs = {};
  let pingCountErrors = {};
  let ping_promises = [];
  let cancel_promises_handlers = [];
  let finished_promises_srvs = [];
  xhr = [];

  server_list.forEach(function(name_server,index_server) {

    pingAvgs[name_server] = null;
    jitterAvgs[name_server] = null;
    pingCountErrors[name_server] = 0;

    let startT = new Date().getTime(); //when the test was started
    let prevT = null; // last time a pong was received
    let ping = 0.0; // current ping value
    let i = 0; // counter of pongs received
    let prevInstspd = 0; // last ping time, used for jitter calculation
    let pingHistoryCurrentSrv =[];
    let finished = false;
    let cancelPing = () => finished = true;

    // ping function
    var doPingPerServer = function (complete) {
      prevT = new Date().getTime();
      xhr[index_server] = new XMLHttpRequest();

      xhr[index_server].onload = function () {

        if (i === 0) {
          prevT = new Date().getTime(); // first pong
        } else {
          var instspd = new Date().getTime() - prevT;
          //Valore di default di settings.ping_allowPerformanceApi = True (tranne che su Firefox). API non lavorano bene su tutti i browser. Disattivare?
          if (settings.ping_allowPerformanceApi) {
            try {
              //try to get accurate performance timing using performance api
              var p = performance.getEntries();
              p = p[p.length - 1];
              var d = p.responseStart - p.requestStart;
              if (d <= 0)
                d = p.duration;

              //Controllare. Se il valore calcolato dalle performance API è minore di instspd allora prendo quel valore.
              if (d > 0 && d < instspd)
                instspd = d;
            }
            catch (e) {
              //if not possible, keep the estimate
              tverb("Performance API not supported, using estimate");
            }
          }
          //noticed that some browsers randomly have 0ms ping
          if (instspd < 1)
            instspd = prevInstspd;
          if (instspd < 1)
            instspd = 1;

          ping = instspd;
          prevInstspd = instspd;
          //console.log("instspd: " + instspd + " prevInstspd: " + prevInstspd + " ping: " + ping);
        }
        //console.log("prevT: " + prevT); prevT viene incrementato

        //Approssimazione del valore di ping a 2 cifre decimali
        //pingStatus = ping.toFixed(2);

        var total = 0;
        //Invece del pingStatus approssimato, io salverei il ping non approssimato per il calcolo di pingAvg e jitter.
        //Approssimo solo alla fine per la visualizzazione
        pingHistoryCurrentSrv.push(ping);
        i++;
        if (i < settings.count_ping_choose_server) {
          doPingPerServer(complete);
        } else {
          // il check su finished sottostante è superfluo, se viene chiamato delete durante l'esecuzione di doPing
          //l'esecuzione si ferma e la promise ritorna prima di concludere la funzione doPing (quindi non arriva qui)
          if (!finished){
            finished = true;
            computefinalAvgs();
            complete(name_server,true);
          }
        }
      }.bind(this);

      xhr[index_server].onerror = function () {
        //onerror: increase te number of failed pings and save the penality value as ping = timeout

        //save penalty value as ping, increase pingCountErrors and go on
        pingCountErrors[name_server]++;
        pingHistoryCurrentSrv.push(settings.timeout_ping_choose_server); //push timeout value as penalty
        i++;
        if (i < settings.count_ping_choose_server) {
          doPingPerServer(complete);
        }
        else {
          // il check su finished sottostante è superfluo, se viene chiamato delete durante l'esecuzione di doPing
          //l'esecuzione si ferma e la promise ritorna prima di concludere la funzione doPing (quindi non arriva qui)
          if (!finished){
            finished = true;
            computefinalAvgs();
            complete(name_server,true);
          }
        }

      }.bind(this);
      xhr[index_server].ontimeout = function () {
        //onTimeout: increase te number of failed pings and save the penality value as ping = timeout
        //NB ontimeout si verifica anche quando manca il pairing tra l'utente e la rete pubblica del server

        //save penalty value as ping, increase pingCountErrors and go on
        pingCountErrors[name_server]++;
        pingHistoryCurrentSrv.push(settings.timeout_ping_choose_server); //push timeout value as penalty
        i++;
        if (i < settings.count_ping_choose_server) {
          doPingPerServer(complete);
        }
        else {
          // il check su finished sottostante è superfluo, se viene chiamato delete durante l'esecuzione di doPing
          //l'esecuzione si ferma e la promise ritorna prima di concludere la funzione doPing (quindi non arriva qui)
          if (!finished){
            finished = true;
            computefinalAvgs();
            complete(name_server,true);
          }
        }

      }.bind(this);
      // send xhr
      var misura_address = "https://" + settings.measurement_servers[name_server].url + ":" + settings.measurement_servers[name_server].port + "/" + settings.url_ping + url_sep(settings.url_ping) + "r=" + Math.random();  // random string to prevent caching
      xhr[index_server].timeout = settings.timeout_ping_choose_server;
      xhr[index_server].open("GET", misura_address, true);
      xhr[index_server].send();


      cancelPing = function() {
        if (finished) {
          tverb("CancelPing: promise already finished,skip " + name_server);
          return;
        }
        else {
          tverb("CancelPing: Cancelling promise " + name_server);
          finished = true;
          pingCountErrors[name_server] = settings.count_ping_choose_server;
          //pingAvgs e jitterAvgs dei server cancellati non vengono mai usati
          //però li settiamo per evitare incongruenze nei modelli
          pingAvgs[name_server] = settings.timeout_ping_choose_server; //push timeout value as penalty
          jitterAvgs[name_server] = 0;
          complete(name_server,false);
        }
      };


    }.bind(this);


    let computefinalAvgs = function() {
      pingHistoryCurrentSrv.shift();
      tlog(" CLEANED PING HISTORY " + name_server + " " + pingHistoryCurrentSrv);
      //Calcolo della media dei ping
      let total = 0;
      for (let j = 0; j < pingHistoryCurrentSrv.length; j++) {
        total += parseFloat(pingHistoryCurrentSrv[j]);
      }

      let pingAvgExact = total / parseFloat(pingHistoryCurrentSrv.length);
      //Trasformazione del ping da RTT a RTT/2
      pingAvgs[name_server] = (pingAvgExact / 2).toFixed(1);
      //Calcolo del jitter
      total = 0;
      for (let j = 0; j < pingHistoryCurrentSrv.length; j++) {
        let quadraticError = Math.pow(pingAvgExact - parseFloat(pingHistoryCurrentSrv[j]), 2);
        total += quadraticError;
      }
      let jitterExact = Math.sqrt(parseFloat(total) / parseFloat(pingHistoryCurrentSrv.length));
      jitterAvgs[name_server] = jitterExact.toFixed(1);
      tlog("Average ping: " + pingAvgs[name_server] + " jitter: " + jitterAvgs[name_server] + ", took " + (
          new Date().getTime() - startT) + "ms");
    };


    ping_promises.push(new Promise(
        (resolve,reject) => {
          doPingPerServer((srv,outcome) =>{

            if (outcome){
              finished_promises_srvs.push(srv);
              if (finished_promises_srvs.length === 1){
                timeout_threshold = setTimeout(cancelPromises, settings.timeout_threshold_choose_server);
              }

              resolve();
            }
            else {
              reject();
            }
          });
        })
    );

    cancel_promises_handlers.push(cancelPing);

  });

  //let timeout_threshold = setTimeout(cancelPromises, settings.timeout_threshold_choose_server);

  function cancelPromises () {
    tlog("ChooseServer: TIMEOUT fired");
    let tolerance = current_tolerance_timeout;
    let solved = 0;
    let ct = 0;

    if(finished_promises_srvs.length >= tolerance){
      while ( ct < finished_promises_srvs.length && solved < tolerance){
        let srv = finished_promises_srvs[ct];
        //console.log("CCC ",srv," ", server_ping_data[srv].promise)
        if (pingCountErrors[srv] < settings.count_ping_choose_server ){
          solved++;
        }
        ct++;
      }
    }
    if (solved >= tolerance || solved === server_list.length) {
      tlog("ChooseServer: Stop pending pings for server selection...");
      for (let j = 0; j < cancel_promises_handlers.length; j++) {
        cancel_promises_handlers[j]();
      }
      clearRequests();

    }
    else {
      //restart counter
      tlog("ChooseServer: TIMEOUT restarted, reachable servers number below the tolerance threshold");
      current_tolerance_timeout = Math.ceil(current_tolerance_timeout / 2);
      clearTimeout(timeout_threshold);
      timeout_threshold = setTimeout(cancelPromises, settings.timeout_threshold_choose_server);
    }

  }

  await Promise.allSettled(ping_promises).then(function () {
    //cancelliamo il timeout pendente che lancia i cancelPing() e ripristiniamo la tolleranza sui server per il timeout
    clearTimeout(timeout_threshold);
    current_tolerance_timeout = settings.tolerance_timeout_choose_server;
    tverb(" Server selection: Promise.allSettled");
    //imp: qui ci troveremo con pingAvgs e jitterAvgs pieni, possiamo scegliere il server
    let serverProbs = [];
    let prob_denominator = 0;

    // tolgo dai server quelli che hanno dato sempre errore/timeout e quelli stoppati da cancelPromises
    server_list.forEach(function(name_server,index_server) {
      if (pingCountErrors[name_server] >= settings.count_ping_choose_server)
      { //remove unreachable servers
        delete pingAvgs[name_server];
      }
      else {
        prob_denominator += Math.exp(- settings.alpha_choose_server_probability *  pingAvgs[name_server]);
      }
    });

    if (Object.keys(pingAvgs).length < 1) {
      //no server is reachable

      if (settings.xhr_ignoreErrors === 0) {
        //abort test
        pingAvg = "NA";
        jitterAvg = "NA";
        erroreStartMisura = true;
        testStatus = 5;
        clearRequests();
        tlog("server selection process failed");
        serverSelectionProgress = 1;
        done();
      }
      if (settings.xhr_ignoreErrors === 1) {
        //retry to perform the test settings.chooseServerRetries times.If no success abort test
        if (countRetryServerSelection < settings.chooseServerRetries) {
          countRetryServerSelection++;
          tlog("server selection failed " + countRetryServerSelection + "times");
          clearRequests();
          cScalled = false;
          chooseServer(done); //retry ping
        } else {
          countRetryServerSelection++;
          tlog("server selection failed " + countRetryServerSelection + "times");
          erroreStartMisura = true;

          testStatus = 5;
          //abort
          pingAvg = "NA";
          jitterAvg = "NA";
          clearRequests();
          tlog("server selection process failed");
          serverSelectionProgress = 1;
          done();
        }
      }
      if (settings.xhr_ignoreErrors === 2) {
        //ignore failed choose server selection, choose the fallback server,
        //the first in the measurement_server objects, and try the next test
        tlog("server selection failed, using first server in the list");
        settings.ip_misura = settings.measurement_servers[server_list[0]].url;
        settings.port_misura = settings.measurement_servers[server_list[0]].port;
        clearRequests();
        serverSelectionProgress = 1;
        done();
      }

    }

    else {
      //at least 1 server is reachable
      //assign probabilities
      Object.keys(pingAvgs).forEach(function (name_server, index_server) {
        serverProbs.push(
            {
              "server": name_server,
              "probability": Math.exp(-settings.alpha_choose_server_probability * pingAvgs[name_server]) / prob_denominator
            }
        );
      });

      // shuffle serverProbs array to increase randomness in server selection
      serverProbs = serverProbs.sort(() => Math.random() - 0.5);

      let weighted_random = function (probsArray_objs) {
        let srv_index=0;
        const rnd = Math.random();
        let cumulative = 0;
        probsArray_objs.forEach((obj, ind) => {
          if (rnd >= cumulative && rnd < cumulative + obj.probability) {
            srv_index = ind;
          }
          cumulative += obj.probability;
        });
        return probsArray_objs[srv_index].server;
      }
      let srv_name = weighted_random(serverProbs);
      tverb("Choose Server: probabilities: " + JSON.stringify(serverProbs));
      tlog("Choose Server: Server selection process terminated. Chosen server: " + srv_name);
      settings.ip_misura = settings.measurement_servers[srv_name].url;
      settings.port_misura = settings.measurement_servers[srv_name].port;
      clearRequests();
      serverSelectionProgress = 1;
      tverb("Choose Server Terminated, starting test...");
      done();
    }

  });
}