let jsURL = new URL(document.currentScript.src);
let currentDir = jsURL.pathname.match(/.*\//)[0];

//utente non soddisfatto
$("#closeResults2").click(function(event) {
  sendFeedback(0);
});

//utente soddisfatto
$("#closeResults").click(function(event) {
  sendFeedback(1);
});

function sendFeedback(feedback_value) {
  //il feedback viene inviato con un delay di 2 secondi
  setTimeout(function() {
    var worker = new Worker(currentDir + "speedtest_worker.js");
    const message = {
      feedback_value: feedback_value,
      idMisura: idMisura,
      csrftoken: csrftoken
    };
    worker.postMessage("feedback|" + JSON.stringify(message));
    worker = null;
  }, 2000);
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

var idMisura = -1;
var csrftoken = "";

// YYY audio beepSound for accessibility, the beepSound show the end of the measurement
var beepSound;
var alreadyBeep = false;

window.onload = function() {
  // controllo se si viene da home... si inizia direttamente
  var getQueryString = function(field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp("[?&]" + field + "=([^&#]*)", "i");
    var string = reg.exec(href);
    return string ? string[1] : null;
  };
  let siInizia = "no";
  siInizia = getQueryString("speedtest");
  if (siInizia === "inizia") {
    parti();
    (function(l) {
      window.history.replaceState({}, "", l.pathname);
    })(location);
  }

  // YYY audio beepSound for accessibility, the beepSound show the end of the measurement
  beepSound = document.getElementById("beepSound");
  beepSound.volume = 0;
};

function I(id) {
  return document.getElementById(id);
}

function playBeep() {
  if (beepSound.canPlayType("audio/mpeg")) {
    beepSound.currentTime = 0;
    beepSound.play();
  }
  alreadyBeep = true;
}

// interface vars/lets for frontend speedtest_UI.js written in P5 XXX
var pingAvg = 0;
var jitterAvg = 0;
var dlAvg = 0;
var ulAvg = 0;
var testStatus = -1;
/*var downloadDurationControl = 0;
      var uploadDurationControl = 0;*/
var downloadDurationDefault = 10000;
var uploadDurationDefault = 10000;
var misureDownload = [
  /* Struttura del dato: { t: ms, misura: 20 }*/
];
var misureUpload = [];
var startDownload = null;
var startUpload = null;
var clientIp;
var clientIpLetto;
var isp;
var errore = false;
var misuraNonValida = false;
var msgErrore =
  "Lo strumento ha rilevato delle anomalie durante la misura, effettua un nuovo test.";
var erroreStartMisura = false;
var msgErroreStartMisura =
  "Speed Test non riesce a far partire la misura! Controlla di essere connesso, di non essere in VPN o di non utilizzare browser con offuscamento del client ad esempio TOR o che integrano VPN verso server esterni al suolo italiano. Se il problema persiste contatta il nostro helpdesk e specifica con che operatore ti stai collegando!";
var isGraceTime = true;
csrftoken = getCookie("csrftoken");

var w = null; //speedtest worker

function parti() {
  now = new Date();
  console.log(now + ": Avvio della misura Speedtest");
  var cnt = 0;

  if (w != null) {
    //speedtest is running, abort

    w.postMessage("abort");
    testStatus = 4; // XXX per tasto ripeti in frontend
    w = null;
    I("startStopBtn").className = "";
  } else {
    //test is not running, begin

    if (alreadyBeep) alreadyBeep = false; // < reset audio beep

    if (errore) {
      // < se c'era un errore di misura non valida al ripartire lo toglie
      errore = false; // < per resettare controllo errore
      $(".alertStatus").fadeOut();
    }

    $("html, body").animate(
      {
        scrollTop: $("#speedtest-section").offset().top - 56
      },
      200
    );

    $("#speedtest-start").hide();
    $("#speedtest-canvas").show();
    $(".speedTestNav").show();

    restartUI(); // XXX
    w = new Worker(currentDir + "speedtest_worker.js");
    const message = { telemetry_level: "basic", csrftoken: csrftoken };
    w.postMessage("start|" + JSON.stringify(message)); // Add optional parameters (see doc.md)
    I("startStopBtn").className = "running";

    w.onmessage = function(e) {
      var data = JSON.parse(e.data);
      var status = data.testState; // -1=not started, 0=starting, 1=download test, 2=ping+jitter test, 3=upload test, 4=finished, 5=abort/error
      testStatus = status; // XXX
      if (status == 2) {
        I("progress").style.backgroundColor = "rgb(50, 214, 214)";
        // visualize alert for accessibility AA
        I("buttonIniziaSpeedtest").innerHTML = ""; // < remove the button if it's already running
        I("alertSpeedTestInCorso").innerHTML =
          "<p>La misura è in corso, ti preghiamo di attendere. Al termine della misura verrai avvisato da un suono. Per autorizzare la riproduzione sonora ti preghiamo di cliccare una volta <a href='' id='autorizzaBeep' onclick='beepSound.volume = 1; return false;' title='Autorizzaci a riprodurre un suono'>qui</a>.</p>";
        //
      }
      if (status >= 4) {
        //test completed
        I("progress").style.backgroundColor = "#0073e5";
        // visualize results for accessibility AA
        if (!alreadyBeep) playBeep(); // < sound confirmation, just once YYY
        I("alertSpeedTestTerminato").innerHTML =
          "<p>La misura è terminata, trovi i risultati di seguito.</p>";
        I("pingAvg").innerHTML =
          "<p>La misura di Ping è pari a <span class='misura'>" +
          pingAvg +
          " ms</span></p>";
        I("dlAvg").innerHTML =
          "<p>La misura di Download è pari a <span class='misura'>" +
          dlAvg +
          " Mbps</span></p>";
        I("ulAvg").innerHTML =
          "<p>La misura di Upload è pari a <span class='misura'>" +
          ulAvg +
          " Mbps</span></p>";
        I("jitterAvg").innerHTML =
          "<p>La misura di Jitter è pari a <span class='misura'>" +
          pingAvg +
          " ms</span></p>";
        I("buttonRipetiSpeedtest").innerHTML =
          "<p>Di seguito trovi le spiegazioni dei dati; per ripetere la misura <a href='?speedtest=inizia' title='Segui questo link per ripetere la misura' aria-label='Segui questo link per ripetere la misura'>segui questo link</a></p>";
        I("infoSpeedTestMisure").innerHTML =
          "<p>Approfondimento sui singoli dati: <ul><li>" +
          helpersMsgs[0] +
          "</li><li>" +
          helpersMsgs[1] +
          "</li><li>" +
          helpersMsgs[2] +
          "</li><li>" +
          helpersMsgs[3] +
          "</li></ul></p>";
        //
        I("startStopBtn").className = "";
        w = null;
        idMisura = data.testId; //id della misura effettuata

        //controllo se è partita la misura
        erroreStartMisura = data.erroreStartMisura;
        if (erroreStartMisura && !errore) {
          errore = true;
        }
        if (errore) {
          $(".alertStatus").fadeIn(); // < se c'è un errore....
          // < passare qui il messaggio d'errore diverso... XXX
          $(".alertStatus .alert-danger").html(msgErroreStartMisura);
        } else {
          // controllo misura valida ...
          misuraNonValida = data.misuraNonValida;
          if (misuraNonValida && !errore) {
            errore = true;
          }
          if (errore) {
            $(".alertStatus").fadeIn(); // < se c'è un errore....
            // < passare qui il messaggio d'errore diverso... XXX
            $(".alertStatus .alert-danger").html(msgErrore);
          } else {
            $("#modalSpeedResults").fadeIn();
          }
          //$("#modalSpeedResults").fadeIn()
        }
      }
      isGraceTime = data.isGraceTime;
      if (status == 1 && isGraceTime != true) {
        if (startDownload == null) {
          startDownload = new Date().getTime();
        }
        I("progress").style.backgroundColor = "#3CF986";
        dlAvg = data.dlAvg; //XXX
        misureDownload.push({
          t: new Date().getTime() - startDownload,
          misura: dlAvg
        });
      }
      if (status == 3 && isGraceTime != true) {
        if (startUpload == null) startUpload = new Date().getTime();
        I("progress").style.backgroundColor = "#4D00FF";
        ulAvg = data.ulAvg; // XXX
        misureUpload.push({
          t: new Date().getTime() - startUpload,
          misura: ulAvg
        });
      }
      //uploadDurationControl = data.uploadDuration;
      //downloadDurationControl = data.downloadDuration;
      pingAvg = data.pingAvg;
      jitterAvg = data.jitterAvg;
      isp = data.ispInfo;
      clientIpLetto = data.clientIp;
      var prog =
        (Number(data.dlProgress) * 2 +
          Number(data.ulProgress) * 2 +
          Number(data.pingProgress)) /
        5;
      I("progress").style.width = 100 * prog + "%"; // FINITO
      if (clientIpLetto != clientIp) {
        I("speedTestIP").innerHTML = clientIpLetto + " - " + isp; // E SCRITTO IP
        clientIp = clientIpLetto;
      }
    };
  }
}

setInterval(function() {
  if (w) w.postMessage("status");
}, 100);
