"use strict";

/***
 *     (                                                          (
 *     )\ )                   (        )             )            )\ )
 *    (()/(          (    (   )\ )  ( /(   (      ( /(        (  (()/(
 *     /(_))`  )    ))\  ))\ (()/(  )\()) ))\ (   )\())       )\  /(_))
 *    (_))  /(/(   /((_)/((_) ((_))(_))/ /((_))\ (_))/     _ ((_)(_))
 *    / __|((_)_\ (_)) (_))   _| | | |_ (_)) ((_)| |_     | | | ||_ _|
 *    \__ \| '_ \)/ -_)/ -_)/ _` | |  _|/ -_)(_-<|  _|    | |_| | | |
 *    |___/| .__/ \___|\___|\__,_|  \__|\___|/__/ \__|_____\___/ |___|
 *         |_|                                       |_____|
 */

// Speedtest 360 _ misurainternet.it
// An original speedtest UI written in P5js.
// Copyright © 2019-2020 Daniele @fupete Tabellini/Lcd Firenze
// Tutti i diritti riservati. All rights reserved.
// Un progetto per la pubblica utilità.
// GNU LGPLv3 License
//
// rel. 1.4 *September 29, 2020*
// - enhanced support for "tiny mode": better reflow 400%/1024px
// - accessibility: keyboard support
// rel. 1.3 *June 8, 2020*
// — strictly converted to ES5 (IE11 friendly)
// - minor adj (cross-browser compatibility)
// - updated p5.js > 0.9.0
// rel. 1.2 *April 22, 2020*
// - minor layout adjustments
// - accessibility support: screen reader workflow
// - enhanced support for "tiny mode": 400% zoom / 320x256px support
// rel. 1.0.0 *Halloween, 2019*
// — 1st official release
//
// Dependencies: it works only with the dedicated JS interface written
// in misura-speedtest.html and the original speedtest_worker.js based
// on HTML5 Speedtest v4.7.2 by Federico Dossena /
// https://github.com/adolfintel/speedtest/ / GNU LGPLv3 License /
// and modified by FUB.
// —

console.log("Speedtest 360 _ misurainternet.it");
console.log("rel. 1.4 *September 29, 2020*");

/* COLORS */
var fondoColoreW1 = "#001A33";
var fondoColoreW2 = "#011629";
var arcoColoreDownload = "#3CF986";
var graficoColoreDownload = "#3CF986";
var arcoColoreUpload = "#4D00FF";
var graficoColoreUpload = "#4D00FF";
// Note: text colors are below, on Blocchetto/Column1 calls as R G B values

/* LITTLE SANTA HELPERS MESSAGES */
var helpersMsgs = [
  /* PING */
  "Ping: ritardo medio, calcolato su 10 test, dal client verso il server di misura.",
  /* DOWNLOAD */
  "Download: velocità dati media nella tratta di downlink (server-client), calcolata su 10 secondi.",
  /* UPLOAD */
  "Upload: velocità dati media nella tratta di uplink (client-server), calcolata su 10 secondi.",
  /* JITTER */
  "Jitter: variazione del ritardo medio, calcolato sui 10 test di Ping.",
  /* PERFORMANCE */
  "Performance: I valori misurati non hanno valore probatorio."
];

/* VARS */
var isKeyboardActive = true; // < for keyboard alternatives, set false to disable keys calling disableKeyboard() or toggleKeyboard() functions;
var tinyMode = 360; //px    special mobile mode for tiny little screens (320x256px or 400% zoom)
var minH = 440; //px    minimin height for desktop
//
var appenaFinito = false;
var initGauge = true;
var incInitGauge = 0;
var UIpiccola = false;
var mobileLayout = false;
var helpers = [];
var hX,
  hY,
  hMsg;
// text
var w3,
  w2,
  w1,
  hInit,
  wTot,
  hTot,
  rowH1,
  colH1;
var ttReg,
  ttBold,
  rbMonoReg;
var tTotaleDownload = downloadDurationDefault / 1000;
var tTotaleUpload = uploadDurationDefault / 1000;
// gauge
var cenx,
  ceny; // < gauge center
var dimx,
  dimy; // < gauge size
var mostraLancette = true;
var mostraNumeriGauge = true;
// chart
var grScala = 10; // 100=0-100 300=0-300 500=0-500 1000=0-1000 per grafico
var grPosX;
var grPosY;
var grW;
var grH;
// save image
var recordImage = false;
var immagine;
var conta = 0;
// canvas
var cnv;

/* FONTS CANVAS */
function preload() {
  // local docker XXX
  // ttReg = loadFont('./assets/TitilliumWeb-Regular.ttf');
  // ttBold = loadFont('./assets/TitilliumWeb-Bold.ttf');
  // rbMonoReg = loadFont('./assets/RobotoMono-Regular.ttf');
  // online docker XXX
  ttReg = loadFont("/static/fonts/TitilliumWeb-Regular.ttf");
  ttBold = loadFont("/static/fonts/TitilliumWeb-Bold.ttf");
  rbMonoReg = loadFont("/static/fonts/RobotoMono-Regular.ttf");
}

/* SETUP (P5js) */
function setup() {
  cnv = createCanvas((wTot = windowWidth), (hTot = windowHeight)); // XXX
  cnv.parent("speedtest-canvas");
  hInit = hTot;
  windowResized(); // let's start
  textSize(18);
  colorMode(RGB, 255, 255, 255, 1);
  helpers.push(new littleSantaHelper(100, 100, helpersMsgs[0])); // PING
  helpers.push(new littleSantaHelper(100, 200, helpersMsgs[1])); // DOWNLOAD
  helpers.push(new littleSantaHelper(100, 300, helpersMsgs[2])); // UPLOAD
  helpers.push(new littleSantaHelper(100, 400, helpersMsgs[3])); // JITTER
  helpers.push(new littleSantaHelper(100, 400, helpersMsgs[4])); // PERFORMANCE
  setupButtons();
}

function setupKeyboardShortcuButton() {
  if (isKeyboardActive) {
    $("#toggleKeyboardShortcuts").html("Disattiva le scorciatoie da tastiera");
  } else {
    $("#toggleKeyboardShortcuts").html("Attiva le scorciatoie da tastiera");
  }
}

function setupButtons() {
  setupKeyboardShortcuButton();
  $("#toggleKeyboardShortcuts").click(function () {
    toggleKeyboard();
    setupKeyboardShortcuButton();
  });
  $("#saveImage").click(function () {
    salvaBtnAction();
  });
  $("#startMeasure").click(function () {
    partiBtnAction();
  });
}

/* RESPONSIVE CANVAS */
function windowResized() {
  wTot = windowWidth;
  hTot = windowHeight;
  var ratio = wTot / hTot;
  // mobileLayout
  if (wTot > 720 && ratio >= 1) 
    mobileLayout = false;
  else 
    mobileLayout = true;
  
  // let's adapt
  if (!mobileLayout) {
    var altezza = hTot - 56 - 96 - 96;
    if (altezza < minH) {
      // desktop layout has minimin height of 440px
      altezza = minH;
      hTot = altezza;
    }
    resizeCanvas(wTot, altezza);
    hInit = altezza;
  } else if (wTot < 360) {
    let altezza = 1047;
    resizeCanvas(wTot, altezza); // tiny layout
    hInit = altezza;
  } else {
    let altezza = 1047;
    resizeCanvas(wTot, altezza); // mobile layout, vertical orientation
    hInit = altezza;
  }
  // so we can init sizes & positions...
  initDim();
}

/* RESPONSIVE SIZES & POSITIONS */
function initDim() {
  if (!mobileLayout) {
    // DESKTOP >= 1024
    // columns
    w3 = (5 / 12) * wTot;
    w2 = (4 / 12) * wTot;
    w1 = (3 / 12) * wTot;
    // gauge
    if (testStatus != 4) {
      dimx = w3 / 1.47;
      dimy = w3 / 1.47; // < running size
    } else if (dimx != 222) {
      dimx = 222;
      dimy = 222; // < finished size
    }
    cenx = wTot - w3 / 2;
    ceny = hInit / 2;
    // textes
    rowH1 = (hInit / 2 + 96) / 4;
    // chart
    grW = w2 - 32 - 68;
    if (testStatus != 4) 
      grH = rowH1 * 4 - 96;
    grPosX = w1 + 68;
    grPosY = hInit / 2 - grH / 2;
  } else if (wTot < 360) {
    // mobile layout, tiny mode
    // columns is a unique column
    w1 = w2 = w3 = wTot;
    // gauge
    var misuraRiavvia;
    misuraRiavvia = 222;
    if (testStatus != 4) {
      // < running size
      var maxGauge = 202;
      dimx = maxGauge;
      dimy = maxGauge;
    } else if (dimx != misuraRiavvia) {
      dimx = misuraRiavvia;
      dimy = misuraRiavvia; // < finished size
    }
    cenx = wTot / 2;
    ceny = 136;
    // textes
    rowH1 = 103;
    // chart
    grW = wTot - 64;
    grH = 190;
    grPosX = 40;
    grPosY = 800;
  } else {
    // mobile layout, vertical orientation
    // columns is a unique column
    w1 = w2 = w3 = wTot;
    // gauge
    var misuraRiavvia;
    if (!UIpiccola) 
      misuraRiavvia = 222;
    else 
      misuraRiavvia = 172;
    if (testStatus != 4) {
      // < running size
      var maxGauge = ((wTot - 360) / (720 - 360)) * (360 - 265) + 265;
      dimx = maxGauge;
      dimy = maxGauge;
    } else if (dimx != misuraRiavvia) {
      dimx = misuraRiavvia;
      dimy = misuraRiavvia; // < finished size
    }
    cenx = wTot / 2;
    ceny = 248;
    // textes
    rowH1 = 124;
    // chart
    grW = wTot - 64;
    grH = 190;
    grPosX = 40;
    grPosY = 800;
  }
}

/* DRAW (P5js || 60fps loop) */
function draw() {
  clear();

  //
  // THE PRELIMINARIES
  //

  // columns backgrounds
  noStroke();
  if (!mobileLayout) {
    if (recordImage) {
      fill("#0066CC"); // < background right column W3 just for save canvas...
      rect(0, 0, wTot, hTot);
    }
    fill(fondoColoreW1);
    rect(0, 0, w1, hTot); // < background left column W1
    fill(fondoColoreW2);
    rect(w1, 0, w2, hTot); // < background center column W2
  } else {
    if (recordImage) {
      fill("#0066CC");
      rect(0, 0, wTot, hInit);
    }
    if (wTot < 360) {
      // tiny mode
      fill(fondoColoreW1);
      rect(0, 300, wTot, 743);
      fill(fondoColoreW2);
      rect(0, 743, wTot, hInit);
    } else {
      fill(fondoColoreW1);
      rect(0, 495, wTot, 743);
      fill(fondoColoreW2);
      rect(0, 743, wTot, hInit);
    }
  }
  // textes
  push();
  var posW1x = 0;
  var posW1y = 0;
  if (!mobileLayout) {
    posW1x = 32;
    posW1y = hInit / 2 - rowH1 * 2 + hInit / 16;
  } else if (wTot < 360) {
    // tiny mode
    posW1x = 24;
    posW1y = 300 + 48;
  } else {
    posW1x = 32;
    posW1y = 495 + 48;
  }
  translate(posW1x, posW1y);
  textAlign(LEFT);
  if (!mobileLayout) {
    posW1x += w1 - 72;
    posW1y -= 10;
    blocchettoTestoW1(0, "PING", pingAvg, 50, 214, 214, 2, "ms", 0);
    helpers[0].posx = posW1x;
    helpers[0].posy = posW1y;
    //
    translate(0, rowH1);
    blocchettoTestoW1(0, "DOWNLOAD", dlAvg, 60, 249, 134, 1, "Mbps", 1);
    posW1y += rowH1;
    helpers[1].posx = posW1x;
    helpers[1].posy = posW1y;
    //
    translate(0, rowH1);
    blocchettoTestoW1(0, "UPLOAD", ulAvg, 139, 135, 251, 3, "Mbps", 2);
    posW1y += rowH1;
    helpers[2].posx = posW1x;
    helpers[2].posy = posW1y;
    //
    translate(0, rowH1);
    blocchettoTestoW1(0, "JITTER", jitterAvg, 50, 214, 214, 2, "ms", 3);
    posW1y += rowH1;
    helpers[3].posx = posW1x;
    helpers[3].posy = posW1y;
  } else if (wTot < 360) {
    // < tiny mode
    posW1x += wTot - 52;
    posW1y -= 8;
    blocchettoTestoW1(0, "PING", pingAvg, 50, 214, 214, 2, "ms", 0);
    helpers[0].posx = posW1x;
    helpers[0].posy = posW1y;
    //
    translate(0, rowH1);
    blocchettoTestoW1(0, "DOWNLOAD", dlAvg, 60, 249, 134, 1, "Mbps", 1);
    posW1y += rowH1;
    helpers[1].posx = posW1x;
    helpers[1].posy = posW1y;
    //
    translate(0, rowH1);
    blocchettoTestoW1(0, "UPLOAD", ulAvg, 139, 135, 251, 3, "Mbps", 2);
    posW1y += rowH1;
    helpers[2].posx = posW1x;
    helpers[2].posy = posW1y;
    //
    translate(0, rowH1);
    blocchettoTestoW1(0, "JITTER", jitterAvg, 50, 214, 214, 2, "ms", 3);
    posW1y += rowH1;
    helpers[3].posx = posW1x;
    helpers[3].posy = posW1y;
  } else {
    // < mobile
    posW1x += wTot / 2 - 52;
    posW1y -= 8;
    blocchettoTestoW1(0, "PING", pingAvg, 50, 214, 214, 2, "ms", 0);
    helpers[0].posx = posW1x;
    helpers[0].posy = posW1y;
    //
    translate(wTot / 2 - 20, 0);
    blocchettoTestoW1(0, "JITTER", jitterAvg, 50, 214, 214, 2, "ms", 1);
    posW1x += wTot / 2 - 20;
    helpers[1].posx = posW1x;
    helpers[1].posy = posW1y;
    //
    translate(-wTot / 2 + 20, rowH1);
    blocchettoTestoW1(0, "DOWNLOAD", dlAvg, 60, 249, 134, 1, "Mbps", 2);
    posW1x += -wTot / 2 + 20;
    posW1y += rowH1;
    helpers[2].posx = posW1x;
    helpers[2].posy = posW1y;
    //
    translate(wTot / 2 - 16, 0);
    blocchettoTestoW1(0, "UPLOAD", ulAvg, 139, 135, 251, 3, "Mbps", 3);
    posW1x += wTot / 2 - 16;
    posW1y += 0;
    helpers[3].posx = posW1x;
    helpers[3].posy = posW1y;
  }
  pop();
  // small UI?
  if (mobileLayout || wTot < 1024) 
    UIpiccola = true;
  else 
    UIpiccola = false;
  
  // gauge background
  stroke(255, 0.1);
  if (!UIpiccola) 
    strokeWeight(32);
  else 
    strokeWeight(24);
  strokeCap(ROUND);
  noFill();
  if (initGauge) {
    incInitGauge++;
    if (incInitGauge > 60) 
      initGauge = false;
    arc(cenx, ceny, dimx - 28, dimy - 28, HALF_PI, map(incInitGauge, 0, 60, HALF_PI, TWO_PI + PI / 4));
  } else 
    arc(cenx, ceny, dimx - 28, dimy - 28, HALF_PI, TWO_PI + PI / 4);
  
  //
  // THE LOOP, RUNNING MODE ON
  //
  if (testStatus >= 0) {
    // < we are running, or we have already finished...

    // Adjust scale real-time for Chart & Gauge...
    if (grScala != 1000) {
      // 100 or 300 or 500
      if (grScala != 500) {
        // 100 or 300
        if (grScala != 300) {
          // 100 could be 300, 500 or 1000
          if (grScala != 100) {
            // 20 could be 100, 300, 500 or 1000
            if (testStatus == 1) {
              if (dlAvg > 500) {
                grScala = 1000;
              } else if (dlAvg > 300) {
                grScala = 500;
              } else if (dlAvg > 100) {
                grScala = 300;
              } else if (dlAvg > 50) {
                grScala = 100;
              } else if (dlAvg > 10) {
                grScala = 50;
              }
            } else if (testStatus == 3) {
              if (ulAvg > 500) {
                grScala = 1000;
              } else if (ulAvg > 300) {
                grScala = 500;
              } else if (ulAvg > 100) {
                grScala = 300;
              } else if (ulAvg > 50) {
                grScala = 100;
              } else if (ulAvg > 10) {
                grScala = 50;
              }
            }
          } else {
            // 100 could be 300, 500 or 1000
            if (testStatus == 1) {
              if (dlAvg > 500) {
                grScala = 1000;
              } else if (dlAvg > 300) {
                grScala = 500;
              } else if (dlAvg > 100) {
                grScala = 300;
              }
            } else if (testStatus == 3) {
              if (ulAvg > 500) {
                grScala = 1000;
              } else if (ulAvg > 300) {
                grScala = 500;
              } else if (ulAvg > 100) {
                grScala = 300;
              }
            }
          }
        } else {
          // 300 could be 500 or 1000
          if (testStatus == 1) {
            if (dlAvg > 500) {
              grScala = 1000;
            } else if (dlAvg > 300) {
              grScala = 500;
            }
          } else if (testStatus == 3) {
            if (ulAvg > 500) {
              grScala = 1000;
            } else if (ulAvg > 300) {
              grScala = 500;
            }
          }
        }
      } else {
        // 500 could be only 1000
        if (testStatus == 1) {
          if (dlAvg > 500) 
            grScala = 1000;
          }
        else if (testStatus == 3) {
          if (ulAvg > 500) 
            grScala = 1000;
          }
        }
    }
    // Gauge Download
    if (dlAvg > 0) {
      // Download in progress or already done
      stroke(arcoColoreDownload);
      if (!UIpiccola) 
        strokeWeight(16);
      else 
        strokeWeight(12);
      strokeCap(ROUND);
      noFill();
      arc(cenx, ceny, dimx, dimy, HALF_PI, map(dlAvg, 0, grScala, HALF_PI, TWO_PI + PI / 4));
      if (mostraLancette) {
        push();
        strokeWeight(2);
        translate(cenx, ceny);
        rotate(map(dlAvg, 0, grScala, HALF_PI, TWO_PI + PI / 4));
        stroke(255, 0.3);
        if (!UIpiccola) 
          strokeWeight(8);
        else 
          strokeWeight(4);
        line(0, 0, dimx / 4, 0);
        pop();
      }
    }
    // Gauge Upload (MULTIPLY blend mode)
    if (ulAvg > 0) {
      // Upload in progress or already done
      blendMode(MULTIPLY);
      stroke(arcoColoreUpload);
      if (!UIpiccola) 
        strokeWeight(16);
      else 
        strokeWeight(12);
      strokeCap(ROUND);
      noFill();
      arc(cenx, ceny, dimx - 16, dimy - 16, HALF_PI, map(ulAvg, 0, grScala, HALF_PI, TWO_PI + QUARTER_PI));
      if (mostraLancette) {
        push();
        strokeWeight(2);
        translate(cenx, ceny);
        rotate(map(ulAvg, 0, grScala, HALF_PI, TWO_PI + QUARTER_PI));
        stroke(255, 0.3);
        if (!UIpiccola) 
          strokeWeight(8);
        else 
          strokeWeight(4);
        line(0, 0, dimx / 4, 0);
        pop();
      }
      blendMode(BLEND);
    }
    // Gauge numbers
    if (mostraNumeriGauge && !initGauge) 
      numeriGauge();
    
    // Measurement done!

    if (testStatus == 4) {
      if (!recordImage) {
        if (!UIpiccola) 
          dimx = dimy = 222;
        else 
          dimx = dimy = 172;
        mostraLancette = false;
        mostraNumeriGauge = false;
        push();
        textAlign(CENTER);
        noStroke();
        fill(255, 0.9);
        textFont(ttBold);
        textSize(16);
        text("RIPETI", cenx, ceny + 8);
        var posSalvaY = ceny + dimy / 2 + 75;
        if (!errore) {
          if (!mobileLayout) {
            // display save2share button only on desktop
            text("SALVA IMMAGINE", cenx, posSalvaY);
            rectMode(CENTER);
            noFill();
            stroke("#fff");
            strokeWeight(2);
            rect(cenx, posSalvaY - 5, dimx, 50);
          }
        }
        pop();
        // restart and save buttons interactivity, could be better I know ;-)
        if (mouseX > cenx - 111 && mouseX < cenx + 111) {
          // restart
          if (mouseY > ceny - 111 && mouseY < ceny + 111) {
            cursor(HAND);
            if (mouseIsPressed == true && mouseButton == LEFT) {
              cursor(ARROW);
              parti();
            }
          } else if (mouseY > posSalvaY - 5 - 25 && mouseY < posSalvaY - 5 + 25 && !errore) {
            // save image
            cursor(HAND);
            if (mouseIsPressed == true && mouseButton == LEFT) {
              cursor(ARROW);
              salva();
            }
          } else 
            cursor(ARROW);
          }
        // Then we have finished... so question...
        visualizzaReport();
      } else {
        // < so we are reconrding an image, we need the real gauge not the final little one...
        if (!mobileLayout) {
          dimx = w3 / 1.47;
          dimy = w3 / 1.47;
        } else {
          var maxGauge = hTot - 80 - rowH1 * 2 + 32 - 128; // too big?
          var tempDim = wTot / 1.47;
          if (tempDim > maxGauge + 64) {
            dimx = maxGauge;
            dimy = maxGauge;
          } else {
            dimx = tempDim;
            dimy = tempDim;
          }
        }
        mostraLancette = true;
        mostraNumeriGauge = true;
      }
    }
    // chart
    var grHU = grH / 5;
    var grScalaHU = grScala / 5;
    push();
    // lines
    strokeWeight(1);
    stroke(255, 0.4);
    translate(grPosX, grPosY);
    // main lines
    line(0, 0, grW, 0); // 100
    line(0, grH, grW, grH); // 0
    // secondary lines
    stroke(255, 0.1);
    line(0, grHU, grW, grHU); // 80
    line(0, grHU * 2, grW, grHU * 2); // 60
    line(0, grHU * 3, grW, grHU * 3); // 40
    line(0, grHU * 4, grW, grHU * 4); // 20
    // central line, dashed
    stroke(255, 0.4);
    drawingContext.setLineDash([5, 15]);
    line(0, grHU * 2.5, grW, grHU * 2.5);
    drawingContext.setLineDash([]);
    // textes
    textFont(ttReg);
    textAlign(LEFT);
    noStroke();
    fill(255);
    if (!UIpiccola) 
      textSize(18);
    else 
      textSize(14);
    text("PERFORMANCE", 0, -8);
    //
    //translate(0, rowH1)
    //blocchettoTestoW1(0, 'JITTER', jitterAvg, 50, 214, 214, 2, 'ms', 3)
    //posW1y += rowH1
    helpers[4].posx = grPosX + grW - 7;
    helpers[4].posy = grPosY - 15;

    textFont(rbMonoReg);
    textAlign(RIGHT);
    textSize(12);
    translate(-8, 0);
    text(grScala, 0, 0);
    text("0", 0, grH);
    fill(255, 0.1);
    text(grScalaHU * 4, 0, grHU);
    text(grScalaHU * 3, 0, grHU * 2);
    text(grScalaHU * 2, 0, grHU * 3);
    text(grScalaHU, 0, grHU * 4);
    pop();
    // chart dl/ul...
    blendMode(ADD);
    var grWCostretto = grW - 8;
    // chart download
    if (misureDownload.length > 0) {
      push();
      strokeWeight(4);
      stroke(graficoColoreDownload);
      translate(grPosX, grPosY);
      // 10 sec = 100 records...
      var quantiDownload = misureDownload.length;
      if (quantiDownload > 120) 
        quantiDownload = 120; // < max 12 sec of records
      beginShape();
      for (var i = 0; i < quantiDownload; i++) {
        var dx = map(misureDownload[i].t, 0, tTotaleDownload * 1000, 0, grWCostretto);
        var dxCostretto = constrain(dx, 0, grWCostretto);
        curveVertex(dxCostretto, map(misureDownload[i].misura, 0, grScala, grH, 0));
      }
      endShape();
      pop();
    }
    // chart upload
    if (misureUpload.length > 0) {
      push();
      strokeWeight(4);
      stroke(graficoColoreUpload);
      translate(grPosX, grPosY);
      // 10 sec = 100 records...
      var quantiUpload = misureUpload.length;
      if (quantiUpload > 120) 
        quantiUpload = 120; // < max 12 sec of records
      beginShape();
      for (var i = 0; i < quantiUpload; i++) {
        var ux = map(misureUpload[i].t, 0, tTotaleUpload * 1000, 0, grWCostretto);
        var dxCostretto = constrain(ux, 0, grWCostretto);
        curveVertex(dxCostretto, map(misureUpload[i].misura, 0, grScala, grH, 0));
      }
      endShape();
      pop();
    }
    blendMode(BLEND);
  }
  // helpers check & display
  if (!recordImage) {
    for (var i = 0; i < helpers.length; i++) {
      helpers[i].display();
      // helpers[i].hoverCheck() XXX ISSUE: It works just the first...
    }
    for (var i = 0; i < helpers.length; i++) {
      helpers[i].displayActive();
    }
  }
  if (recordImage) {
    // save image
    push();
    textFont(ttBold);
    textSize(16);
    textAlign(LEFT);
    rectMode(CORNER);
    noStroke();
    var textBranding = "Source: www.misurainternet.it";
    var textBrandingwWidth = textWidth(textBranding) + 48;
    if (!mobileLayout) {
      fill("#fff");
      rect(32, 8, textBrandingwWidth, 48, 4);
      fill(fondoColoreW2);
      text(textBranding, 56, 36);
    } else {
      textAlign(CENTER);
      fill("#fff");
      rect(16, 8, wTot - 32, 48, 4);
      fill(fondoColoreW2);
      text(textBranding, width / 2, 36);
    }
    pop();
    conta++;
    if (conta > 60) 
      salvaImmagine();
    }
  }

function disableKeyboard() {
  isKeyboardActive = false;
}

function toggleKeyboard() {
  isKeyboardActive = !isKeyboardActive;
}

function activateHelperByKey(which) {
  let status = helpers[which - 1].active;
  for (var i = 0; i < helpers.length; i++) {
    helpers[i].active = false;
  }
  helpers[which - 1].active = !status;
}

/* KEYTYPED */
function keyTyped(e) {
  key = e.key
  if (!recordImage && isKeyboardActive) {
    if (["1", "2", "3", "4", "5"].includes(key))
      activateHelperByKey(int(key));
    else if (testStatus == 4 && key === "r") 
      parti();
    else if (testStatus == 4 && key === "s") 
      salva();
    return false;
  }
}

function partiBtnAction() {
  if (!recordImage && testStatus == 4) 
    parti();
  }

function salvaBtnAction() {
  if (!recordImage && testStatus == 4) 
    salva();
  }

/* HELPERS CHECK */
function mousePressed() {
  for (var i = 0; i < helpers.length; i++) {
    helpers[i].clicked();
  }
}

/* LEFT COLUMN TEXTES, RESPONSIVE */
function blocchettoTestoW1(yCoord, titleString, valueVar, R, G, B, testStatusValue, unit, helperNo) {
  noStroke();
  textFont(ttReg);
  if (!UIpiccola) 
    textSize(18);
  else 
    textSize(14);
  translate(0, yCoord);
  fill("white");
  text(titleString, 0, 0);
  if (testStatus == testStatusValue) {
    fill(R, G, B, 1);
    ellipse(-12, -6, 8);
    stroke(R, G, B, 1);
  } else 
    stroke(R, G, B, 0.4);
  strokeWeight(1);
  if (!mobileLayout) {
    // < desktop
    line(0, 6, w1 - 64, 6);
  } else if (wTot < 360) {
    // < tiny mode
    line(0, 6, w1 - 40, 6);
  } else {
    // mobile or general portrait mode
    line(0, 6, w1 / 2 - 40, 6);
  }
  fill(R, G, B, 1);
  noStroke();
  if (testStatus == testStatusValue) {
    if (!UIpiccola) {
      textSize(56);
      translate(0, 16);
    } else 
      textSize(32);
    textFont(ttBold);
  } else 
    textSize(28);
  if (valueVar != 0) {
    text(valueVar, 0, 40);
    var tLarge = textWidth(valueVar);
    textSize(12);
    text(unit, tLarge + 8, 40);
  }
}

/* RESET UI */
function restartUI() {
  testStatus = -1;
  initDim();
  initGauge = true;
  mostraLancette = true;
  mostraNumeriGauge = true;
  misureDownload.length = 0;
  misureUpload.length = 0;
  ulAvg = 0;
  dlAvg = 0;
  jitterAvg = 0;
  pingAvg = 0;
  grScala = 10;
  startUpload = null;
  startDownload = null;
}

/* GAUGE NUMBERS */
function numeriGauge() {
  textAlign(CENTER);
  if (!UIpiccola) 
    textSize(18);
  else 
    textSize(12);
  textFont(rbMonoReg);
  push();
  noStroke();
  fill(255);
  translate(cenx, ceny);
  var grScalaUnit = grScala / 10;
  for (var i = 0; i <= 10; i++) {
    push();
    rotate(map(i, 0, 10, -HALF_PI, PI + QUARTER_PI));
    if (!UIpiccola) 
      translate(-dimx / 2 + 40, 0);
    else 
      translate(-dimx / 2 + 30, 0);
    rotate(-map(i, 0, 10, -HALF_PI, PI + QUARTER_PI));
    text(grScalaUnit * i, 0, 5);
    pop();
  }
  pop();
}

/* LITTLE HELPERS */
function littleSantaHelper(x, y, msg) {
  this.active = false;
  this.msg = msg;
  this.posx = x;
  this.posy = y;
  this.col = "white";
  this.colActive = "#212121";
  this.diameter = 18;
  this.sizex = 200;
  this.sizey = 100;
  this.display = function () {
    push();
    noStroke();
    // background
    if (this.active) 
      fill(this.colActive);
    else 
      fill(this.col);
    ellipse(this.posx, this.posy, this.diameter, this.diameter);
    // ?
    if (this.active) 
      fill(this.col);
    else 
      fill(this.colActive);
    textFont(ttReg);
    textSize(14);
    textAlign(CENTER);
    text("?", this.posx, this.posy + 4);
    pop();
  };
  this.displayActive = function () {
    // is active
    if (this.active) {
      if (!mobileLayout) {
        push();
        noStroke();
        textAlign(LEFT);
        // background
        fill("white");
        rect(this.posx + 22, this.posy - 10, this.sizex, this.sizey, 8);
        // message
        fill("#212121");
        textFont(ttReg);
        textSize(14);
        text(this.msg, this.posx + 37, this.posy + 2, this.sizex - 27, this.sizey - 22);
        pop();
      } else {
        push();
        noStroke();
        textAlign(LEFT);
        // background
        fill("white");
        rect(width / 2 - this.sizex / 2, this.posy - 10, this.sizex, this.sizey, 8);
        // message
        fill("#212121");
        textFont(ttReg);
        textSize(14);
        text(this.msg, width / 2 - this.sizex / 2 + 15, this.posy + 2, this.sizex - 27, this.sizey - 22);
        pop();
      }
    }
  };
  this.clicked = function () {
    var d = dist(mouseX, mouseY, this.posx, this.posy);
    if (d < this.diameter / 2) {
      this.active = !this.active;
    } else 
      this.active = false;
    }
  ;
}

function salva() {
  recordImage = true;
}

function salvaImmagine() {
  var nomefile = "Misurainternet_" + Date.now();
  saveCanvas(nomefile, "png");
  recordImage = false;
  conta = 0;
}

/* FINISHED, LET'S TELL EVERYONE... */
function visualizzaReport() {
  if (!appenaFinito && !errore) {
    //$("#modalSpeedResults").fadeIn();
    appenaFinito = true;
  }
}
