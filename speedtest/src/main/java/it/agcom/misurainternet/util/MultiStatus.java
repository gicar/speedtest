package it.agcom.misurainternet.util;

import java.util.Arrays;

/** Check if there has been a status change on multiple conditions */
public class MultiStatus {

    /** The currents stored status */
    private Object[] status = new Object[0];

    /**
     * Checks for changes in the status
     * @param newStatus The new status items; all items shall support equality operator
     */
    public boolean isChanged(Object... newStatus)
    {
        if (Arrays.deepEquals(status, newStatus)) return false;
        status = newStatus;
        return true;
    }
}
