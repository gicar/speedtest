package it.agcom.misurainternet;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import it.agcom.misurainternet.speedtest.MeasurementServer;

/** Constants */
public class MisuraConst {

    /** Test servers */
    public static final List<MeasurementServer> servers = Arrays.asList(
            new MeasurementServer("eagle3", "eagle3.misurainternet.it", 8080),
            new MeasurementServer("eagle4", "eagle4.misurainternet.it", 8080),
            new MeasurementServer("hawk5", "hawk5.misurainternet.it", 8080),
            new MeasurementServer("hawk6", "hawk6.misurainternet.it", 8080),
            new MeasurementServer("hawk4", "hawk4.misurainternet.it", 8080)
    );

    /** Default locale */
    public static final Locale LOCALE = Locale.ITALY;

    /** Default date and time format */
    public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", LOCALE);
}
