package it.agcom.misurainternet;


import android.app.Application;
import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.appcompat.XApplication;
import net.gicar.util.services.CrashReporting;

import it.agcom.misurainternet.preferences.MisuraSettings;

/** Main Application class. Needed to share settings among activities and service */
public class MisuraApplication extends XApplication {

    /** The log tag */
    static public final String TAG = "MisuraApplication";

    /** General settings */
    private MisuraSettings settings;

    @Override
    public void onCreate()
    {
        super.onCreate();
        configure(BuildConfig.DEBUG, null, null);

        if (Misc.DEBUG) Log.d(TAG, "onCreate");

        // force the crash reporting
        CrashReporting.setCollectionEnabled(true);

        // get the preferences and settings
        settings = new MisuraSettings();
        settings.onCreate(getApplicationContext());

    }

    public static MisuraSettings getSettings(Application application) {
        return ((MisuraApplication)application).settings;
    }
}
