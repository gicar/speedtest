package it.agcom.misurainternet.preferences;

import net.gicar.util.preferences.Settings;

/** This class manages settings which are not included in the Preferences screen */
public class MisuraSettings extends Settings {

    /** Name of the preferences file for these settings */
    public final static String PREFS_NAME = "misurasettings";

    public MisuraSettings() {
        super(PREFS_NAME);
    }
}
