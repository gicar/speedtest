package it.agcom.misurainternet.speedtest.server;

import android.util.Log;

import net.gicar.util.Misc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.speedtest.Measurement;
import it.agcom.misurainternet.speedtest.MeasurementProgress;
import it.agcom.misurainternet.speedtest.MeasurementServer;
import it.agcom.misurainternet.speedtest.MeasurementStatus;
import it.agcom.misurainternet.speedtest.ping.MeasurePing;
import it.agcom.misurainternet.speedtest.ping.MeasurePingHttp;
import it.agcom.misurainternet.speedtest.ping.PingMeasurement;

/** Performs test server selection */
public class ChooseServer extends Measurement<ServerMeasurement> implements Observer {

    /** The ping measurements for each server */
    private final HashMap<MeasurePing, MeasurementProgress<PingMeasurement>> pings = new HashMap<>();

    /** The result */
    protected ServerMeasurement result;

    public ChooseServer(NetworkStatus networkStatus, List<MeasurementServer> servers, int numPings, int delay, int timeout)
    {
        super(networkStatus);

        // create a ping measurement for each server
        for (MeasurementServer server : servers) {
            MeasurePingHttp ping = new MeasurePingHttp(networkStatus, server, numPings, delay, timeout);
            pings.put(ping, null);
            ping.addObserver(this);
        }
    }

    @Override
    public void start()
    {
        // start the pings
        for (MeasurePing ping : pings.keySet()) ping.start();
    }

    @Override
    public void stop()
    {
        // stop the pings
        for (MeasurePing ping : pings.keySet()) ping.stop();
    }

    @Override
    public ServerMeasurement getValue() {
        return result;
    }

    @Override
    public synchronized void update(Observable o, Object arg)
    {
        // update the corresponding ping status
        MeasurePing ping = (MeasurePing) o;
        MeasurementProgress<PingMeasurement> pingMeasurement = ping.getMeasurementProgress();
        pings.put(ping, pingMeasurement);

        // inspect the statuses
        boolean started = false, finished = true, aborted = false;
        for (MeasurementProgress<PingMeasurement> m : pings.values()) {
            started |= m != null && m.status != MeasurementStatus.READY;
            finished &= m != null && m.status.finished;
            aborted |= m != null && m.status == MeasurementStatus.ABORTED;
        }

        // check if finished
        MeasurementStatus status;
        MeasurementServer server = null;
        if (!finished) status = started ? MeasurementStatus.STARTED : MeasurementStatus.READY;
        else {

            if (aborted) status = MeasurementStatus.ABORTED;
            else {
                // for the time being just select the one with the lowest ping value
                PingMeasurement pingResult = null;
                for (Map.Entry<MeasurePing, MeasurementProgress<PingMeasurement>> e : pings.entrySet()) {
                    MeasurePing k = e.getKey();
                    MeasurementProgress<PingMeasurement> v = e.getValue();
                    if (Misc.DEBUG) Log.i(TAG, k.getServer() + " -> " + v);
                    if (!v.status.failed) {
                        if (server == null || pingResult.timeAverage > v.value.timeAverage) {
                            server = k.getServer();
                            pingResult = v.value;
                        }
                    }
                }
                if (Misc.DEBUG) Log.i(TAG, "Selected server: " + server);

                // we should have at least a working server to continue
                status = server != null ? MeasurementStatus.FINISHED : MeasurementStatus.FAILED_STARTED;
            }
        }

        // update our status
        synchronized (this) {
            this.result = server != null ? new ServerMeasurement(server) : null;
            if (status != getStatus()) setStatus(status);
        }

        // notify observers
        notifyChange();
    }
}
