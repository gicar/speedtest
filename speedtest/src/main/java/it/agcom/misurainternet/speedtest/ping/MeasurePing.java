package it.agcom.misurainternet.speedtest.ping;

import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.speedtest.Measurement;
import it.agcom.misurainternet.speedtest.MeasurementServer;

import static java.lang.Float.NaN;

abstract public class MeasurePing extends Measurement<PingMeasurement> {

    /** The server that will be used for testing */
    protected final MeasurementServer server;

    /** The number of pings, excluding the first one */
    protected final int numPings;

    /** The delay among pings */
    protected final int delay;

    /** The timeout for a ping answer */
    protected final int timeout;

    /** The execution time in ms for each ping request */
    protected final float[] pingTime;

    /** The number of performed pings */
    protected int count;

    public MeasurePing(NetworkStatus networkStatus, MeasurementServer server, int numPings, int delay, int timeout)
    {
        super(networkStatus);
        this.server = server;
        this.numPings = numPings;
        this.delay = delay;
        this.timeout = timeout;

        // initialize the ping time array, with an extra element as we perform an extra ping at start
        pingTime = new float[numPings + 1];
    }

    public MeasurementServer getServer() {
        return server;
    }

    @Override
    protected synchronized PingMeasurement getValue()
    {
        float timeAverage = NaN;
        float timeMin = NaN;
        float timeMax = NaN;
        float jitter = NaN;

        if (count > 1) {
            double x = 0;
            for (int i = 1; i < count; i++) {
                x += pingTime[i];
                if (Float.isNaN(timeMin) ||  timeMin > pingTime[i]) timeMin = pingTime[i];
                if (Float.isNaN(timeMax) ||  timeMax < pingTime[i]) timeMax = pingTime[i];
            }
            timeAverage = (float) (x / (count - 1));
            x = 0;
            for (int i = 1; i < count; i++) x += Math.pow(timeAverage - pingTime[i], 2);
            jitter = (float) Math.sqrt(x / (count - 1));
        }

        return new PingMeasurement(timeAverage, timeMin, timeMax, jitter, count, numPings + 1, pingTime);
    }
}
