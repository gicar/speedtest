package it.agcom.misurainternet.speedtest;

import androidx.annotation.NonNull;

/** The current status of the measurement */
public class MeasurementProgress<T> {

    /** The current status */
    public final MeasurementStatus status;

    /** The percentage of time we have been in this status compared to the max */
    public final float statusPercent;

    /** The test start time */
    private final Long startTime;

    /** The test end time */
    private final Long endTime;

    /** The current measured value */
    public final T value;

    public MeasurementProgress(MeasurementStatus status, float statusPercent, Long startTime, Long endTime, T value) {
        this.status = status;
        this.statusPercent = statusPercent;
        this.startTime = startTime;
        this.endTime = endTime;
        this.value = value;
    }

    @NonNull
    @Override
    public String toString() {
        return "MeasurementProgress{" +
                "status=" + status +
                ", statusPercent=" + statusPercent +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", value=" + value +
                '}';
    }
}
