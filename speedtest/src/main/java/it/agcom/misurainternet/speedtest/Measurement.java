package it.agcom.misurainternet.speedtest;

import android.os.SystemClock;
import android.util.Log;

import net.gicar.util.Misc;

import java.util.Observable;

import it.agcom.misurainternet.environment.NetworkStatus;

/** Base class for all measurement processes */
abstract public class Measurement<T> extends Observable {

    /** The tag */
    protected final String TAG = getClass().getSimpleName();

    /** Poll time in ms */
    public final static long POLL_TIME = 100;

    /** The network status at start of test */
    protected final NetworkStatus networkStatus;

    /** The current status */
    private MeasurementStatus status = MeasurementStatus.READY;

    /** The test start time */
    private Long startTime;

    /** The test end time */
    private Long endTime;

    /** The time when this status was reached */
    private long statusTime;

    /** The percentage of time we have been in this status */
    private float statusPercent;

    protected Measurement(NetworkStatus networkStatus) {
        this.networkStatus = networkStatus;
    }

    protected MeasurementStatus getStatus() {
        return status;
    }

    protected long getStatusTime() {
        return statusTime;
    }

    protected float getStatusPercent() {
        return statusPercent;
    }

    /**
     * Sets the status
     * @param status The current status
     */
    protected synchronized void setStatus(MeasurementStatus status)
    {
        // log start and end time
        long currTime = SystemClock.elapsedRealtime();
        if (startTime == null && status != MeasurementStatus.READY) startTime = currTime;
        if (endTime == null && status.finished) endTime = currTime;

        // update the status
        this.status = status;
        statusTime = currTime;
        statusPercent = 0;
        if (Misc.DEBUG) Log.i(TAG, "New measurement status: " + status);
    }

    /**
     * Updates the status progress
     * @param elapsed The units elapsed
     * @param total The total units that needs to be present
     */
    protected void updateStatusPercent(int elapsed, int total)
    {
        // correct the percentage to be sure to reach 100% in the visualization
        statusPercent = (float) (elapsed + 1) / total;
        if (statusPercent > 1) statusPercent = 1;
    }

    /**
     * Updates the status progress using a time base
     * @param elapsed The time elapsed
     * @param max The total time that needs to pass
     */
    protected void updateStatusPercentTime(long elapsed, long max)
    {
        // correct the percentage to be sure to reach 100% in the visualization
        statusPercent = (float) (elapsed + POLL_TIME) / max;
        if (statusPercent > 1) statusPercent = 1;
    }

    /**
     * Getter for the current progress of the test
     * @return The measurement progress
     */
    synchronized public MeasurementProgress<T> getMeasurementProgress() {
        return new MeasurementProgress<>(status, statusPercent, startTime, endTime, getValue());
    }

    /** Sends a notification of change in state */
    protected void notifyChange()
    {
        setChanged();
        notifyObservers();
    }

    /** Start the measurement process */
    abstract public void start();

    /** Abort the ongoing measurement */
    abstract public void stop();

    /** Getter for the current value of the measurement results */
    abstract protected T getValue();
}
