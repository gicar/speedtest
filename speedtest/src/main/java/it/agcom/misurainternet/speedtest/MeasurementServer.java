package it.agcom.misurainternet.speedtest;

import androidx.annotation.NonNull;

/** MeasurementServer for tests */
public class MeasurementServer {

    public final String name;

    public final String address;

    public final int port;

    public MeasurementServer(String name, String address, int port) {
        this.name = name;
        this.address = address;
        this.port = port;
    }

    @NonNull
    @Override
    public String toString() {
        return "MeasurementServer{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", port=" + port +
                '}';
    }
}
