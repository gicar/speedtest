package it.agcom.misurainternet.speedtest;

/** Status of a measurement */
public enum MeasurementStatus {

    READY(false, false),
    CONNECTING(false, false),
    STARTING(false, false),
    STARTED(false, false),
    FINISHED(true, false),
    ABORTED(true, true),
    FAILED_CONNECTING(true, true),
    FAILED_STARTING(true, true),
    FAILED_STARTED(true, true);

    /** True if the test is finished */
    public final boolean finished;

    /** True if the test is failed */
    public final boolean failed;

    MeasurementStatus(boolean finished, boolean failed) {
        this.finished = finished;
        this.failed = failed;
    }
}
