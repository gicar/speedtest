package it.agcom.misurainternet.speedtest.ping;

import java.util.Arrays;

import androidx.annotation.NonNull;

public class PingMeasurement {

    /** Time in ms of the average ping */
    public final float timeAverage;

    /** Min time in ms of the ping */
    public final float timeMin;

    /** Max time in ms of the ping */
    public final float timeMax;

    /** Jitter */
    public final float jitter;

    /** Number of collected samples */
    public final int numPings;

    /** Max number of samples that will be collected */
    public final int maxPings;

    /** Time of each sample, first one shall be discarded */
    public final float[] pingTime;

    public PingMeasurement(float timeAverage, float timeMin, float timeMax, float jitter, int numPings, int maxPings, float[] pingTime) {
        this.timeAverage = timeAverage;
        this.timeMin = timeMin;
        this.timeMax = timeMax;
        this.jitter = jitter;
        this.numPings = numPings;
        this.maxPings = maxPings;
        this.pingTime = pingTime;
    }

    @NonNull
    @Override
    public String toString() {
        return "PingMeasurement{" +
                "timeAverage=" + timeAverage +
                ", timeMin=" + timeMin +
                ", timeMax=" + timeMax +
                ", jitter=" + jitter +
                ", numPings=" + numPings +
                ", maxPings=" + maxPings +
                ", pingTime=" + Arrays.toString(pingTime) +
                '}';
    }
}
