package it.agcom.misurainternet.speedtest.transfer;

import android.os.SystemClock;
import android.util.Log;

import net.gicar.util.Misc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.speedtest.Measurement;
import it.agcom.misurainternet.speedtest.MeasurementServer;
import it.agcom.misurainternet.speedtest.MeasurementStatus;

import static java.lang.Float.NaN;

/** Base classes for a transfer test */
abstract public class TransferControl extends Measurement<SpeedMeasurement> {

    /** The server */
    protected final MeasurementServer server;

    /** Number of parallel connections to use */
    protected final int numConnections;

    /** Size of the data chunks to get in MB */
    protected final int chunkSize;

    /** The test connect timeout in ms */
    protected final long connectTime;

    /** The test start grace period in ms */
    protected final long startTime;

    /** The test duration in ms */
    protected final long testTime;

    /** The amount of samples in the start phase that should be discarded in ms */
    protected final long prechargeTime;

    /** The number of samples to collect in starting state */
    protected final int startingSamples;

    /** The number of samples to collect in started state */
    protected final int startedSamples;

    /** The number of samples to discard in starting state */
    protected final int prechargeSamples;

    /** Max number of samples that will be collected */
    protected final int maxSamples;

    /** The workers */
    private final List<TransferWorker> workers = new ArrayList<>();

    /** The control thread */
    private Thread controlThread;

    /** Latch used to start all the transfers together */
    protected CountDownLatch connectLatch = new CountDownLatch(1);

    /** Number of collected samples */
    private int numSamples;

    /** Time of each sample */
    private final long[] time;

    /** Transferred bytes for each sample */
    private final long[] data;

    /** The calculated speed for each sample */
    private final float[] speeds;

    /** The speed estimations at each sample */
    private final float[] speedEst;

    TransferControl(NetworkStatus networkStatus, MeasurementServer server, int numConnections, int chunkSize,
                    long connectTime, long startTime, long testTime,
                    long prechargeTime)
    {
        super(networkStatus);

        // save the parameters
        this.server = server;
        this.numConnections = numConnections;
        this.chunkSize = chunkSize;
        this.connectTime = connectTime;
        this.startTime = startTime;
        this.testTime = testTime;
        this.prechargeTime = prechargeTime;

        // initialize the result
        startingSamples = (int) (startTime / POLL_TIME) + 1;
        startedSamples = (int) (testTime / POLL_TIME);
        prechargeSamples = (int) (prechargeTime / POLL_TIME);
        maxSamples = startingSamples + startedSamples;
        time = new long[maxSamples];
        data = new long[maxSamples];
        speeds = new float[maxSamples];
        speeds[0] = NaN;
        speedEst = new float[maxSamples];
        speedEst[0] = NaN;
    }

    @Override
    public synchronized void start()
    {
        if (controlThread != null) throw new IllegalStateException("Already stared");

        // create and start the control thread
        controlThread = new Thread(this::monitorTransfer);
        controlThread.setName("ControlThread");
        controlThread.start();
    }

    @Override
    public synchronized void stop()
    {
        if (controlThread == null) setStatus(MeasurementStatus.ABORTED);
        else controlThread.interrupt();
    }

    @Override
    protected synchronized SpeedMeasurement getValue() {
        return new SpeedMeasurement(numSamples > startingSamples ? speedEst[numSamples - 1] : NaN,
                numSamples, startingSamples, maxSamples, time, data, speeds, speedEst);
    }

    private void monitorTransfer()
    {
        try {
            if (Misc.DEBUG) Log.i(TAG, "ControlThread started");

            setStatus(MeasurementStatus.CONNECTING);
            notifyChange();

            // create and start all the receivers
            for (int i = 0; i < numConnections; i++) {
                TransferWorker worker = createWorker(networkStatus, i);
                workers.add(worker);
                worker.start();
            }

            while (!getStatus().finished) {

                try {
                    //noinspection BusyWait
                    Thread.sleep(POLL_TIME);
                } catch (InterruptedException e) {
                    setStatus(MeasurementStatus.ABORTED);
                    notifyChange();
                    continue;
                }

                // get the elapsed time in this status
                long time = SystemClock.elapsedRealtime();
                long elapsed = time - getStatusTime();

                // get info from the workers
                boolean connected = true, shutdown = false;
                long bytes = 0;
                for (TransferWorker w : workers) {
                    int connections = w.getConnections();
                    if (connections == 0) connected = false;
                    shutdown |= w.isShutdown();
                    bytes += w.getBytes();
                }

                synchronized (this) {
                    switch (getStatus()) {

                        case CONNECTING:
                            if (connected) {
                                setStatus(MeasurementStatus.STARTING);
                                this.time[numSamples] = time;
                                this.data[numSamples++] = bytes;
                                connectLatch.countDown();
                            } else if (elapsed > connectTime || shutdown)
                                setStatus(MeasurementStatus.FAILED_CONNECTING);
                            else updateStatusPercentTime(elapsed, connectTime);
                            break;

                        case STARTING:
                            if (shutdown) setStatus(MeasurementStatus.FAILED_STARTING);
                            else {
                                addSample(bytes, time);
                                if (numSamples < startingSamples) updateStatusPercent(numSamples, startingSamples);
                                else setStatus(MeasurementStatus.STARTED);
                            }
                            break;

                        case STARTED:
                            if (shutdown) setStatus(MeasurementStatus.FAILED_STARTED);
                            else {
                                addSample(bytes, time);
                                if (numSamples < this.time.length) updateStatusPercent(numSamples, this.time.length);
                                else setStatus(MeasurementStatus.FINISHED);
                            }
                            break;
                    }
                }

                notifyChange();
            }

            for (TransferWorker w : workers) w.stop();

            if (Misc.DEBUG) Log.i(TAG, "ControlThread stopped");
        }
        catch (Exception e) {
            if (Misc.DEBUG) Log.e(TAG, "Unexpected exception", e);
        }
    }

    private void addSample(long data, long time)
    {
        if (numSamples >= this.time.length) return;
        this.time[numSamples] = time;
        this.data[numSamples] = data;
        if (numSamples > 0) {
            speeds[numSamples] = calculateAverageSpeed(numSamples - 1, numSamples);
            speedEst[numSamples] = calculateAverageSpeed(Math.max(prechargeSamples, numSamples - startedSamples), numSamples);
            if (Misc.DEBUG) Log.v(TAG, "Speed [Mbps]: " + speeds[numSamples]);
        }
        numSamples++;
    }

    /**
     * Calculates the average speed value between two samples
     * @param startSample The starting sample
     * @param endSample The ending sample
     * @return The average speed value in the interval
     */
    private float calculateAverageSpeed(int startSample, int endSample)
    {
        if (endSample - startSample < 1) return NaN;
        else {
            long d = data[endSample] - data[startSample];
            long t = time[endSample] - time[startSample];
            return (d * 8f / (1000*1000)) / t * 1000;
        }
    }

    /**
     * Requests a worker for executing the transfers
     * @param networkStatus The status of the network
     * @param index The index of this worker
     * @return The worker that will bused to transfer the data
     */
    abstract TransferWorker createWorker(NetworkStatus networkStatus, int index);
}
