package it.agcom.misurainternet.speedtest.transfer;

import net.gicar.util.http.HttpAnswerHeader;
import net.gicar.util.http.HttpParser;

import java.io.IOException;
import java.io.InputStream;

import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.speedtest.MeasurementServer;

/** Performs download speed testing */
public class MeasureDownload extends TransferControl {

    public MeasureDownload(NetworkStatus networkStatus,
                           MeasurementServer server, int numConnections, int chunkSize,
                           long connectTime, long startTime, long testTime, long prechargeTime) {
        super (networkStatus, server, numConnections, chunkSize, connectTime, startTime, testTime, prechargeTime);
    }

    @Override
    DownloadWorker createWorker(NetworkStatus networkStatus, int index) {
        return new DownloadWorker(networkStatus, index, this);
    }

    /** The data receiver thread */
    class DownloadWorker extends TransferWorker implements HttpParser {

        public DownloadWorker(NetworkStatus networkStatus, int index, MeasureDownload measureDownload) {
            super(networkStatus, index, measureDownload);
        }

        @Override
        public boolean prefersGzip() { return false; }

        @Override
        public Object parse(HttpAnswerHeader header, InputStream stream) throws IOException
        {
            if (stream == null) return null;

            // allocate the buffer that will hold the received data and mark us as connected
            byte[] data = new byte[8192];
            setConnected();

            while (true) {

                // check for available data
                int avail = stream.available();
                if (avail == 0) avail = 1;

                // read the data
                int n = stream.read(data, 0, Math.min(avail, data.length));
                if (n == -1) break;

                // increment the read bytes counter
                addBytes(n);
            }

            return null;
        }

        @Override
        public int streamBufferSize() {
            return 0;
        }

        @Override
        void transfer() throws IOException {
            fetcher.getUri("https://" + server.address + ":" + server.port + "/garbage?r=" + Math.random() +
                    "&ckSize=" + chunkSize, this, null);
        }
    }
}
