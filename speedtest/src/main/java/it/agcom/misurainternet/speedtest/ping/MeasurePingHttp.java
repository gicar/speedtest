package it.agcom.misurainternet.speedtest.ping;

import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.http.HttpAnswer;
import net.gicar.util.http.HttpAnswerHeader;
import net.gicar.util.http.HttpFetcherURLConnection;
import net.gicar.util.http.HttpParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.speedtest.MeasurementServer;
import it.agcom.misurainternet.speedtest.MeasurementStatus;

/** Implements "ping" testing using HTTP */
public class MeasurePingHttp extends MeasurePing implements HttpParser {

    /** The executor which will be used for scheduling requests */
    private final static ExecutorService executor = Executors.newCachedThreadPool();

    /** The monitor thread */
    @SuppressWarnings("rawtypes")
    Future monitorFuture;

    public MeasurePingHttp(NetworkStatus networkStatus,
                           MeasurementServer server, int numPings, int delay, int timeout) {
        super(networkStatus, server, numPings, delay, timeout);
    }

    @Override
    public synchronized void start()
    {
        // create and start the ping monitor thread
        monitorFuture = executor.submit(this::monitorPings);
    }

    @Override
    public synchronized void stop() {
        if (monitorFuture == null) setStatus(MeasurementStatus.ABORTED);
        else monitorFuture.cancel(true);
    }

    private void monitorPings()
    {
        try {
            if (Misc.DEBUG) Log.i(TAG, "PingMonitor thread started");

            // Create the fetcher
            final HttpFetcherURLConnection fetcher = new HttpFetcherURLConnection(false, null);

            // create the ping executor object instance
            Callable<Float> executePing = () -> {

                // execute a GET request monitoring the execution time
                long start = System.nanoTime();
                HttpAnswer answer = fetcher.getUri("https://" + server.address + ":" + server.port +
                                "/empty?r=" + Math.random(),
                        this, null);
                if (answer == null) return null;
                float time = ((long) answer.getData() - start) / 1_000_000f;
                if (Misc.DEBUG) Log.v(TAG, "Ping executed, t=" + time);

                return time;
            };

            // loop until shutdown or all pings have been executed (including first "connect" one)
            while (!getStatus().finished) {

                Float time = null;
                MeasurementStatus status;

                if (getStatus() == MeasurementStatus.READY) status = MeasurementStatus.CONNECTING;
                else {
                    Future<Float> future = null;
                    try {
                        // wait fot the requested delay if not on the first ping
                        if (count != 0) //noinspection BusyWait
                            Thread.sleep(delay);

                        // submit the request to the executor
                        future = executor.submit(executePing);

                        // wait for the ping execution
                        time = future.get(timeout, TimeUnit.MILLISECONDS);

                        // if no result, the request has been aborted
                        if (time == null) status = MeasurementStatus.ABORTED;
                        else
                            status = count == numPings ? MeasurementStatus.FINISHED : MeasurementStatus.STARTED;

                    } catch (CancellationException e) {
                        // the future has been cancelled, even though this should not happen...
                        status = MeasurementStatus.ABORTED;
                    } catch (ExecutionException e) {
                        // we got an I/O exception
                        status = count == 0 ? MeasurementStatus.FAILED_CONNECTING : MeasurementStatus.FAILED_STARTED;
                    } catch (InterruptedException e) {
                        // we have been aborted
                        if (future != null) future.cancel(true);
                        status = MeasurementStatus.ABORTED;
                    } catch (TimeoutException e) {
                        // the ping took too much time
                        future.cancel(true);
                        status = count == 0 ? MeasurementStatus.FAILED_CONNECTING : MeasurementStatus.FAILED_STARTED;
                    }
                }

                // update our status
                synchronized (this) {
                    if (time != null) pingTime[count++] = time;
                    if (status != getStatus()) setStatus(status);
                }

                // notify observers
                notifyChange();
            }

            // shutdown the fetcher
            fetcher.shutdown();

            if (Misc.DEBUG) Log.i(TAG, "PingMonitor thread stopped");
        }
        catch (Exception e) {
            if (Misc.DEBUG) Log.e(TAG, "Unexpected exception", e);
        }
    }

    @Override
    public boolean prefersGzip() { return false; }

    @Override
    public Object parse(HttpAnswerHeader header, InputStream stream) throws IOException
    {
        if (stream == null) return null;

        // allocate the buffer that will hold the received data
        byte[] data = new byte[8192];

        while (true) {

            // check for available data
            int avail = stream.available();
            if (avail == 0) avail = 1;

            // read the data
            int n = stream.read(data, 0, Math.min(avail, data.length));
            if (n == -1) break;
        }

        return System.nanoTime();
    }

    @Override
    public int streamBufferSize() {
        return 0;
    }
}
