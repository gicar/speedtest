package it.agcom.misurainternet.speedtest.transfer;

import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.http.HttpFetcherURLConnection;
import net.gicar.util.http.SocketCreationObserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import it.agcom.misurainternet.environment.NetworkStatus;

abstract class TransferWorker implements SocketCreationObserver {

    /** The tag */
    private final static String TAG = "TransferWorker";

    /** The fetcher */
    protected final HttpFetcherURLConnection fetcher =
            new HttpFetcherURLConnection(false, this);

    /** The network status */
    private final NetworkStatus networkStatus;

    /** The index */
    private final int index;

    /** Our control */
    private final TransferControl control;

    /** The worker thread */
    private Thread workerThread;

    /** The number of connections performed */
    private final AtomicInteger connections = new AtomicInteger();

    /** The number of processed bytes */
    private final AtomicLong bytes = new AtomicLong();

    /** If true we have been shutdown */
    private volatile boolean shutdown;

    TransferWorker(NetworkStatus networkStatus, int index, TransferControl control) {
        this.networkStatus = networkStatus;
        this.index = index;
        this.control = control;
    }

    /** Starts the measurement process */
    void start()
    {
        // create and start the worker thread
        workerThread = new Thread(this::execute);
        workerThread.setName("WorkerThread-" + index);
        workerThread.start();
    }

    /** Stops the measurement process */
    void stop()
    {
        shutdown = true;
        abort();
    }

    /** Measurement thread runner */
    private void execute()
    {
        if (Misc.DEBUG) Log.i(TAG, "WorkerThread started");

        try {

            // loop until there is a shutdown request
            while (!shutdown) {

                try {
                    transfer();
                } catch (IOException e) {
                    if (Misc.DEBUG) Log.e(TAG, "Got error downloading data", e);
                    stop();
                }
            }
        }
        finally {
            shutdown();
        }

        if (Misc.DEBUG) Log.i(TAG, "WorkerThread stopped");
    }

    protected void setConnected() throws IOException {
        connections.incrementAndGet();
        try {
            control.connectLatch.await();
        } catch (InterruptedException e) {
            throw new IOException(e);
        }
    }

    protected void addBytes(long bytes) {
        this.bytes.addAndGet(bytes);
    }

    int getConnections() {
        return connections.get();
    }

    long getBytes() {
        return bytes.get();
    }

    boolean isShutdown() {
        return shutdown;
    }

    /** Perform the transfer of a chunk of data */
    abstract void transfer() throws IOException;

    /** Aborts the transfer */
    void abort() {
        fetcher.abort();
    }

    /** Shutdowns the transfer */
    void shutdown() {
        fetcher.shutdown();
    }

    @Override
    public void onSocketCreated(Socket socket, String host, InetAddress address, int port, InetAddress localAddress, Integer localPort)
    {
        Log.d(TAG, "onSocketCreated() called with: socket = [" + socket + "], host = [" + host + "], address = [" + address + "], port = [" + port + "], localAddress = [" + localAddress + "], localPort = [" + localPort + "]");
        try {
            if (Misc.DEBUG) Log.d(TAG, "socket receive buffer size = " + socket.getReceiveBufferSize());
            if (Misc.DEBUG) Log.d(TAG, "socket send buffer size = " + socket.getSendBufferSize());
            if (networkStatus.connectionType == NetworkStatus.ConnectionType.CELLULAR) {
                // FIXME Is this needed?
                socket.setSendBufferSize(8192);
                if (Misc.DEBUG) Log.d(TAG, "adjusted socket send buffer size = " + socket.getSendBufferSize());
            }
        } catch (SocketException ignored) {}
    }
}
