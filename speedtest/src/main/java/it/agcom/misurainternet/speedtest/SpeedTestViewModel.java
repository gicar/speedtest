package it.agcom.misurainternet.speedtest;

import android.app.Application;
import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.MutableNonNullLiveData;

import java.util.Observable;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import it.agcom.misurainternet.MisuraConst;
import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.speedtest.ping.MeasurePing;
import it.agcom.misurainternet.speedtest.ping.MeasurePingHttp;
import it.agcom.misurainternet.speedtest.ping.PingMeasurement;
import it.agcom.misurainternet.speedtest.server.ChooseServer;
import it.agcom.misurainternet.speedtest.server.ServerMeasurement;
import it.agcom.misurainternet.speedtest.transfer.MeasureDownload;
import it.agcom.misurainternet.speedtest.transfer.MeasureUpload;
import it.agcom.misurainternet.speedtest.transfer.SpeedMeasurement;

public class SpeedTestViewModel extends AndroidViewModel {

    public enum Status {
        IDLE(            false, false, false),
        PREPARING(       true,  false, false),
        PING(            true,  false, false),
        DOWNLOAD(        true,  false, false),
        UPLOAD(          true,  false, false),
        FINISHED(        false, true,  false),
        ABORTED(         false, true,  true),
        FAILED_PREPARING(false, true,  true),
        FAILED_PING(     false, true,  true),
        FAILED_DOWNLOAD( false, true,  true),
        FAILED_UPLOAD(   false, true,  true);

        /** True if the tests are ongoing */
        public final boolean ongoing;

        /** True if the tests are finished */
        public final boolean finished;

        /** True if the tests are failed */
        public final boolean failed;

        Status(boolean ongoing, boolean finished, boolean failed) {
            this.ongoing = ongoing;
            this.finished = finished;
            this.failed = failed;
        }
    }

    /** The tag */
    protected final String TAG = "SpeedTestViewModel";

    /** The network status at test start */
    private NetworkStatus networkStatus;

    /** The server selection measurement */
    private ChooseServer chooseServer;

    /** The ping speed measurement */
    private MeasurePing ping;

    /** The download speed measurement */
    private MeasureDownload download;

    /** The upload speed measurement */
    private MeasureUpload upload;

    /** The current overall test status */
    public MutableNonNullLiveData<Status> status = new MutableNonNullLiveData<>(Status.IDLE);

    /** The server selection progress */
    public MutableLiveData<MeasurementProgress<ServerMeasurement>> statusServer = new MutableLiveData<>();

    /** The ping measurements progress */
    public MutableLiveData<MeasurementProgress<PingMeasurement>> statusPing = new MutableLiveData<>();

    /** The speed measurements progress */
    public MutableLiveData<MeasurementProgress<SpeedMeasurement>>
            statusDownload = new MutableLiveData<>(),
            statusUpload = new MutableLiveData<>();

    /** Test overall time */
    public Long startTime, endTime;

    public SpeedTestViewModel(@NonNull Application application) {
        super(application);
    }

    /** Starts the tests execution */
    public void startTests(NetworkStatus networkStatus)
    {
        // save the network status
        this.networkStatus = networkStatus;

        // start the server selection
        chooseServer = new ChooseServer(networkStatus, MisuraConst.servers,50, 5, 1000);
        statusServer.postValue(chooseServer.getMeasurementProgress());
        chooseServer.addObserver(this::chooseServerObserver);

        statusPing.postValue(null);
        statusDownload.postValue(null);
        statusUpload.postValue(null);

        startTime = System.currentTimeMillis();
        endTime = null;

        setStatus(Status.PREPARING);
        chooseServer.start();
    }

    /** Aborts the tests execution */
    public synchronized void stopTests()
    {
        if (!status.getValue().finished) {
            setStatus(Status.ABORTED);
            chooseServer.stop();
            if (ping != null) ping.stop();
            if (download != null) download.stop();
            if (upload != null) upload.stop();
        }
    }

    private void setStatus(Status status)
    {
        // log end time
        if (status.finished) endTime = System.currentTimeMillis();

        this.status.setValue(status);
        if (Misc.DEBUG) Log.i(TAG, "New status: " + status);
    }

    private void postStatus(Status status)
    {
        // log end time
        if (status.finished) endTime = System.currentTimeMillis();

        this.status.postValue(status);
        if (Misc.DEBUG) Log.i(TAG, "New status: " + status);
    }

    public synchronized void chooseServerObserver(Observable o, Object arg)
    {
        MeasurementProgress<ServerMeasurement> s = chooseServer.getMeasurementProgress();
        statusServer.postValue(s);
        if (status.getValue().finished) return;
        if (status.getValue() == Status.PREPARING && s.status.finished) {
            if (s.status.failed) postStatus(Status.FAILED_PREPARING);
            else {
                ping = new MeasurePingHttp(networkStatus, s.value.measurementServer,
                        15, 10, 2000);
                statusPing.postValue(ping.getMeasurementProgress());
                ping.addObserver(this::pingObserver);

                download = new MeasureDownload(networkStatus, s.value.measurementServer,
                        4,100, 5000, 2000, 10000, 1000);
                statusDownload.postValue(download.getMeasurementProgress());
                download.addObserver(this::downloadObserver);

                upload = new MeasureUpload(networkStatus, s.value.measurementServer,
                        4,100, 5000, 2000, 10000, 1000);
                statusUpload.postValue(upload.getMeasurementProgress());
                upload.addObserver(this::uploadObserver);

                postStatus(Status.PING);
                ping.start();
            }
        }
    }

    public synchronized void pingObserver(Observable o, Object arg)
    {
        MeasurementProgress<PingMeasurement> s = ping.getMeasurementProgress();
        statusPing.postValue(s);
        if (status.getValue().finished) return;
        if (status.getValue() == Status.PING && s.status.finished) {
            if (s.status.failed) postStatus(Status.FAILED_PING);
            else {
                postStatus(Status.DOWNLOAD);
                download.start();
            }
        }
    }

    public synchronized void downloadObserver(Observable o, Object arg)
    {
        MeasurementProgress<SpeedMeasurement> s = download.getMeasurementProgress();
        statusDownload.postValue(s);
        if (status.getValue().finished) return;
        if (status.getValue() == Status.DOWNLOAD && s.status.finished) {
            if (s.status.failed) postStatus(Status.FAILED_DOWNLOAD);
            else {
                postStatus(Status.UPLOAD);
                upload.start();
            }
        }
    }

    public synchronized void uploadObserver(Observable o, Object arg)
    {
        MeasurementProgress<SpeedMeasurement> s = upload.getMeasurementProgress();
        statusUpload.postValue(s);
        if (status.getValue().finished) return;
        if (status.getValue() == Status.UPLOAD && s.status.finished) {
            if (s.status.failed) postStatus(Status.FAILED_UPLOAD);
            else {
                postStatus(Status.FINISHED);
            }
        }
    }
}
