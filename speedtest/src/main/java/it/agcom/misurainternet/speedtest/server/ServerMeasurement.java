package it.agcom.misurainternet.speedtest.server;

import androidx.annotation.NonNull;
import it.agcom.misurainternet.speedtest.MeasurementServer;

public class ServerMeasurement {

    /** The selected server */
    public final MeasurementServer measurementServer;

    public ServerMeasurement(MeasurementServer measurementServer) {
        this.measurementServer = measurementServer;
    }

    @NonNull
    @Override
    public String toString() {
        return "ServerMeasurement{" +
                "measurementServer=" + measurementServer +
                '}';
    }
}
