package it.agcom.misurainternet.speedtest.transfer;

import net.gicar.util.http.HttpProducer;

import java.io.IOException;
import java.io.OutputStream;

import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.speedtest.MeasurementServer;

/** Performs upload speed testing */
public class MeasureUpload extends TransferControl {

    public MeasureUpload(NetworkStatus networkStatus,
                         MeasurementServer server, int numConnections, int chunkSize,
                         long connectTime, long startTime, long testTime, long prechargeTime) {
        super (networkStatus, server, numConnections, chunkSize, connectTime, startTime, testTime, prechargeTime);
    }

    @Override
    UploadWorker createWorker(NetworkStatus networkStatus, int index) {
        return new UploadWorker(networkStatus, index, this);
    }

    /** The data receiver thread */
    class UploadWorker extends TransferWorker implements HttpProducer {

        public UploadWorker(NetworkStatus networkStatus, int index, MeasureUpload measureUpload) {
            super(networkStatus, index, measureUpload);
        }

        @Override
        public boolean prefersGzip() { return false; }

        @Override
        public String getContentType() {
            return "application/octet-stream";
        }

        @Override
        public long getDataLength() {
            return -1;
        }

        @Override
        public void produce(OutputStream stream) throws IOException
        {
            if (stream == null) return;

            // allocate the buffer that will hold the data to be sent and mark us as connected
            byte[] data = new byte[8192];
            setConnected();

            for (long i = chunkSize * 1024L * 1024L / data.length; i >= 0 ; i--) {
                stream.write(data);
                addBytes(data.length);
            }
        }

        @Override
        public int streamBufferSize() {
            return 0;
        }

        @Override
        void transfer() throws IOException {
            fetcher.requestUri("https://" + server.address + ":" + server.port + "/empty?r=" + Math.random(),
                    this, null, null);
        }
    }
}
