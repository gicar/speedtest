package it.agcom.misurainternet.speedtest.transfer;

import java.util.Arrays;

import androidx.annotation.NonNull;

public class SpeedMeasurement {

    /** Speed value in Mbps */
    public final float speed;

    /** Number of collected samples */
    public final int numSamples;

    /** The number of samples belonging to the starting state */
    public final int startingSamples;

    /** Max number of samples that will be collected */
    public final int maxSamples;

    /** Time of each sample */
    public final long[] time;

    /** Transferred bytes for each sample */
    public final long[] data;

    /** Average speed for each sample, excluding the first one which is undefined */
    public final float[] speeds;

    /** The speed estimations at each sample */
    public final float[] speedEst;

    public SpeedMeasurement(float speed, int numSamples, int startingSamples, int maxSamples, long[] time, long[] data, float[] speeds, float[] speedEst) {
        this.speed = speed;
        this.numSamples = numSamples;
        this.startingSamples = startingSamples;
        this.maxSamples = maxSamples;
        this.time = time;
        this.data = data;
        this.speeds = speeds;
        this.speedEst = speedEst;
    }

    @NonNull
    @Override
    public String toString() {
        return "SpeedMeasurement{" +
                "speed=" + speed +
                ", numSamples=" + numSamples +
                ", startingSamples=" + startingSamples +
                ", maxSamples=" + maxSamples +
                ", time=" + Arrays.toString(time) +
                ", data=" + Arrays.toString(data) +
                ", speeds=" + Arrays.toString(speeds) +
                ", speedEst=" + Arrays.toString(speedEst) +
                '}';
    }
}
