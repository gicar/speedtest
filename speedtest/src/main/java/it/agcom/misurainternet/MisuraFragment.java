package it.agcom.misurainternet;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.progressindicator.LinearProgressIndicator;

import net.gicar.util.Misc;
import net.gicar.util.appcompat.XAppCompatActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.lifecycle.ViewModelProvider;
import it.agcom.misurainternet.dialog.ErrorDialog;
import it.agcom.misurainternet.environment.LocationViewModel;
import it.agcom.misurainternet.environment.NetworkViewModel;
import it.agcom.misurainternet.speedtest.MeasurementProgress;
import it.agcom.misurainternet.speedtest.SpeedTestViewModel;
import it.agcom.misurainternet.speedtest.ping.PingMeasurement;
import it.agcom.misurainternet.speedtest.server.ServerMeasurement;
import it.agcom.misurainternet.speedtest.transfer.SpeedMeasurement;
import it.agcom.misurainternet.view.PingChart;
import it.agcom.misurainternet.view.PingValue;
import it.agcom.misurainternet.view.SpeedChart;
import it.agcom.misurainternet.view.SpeedIndicator;
import it.agcom.misurainternet.view.SpeedValue;

import static it.agcom.misurainternet.speedtest.SpeedTestViewModel.Status.FAILED_PREPARING;

/** Fragment managing the measurement process interface */
public class MisuraFragment extends Fragment {

    /** Log tag */
    private final String TAG = "MisuraFragment";

    /** ViewModel for retained data */
    private MisuraViewModel misuraViewModel;

    /** ViewModel controlling the measurement process */
    private SpeedTestViewModel speedTestViewModel;

    /** ViewModel controlling the network status */
    private NetworkViewModel networkViewModel;

    /** ViewModel controlling the location */
    private LocationViewModel locationViewModel;

    /** The MotionLayout */
    private MotionLayout motionLayout;

    /** Start and stop button */
    private Button button;

    /** Textviews */
    private TextView statusText;

    /** Test progress indicator */
    private LinearProgressIndicator progressIndicator;

    /** View holding the results fragment */
    FragmentContainerView resultsFragmentView;

    public MisuraFragment() {
        super(R.layout.fragment_misura);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        // get reference to the ViewModels
        misuraViewModel = new ViewModelProvider(requireActivity()).get(MisuraViewModel.class);
        speedTestViewModel = new ViewModelProvider(requireActivity()).get(SpeedTestViewModel.class);
        networkViewModel = new ViewModelProvider(requireActivity()).get(NetworkViewModel.class);
        locationViewModel = new ViewModelProvider(requireActivity()).get(LocationViewModel.class);

        // find the views
        motionLayout = view.findViewById(R.id.motion_layout);
        button = view.findViewById(R.id.test_button);
        statusText = view.findViewById(R.id.test_progress_text);
        progressIndicator = view.findViewById(R.id.test_progress_indicator);
        resultsFragmentView = view.findViewById(R.id.test_result_fragment);

        speedTestViewModel.statusPing.observe(getViewLifecycleOwner(),
                new PingValue(view.findViewById(R.id.ping_value), R.color.invalid_value, R.color.ping));
        speedTestViewModel.statusDownload.observe(getViewLifecycleOwner(),
                new SpeedValue(view.findViewById(R.id.download_value), R.color.invalid_value, R.color.download));
        speedTestViewModel.statusUpload.observe(getViewLifecycleOwner(),
                new SpeedValue(view.findViewById(R.id.upload_value), R.color.invalid_value, R.color.upload));

        speedTestViewModel.statusPing.observe(getViewLifecycleOwner(),
                new PingChart(view.findViewById(R.id.ping_chart),
                        R.color.chart_background, R.color.chart_border, R.dimen.chart_border,
                        R.color.chart_ping_line, R.color.chart_ping_fill_high, R.color.chart_ping_fill_low, R.dimen.chart_ping_line,
                .75f));
        speedTestViewModel.statusDownload.observe(getViewLifecycleOwner(),
                new SpeedChart(view.findViewById(R.id.download_chart),
                        R.color.chart_background, R.color.chart_border, R.dimen.chart_border,
                        R.color.chart_download_line, R.color.chart_download_fill_high, R.color.chart_download_fill_low, R.dimen.chart_speed_line));
        speedTestViewModel.statusUpload.observe(getViewLifecycleOwner(),
                new SpeedChart(view.findViewById(R.id.upload_chart),
                        R.color.chart_background, R.color.chart_border, R.dimen.chart_border,
                        R.color.chart_upload_line, R.color.chart_upload_fill_high, R.color.chart_upload_fill_low, R.dimen.chart_speed_line));

        new SpeedIndicator(view.findViewById(R.id.speed_gauge), speedTestViewModel, getViewLifecycleOwner());

        if (speedTestViewModel.status.getValue() == SpeedTestViewModel.Status.IDLE)
            motionLayout.transitionToState(R.id.welcome_screen, 0);
        else motionLayout.transitionToState(R.id.measurement_screen, 0);

        // configure the button
        button.setOnClickListener(v -> {

            SpeedTestViewModel.Status status = speedTestViewModel.status.getValue();
            if (status == SpeedTestViewModel.Status.IDLE || status.finished) {

                // prototype expiration check (30 days)
                if (System.currentTimeMillis() > BuildConfig.TIMESTAMP + 1000L * 60 * 60 * 24 * 30) {
                    ErrorDialog.show((XAppCompatActivity) requireActivity(),
                            R.string.proto_expired_title, getString(R.string.proto_expired_text));
                    return;
                }

                // start test
                misuraViewModel.networkStatusAtStart = networkViewModel.networkStatus.getValue();
                speedTestViewModel.startTests(misuraViewModel.networkStatusAtStart);
                motionLayout.transitionToState(R.id.measurement_screen, 250);
            }
            else {
                // stop test
                speedTestViewModel.stopTests();
            }
        });

        // update with the test status changes
        speedTestViewModel.status.observe(getViewLifecycleOwner(), status -> updateStatus());
        speedTestViewModel.statusServer.observe(getViewLifecycleOwner(), status -> updateStatus());
        speedTestViewModel.statusPing.observe(getViewLifecycleOwner(), statusPing -> updateStatus());
        speedTestViewModel.statusDownload.observe(getViewLifecycleOwner(), statusDownload -> updateStatus());
        speedTestViewModel.statusUpload.observe(getViewLifecycleOwner(), statusUpload -> updateStatus());

        // fill the welcome screen text
        TextView text = view.findViewById(R.id.welcome_text);
        TextAppearanceSpan welcomeHeading = new TextAppearanceSpan(requireContext(), R.style.WelcomeHeading);
        TextAppearanceSpan welcomeText1 = new TextAppearanceSpan(requireContext(), R.style.WelcomeText);
        TextAppearanceSpan welcomeText2 = new TextAppearanceSpan(requireContext(), R.style.WelcomeText);
        TextAppearanceSpan welcomeText3 = new TextAppearanceSpan(requireContext(), R.style.WelcomeText);
        text.setText(new SpannableStringBuilder()
                .append(getString(R.string.welcome_1a) + "\n\n", welcomeHeading, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                .append(getString(R.string.welcome_1b) + "\n\n", welcomeText1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                .append(getString(R.string.welcome_1c) + "\n\n", welcomeText2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                .append(getString(R.string.welcome_2a, Misc.getAppVersionName(requireContext())), welcomeText3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE));
    }

    private void updateStatus()
    {
        if (Misc.DEBUG) Log.d(TAG, "Update status: " +
                speedTestViewModel.status.getValue() + ", " +
                (speedTestViewModel.statusServer.getValue() != null ? speedTestViewModel.statusServer.getValue().status : "-") + ", " +
                (speedTestViewModel.statusPing.getValue() != null ? speedTestViewModel.statusPing.getValue().status : "-") + ", " +
                (speedTestViewModel.statusDownload.getValue() != null ? speedTestViewModel.statusDownload.getValue().status : "-") + ", " +
                (speedTestViewModel.statusUpload.getValue() != null ? speedTestViewModel.statusUpload.getValue().status : "-")
                );

        SpeedTestViewModel.Status status = speedTestViewModel.status.getValue();
        boolean transitionToResults = false;
        String resultMessage = null;
        MisuraResult result = null;
        switch (status) {

            case IDLE:
                button.setText(getString(R.string.start_test));
                break;

            case PREPARING:
                button.setText(getString(R.string.stop_test));
                MeasurementProgress<ServerMeasurement> serverMeasurement = speedTestViewModel.statusServer.getValue();
                if (serverMeasurement == null) break;
                switch (serverMeasurement.status) {
                    case CONNECTING:
                    case STARTING:
                    case STARTED:
                        setProgress(Float.NaN);
                        statusText.setText(getString(R.string.status_initializing));
                        break;
                }
                break;

            case PING:
                button.setText(getString(R.string.stop_test));
                MeasurementProgress<PingMeasurement> pingMeasurement = speedTestViewModel.statusPing.getValue();
                if (pingMeasurement == null) break;
                switch (pingMeasurement.status) {
                    case CONNECTING:
                    case STARTING:
                    case STARTED:
                        setProgress(Float.NaN);
                        statusText.setText(getString(R.string.status_ping_executing));
                        break;
                }
                break;

            case DOWNLOAD:
                button.setText(getString(R.string.stop_test));
                MeasurementProgress<SpeedMeasurement> downloadMeasurement = speedTestViewModel.statusDownload.getValue();
                if (downloadMeasurement == null) break;
                switch (downloadMeasurement.status) {
                    case CONNECTING:
                        setProgress(0);
                        statusText.setText(getString(R.string.status_download_starting));
                        break;

                    case STARTING:
                        setProgress(downloadMeasurement.statusPercent);
                        statusText.setText(getString(R.string.status_download_starting));
                        break;

                    case STARTED:
                        setProgress(downloadMeasurement.statusPercent);
                        statusText.setText(getString(R.string.status_download_executing));
                        break;
                }
                break;

            case UPLOAD:
                button.setText(getString(R.string.stop_test));
                MeasurementProgress<SpeedMeasurement> uploadMeasurement = speedTestViewModel.statusUpload.getValue();
                if (uploadMeasurement == null) break;
                switch (uploadMeasurement.status) {
                    case CONNECTING:
                        setProgress(0);
                        statusText.setText(getString(R.string.status_upload_starting));
                        break;

                    case STARTING:
                        setProgress(uploadMeasurement.statusPercent);
                        statusText.setText(getString(R.string.status_upload_starting));
                        break;

                    case STARTED:
                        setProgress(uploadMeasurement.statusPercent);
                        statusText.setText(getString(R.string.status_upload_executing));
                        break;
                }
                break;

            case FINISHED:
                button.setText(getString(R.string.repeat_test));
                statusText.setText(getString(R.string.status_completed));
                transitionToResults = true;

                // create the result object
                // the location is fetched at the end as it could not be available at test start
                result = new MisuraResult(speedTestViewModel.startTime, speedTestViewModel.endTime,
                        speedTestViewModel, misuraViewModel.networkStatusAtStart, locationViewModel.location.getValue());
                break;

            case ABORTED:
                button.setText(getString(R.string.repeat_test));
                statusText.setText(getString(R.string.status_aborted));
                transitionToResults = true;
                resultMessage = getString(R.string.error_aborted);
                break;

            case FAILED_PREPARING:
            case FAILED_PING:
            case FAILED_DOWNLOAD:
            case FAILED_UPLOAD:
                button.setText(getString(R.string.repeat_test));
                statusText.setText(getString(R.string.status_error));
                transitionToResults = true;
                if (status == FAILED_PREPARING) resultMessage = getString(R.string.error_connecting);
                else resultMessage = getString(R.string.error_connected);
                break;
        }

        // transition to results if needed
        if (transitionToResults) {
            TestResultFragment testResultFragment = (TestResultFragment) getChildFragmentManager().findFragmentByTag("test_result_fragment");
            assert testResultFragment != null;
            if (resultMessage != null) testResultFragment.showResults(resultMessage);
            else testResultFragment.showResults(result);
            motionLayout.transitionToState(R.id.results_screen, 250);
        }

        // keep the screen on during active testing
        if (status == SpeedTestViewModel.Status.IDLE || status.finished)
            requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void setProgress(float percent)
    {
        if (Float.isNaN(percent)) {
            if (!progressIndicator.isIndeterminate()) {
                progressIndicator.setVisibility(View.INVISIBLE);
                progressIndicator.setIndeterminate(true);
                progressIndicator.setVisibility(View.VISIBLE);
            }
        }
        else {
            if (progressIndicator.isIndeterminate()) {
                progressIndicator.setVisibility(View.INVISIBLE);
                progressIndicator.setIndeterminate(false);
                progressIndicator.setVisibility(View.VISIBLE);
            }
            progressIndicator.setProgress((int) (percent * 100));
        }
    }
}
