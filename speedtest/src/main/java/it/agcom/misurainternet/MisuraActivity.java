package it.agcom.misurainternet;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import net.gicar.util.IntentUtils;
import net.gicar.util.Misc;
import net.gicar.util.appcompat.NavigationDrawerActivity;
import net.gicar.util.appcompat.PermissionRequester;
import net.gicar.util.appcompat.RetainedInstance;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import it.agcom.misurainternet.dialog.PermissionRequestDialog;
import it.agcom.misurainternet.environment.LocationViewModel;
import it.agcom.misurainternet.environment.NetworkViewModel;
import it.agcom.misurainternet.preferences.MisuraSettings;

/** Main activity */
public class MisuraActivity extends NavigationDrawerActivity {

    /** The request of permission to access location and telephony services */
    static private final int PERMISSION_REQUEST = 2001;

    /** General settings */
    private MisuraSettings settings;

    /** ViewModel controlling the location */
    private LocationViewModel locationViewModel;

    /** The action bar */
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (Misc.DEBUG) Log.d(TAG, "onCreate");

        setContentView(R.layout.main);

        // set our toolbar as the actionbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // configure the drawer
        configureNavigationDrawer(R.id.drawer_layout, R.id.left_drawer);
        NavigationDrawer leftDrawer = getLeftNavigationDrawer();
//        showAll = leftDrawer.getMenuItem(R.id.nav_show_all);

        RetainedInstance.getInstance(this, NetworkViewModel.class);
        locationViewModel = RetainedInstance.getInstance(this, LocationViewModel.class);

        // get the settings objects
        MisuraApplication application = (MisuraApplication) getApplication();
        settings = MisuraApplication.getSettings(application);

        // on first run force the display of the explanation dialog
        permissionsRequester.request(settings.isFirstRun() ?
                PermissionRequester.AskStrategy.FORCE_ASK : PermissionRequester.AskStrategy.ASK_DEFAULT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            getLeftNavigationDrawer().open();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void executeNavigationItem(int menuItemId)
    {
        if (menuItemId == R.id.nav_open_site_agcom) {
            IntentUtils.viewIntent(this, "https://www.agcom.it");
        } else if (menuItemId == R.id.nav_open_site_misurainternet) {
            IntentUtils.viewIntent(this, "https://www.misurainternet.it");
        } else {
            super.executeNavigationItem(menuItemId);
        }
    }

    /** Requester for file export */
    final PermissionRequester permissionsRequester = new PermissionRequester(this,
            new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST,
            (permissionRequester, missing) -> {
                locationViewModel.updateLocation();
                if (missing.size() == 0) {
                    // OK
                } else {
                    // TODO save that the user did not gave the requested permission to avoid repeating the request
                }
            }, (permissionRequester, missing, args) -> PermissionRequestDialog.show(MisuraActivity.this, args));
}