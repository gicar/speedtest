package it.agcom.misurainternet.dialog;

import android.app.Dialog;
import android.os.Bundle;

import net.gicar.util.appcompat.XAppCompatActivity;
import net.gicar.util.appcompat.XAppCompatDialogFragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import it.agcom.misurainternet.R;

/** Shows a generic error message */
public class ErrorDialog extends XAppCompatDialogFragment {

	/** The name we will use for our preferences file, fragment name and logs */
	private final static String ERROR_DIALOG = "ERROR_DIALOG";

    /** The keys for the fields in the bundles */
    private final static String
            TITLE_KEY = "TITLE_KEY",
            MESSAGE_KEY = "MESSAGE_KEY";

    /**
     * Starts a dialog showing the specified error message
     * @param activity The activity requesting the dialog
     * @param title The title text
     * @param message The message text
     */
    public static void show(XAppCompatActivity activity, int title, String message)
    {
        // create our fragment, save the parameters and show it
        Bundle args = new Bundle();
        args.putInt(TITLE_KEY, title);
        args.putString(MESSAGE_KEY, message);
        new ErrorDialog().showRequest(activity, ERROR_DIALOG, args);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle arg = requireArguments();
        return new AlertDialog.Builder(requireActivity(), R.style.DialogAlertFragmentTheme)
                .setTitle(arg.getInt(TITLE_KEY))
                .setMessage(arg.getString(MESSAGE_KEY))
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(R.drawable.round_warning_24)
                .create();
    }
}
