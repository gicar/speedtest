package it.agcom.misurainternet.dialog;

import android.app.Dialog;
import android.os.Bundle;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import net.gicar.util.appcompat.PermissionRequestBaseDialog;
import net.gicar.util.appcompat.XAppCompatActivity;

import androidx.annotation.NonNull;
import it.agcom.misurainternet.R;

/** Requests an Android permission */
public class PermissionRequestDialog extends PermissionRequestBaseDialog {

	/** The name we will use for our preferences file, fragment name and logs */
	private final static String PERMISSION_REQUEST_DIALOG = "PERMISSION_REQUEST_DIALOG";

    /**
     * Starts a dialog showing the specified permission request message
     * @param activity The activity requesting the dialog
     * @param args The base args
     */
    public static void show(XAppCompatActivity activity, Bundle args)
    {
        new PermissionRequestDialog().showRequest(activity, PERMISSION_REQUEST_DIALOG, args);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        return new MaterialAlertDialogBuilder(requireActivity(), R.style.DialogAlertFragmentTheme)
                .setTitle(R.string.permission_title)
                .setMessage(R.string.permission_missing)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> notifyRequestDialogResult(true))
                .setIcon(R.drawable.round_not_listed_location_24)
                .show();
    }
}
