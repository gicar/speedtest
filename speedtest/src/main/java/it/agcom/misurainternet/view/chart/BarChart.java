package it.agcom.misurainternet.view.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

public class BarChart extends Chart {

    /** Margin of each bar on the bar width */
    private float barMarginPercent = .75f;

    /** Paint */
    private final Paint paint = new Paint();

    /** Rect */
    private final RectF r = new RectF(), r2 = new RectF();

    /** Paths */
    private final Path linePath = new Path(), fillPath = new Path();

    /** Draw positions of the valus */
    private float[] vx, vy;

    public void setBarMarginPercent(float barMarginPercent) {
        this.barMarginPercent = barMarginPercent;
    }

    @Override
    protected void draw(RectF fullRect, Canvas canvas)
    {
        if (num > 0) {

            // check if we need to (re)allocate the values position arrays
            if (vx == null || vx.length < num) {
                vx = new float[totNum];
                vy = new float[totNum];
            }

            // find the max value
            float max = 0;
            for (int i = 0; i < num; i++) if (values[offset + i] > max) max = values[offset + i];

            r.set(fullRect);
            r.inset(valueLineSize / 2, valueLineSize / 2);
            float z = (totNum + barMarginPercent * (totNum + 1));
            float margin = (r.width() * barMarginPercent) / z;
            float width = r.width() / z;
            r.left += margin;
            float xd = r.width() / totNum;
            float yd = r.height() / max;
            paint.setAntiAlias(true);
            for (int i = 0; i < num; i++) {
                r2.left = r.left + i * xd;
                r2.right = r2.left + width;
                r2.bottom = r.bottom;
                r2.top = r.bottom - values[offset + i] * yd;

                // draw the fill
                if (hasFillShader()) paint.setShader(getFillShader(fullRect));
                else paint.setColor(getFillColor());
                paint.setStrokeWidth(0);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawRect(r2, paint);

                // draw the border
                paint.setShader(null);
                paint.setColor(valueLineColor);
                paint.setStrokeWidth(valueLineSize);
                paint.setStyle(Paint.Style.STROKE);
                canvas.drawRect(r2, paint);
            }
        }
    }}
