package it.agcom.misurainternet.view;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;

/** Base class for displaying values during testing phase */
abstract class BaseValue {

    /** View holding the value */
    protected final TextView valueView;

    /** Colors */
    protected final int invalidColor, validColor;

    protected BaseValue(TextView valueView, @ColorRes int invalidColorId, @ColorRes int validColorId) {
        this.valueView = valueView;
        Context context = valueView.getContext();
        invalidColor = ContextCompat.getColor(context, invalidColorId);
        validColor = ContextCompat.getColor(context, validColorId);
    }

    protected void setText(String text)
    {
        int w = (int) (valueView.getPaint().measureText(text) +
                        valueView.getPaddingLeft() + valueView.getPaddingRight() + 2);
        if (valueView.getWidth() != w) valueView.setWidth(w);
        valueView.setText(text);
    }
}