package it.agcom.misurainternet.view.chart;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.RectF;
import android.graphics.Shader;

import androidx.annotation.ColorInt;
import androidx.annotation.Dimension;

/** Generic chart */
public abstract class Chart {

    /** Value line colors */
    protected @ColorInt int valueLineColor;

    /** Gradient colors (or single fill color) */
    protected @ColorInt int[] valueFillColors = new int[] { 0 };

    /** Gradient positions */
    protected float[] valueFillPositions;

    /** Gradient */
    protected LinearGradient gradient;

    /** Size of the value line */
    protected @Dimension float valueLineSize;

    /** Offset in the values array */
    protected int offset;

    /** Number of valid values */
    protected int num;

    /** Total number of values */
    protected int totNum;

    /** Values */
    protected float[] values;

    public void setValueLineColor(int valueLineColor) {
        this.valueLineColor = valueLineColor;
    }

    public void setValueFillColor(int valueFillColor) {
        this.valueFillColors = new int[]{valueFillColor};
        this.valueFillPositions = null;
        gradient = null;
    }

    public void setValueFillColors(int[] valueFillColors, float[] valueFillPositions) {
        this.valueFillColors = valueFillColors;
        this.valueFillPositions = valueFillPositions;
        gradient = null;
    }

    public void setValueLineSize(float valueLineSize) {
        this.valueLineSize = valueLineSize;
    }

    /**
     * Sets the data to be displayed
     * @param offset Offset in the values array
     * @param num Number of valid samples
     * @param totNum Total number of samples to be considered
     * @param values Value samples
     */
    public void setValues(int offset, int num, int totNum, float[] values) {
        this.offset = offset;
        this.num = num;
        this.totNum = totNum;
        this.values = values;
    }

    void onSizeChanged() {
        gradient = null;
    }

    protected boolean hasFillShader() {
        return valueFillPositions != null;
    }

    protected Shader getFillShader(RectF fullRect)
    {
        if (gradient == null) gradient = new LinearGradient(0, 0, 0, fullRect.height(),
                valueFillColors, valueFillPositions, Shader.TileMode.CLAMP);
        return gradient;
    }

    protected int getFillColor() {
        return valueFillColors[0];
    }

    abstract protected void draw(RectF r, Canvas canvas);
}
