package it.agcom.misurainternet.view.chart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.ColorInt;
import androidx.annotation.Dimension;
import androidx.annotation.Nullable;

/** Visualizes the speed chart */
public class ChartView extends View {

    /** Colors */
    private @ColorInt int backgroundColor, borderLineColor;

    /** Border line size */
    private @Dimension float borderLineSize;

    /** Rect used during drawing */
    private final RectF fullRect = new RectF(), r = new RectF();

    /** Paint */
    private final Paint paint = new Paint();

    /** List of charts to be drawn */
    private final List<Chart> charts = new ArrayList<>();

    /** Constructs the view from XML inflation */
    public ChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setBorderLineColor(int borderLineColor) {
        this.borderLineColor = borderLineColor;
    }

    public void setBorderLineSize(float borderLineSize) {
        this.borderLineSize = borderLineSize;
    }

    public void setCharts(List<Chart> charts) {
        this.charts.clear();
        this.charts.addAll(charts);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        for (Chart chart : charts) chart.onSizeChanged();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // fill the full area
        fullRect.set(0,0, getWidth(), getHeight());
        paint.setColor(backgroundColor);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(fullRect, paint);

        // draw all the charts
        for (Chart chart : charts) chart.draw(fullRect, canvas);

        // draw the outside border
        r.set(fullRect);
        r.inset(borderLineSize / 2, borderLineSize / 2);
        paint.setColor(borderLineColor);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderLineSize);
//        canvas.drawRect(r, paint);
        canvas.drawLine(r.left, r.bottom, r.right, r.bottom, paint);
    }
}
