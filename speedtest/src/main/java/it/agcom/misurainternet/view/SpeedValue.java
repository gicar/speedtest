package it.agcom.misurainternet.view;

import android.widget.TextView;

import java.util.Locale;

import androidx.annotation.ColorRes;
import androidx.lifecycle.Observer;
import it.agcom.misurainternet.speedtest.MeasurementProgress;
import it.agcom.misurainternet.speedtest.transfer.SpeedMeasurement;

/** Displays the speed value during testing phase */
public class SpeedValue extends BaseValue implements Observer<MeasurementProgress<SpeedMeasurement>> {

    public SpeedValue(TextView valueView, @ColorRes int invalidColorId, @ColorRes int validColorId) {
        super(valueView, invalidColorId, validColorId);
    }

    @Override
    public void onChanged(MeasurementProgress<SpeedMeasurement> measurementProgress) {
        if (measurementProgress == null || Float.isNaN(measurementProgress.value.speed)) {
            valueView.setTextColor(invalidColor);
            setText("\u2014 ");
        }
        else {
            valueView.setTextColor(validColor);
            setText(String.format(Locale.ITALY, "%.1f", measurementProgress.value.speed));
        }
    }
}
