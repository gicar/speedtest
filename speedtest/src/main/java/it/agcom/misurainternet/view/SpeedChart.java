package it.agcom.misurainternet.view;

import android.content.Context;
import android.content.res.Resources;

import java.util.Collections;

import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import it.agcom.misurainternet.speedtest.MeasurementProgress;
import it.agcom.misurainternet.speedtest.transfer.SpeedMeasurement;
import it.agcom.misurainternet.view.chart.ChartView;
import it.agcom.misurainternet.view.chart.LineChart;

public class SpeedChart implements Observer<MeasurementProgress<SpeedMeasurement>> {

    /** Chart */
    private final ChartView chart;

    /** Line chart for displaying speed */
    private final LineChart lineChart = new LineChart();

    public SpeedChart(ChartView chart, @ColorRes int backgroundColorId, @ColorRes int borderColorId, @DimenRes int borderDimenId,
                      @ColorRes int lineColorId, @ColorRes int fillHighColorId, @ColorRes int fillLowColorId, @DimenRes int lineDimenId)
    {
        // save the parameters
        this.chart = chart;

        // configure the chart
        Context context = chart.getContext();
        Resources res = chart.getResources();
        chart.setBackgroundColor(ContextCompat.getColor(context, backgroundColorId));
        chart.setBorderLineColor(ContextCompat.getColor(context, borderColorId));
        chart.setBorderLineSize(res.getDimension(borderDimenId));
        chart.setCharts(Collections.singletonList(lineChart));
        lineChart.setValueLineColor(ContextCompat.getColor(context, lineColorId));
        lineChart.setValueFillColors(
                new int[] {
                        ContextCompat.getColor(context, fillHighColorId),
                        ContextCompat.getColor(context, fillLowColorId) },
                new float[] { 0f, 1f });
        lineChart.setValueLineSize(res.getDimension(lineDimenId));
        chart.invalidate();
    }

    @Override
    public void onChanged(MeasurementProgress<SpeedMeasurement> measurementProgress)
    {
        if (measurementProgress == null) lineChart.setValues(0, 0,0, null);
        else {
            SpeedMeasurement m = measurementProgress.value;
            lineChart.setValues(m.startingSamples, Math.max(0, m.numSamples - m.startingSamples),
                    m.maxSamples - m.startingSamples, m.speedEst);
        }
        chart.invalidate();
    }
}
