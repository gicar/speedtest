package it.agcom.misurainternet.view.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

/** Line chart */
public class LineChart extends Chart {

    /** Paint */
    private final Paint paint = new Paint();

    /** Rect */
    private final RectF r = new RectF();

    /** Paths */
    private final Path linePath = new Path(), fillPath = new Path();

    /** Draw positions of the valus */
    private float[] vx, vy;

    @Override
    protected void draw(RectF fullRect, Canvas canvas)
    {
        if (num > 0) {

            // check if we need to (re)allocate the values position arrays
            if (vx == null || vx.length < num) {
                vx = new float[totNum];
                vy = new float[totNum];
            }

            // find the max value
            float max = 0;
            for (int i = 0; i < num; i++) if (values[offset + i] > max) max = values[offset + i];

            // calculate the positions of the points
            r.set(fullRect);
            r.inset(valueLineSize / 2, valueLineSize / 2);
            float xd = r.width() / (totNum - 1);
            float yd = r.height() / max;
            for (int i = 0; i < num; i++) {
                vx[i] = r.left + i * xd;
                vy[i] = r.bottom - values[offset + i] * yd;
            }

            // create the path for the line
            linePath.rewind();
            linePath.moveTo(vx[0], vy[0]);
            for (int i = 1; i < num; i++) linePath.lineTo(vx[i], vy[i]);

            // create the path for the fill
            fillPath.set(linePath);
            fillPath.lineTo(r.left + (num - 1) * xd, r.bottom);
            fillPath.lineTo(r.left, r.bottom);
            fillPath.lineTo(r.left, r.top);

            // draw the fill
            paint.setAntiAlias(true);
            if (hasFillShader()) paint.setShader(getFillShader(fullRect));
            else paint.setColor(getFillColor());
            paint.setStrokeWidth(0);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawPath(fillPath, paint);

            // draw the line
            paint.setShader(null);
            paint.setColor(valueLineColor);
            paint.setStrokeWidth(valueLineSize);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(linePath, paint);
        }
    }
}
