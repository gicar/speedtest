package it.agcom.misurainternet.view;

import android.content.Context;
import android.util.Log;

import com.github.anastr.speedviewlib.PointerSpeedometer;

import net.gicar.util.Misc;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import it.agcom.misurainternet.R;
import it.agcom.misurainternet.speedtest.MeasurementProgress;
import it.agcom.misurainternet.speedtest.transfer.SpeedMeasurement;
import it.agcom.misurainternet.speedtest.SpeedTestViewModel;
import it.agcom.misurainternet.util.MultiStatus;

import static java.lang.Float.NaN;

/** Controls the speedometer appearance */
public class SpeedIndicator {

    /** Log tag */
    private final static String TAG = "SpeedIndicator";

    /** Smooth movement time */
    private final static int MOVE_TIME = 500;

    /** Speedometer */
    private final PointerSpeedometer speedView;

    /** ViewModel controlling the measurement process */
    private final SpeedTestViewModel speedTestViewModel;

    /** Our LifecycleOwner */
    private final LifecycleOwner owner;

    /** Colors */
    private final int noneColor, downloadColor, uploadColor;

    /** Current displayed speed status */
    private final MultiStatus currentSpeedTestStatus = new MultiStatus();

    public SpeedIndicator(PointerSpeedometer speedView, SpeedTestViewModel speedTestViewModel,
                          @NonNull LifecycleOwner owner)
    {
        this.speedView = speedView;
        this.speedTestViewModel = speedTestViewModel;
        this.owner = owner;

        Context context = speedView.getContext();
        noneColor = ContextCompat.getColor(context, R.color.speedgauge_sweep);
        downloadColor = ContextCompat.getColor(context, R.color.download);
        uploadColor = ContextCompat.getColor(context, R.color.upload);

        // observe test status changes
        speedTestViewModel.status.observe(owner, this::onSpeedTestStatusChanged);
    }

    public void onSpeedTestStatusChanged(SpeedTestViewModel.Status status)
    {
        if (Misc.DEBUG) Log.d(TAG, "onSpeedTestStatusChanged() called with: status = [" + status + "]");

        // remove previous observers
        speedTestViewModel.statusDownload.removeObserver(this::onTransferStatusChanged);
        speedTestViewModel.statusUpload.removeObserver(this::onTransferStatusChanged);

        switch (status) {

            case IDLE:
                speedView.setPointerColor(noneColor);
                speedView.setSpeedTextColor(0);
                speedView.setUnitTextColor(0);
                setSpeed(NaN, true);
                break;

            case DOWNLOAD:
                speedView.setPointerColor(noneColor);
                speedView.setSpeedTextColor(noneColor);
                speedView.setUnitTextColor(noneColor);
                speedTestViewModel.statusDownload.observe(owner, this::onTransferStatusChanged);
                break;

            case UPLOAD:
                speedView.setPointerColor(noneColor);
                speedView.setSpeedTextColor(noneColor);
                speedView.setUnitTextColor(noneColor);
                speedTestViewModel.statusUpload.observe(owner, this::onTransferStatusChanged);
                break;

            case FINISHED:
                speedView.setPointerColor(noneColor);
                speedView.setSpeedTextColor(0);
                speedView.setUnitTextColor(0);
                setSpeed(NaN, true);
                break;
        }
    }


    public void onTransferStatusChanged(MeasurementProgress<SpeedMeasurement> measurementProgress)
    {
        if (Misc.DEBUG) Log.d(TAG, "onTransferStatusChanged() called with: measurementProgress = [" + measurementProgress + "]");

        if (measurementProgress == null) setSpeed(NaN, false);
        else switch (measurementProgress.status) {

            case READY:
                setSpeed(NaN, false);
                break;

            case CONNECTING:
            case STARTING:
                setSpeed(NaN, false);
                break;

            case STARTED:
            case FINISHED:
                SpeedTestViewModel.Status status = speedTestViewModel.status.getValue();
                if (currentSpeedTestStatus.isChanged(status)) {
                    switch (status) {
                        case DOWNLOAD:
                            speedView.setPointerColor(downloadColor);
                            speedView.setSpeedTextColor(downloadColor);
                            break;

                        case UPLOAD:
                            speedView.setPointerColor(uploadColor);
                            speedView.setSpeedTextColor(uploadColor);
                            break;
                    }
                }
                setSpeed(measurementProgress.value.speed, true);
                break;

            default:
                setSpeed(NaN, true);
                break;
        }

    }

    private void setSpeed(float speed, boolean immediate)
    {
        if (Float.isNaN(speed)) {
            speedView.speedTo(0, immediate ? 0 : MOVE_TIME);
        }
        else {
            if (speed > 1000) setMaxSpeed(2500);
            else if (speed > 400) setMaxSpeed(1000);
            else if (speed > 100) setMaxSpeed(400);
            else if (speed > 50) setMaxSpeed(100);
            else setMaxSpeed(50);
            speedView.speedTo(speed, immediate ? 0 : MOVE_TIME);
        }
    }

    private void setMaxSpeed(float max)
    {
        float prev = speedView.getMaxSpeed();
        if (max > prev) speedView.setMaxSpeed(max);
    }
}
