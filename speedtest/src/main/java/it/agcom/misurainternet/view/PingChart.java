package it.agcom.misurainternet.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import net.gicar.util.Misc;

import java.util.Collections;

import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import it.agcom.misurainternet.speedtest.MeasurementProgress;
import it.agcom.misurainternet.speedtest.ping.PingMeasurement;
import it.agcom.misurainternet.view.chart.BarChart;
import it.agcom.misurainternet.view.chart.ChartView;

public class PingChart implements Observer<MeasurementProgress<PingMeasurement>> {

    /** Log tag */
    private final static String TAG = "PingChart";

    /** Chart */
    private final ChartView chart;

    /** Bar chart for displaying speed */
    private final BarChart barChart = new BarChart();

    public PingChart(ChartView chart, @ColorRes int backgroundColorId, @ColorRes int borderColorId, @DimenRes int borderDimenId,
                     @ColorRes int lineColorId, @ColorRes int fillHighColorId, @ColorRes int fillLowColorId, @DimenRes int lineDimenId,
                     float barMarginPercent)
    {
        // save the parameters
        this.chart = chart;

        // configure the chart
        Context context = chart.getContext();
        Resources res = chart.getResources();
        chart.setBackgroundColor(ContextCompat.getColor(context, backgroundColorId));
        chart.setBorderLineColor(ContextCompat.getColor(context, borderColorId));
        chart.setBorderLineSize(res.getDimension(borderDimenId));
        chart.setCharts(Collections.singletonList(barChart));
        barChart.setValueLineColor(ContextCompat.getColor(context, lineColorId));
        barChart.setValueFillColors(
                new int[] {
                        ContextCompat.getColor(context, fillHighColorId),
                        ContextCompat.getColor(context, fillLowColorId) },
                new float[] { 0f, 1f });
        barChart.setValueLineSize(res.getDimension(lineDimenId));
        barChart.setBarMarginPercent(barMarginPercent);
        chart.invalidate();
    }

    @Override
    public void onChanged(MeasurementProgress<PingMeasurement> measurementProgress)
    {
        if (Misc.DEBUG) Log.d(TAG, "onChanged() called with: measurementProgress = [" + measurementProgress + "]");
        if (measurementProgress == null) barChart.setValues(0, 0,0, null);
        else {
            PingMeasurement m = measurementProgress.value;
            barChart.setValues(1, Math.max(0, m.numPings - 1), m.maxPings - 1, m.pingTime);
        }
        chart.invalidate();
    }
}
