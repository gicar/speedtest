package it.agcom.misurainternet;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.gicar.util.appcompat.PermissionRequester;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.environment.NetworkViewModel;
import it.agcom.misurainternet.speedtest.SpeedTestViewModel;

/** Fragment displaying the network status */
public class NetworkStatusFragment extends Fragment {

    /** Log tag */
    private final String TAG = "NetworkStatusFragment";

    /** ViewModel controlling the network status */
    private NetworkViewModel networkViewModel;

    /** ViewModel controlling the measurement process */
    private SpeedTestViewModel speedTestViewModel;

    /** Textviews */
    private ImageView networkTypeIcon;

    /** Textviews */
    private TextView networkNameText;

    /** Button */
    private Button authorizeButton;

    public NetworkStatusFragment() {
        super(R.layout.fragment_network_status);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        // get reference to the ViewModels
        networkViewModel = new ViewModelProvider(requireActivity()).get(NetworkViewModel.class);
        speedTestViewModel = new ViewModelProvider(requireActivity()).get(SpeedTestViewModel.class);

        // find the views
        networkTypeIcon = view.findViewById(R.id.network_type_icon);
        networkNameText = view.findViewById(R.id.network_info);
        authorizeButton = view.findViewById(R.id.authorize_button);

        // request permissions on button click
        authorizeButton.setOnClickListener(view1 ->
        {
            MisuraActivity activity = (MisuraActivity)requireActivity();
            if (!activity.permissionsRequester.missingPermissionsCantRequest().isEmpty())
                PermissionRequester.showAppSystemSettings(getContext());
            else activity.permissionsRequester.request();
        });

        networkViewModel.networkStatus.observe(getViewLifecycleOwner(), networkStatus -> {
            switch (networkStatus.connectionType) {

                case ABSENT:
                    networkTypeIcon.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.round_signal_cellular_nodata_24));
                    networkNameText.setText(null);
                    break;

                case WIFI:
                    networkTypeIcon.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.round_wifi_24));
                    String ssid = networkStatus.wifiSsid;
                    TextAppearanceSpan ssidStyle = new TextAppearanceSpan(requireContext(), R.style.WiFiName);
                    TextAppearanceSpan bandTypeStyle = new TextAppearanceSpan(requireContext(), R.style.WiFiBand);
                    SpannableStringBuilder info = new SpannableStringBuilder()
                            .append(ssid != null ? "  " + ssid + "  " : "  ", ssidStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                            .append(networkStatus.wifiBand != null && networkStatus.wifiBand != NetworkStatus.WifiFrequencyBand.BAND_UNKNOWN ?
                                    networkStatus.wifiBand.name : "", bandTypeStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    networkNameText.setText(info);
                    break;

                case CELLULAR:
                    networkTypeIcon.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.round_signal_cellular_alt_24));
                    TextAppearanceSpan operatorStyle = new TextAppearanceSpan(requireContext(), R.style.OperatorName);
                    TextAppearanceSpan networkTypeStyle = new TextAppearanceSpan(requireContext(), R.style.OperatorNetwork);
                    info = new SpannableStringBuilder()
                            .append(networkStatus.cellularOperatorName != null ? "  " + networkStatus.cellularOperatorName + "  " : "  ", operatorStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                            .append(networkStatus.cellularNetworkType != null && networkStatus.cellularNetworkType != NetworkStatus.CellularNetworkType.UNKNOWN ?
                                    networkStatus.cellularNetworkType.technology.name + " (" + networkStatus.cellularNetworkType.name + ")" :
                                    "", networkTypeStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    networkNameText.setText(info);
                    break;

                case UNKNOWN:
                    networkTypeIcon.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.baselinex_cable_data));
                    networkNameText.setText(null);
                    break;
            }

        });

        speedTestViewModel.status.observe(getViewLifecycleOwner(), status -> showAuthorizeButtonIfNeeded());
    }

    @Override
    public void onResume() {
        super.onResume();
        showAuthorizeButtonIfNeeded();
    }

    private void showAuthorizeButtonIfNeeded()
    {
        // check if we have some permission missing and show the button if needed, except furing testing
        authorizeButton.setVisibility(
                ((MisuraActivity)requireActivity()).permissionsRequester.missingPermissions().isEmpty() ||
                speedTestViewModel.status.getValue().ongoing ? View.GONE : View.VISIBLE);
    }
}
