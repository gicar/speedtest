package it.agcom.misurainternet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import net.gicar.util.AsyncLoader;
import net.gicar.util.Misc;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import it.agcom.misurainternet.environment.NetworkStatus;

/** Fragment displaying the test results */
public class TestResultFragment extends Fragment {

    /** Log tag */
    private final String TAG = "TestResultFragment";

    /** The executor which will be used for performing background requests */
    private final static ExecutorService executor = Executors.newCachedThreadPool();

    /** Async address loader */
    private AsyncLoader<Location,TextView> loader;

    /** Text views */
    private TextView errorTextView,
            pingValue, jitterValue, pingMinValue,
            downloadValue, downloadTransmittedValue,
            uploadValue, uploadTransmittedValue,
            infoWifiNetworkLabel, infoWifiNetworkValue, infoWifiFrequencyLabel, infoWifiFrequencyValue, infoWifiStandardLabel, infoWifiStandardValue,
            infoCellularNetworkLabel, infoCellularNetworkValue, infoCellularTechnologyLabel, infoCellularTechnologyValue,
            infoDateValue, infoServerValue, infoDeviceModelValue, infoDeviceVersionValue,
            locationPositionValue;

    /** Result groups */
    private ViewGroup resultViewGroup, wifiGroup, cellularGroup, locationGroup;

    /** View holding the map */
    MapView mapView;

    /** Units */
    private String unitMbps, unitMs, unitMB;

    /** The map */
    private GoogleMap map;

    /** The error text */
    private String errorText;

    /** The result */
    private MisuraResult result;

    public TestResultFragment() {
        super(R.layout.fragment_test_result);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        // find the views
        errorTextView = view.findViewById(R.id.error_message);
        resultViewGroup = view.findViewById(R.id.result);
        pingValue = view.findViewById(R.id.result_ping_value);
        jitterValue = view.findViewById(R.id.result_ping_jitter_value);
        pingMinValue = view.findViewById(R.id.result_ping_min_value);
        downloadValue = view.findViewById(R.id.result_download_value);
        downloadTransmittedValue = view.findViewById(R.id.result_download_data_value);
        uploadValue = view.findViewById(R.id.result_upload_value);
        uploadTransmittedValue = view.findViewById(R.id.result_upload_data_value);
        infoDateValue = view.findViewById(R.id.result_info_date_value);
        infoServerValue = view.findViewById(R.id.result_info_server_value);
        infoDeviceModelValue = view.findViewById(R.id.result_info_device_model_value);
        infoDeviceVersionValue = view.findViewById(R.id.result_info_device_version_value);
        wifiGroup = view.findViewById(R.id.result_wifi);
        infoWifiNetworkLabel = view.findViewById(R.id.result_wifi_network);
        infoWifiNetworkValue = view.findViewById(R.id.result_wifi_network_value);
        infoWifiFrequencyLabel = view.findViewById(R.id.result_wifi_frequency);
        infoWifiFrequencyValue = view.findViewById(R.id.result_wifi_frequency_value);
        infoWifiStandardLabel = view.findViewById(R.id.result_wifi_standard);
        infoWifiStandardValue = view.findViewById(R.id.result_wifi_standard_value);
        cellularGroup = view.findViewById(R.id.result_cellular);
        infoCellularNetworkLabel = view.findViewById(R.id.result_cellular_network);
        infoCellularNetworkValue = view.findViewById(R.id.result_cellular_network_value);
        infoCellularTechnologyLabel = view.findViewById(R.id.result_cellular_technology);
        infoCellularTechnologyValue = view.findViewById(R.id.result_cellular_technology_value);
        locationGroup = view.findViewById(R.id.result_location);
        locationPositionValue = view.findViewById(R.id.result_info_position_value);
        mapView = view.findViewById(R.id.result_map);

        // get strings
        unitMbps = getString(R.string.speed_mbps);
        unitMs = getString(R.string.time_ms);
        unitMB = getString(R.string.data_mb);

        // find the map object and configure it
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(googleMap -> {
            mapView.setClickable(false);
            map = googleMap;
            UiSettings ui = map.getUiSettings();
            ui.setMapToolbarEnabled(false);
            if (result != null) setMap(result.location);
        });

        // create the async loader, with just one handler
        loader = new AsyncLoader<>(Collections.singletonList(new AddressLoader()), 10, null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        loader.shutdown();
    }

    void showResults(String errorText)
    {
        if (Misc.DEBUG) Log.d(TAG, "Displaying error: " + errorText);

        this.errorText = errorText;
        this.result = null;

        errorTextView.setText(errorText);
        errorTextView.setVisibility(View.VISIBLE);
        resultViewGroup.setVisibility(View.GONE);
    }

    @SuppressLint("SetTextI18n")
    void showResults(MisuraResult result)
    {
        if (Misc.DEBUG) Log.d(TAG, "Displaying result: " + result);

        this.errorText = null;
        this.result = result;

        errorTextView.setVisibility(View.GONE);
        resultViewGroup.setVisibility(View.VISIBLE);

        setResult(pingValue, result.pingTimeAverage, unitMs, R.style.ResultMeasurementValue, R.style.ResultMeasurementUnit);
        setResult(jitterValue, result.pingJitter, unitMs, R.style.ResultMeasurementValue2, R.style.ResultMeasurementUnit2);
        setResult(pingMinValue, result.pingTimeMin, unitMs, R.style.ResultMeasurementValue2, R.style.ResultMeasurementUnit2);

        setResult(downloadValue, result.downloadSpeed, unitMbps, R.style.ResultMeasurementValue, R.style.ResultMeasurementUnit);
        setResult(downloadTransmittedValue, result.downloadData / (1024*1024), unitMB, R.style.ResultMeasurementValue2, R.style.ResultMeasurementUnit2);

        setResult(uploadValue, result.uploadSpeed, unitMbps, R.style.ResultMeasurementValue, R.style.ResultMeasurementUnit);
        setResult(uploadTransmittedValue, result.uploadData / (1024*1024), unitMB, R.style.ResultMeasurementValue2, R.style.ResultMeasurementUnit2);

        setResult(infoDateValue, new Date(result.startTime), R.style.ResultMeasurementValue2);
        infoServerValue.setText(result.server.name != null ? result.server.name : result.server.address);
        infoDeviceModelValue.setText(result.deviceBrand + " " + result.deviceModel);
        infoDeviceVersionValue.setText(result.deviceBuild + " SDK:" + result.deviceSdk);

        boolean showWiFi = false, showCellular = false;
        switch (result.networkStatus.connectionType) {

            case WIFI:
                showWiFi = setResult(infoWifiNetworkLabel, infoWifiNetworkValue, result.networkStatus.wifiSsid);
                showWiFi |= setResult(infoWifiFrequencyLabel, infoWifiFrequencyValue,
                        result.networkStatus.wifiBand != null ? result.networkStatus.wifiBand.name : null);
                showWiFi |= setResult(infoWifiStandardLabel, infoWifiStandardValue,
                        result.networkStatus.wifiStandard != null && result.networkStatus.wifiStandard != NetworkStatus.WifiStandard.STANDARD_UNKNOWN ?
                                result.networkStatus.wifiStandard.name : null);
                break;

            case CELLULAR:
                showCellular = setResult(infoCellularNetworkLabel, infoCellularNetworkValue, result.networkStatus.cellularOperatorName);
                showCellular |= setResult(infoCellularTechnologyLabel, infoCellularTechnologyValue, result.networkStatus.getCellularTechnology());
                break;
        }
        wifiGroup.setVisibility(showWiFi ? View.VISIBLE : View.GONE);
        cellularGroup.setVisibility(showCellular ? View.VISIBLE : View.GONE);

        if (result.location != null) {
            locationGroup.setVisibility(View.VISIBLE);
            locationPositionValue.setText(null);
            loader.loadRequest(result.location, locationPositionValue);
            setMap(result.location);
        }
        else {
            locationGroup.setVisibility(View.GONE);
        }
    }

    private void setResult(TextView view, float value, String unit, @StyleRes int valueStyleId, @StyleRes int unitStyleId)
    {
        TextAppearanceSpan valueStyle = new TextAppearanceSpan(requireContext(), valueStyleId);
        TextAppearanceSpan unitStyle = new TextAppearanceSpan(requireContext(), unitStyleId);
        Spannable result = new SpannableStringBuilder()
                .append(String.format(Locale.ITALY, "%.1f  ", value), valueStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                .append(unit, unitStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(result);
    }

    private void setResult(TextView view, long value, String unit, @StyleRes int valueStyleId, @StyleRes int unitStyleId)
    {
        TextAppearanceSpan valueStyle = new TextAppearanceSpan(requireContext(), valueStyleId);
        TextAppearanceSpan unitStyle = new TextAppearanceSpan(requireContext(), unitStyleId);
        Spannable result = new SpannableStringBuilder()
                .append(String.format(Locale.ITALY, "%d  ", value), valueStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                .append(unit, unitStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(result);
    }

    private void setResult(TextView view, Date value, @StyleRes int valueStyleId)
    {
        TextAppearanceSpan valueStyle = new TextAppearanceSpan(requireContext(), valueStyleId);
        Spannable result = new SpannableStringBuilder()
                .append(MisuraConst.DATE_TIME_FORMAT.format(value), valueStyle, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        view.setText(result);
    }

    private boolean setResult(TextView labelView, TextView valueView, String value)
    {
        if (value != null) {
            labelView.setVisibility(View.VISIBLE);
            valueView.setVisibility(View.VISIBLE);
            valueView.setText(value);
            return true;
        }
        else {
            labelView.setVisibility(View.GONE);
            valueView.setVisibility(View.GONE);
            return false;
        }
    }

    private void setResult(TextView labelView, TextView valueView, String label, String value)
    {
        if (value != null) {
            labelView.setVisibility(View.VISIBLE);
            valueView.setVisibility(View.VISIBLE);
            labelView.setText(label);
            valueView.setText(value);
        }
        else {
            labelView.setVisibility(View.GONE);
            valueView.setVisibility(View.GONE);
        }
    }

    private void setMap(Location location)
    {
        if (location == null) mapView.setVisibility(View.GONE);
        else {
            mapView.setVisibility(View.INVISIBLE);
            if (map != null) {
                map.clear();
                LatLng l = new LatLng(location.getLatitude(), location.getLongitude());
                map.addMarker(new MarkerOptions()
                        .position(l));
                Context context = requireContext();
                Resources res = getResources();
                map.addCircle(new CircleOptions()
                        .center(l)
                        .radius(location.getAccuracy())
                        .strokeWidth(res.getDimension(R.dimen.result_map_accuracy_line))
                        .strokeColor(ContextCompat.getColor(context, R.color.result_map_accuracy_line))
                        .fillColor(ContextCompat.getColor(context, R.color.result_map_accuracy_fill)));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(l)      // Sets the center of the map to the locatiom
                        .zoom(14)       // Sets the zoom
                        .build();       // Creates a CameraPosition from the builder
                map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                map.setOnMapLoadedCallback(() -> mapView.setVisibility(View.VISIBLE));
            }
        }
    }

    private class AddressLoader implements AsyncLoader.AsyncHandler<Location,TextView> {

        @Override
        public AsyncLoader.AsyncData<TextView> backgroundLoad(Location location)
        {
            if (location == null) return null;

            List<Address> addresses = null;
            Geocoder gc = new Geocoder(getContext(), Locale.getDefault());
            try {
                addresses = gc.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (Misc.DEBUG) Log.d(TAG, "Address: " + addresses);
            } catch (IOException ignored) {}

            return new AddressText(addresses);
        }

        @Override public boolean cancel() { return false; }

        @Override public void shutdown() {}
    }

    private static class AddressText implements AsyncLoader.AsyncData<TextView> {

        private final List<Address> addresses;

        private AddressText(List<Address> addresses) {
            this.addresses = addresses;
        }

        @Override public boolean isCachable() { return true; }

        @Override public int sizeOf() { return 1; }

        @SuppressLint("SetTextI18n")
        @Override
        public void display(TextView view) {
            if (addresses == null || addresses.isEmpty()) return;
            Address address = addresses.get(0);
            String postalCode = address.getPostalCode();
            String locality = address.getLocality();
            String countryCode = address.getCountryCode();
            view.setText(
                    (postalCode != null ? postalCode : "") +
                    (locality != null ? " " + locality : "") +
                    ((postalCode != null || locality != null) && countryCode != null ? ", " : "") +
                    (countryCode != null ? countryCode : ""));
        }
    }
}
