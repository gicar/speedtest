package it.agcom.misurainternet.environment;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyDisplayInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.appcompat.RetainedInstance;
import net.gicar.util.services.CrashReporting;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.MutableLiveData;

/** Monitors the network status changes */
public class NetworkViewModel extends RetainedInstance {

    /** Log tag */
    private static final String TAG = "NetworkViewModel";

    /** Delay for updating the network info to avoid unnecessary load */
    private static final int UPDATE_DELAY = 200;

    /** The UI thread message handler */
    private final Handler handler = new Handler(Looper.getMainLooper());

    /** The connectivity manager */
    private final ConnectivityManager connectivityManager;

    /** The WiFi manager */
    private final WifiManager wifiManager;

    /** The Telephony manager */
    private final TelephonyManager telephonyManager;

    /** The last display info, if available */
    private TelephonyDisplayInfo telephonyDisplayInfo;

    /** If true the callback has been registered */
    private boolean networkCallbackRegistered, telephonyCallbackRegistered;

    /** The network monitoring callback */
    private final ConnectivityManager.NetworkCallback callback = new ConnectivityManager.NetworkCallback() {
        @Override
        public void onAvailable(@NonNull Network network) {
            if (Misc.DEBUG) Log.d(TAG, "onAvailable() called with: network = [" + network + "]");
            handler.postDelayed(updateNetworkInfo, UPDATE_DELAY);
        }

        @Override
        public void onLosing(@NonNull Network network, int maxMsToLive) {
            if (Misc.DEBUG)
                Log.d(TAG, "onLosing() called with: network = [" + network + "], maxMsToLive = [" + maxMsToLive + "]");
            handler.postDelayed(updateNetworkInfo, UPDATE_DELAY);
        }

        @Override
        public void onUnavailable() {
            if (Misc.DEBUG) Log.d(TAG, "onUnavailable() called");
            handler.postDelayed(updateNetworkInfo, UPDATE_DELAY);
        }

        @Override
        public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
            if (Misc.DEBUG)
                Log.d(TAG, "onCapabilitiesChanged() called with: network = [" + network + "], networkCapabilities = [" + networkCapabilities + "]");
            handler.postDelayed(updateNetworkInfo, UPDATE_DELAY);
        }

        @Override
        public void onLinkPropertiesChanged(@NonNull Network network, @NonNull LinkProperties linkProperties) {
            if (Misc.DEBUG)
                Log.d(TAG, "onLinkPropertiesChanged() called with: network = [" + network + "], linkProperties = [" + linkProperties + "]");
            handler.postDelayed(updateNetworkInfo, UPDATE_DELAY);
        }

        @Override
        public void onBlockedStatusChanged(@NonNull Network network, boolean blocked) {
            if (Misc.DEBUG)
                Log.d(TAG, "onBlockedStatusChanged() called with: network = [" + network + "], blocked = [" + blocked + "]");
            handler.postDelayed(updateNetworkInfo, UPDATE_DELAY);
        }

        @Override
        public void onLost(@NonNull Network network) {
            if (Misc.DEBUG) Log.d(TAG, "onLost() called with: network = [" + network + "]");
            handler.postDelayed(updateNetworkInfo, UPDATE_DELAY);
        }
    };

    private final PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public synchronized void onDisplayInfoChanged(@NonNull TelephonyDisplayInfo telephonyDisplayInfo) {
            if (Misc.DEBUG) Log.d(TAG, "onDisplayInfoChanged() called with: telephonyDisplayInfo = [" + telephonyDisplayInfo + "]");
            NetworkViewModel.this.telephonyDisplayInfo = telephonyDisplayInfo;
            handler.postDelayed(updateNetworkInfo, UPDATE_DELAY);
        }
    };

    private final Runnable updateNetworkInfo = new Runnable() {
        @Override
        public synchronized void run() {
            if (Misc.DEBUG) Log.d(TAG, "Updating network info");
            handler.removeCallbacks(updateNetworkInfo);

            // collect info about the network
            NetworkInformation n = new NetworkInformation(getApplication(), telephonyDisplayInfo);
            networkInformation.setValue(n);
            networkStatus.setValue(new NetworkStatus(n));
        }
    };

    /** The current network information */
    public MutableLiveData<NetworkInformation> networkInformation = new MutableLiveData<>();

    /** The current network status */
    public MutableLiveData<NetworkStatus> networkStatus = new MutableLiveData<>();

    public NetworkViewModel(@NonNull Application application) {
        super(application);

        // get the managers
        connectivityManager = (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiManager = (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
        telephonyManager = (TelephonyManager) application.getSystemService(Context.TELEPHONY_SERVICE);

        // fetch the active network, will be immediately updated by the callbacks
        updateNetworkInfo.run();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // try to register callbacks onResume as some on them depend on permissions that could change
        // with permission dialog
        registerNetworkCallback();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // if we are not going to be restarted unregister the callbacks
        if (!requireActivity().isChangingConfigurations()) unregisterNetworkCallback();
    }

    /** Register the network callback */
    private void registerNetworkCallback()
    {
        if (!networkCallbackRegistered) {
            if (Misc.DEBUG) Log.d(TAG, "Registering network callback");

            // create the request we are interest into: networks with internet connectivity
            NetworkRequest request = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .build();
            connectivityManager.registerNetworkCallback(request, callback);
            networkCallbackRegistered = true;
        }

        if (!telephonyCallbackRegistered) {
            if (Misc.DEBUG) Log.d(TAG, "Registering telephony callback");

            // register to network display info if available
            try {
                if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_DISPLAY_INFO_CHANGED);
                    }
                    telephonyCallbackRegistered = true;
                }
            }
            catch (Exception e) {
                // be prepared for something going wrong
                CrashReporting.logException(e);
            }
        }
    }

    /** Unregister the network callback */
    private void unregisterNetworkCallback()
    {
        if (networkCallbackRegistered) {
            if (Misc.DEBUG) Log.d(TAG, "Unregistering network callback");
            connectivityManager.unregisterNetworkCallback(callback);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
                telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
            networkCallbackRegistered = false;
        }

        if (telephonyCallbackRegistered) {
            if (Misc.DEBUG) Log.d(TAG, "Unregistering telephony callback");
            try {
                if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_DISPLAY_INFO_CHANGED);
                    }
                }
            }
            catch (Exception e) {
                // be prepared for something going wrong
                CrashReporting.logException(e);
            }
            telephonyCallbackRegistered = false;
        }
    }

    /**
     * Verifies if the Internet connection is currently available
     * @return <code>true</code> if the Internet connection is available
     */
    public boolean hasNetwork()
    {
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
