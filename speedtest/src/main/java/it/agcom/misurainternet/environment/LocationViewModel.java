package it.agcom.misurainternet.environment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Application;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.CancellationTokenSource;

import net.gicar.util.Misc;
import net.gicar.util.appcompat.RetainedInstance;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.MutableLiveData;

/** Monitors the location status changes */
public class LocationViewModel extends RetainedInstance {

    /** Log tag */
    private static final String TAG = "LocationViewModel";

    /** Task cancellation source */
    private final CancellationTokenSource cancellationSource = new CancellationTokenSource();

    /** Location service  */
    private final FusedLocationProviderClient fusedLocationClient;

    /** Cancellation token of ongoing location request, if any */
    private CancellationToken cancellationToken;

    /** Last known location */
    public MutableLiveData<Location> location = new MutableLiveData<>();

    public LocationViewModel(@NonNull Application application) {
        super(application);

        // location provider
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(application);
    }

    private boolean locationAllowed()
    {
        return ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("MissingPermission")
    public void updateLocation()
    {
        if (locationAllowed()) {

            // start a new location request if it is not already ongoing
            if (cancellationToken == null) {
                if (Misc.DEBUG) Log.d(TAG, "Requesting a location update");
                cancellationToken = cancellationSource.getToken();
                fusedLocationClient.getCurrentLocation(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY, cancellationToken)
                        .addOnSuccessListener(l -> {
                            if (Misc.DEBUG) Log.d(TAG, "Got location: " + l);
                            location.setValue(l);
                        })
                        .addOnFailureListener(e -> {
                            if (Misc.DEBUG) Log.w(TAG, "Location request failed", e);
                        })
                        .addOnCompleteListener(task -> cancellationToken = null);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateLocation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!requireActivity().isChangingConfigurations() && cancellationToken != null) {
            cancellationSource.cancel();
            cancellationToken = null;
        }
    }
}
