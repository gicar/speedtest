package it.agcom.misurainternet.environment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyDisplayInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.services.CrashReporting;

import java.lang.reflect.Method;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import static android.telephony.TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NONE;

public class NetworkInformation {

    /** Log tag */
    private static final String TAG = "NetworkInformation";

    /** If true we have a network up and running */
    private final boolean hasNetworkConnectivity;

    /** The active network or null if none */
    private Network activeNetwork;

    /** The current network capabilities */
    private NetworkCapabilities networkCapabilities;

    /** The current link properties */
    private LinkProperties linkProperties;

    /** WiFi info for WiFi connections */
    private WifiInfo wifiInfo;

    /** Telephony display info if available */
    private TelephonyDisplayInfo telephonyDisplayInfo;

    /** The cellular network type */
    private int cellularNetworkType;

    /** MCC+MNC of the network operator */
    private String cellularOperatorMccMnc;

    /** Network operator name */
    private String cellularOperatorName;

    public NetworkInformation(Application application, TelephonyDisplayInfo telephonyDisplayInfo)
    {
        // get the managers
        ConnectivityManager connectivityManager = (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
        WifiManager wifiManager = (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
        TelephonyManager telephonyManager = (TelephonyManager) application.getSystemService(Context.TELEPHONY_SERVICE);
        SubscriptionManager subscriptionManager = (SubscriptionManager) application.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);

        activeNetwork = connectivityManager.getActiveNetwork();
        if (Misc.DEBUG) Log.d(TAG, "Updated active network: " + activeNetwork);

        // this has been deprecated but is still the fastest way to check for a real connection
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetworkInfo.isConnectedOrConnecting();

        // give up if no network available
        hasNetworkConnectivity = activeNetwork != null && isConnected;
        if (Misc.DEBUG) Log.d(TAG, "Has network connectivity: " + hasNetworkConnectivity);

        // fetch capabilities and link properties
        if (activeNetwork != null) {
            networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
            linkProperties = connectivityManager.getLinkProperties(activeNetwork);
            if (Misc.DEBUG) {
                Log.d(TAG, "Network capabilities: " + networkCapabilities);
                Log.d(TAG, "Link properties: " + linkProperties);
            }

            // get info about wifi connection
            wifiInfo = wifiManager.getConnectionInfo();
            if (Misc.DEBUG) Log.d(TAG, "Wifi info: " + wifiInfo);

            // get info about cellular connection
            try {
                if (ActivityCompat.checkSelfPermission(application, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

                    int dataSubId = -1;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        cellularNetworkType = telephonyManager.getDataNetworkType();
                        dataSubId = SubscriptionManager.getDefaultDataSubscriptionId();
                    }
                    else {
                        cellularNetworkType = telephonyManager.getNetworkType();
                        dataSubId = getDataSimSubscriptionIdBeforeN(application);
                    }

                    if (dataSubId != -1) {
                        SubscriptionInfo si = subscriptionManager.getActiveSubscriptionInfo(dataSubId);
                        if (si != null) {
                            // format keep the same with android.telephony.TelephonyManager#getSimOperator
                            // MCC + MNC format
                            cellularOperatorMccMnc = String.valueOf(si.getMcc()) + si.getMnc();
                            cellularOperatorName = si.getCarrierName().toString();
                        }
                    }
                    if (cellularOperatorMccMnc == null) telephonyManager.getSimOperator();
                    if (cellularOperatorName == null) telephonyManager.getNetworkOperatorName();
                }
            }
            catch (Exception e) {
                // be prepared for something going wrong
                CrashReporting.logException(e);
            }
        }

        // save the display info if available
        this.telephonyDisplayInfo = telephonyDisplayInfo;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    private static int getDataSimSubscriptionIdBeforeN(Context context)
    {
        int dataSubId = -1;
        try {
            //noinspection JavaReflectionMemberAccess
            Method getDefaultDataSubId = SubscriptionManager.class.getDeclaredMethod("getDefaultDataSubId");
            //noinspection ConstantConditions
            if (getDefaultDataSubId != null) {
                getDefaultDataSubId.setAccessible(true);
                //noinspection ConstantConditions
                dataSubId = (int) getDefaultDataSubId.invoke(null);
            }
        } catch (Exception e) {
            CrashReporting.logException(e);
        }
        return dataSubId;
    }

    public boolean hasNetworkConnectivity() {
        return hasNetworkConnectivity;
    }

    public boolean hasWifi() {
        return networkCapabilities != null && networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
    }

    public boolean hasCellular() {
        return networkCapabilities != null && networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR);
    }

    @SuppressLint("NewApi")
    public int getCellularNetworkType() {
        return telephonyDisplayInfo != null ? telephonyDisplayInfo.getNetworkType() : cellularNetworkType;
    }

    @SuppressLint("NewApi")
    public int getCellularOverrideNetworkType() {
        return telephonyDisplayInfo != null ? telephonyDisplayInfo.getOverrideNetworkType() : OVERRIDE_NETWORK_TYPE_NONE;
    }

    public String getCellularOperatorMccMnc() {
        return cellularOperatorMccMnc;
    }

    public String getCellularOperatorName() {
        return cellularOperatorName;
    }

    public String getWifiSsid()
    {
        return wifiInfo.getSSID();
    }

    public int getWifiFrequency()
    {
        return wifiInfo.getFrequency();
    }

    public int getWifiStandard()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) return wifiInfo.getWifiStandard();
        else return 0;
    }
}
