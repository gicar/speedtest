package it.agcom.misurainternet.environment;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyDisplayInfo;
import android.telephony.TelephonyManager;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class NetworkStatus {

    public enum ConnectionType {
        ABSENT, WIFI, CELLULAR, UNKNOWN
    }

    public enum CellularNetworkTechnology {
        UNKNOWN(""),
        TECH_CDMA("CDMA"),
        TECH_TD_SCDMA("TD-SCDMA"),
        TECH_2G("2G"),
        TECH_3G("3G"),
        TECH_4G("4G"),
        TECH_5G("5G");

        public final String name;

        CellularNetworkTechnology(String name) {
            this.name = name;
        }
    }

    public enum CellularNetworkType {

        UNKNOWN("", CellularNetworkTechnology.UNKNOWN),

        CDMA("CDMA", CellularNetworkTechnology.TECH_CDMA),
        EVDO_0("EVDO rev0", CellularNetworkTechnology.TECH_CDMA),
        EVDO_A("EVDO revA", CellularNetworkTechnology.TECH_CDMA),
        EVDO_B("EVDO revB", CellularNetworkTechnology.TECH_CDMA),
        ONExRTT("EVDO rev0", CellularNetworkTechnology.TECH_CDMA),

        TD_SCDMA("TD-SCDMA", CellularNetworkTechnology.TECH_TD_SCDMA),

        GPRS("GPRS", CellularNetworkTechnology.TECH_2G),
        EDGE("EDGE", CellularNetworkTechnology.TECH_2G),
        GSM("GSM", CellularNetworkTechnology.TECH_2G),

        UMTS("UMTS", CellularNetworkTechnology.TECH_3G),
        HSDPA("HSDPA", CellularNetworkTechnology.TECH_3G),
        HSUPA("HSUPA", CellularNetworkTechnology.TECH_3G),
        HSPA("HSPA", CellularNetworkTechnology.TECH_3G),
        HSPAP("HSPAP", CellularNetworkTechnology.TECH_3G),

        LTE("LTE", CellularNetworkTechnology.TECH_4G),
        IWLAN("IWLAN", CellularNetworkTechnology.TECH_4G),
        LTE_CA("LTE-CA", CellularNetworkTechnology.TECH_4G),
        LTE_ADVANCED_PRO("LTE+", CellularNetworkTechnology.TECH_4G),

        NR_NSA("NSA", CellularNetworkTechnology.TECH_5G),
        NR("SA", CellularNetworkTechnology.TECH_5G),
        NR_ADVANCED("NR+", CellularNetworkTechnology.TECH_5G);

        public final String name;

        public final CellularNetworkTechnology technology;

        CellularNetworkType(String name, CellularNetworkTechnology technology) {
            this.name = name;
            this.technology = technology;
        }

        public static CellularNetworkType mapTelephonyType(int networkType, int overrideNetworkType) {

            switch (overrideNetworkType) {
                case TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_LTE_ADVANCED_PRO:   return LTE_ADVANCED_PRO;
                case TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_LTE_CA:             return LTE_CA;
                case TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_ADVANCED:        return NR_ADVANCED;
                case TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_NSA:             return NR_NSA;
                case TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_NSA_MMWAVE:      return NR_ADVANCED;
            }

            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:    return GPRS;
                case TelephonyManager.NETWORK_TYPE_EDGE:    return EDGE;
                case TelephonyManager.NETWORK_TYPE_GSM:     return GSM;

                case TelephonyManager.NETWORK_TYPE_CDMA:    return CDMA;
                case TelephonyManager.NETWORK_TYPE_EVDO_0:  return EVDO_0;
                case TelephonyManager.NETWORK_TYPE_EVDO_A:  return EVDO_A;
                case TelephonyManager.NETWORK_TYPE_EVDO_B:  return EVDO_B;
                case TelephonyManager.NETWORK_TYPE_1xRTT:   return ONExRTT;

                case TelephonyManager.NETWORK_TYPE_UMTS:    return UMTS;
                case TelephonyManager.NETWORK_TYPE_HSDPA:   return HSDPA;
                case TelephonyManager.NETWORK_TYPE_HSUPA:   return HSUPA;
                case TelephonyManager.NETWORK_TYPE_HSPA:    return HSPA;
                case TelephonyManager.NETWORK_TYPE_HSPAP:   return HSPAP;

                case TelephonyManager.NETWORK_TYPE_LTE:     return LTE;
                case TelephonyManager.NETWORK_TYPE_IWLAN:   return IWLAN;

                case TelephonyManager.NETWORK_TYPE_TD_SCDMA:return TD_SCDMA;

                case TelephonyManager.NETWORK_TYPE_NR:      return NR;
            }

            return UNKNOWN;
        }
    }

    public enum WifiFrequencyBand {

        BAND_UNKNOWN(""),
        BAND_900_MHZ("900 MHz"),
        BAND_2_4_GHZ("2.4 GHz"),
        BAND_3_6_GHZ("3.6 GHz"),
        BAND_5_GHZ("5 GHz"),
        BAND_6_GHZ("6 GHz"),
        BAND_60_GHZ("60 GHz");

        public final String name;

        WifiFrequencyBand(String name) {
            this.name = name;
        }

        public static WifiFrequencyBand mapFrequency(int frequency) {
            if (frequency >= 915 && frequency <= 928) return BAND_900_MHZ;
            if (frequency >= 2401 && frequency <= 2495) return BAND_2_4_GHZ;
            if (frequency >= 3655 && frequency <= 3695) return BAND_3_6_GHZ;
            if (frequency >= 5030 && frequency <= 5895) return BAND_5_GHZ;
            if (frequency >= 5945 && frequency <= 6425) return BAND_6_GHZ;
            if (frequency >= 57240 && frequency <= 70200) return BAND_60_GHZ;
            return BAND_UNKNOWN;
        }
    }

    public enum WifiStandard {

        STANDARD_UNKNOWN(""),
        STANDARD_LEGACY("802.11a/b/g"),
        STANDARD_11N("802.11n"),
        STANDARD_11AC("802.11ac"),
        STANDARD_11AX("802.11ax"),
        STANDARD_11AD("802.11ad");

        public final String name;

        WifiStandard(String name) {
            this.name = name;
        }

        public static WifiStandard mapStandard(int standard) {
            switch (standard) {
                case ScanResult.WIFI_STANDARD_11AC:     return STANDARD_11AC;
                case ScanResult.WIFI_STANDARD_11AD:     return STANDARD_11AD;
                case ScanResult.WIFI_STANDARD_11AX:     return STANDARD_11AX;
                case ScanResult.WIFI_STANDARD_11N:      return STANDARD_11N;
                case ScanResult.WIFI_STANDARD_LEGACY:   return STANDARD_LEGACY;
            }
            return STANDARD_UNKNOWN;
        }
    }

    /** Type of the connection */
    public final @Nonnull ConnectionType connectionType;

    /** For cellular connections, the type of the network */
    public final @Nullable CellularNetworkType cellularNetworkType;

    /** For cellular connections, the name of the network */
    public final @Nullable String cellularOperatorName;

    /** For cellular connections, the MCC and MNC of the network */
    public final @Nullable String cellularMccMnc;

    /** For wifi connections, the SSID */
    public final @Nullable String wifiSsid;

    /** For wifi connections, the frequency band */
    public final @Nullable WifiFrequencyBand wifiBand;

    /** For wifi connections, the wifi standard */
    public final @Nullable WifiStandard wifiStandard;

    NetworkStatus(NetworkInformation n)
    {
        CellularNetworkType cellularNetworkType = null;
        String cellularOperatorName = null;
        String cellularMccMnc = null;
        String wifiSsid = null;
        WifiFrequencyBand wifiBand = null;
        WifiStandard wifiStandard = null;

        if (n.hasNetworkConnectivity()) {

            // give precedence to wifi over cellular
            if (n.hasWifi()) {
                connectionType = ConnectionType.WIFI;
                wifiSsid = nullOnEmpty(n.getWifiSsid());
                if (WifiManager.UNKNOWN_SSID.equals(wifiSsid)) wifiSsid = null;
                if (wifiSsid != null) {
                    if (wifiSsid.startsWith("\"") && wifiSsid.endsWith("\"")) wifiSsid = wifiSsid.substring(1, wifiSsid.length() - 1);
                    else wifiSsid = "0x" + wifiSsid;
                }
                wifiBand = WifiFrequencyBand.mapFrequency(n.getWifiFrequency());
                wifiStandard = WifiStandard.mapStandard(n.getWifiStandard());
            }
            else if (n.hasCellular()) {
                connectionType = ConnectionType.CELLULAR;
                cellularNetworkType = CellularNetworkType.mapTelephonyType(n.getCellularNetworkType(), n.getCellularOverrideNetworkType());
                cellularOperatorName = nullOnEmpty(n.getCellularOperatorName());
                cellularMccMnc = nullOnEmpty(n.getCellularOperatorMccMnc());
            }
            else connectionType = ConnectionType.UNKNOWN;
        }
        else connectionType = ConnectionType.ABSENT;

        this.cellularNetworkType = cellularNetworkType;
        this.cellularOperatorName = cellularOperatorName;
        this.cellularMccMnc = cellularMccMnc;
        this.wifiSsid = wifiSsid;
        this.wifiBand = wifiBand;
        this.wifiStandard = wifiStandard;
    }

    static String nullOnEmpty(String s) {
        return s != null && s.isEmpty() ? null : s;
    }

    public String getCellularTechnology()
    {
        return cellularNetworkType != null && cellularNetworkType != NetworkStatus.CellularNetworkType.UNKNOWN ?
                cellularNetworkType.technology.name + " (" + cellularNetworkType.name + ")" : null;
    }
}
