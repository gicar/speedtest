package it.agcom.misurainternet;

import android.location.Location;
import android.os.Build;

import androidx.annotation.NonNull;
import it.agcom.misurainternet.environment.NetworkStatus;
import it.agcom.misurainternet.speedtest.MeasurementProgress;
import it.agcom.misurainternet.speedtest.MeasurementServer;
import it.agcom.misurainternet.speedtest.MeasurementStatus;
import it.agcom.misurainternet.speedtest.SpeedTestViewModel;
import it.agcom.misurainternet.speedtest.ping.PingMeasurement;
import it.agcom.misurainternet.speedtest.server.ServerMeasurement;
import it.agcom.misurainternet.speedtest.transfer.SpeedMeasurement;

/** Holds all the info related to a measurement */
public class MisuraResult {

    Long startTime;

    Long endTime;

    MeasurementServer server;

    Float pingTimeAverage;

    Float pingTimeMin;

    Float pingTimeMax;

    Float pingJitter;

    Float downloadSpeed;

    Long downloadData;

    Float uploadSpeed;

    Long uploadData;

    NetworkStatus networkStatus;

    Location location;

    String deviceBrand;

    String deviceModel;

    String deviceBuild;

    Integer deviceSdk;

    public MisuraResult(Long startTime, Long endTime, SpeedTestViewModel speedTest,
                        NetworkStatus networkStatus, Location location)
    {
        this.startTime = startTime;
        this.endTime = endTime;

        MeasurementProgress<ServerMeasurement> statusServer = speedTest.statusServer.getValue();
        if (statusServer != null && statusServer.status == MeasurementStatus.FINISHED) {
            ServerMeasurement m = statusServer.value;
            server = m.measurementServer;
        }

        MeasurementProgress<PingMeasurement> statusPing = speedTest.statusPing.getValue();
        if (statusPing != null && statusPing.status == MeasurementStatus.FINISHED) {
            PingMeasurement m = statusPing.value;
            pingTimeAverage = m.timeAverage;
            pingTimeMin = m.timeMin;
            pingTimeMax = m.timeMax;
            pingJitter = m.jitter;
        }

        MeasurementProgress<SpeedMeasurement> statusDownload = speedTest.statusDownload.getValue();
        if (statusDownload != null && statusDownload.status == MeasurementStatus.FINISHED) {
            SpeedMeasurement m = statusDownload.value;
            downloadSpeed = m.speed;
            downloadData = m.data[m.numSamples - 1];
        }

        MeasurementProgress<SpeedMeasurement> statusUpload = speedTest.statusUpload.getValue();
        if (statusUpload != null && statusUpload.status == MeasurementStatus.FINISHED) {
            SpeedMeasurement m = statusUpload.value;
            uploadSpeed = m.speed;
            uploadData = m.data[m.numSamples - 1];
        }

        this.networkStatus = networkStatus;
        this.location = location;
        this.deviceBrand = Build.BRAND;
        this.deviceModel = Build.MODEL;
        this.deviceBuild = Build.DISPLAY;
        this.deviceSdk = Build.VERSION.SDK_INT;
    }

    @NonNull
    @Override
    public String toString() {
        return "MisuraResult{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", server=" + server +
                ", pingTimeAverage=" + pingTimeAverage +
                ", pingTimeMin=" + pingTimeMin +
                ", pingTimeMax=" + pingTimeMax +
                ", pingJitter=" + pingJitter +
                ", downloadSpeed=" + downloadSpeed +
                ", downloadData=" + downloadData +
                ", uploadSpeed=" + uploadSpeed +
                ", uploadData=" + uploadData +
                ", networkStatus=" + networkStatus +
                ", location=" + location +
                ", deviceBrand='" + deviceBrand + '\'' +
                ", deviceModel='" + deviceModel + '\'' +
                ", deviceBuild='" + deviceBuild + '\'' +
                ", deviceSdk=" + deviceSdk +
                '}';
    }
}
