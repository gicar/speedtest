package it.agcom.misurainternet;

import androidx.lifecycle.ViewModel;
import it.agcom.misurainternet.environment.NetworkStatus;

/** Retained data for the MisuraActivity */
public class MisuraViewModel extends ViewModel {

    /** Network status at test start */
    NetworkStatus networkStatusAtStart;

}
