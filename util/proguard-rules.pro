### Rules for library "gicci-library" ###

# Avoid messing with classes where we use reflection
-keepnames public class * extends androidx.appcompat.widget.PopupMenu
-keep public class * extends androidx.appcompat.widget.PopupMenu {*;}
-keepnames public class * extends androidx.appcompat.view.menu.MenuPopupHelper
-keep public class * extends androidx.appcompat.view.menu.MenuPopupHelper {*;}
-keepclassmembers class * extends androidx.appcompat.view.menu.MenuPopupHelper {
    public void setForceShowIcon(boolean);
}

# Not sure why, but each now and then this class is not found
-keep public class androidx.appcompat.app.AppCompatViewInflater {
    public protected *;
}

# Something is failing in AdMob mediation Proguard configuration; try to keep everything
# Old rules, not sure if they are still needed, but they will not harm
-dontwarn com.google.ads.**
-keep class com.google.ads.**
-keep class com.google.ads.mediation.** {*;}
-keep class com.google.ads.mediation.admob.** {*;}
-keep class com.google.ads.mediation.customevent.** {*;}
-keep class com.google.android.gms.ads.**

# Not clear why the Google Play library istructions ask to add this
-keep class com.android.vending.billing.**

# Requested by CrashLytics
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception
-keep class com.google.firebase.crashlytics.** { *; }
-dontwarn com.google.firebase.crashlytics.**

# INCREDIBLE: default Android Proguard configuration does not properly handle Serializable objects
-keepnames class * implements java.io.Serializable
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
