package net.gicar.util;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/** Utilities related to intents */
public class IntentUtils {

    /**
     * Execute a view intent to the specified URL
     * @param context The context to be used
     * @param scheme The URL
     * @param flags The intent flags or 0 if none
     * @return <code>true</code> if it was successfully executed, <code>false</code> on error
     */
    public static boolean viewIntent(Context context, String scheme, int flags)
    {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(scheme));
            if (flags != 0) intent.addFlags(flags);
            context.startActivity(intent);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Execute a view intent to the specified URL
     * @param context The context to be used
     * @param scheme The URL
     * @return <code>true</code> if it was successfully executed, <code>false</code> on error
     */
    public static boolean viewIntent(Context context, String scheme)
    {
        return viewIntent(context, scheme, 0);
    }

    /**
     * Execute a share text intent
     * @param context The context to be used
     * @param text The text to be shared
     * @return <code>true</code> if it was successfully executed, <code>false</code> on error
     */
    public static boolean shareTextIntent(Context context, String text)
    {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
            sendIntent.setType("text/plain");

            context.startActivity(Intent.createChooser(sendIntent, null));
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Perform a web search of the provided text
     * @param context The <code>Context</code> to be used for the operation
     * @param query The string to be searched
     * @return true if the operation has been successfully performed
     */
    public static boolean webSearch(Context context, String query)
    {
        // try web search intent first
        try {
            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, query); // query contains search string
            context.startActivity(intent);
            return true;
        }
        catch (Exception ignored) {}

        // revert to google web page if failed
        return viewIntent(context, "https://www.google.com/search?q=" + query);
    }
}
