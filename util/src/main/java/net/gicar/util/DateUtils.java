package net.gicar.util;

import android.annotation.SuppressLint;
import android.content.Context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/** Utilities related to date handling */
public class DateUtils {

    /**
     * Return a short date formatter which avoid the Android bug:
     * https://code.google.com/p/android/issues/detail?id=181201
     * @param context The <code>Context</code> to be used for the operation
     * @return DateFormat for DateFormat.SHORT
     */
    @SuppressLint("ObsoleteSdkInt")
    public static DateFormat getShortDateFormat(Context context)
    {
        if (android.os.Build.VERSION.SDK_INT < 18)
            return DateFormat.getTimeInstance(DateFormat.SHORT);

        Locale locale = Locale.getDefault();
        boolean use24Hour = android.text.format.DateFormat.is24HourFormat(context);
        final String skeleton = use24Hour ? "Hm" : "hm";
        final String pattern = android.text.format.DateFormat.getBestDateTimePattern(locale, skeleton);
        return new SimpleDateFormat(pattern, locale);
    }

    /**
     * Return a long date formatter which avoid the Android bug:
     * https://code.google.com/p/android/issues/detail?id=181201
     * @param context The <code>Context</code> to be used for the operation
     * @return DateFormat for DateFormat.LONG
     */
    @SuppressLint("ObsoleteSdkInt")
    public static DateFormat getLongDateFormat(Context context)
    {
        if (android.os.Build.VERSION.SDK_INT < 18)
            return DateFormat.getTimeInstance(DateFormat.LONG);

        Locale locale = Locale.getDefault();
        boolean use24Hour = android.text.format.DateFormat.is24HourFormat(context);
        final String skeleton = use24Hour ? "Hms" : "hms";
        final String pattern = android.text.format.DateFormat.getBestDateTimePattern(locale, skeleton);
        return new SimpleDateFormat(pattern, locale);
    }
}
