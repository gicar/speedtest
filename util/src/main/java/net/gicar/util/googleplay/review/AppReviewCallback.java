package net.gicar.util.googleplay.review;

/** Notifies an activity of in-app review events */
public interface AppReviewCallback {

    /**
     * Signals that a review request has been offered to the user
     * @param accepted true if it has been accepted, false if not, null if unknown
     */
    void appReviewRequest(Boolean accepted);

}
