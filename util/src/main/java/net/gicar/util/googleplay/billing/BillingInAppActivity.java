package net.gicar.util.googleplay.billing;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import net.gicar.util.Market;
import net.gicar.util.Misc;
import net.gicar.util.services.CrashReporting;

import java.util.List;

/**
 * This activity can be used to trigger in-app purchases from an URI.
 * The supported URI format is: "gicar://<app package>/in-app-purchase/item"
 * To enable it you need to specify the activity in the following way in the AndroidManifest.xml:
 *
 *		<activity android:name="net.gicar.util.googleplay.billing.BillingInAppActivity" android:label="@string/app_name" android:theme="@android:style/Theme.NoDisplay">
 *			<intent-filter>
 *				<action android:name="android.intent.action.VIEW" />
 *				<category android:name="android.intent.category.DEFAULT" />
 *				<category android:name="android.intent.category.BROWSABLE" />
 *				<data android:scheme="gicar" android:host="${applicationId}" android:pathPrefix="/in-app-purchase" />
 *			</intent-filter>	    
 *		</activity>			    
 */
@SuppressLint("Registered")
public class BillingInAppActivity extends Activity {

    private final static String TAG = "BillingClient.InApp";

    /** The URI scheme */
    private final static String URI_SCHEME = "gicar";

    /** The URI path prefix */
    private final static String URI_PATH_PREFIX = "in-app-purchase";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        try {
            // get the intent which launched us
            Intent intent = getIntent();
            Uri data = intent.getData();

            if (Misc.DEBUG) Log.i(TAG, "Billing intent received: " + intent);

            // get the no ads billing instance (if any)
            BillingNoAds billingNoAds = BillingNoAds.getInstance();
            if (billingNoAds == null) return;

            // avoid purchases if the app was not installed via Google Play
            if (!Market.canUseGooglePlay(this)) return;

            if (data != null && URI_SCHEME.equals(data.getScheme()) &&
                    getApplicationContext().getPackageName().equals(data.getHost())) {
                List<String> path = data.getPathSegments();
                if (path.size() == 2 && URI_PATH_PREFIX.equals(path.get(0))) {

                    // purchase the specified item
                    String item = path.get(1);
                    if (Misc.DEBUG) Log.i(TAG, "Starting purchase of item: " + item);
                    billingNoAds.startPurchaseFlow(this);
                } else Log.w(TAG, "Unrecognized path");
            } else Log.w(TAG, "Unrecognized scheme or host");
        }
        catch (Exception e) {
            CrashReporting.logException(e);
        }
        finally {
            // just quit (always required by Theme.NoDisplay)
            finish();
        }
    }
}
