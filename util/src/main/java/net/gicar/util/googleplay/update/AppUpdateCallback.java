package net.gicar.util.googleplay.update;

/** Notifies an activity of in-app update events */
public interface AppUpdateCallback {

    /**
     * Notifies a change in status of the in-app update handler.
     * @param appUpdateStatus The current status
     */
    void appUpdateStatus(AppUpdateStatus appUpdateStatus);
}
