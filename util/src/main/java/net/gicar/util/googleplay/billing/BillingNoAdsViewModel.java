package net.gicar.util.googleplay.billing;

import android.app.Activity;
import android.app.Application;

import java.util.Observable;
import java.util.Observer;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

/** ViewModel for no ads status */
public class BillingNoAdsViewModel extends AndroidViewModel implements Observer {

    /** The no ads billing handler */
    private final BillingNoAds billingNoAds;

    /** The current status */
    public final MutableLiveData<BillingNoAdsStatus> status = new MutableLiveData<>();

    public BillingNoAdsViewModel(@NonNull Application application) {
        super(application);

        // get billing instance and check if it present
        billingNoAds = BillingNoAds.getInstance();
        if (billingNoAds == null) {
            status.setValue(BillingNoAdsStatus.ERROR_UNAVAILABLE);
            return;
        }

        // add ourselves to the observers and start it if not yet done
        status.setValue(billingNoAds.getStatus());
        billingNoAds.addObserver(this);
        billingNoAds.start();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (billingNoAds != null) billingNoAds.deleteObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        status.postValue((BillingNoAdsStatus) arg);
    }

    /**
     * Returns the initialization status of no ads value.
     * @return true if no ads status has been received at least once from Google Play
     */
    public boolean isNoAdsInitialized() {
        return billingNoAds == null || billingNoAds.isNoAdsInitialized();
    }

    /**
     * Returns the current known status of the no ads license.
     * @return true if we own the no ads license
     */
    public boolean isNoAds() {
        return billingNoAds != null && billingNoAds.isNoAds();
    }

    /**
     * Starts the no ads purchase flow
     * @param activity The activity starting the purchase flow
     */
    public void startPurchaseFlow(Activity activity) {
        if (billingNoAds != null) billingNoAds.startPurchaseFlow(activity);
    }

    /**
     * Consumes the purchase for test purposes
     * @return true is we have a valid token to be consumed
     */
    public boolean consumeTestPurchase() {
        return billingNoAds != null && billingNoAds.consumeTestPurchase();
    }
}
