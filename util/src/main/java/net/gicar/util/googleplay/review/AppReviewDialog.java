package net.gicar.util.googleplay.review;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import net.gicar.util.Market;
import net.gicar.util.Misc;
import net.gicar.util.appcompat.XAppCompatDialogFragment;

import static net.gicar.util.googleplay.review.AppReviewHandler.LAYOUT_KEY;
import static net.gicar.util.googleplay.review.AppReviewHandler.NO_KEY;
import static net.gicar.util.googleplay.review.AppReviewHandler.OK_KEY;
import static net.gicar.util.googleplay.review.AppReviewHandler.THEME_KEY;

/** Enhanced App rating requester */
public class AppReviewDialog extends XAppCompatDialogFragment {

    /** The log tag */
    final private static String TAG = AppReviewHandler.class.getSimpleName();

    /** The name we will use for the dialog fragment name */
    final static String APP_REVIEW_DIALOG_FRAGMENT_NAME = "AppReviewDialog";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // set our style
        //noinspection ConstantConditions
        setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, getArguments().getInt(THEME_KEY));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // get the arguments
        Bundle arg = getArguments();

        // inflate the dialog view
        //noinspection ConstantConditions
        View view = inflater.inflate(arg.getInt(LAYOUT_KEY), container);

        if (Misc.DEBUG) Log.i(TAG, "Displaying the App rate request dialog");

        // on "ok" go to the market and dismiss forever this dialog
        view.findViewById(arg.getInt(OK_KEY)).setOnClickListener(v -> {
            if (Misc.DEBUG) Log.i(TAG, "Rating request accepted");
            rateRequest(true);
            Market.launchMarket(getContext());
        });

        // on "no" dismiss forever this dialog
        view.findViewById(arg.getInt(NO_KEY)).setOnClickListener(v -> {
            if (Misc.DEBUG) Log.i(TAG, "Rating request denied");
            rateRequest(false);
        });

        return view;
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog)
    {
        super.onCancel(dialog);

        // consider this a no
        rateRequest(false);
    }

    private void rateRequest(final boolean accepted)
    {
        dismissRequest(() -> {
            // call the handler and dismiss us
            //noinspection ConstantConditions
            ((AppReviewCallback)getActivity()).appReviewRequest(accepted);
        });
    }
}
