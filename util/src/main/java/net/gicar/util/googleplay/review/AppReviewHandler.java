package net.gicar.util.googleplay.review;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;

import java.util.Date;

import androidx.annotation.NonNull;
import net.gicar.util.Misc;
import net.gicar.util.appcompat.RetainedInstance;
import net.gicar.util.appcompat.XAppCompatActivity;

import static net.gicar.util.googleplay.review.AppReviewDialog.APP_REVIEW_DIALOG_FRAGMENT_NAME;

/** Retained fragment used to handle state for in-app reviews */
public class AppReviewHandler extends RetainedInstance {

    /** The log tag */
    final private static String TAG = AppReviewHandler.class.getSimpleName();

    /** The keys for the fields in the bundles */
    final static String LAUNCHES_KEY = "LAUNCHES", DAYS_KEY = "DAYS", EVENTS_KEY = "EVENTS",
            LAYOUT_KEY = "LAYOUT", OK_KEY = "OK", NO_KEY = "NO", THEME_KEY = "THEME";

    public AppReviewHandler(@NonNull Application application) {
        super(application);
    }

    /**
     * Returns the single instance of this fragment, creating it if necessary
     * @param activity The Activity performing the request
     * @param launchesUntilPrompt Number of application launches until the rating request shall be shown
     *                            Set to -1 to always show for testing
     * @param daysUntilPrompt Number of days until the rating request shall be shown
     * @param eventsUntilPrompt Number of user defined events until the rating request shall be shown
     * @param themeId ID of the no theme for the dialog
     * @param layoutId ID of the layout which shall be used
     * @param okId ID of the ok button in the layout
     * @param noId ID of the no button in the layout
     * @return The Fragment
     */
    public static AppReviewHandler get(XAppCompatActivity activity,
                                       int launchesUntilPrompt, int daysUntilPrompt, int eventsUntilPrompt,
                                       int themeId, int layoutId, int okId, int noId)
    {
        if (!(activity instanceof AppReviewCallback))
            throw new RuntimeException("Activity class shall implement AppReviewCallback");

        // get the instance
        Bundle args = new Bundle();
        args.putInt(LAUNCHES_KEY, launchesUntilPrompt);
        args.putInt(DAYS_KEY, daysUntilPrompt);
        args.putInt(EVENTS_KEY, eventsUntilPrompt);
        args.putInt(THEME_KEY, themeId);
        args.putInt(LAYOUT_KEY, layoutId);
        args.putInt(OK_KEY, okId);
        args.putInt(NO_KEY, noId);
        return getInstance(activity, AppReviewHandler.class, args);
    }

    /** The rating launch conditions */
    private int launchesUntilPrompt, daysUntilPrompt, eventsUntilPrompt;

    /** Settings of the noAds */
    private AppReviewSettings settings;

    /** The review manager; could be null if reviews are disabled */
    private ReviewManager reviewManager;

    /** The pending review info load task, if any */
    private Task<ReviewInfo> reviewInfoTask;

    /** The review info waiting to be launched */
    private ReviewInfo reviewInfo;

    /** If true we shall show a review request */
    private boolean reviewShowPending;

    @Override
    public void onCreate()
    {
        if (Misc.DEBUG) Log.d(TAG, "onCreate");
        super.onCreate();

        // recover the arguments
        Bundle args = requireArguments();
        launchesUntilPrompt = args.getInt(LAUNCHES_KEY);
        daysUntilPrompt = args.getInt(DAYS_KEY);
        eventsUntilPrompt = args.getInt(EVENTS_KEY);
    }

    @Override
    public void onActivityCreate() {
        if (Misc.DEBUG) Log.d(TAG, "onActivityCreate");
        super.onActivityCreate();

        // the context we'll be using
        Context context = requireActivity().getApplicationContext();

        // get our preferences
        settings = new AppReviewSettings();
        settings.onCreate(context);

        // exit if the user decided to not rate the app
        if (settings.dontShowAgain.getValue()) {
            if (Misc.DEBUG) Log.d(TAG, "App rater dialog disabled");
            return;
        }

        // creates instance of the manager
        reviewManager = ReviewManagerFactory.create(context);

        // increment the launch counter
        settings.launchCount.setValue(settings.launchCount.getValue() + 1);

        // initialize the date of first launch if not set
        if (settings.dateFirstLaunch.getValue() == 0)
            settings.dateFirstLaunch.setValue(System.currentTimeMillis());

        if (Misc.DEBUG)
            Log.i(TAG, "App first run: " + new Date(settings.dateFirstLaunch.getValue()) +
                    ", launch count: " + settings.launchCount.getValue() +
                    ", events count: " + settings.eventCount.getValue());
    }

    @Override
    public void onResume()
    {
        if (Misc.DEBUG) Log.d(TAG, "onResume");
        super.onResume();

        // check if we have a review request ready to be shown
        checkReviewStart();
        triggerReviewStart();
    }

    @Override
    public void onPause()
    {
        if (Misc.DEBUG) Log.d(TAG, "onResume");
        super.onPause();

        // notify the settings
        settings.onPause();
    }

    @Override
    public void onDestroy()
    {
        if (Misc.DEBUG) Log.d(TAG, "onDestroy");

        // notify the settings
        settings.onDestroy();

        super.onDestroy();
    }

    /**
     * Increase the event count and re-evaluate the need to show the review request
     * @param count The event count
     */
    public void event(int count)
    {
        // avoid to continue counting events if we will no longer show the request
        if (settings.dontShowAgain.getValue()) return;

        // update the event count and check if we need to show the review request
        settings.eventCount.setValue(settings.eventCount.getValue() + count);
        checkReviewStart();
    }

    /** Checks if the review flow shall be started */
    private void checkReviewStart()
    {
        // avoid to proceed if not allowed
        if (settings.dontShowAgain.getValue()) return;

        // avoid to proceed if a request is already ongoing or we already are ready to show it
        if (reviewInfoTask != null || reviewShowPending) return;

        // check the launch conditions
        if (settings.launchCount.getValue() < launchesUntilPrompt ||
                System.currentTimeMillis() - settings.dateFirstLaunch.getValue() < daysUntilPrompt * 24 * 60 * 60 * 1000 ||
            settings.eventCount.getValue() < eventsUntilPrompt) {
            if (Misc.DEBUG)
                Log.i(TAG, "It is not yet time to show the App review request");
            return;
        }

        // a review request must be shown
        Log.i(TAG, "A review request shall be shown to the user");

        // we could for some reason don't even have a ReviewManager
        if (reviewManager == null) {
            reviewShowPending = true;
            triggerReviewStart();
            return;
        }

        // start the task to retrieve the ReviewInfo
        reviewInfoTask = reviewManager.requestReviewFlow();
        reviewInfoTask.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                reviewInfo = task.getResult();
                if (Misc.DEBUG) Log.d(TAG, "Request completed successfully: " + reviewInfo);
            }
            else {
                if (Misc.DEBUG) Log.w(TAG, "Request failed with exception: " + task.getException());
            }

            // set that we are ready to show something
            reviewInfoTask = null;
            reviewShowPending = true;
            triggerReviewStart();
        });
    }

    private void triggerReviewStart()
    {
        // avoid to proceed if we have no review pending
        if (!reviewShowPending) return;

        // we need to delay as well if we arrived here without an activity
        Activity activity = getActivity();
        if (activity == null) return;

        // we will try this only once
        reviewShowPending = false;
        settings.dontShowAgain.setValue(true);

        // use Google Play if we have a ReviewInfo
        if (reviewInfo != null) {

            // start the review flow
            Task<Void> flow = reviewManager.launchReviewFlow(activity, reviewInfo);
            flow.addOnCompleteListener(task -> {
                // The flow has finished. The API does not indicate whether the user
                // reviewed or not, or even whether the review dialog was shown. Thus, no
                // matter the result, we continue our app flow.
                if (Misc.DEBUG) Log.d(TAG, "Review task completed");

                // if we do not have an activity now, just avoid the callback: it is better than crash
                Activity activity1 = getActivity();
                if (activity1 instanceof AppReviewCallback)
                    ((AppReviewCallback)activity).appReviewRequest(null);
            });
        }
        else {
            // start the review request dialog, if available
            Bundle args = requireArguments();
            if (args.getInt(LAYOUT_KEY) != 0) {
                new AppReviewDialog().showRequest((XAppCompatActivity) activity, APP_REVIEW_DIALOG_FRAGMENT_NAME, args);
            }
        }
    }
}