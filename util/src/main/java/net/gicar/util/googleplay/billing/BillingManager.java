/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.gicar.util.googleplay.billing;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.FeatureType;
import com.android.billingclient.api.BillingClient.SkuType;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import net.gicar.util.Misc;

/**
 * Handles all the interactions with Play Store (via Billing library), maintains connection to
 * it through BillingClient and caches temporary states/data if needed
 */
public class BillingManager implements PurchasesUpdatedListener {

    private static final String TAG = "BillingClient.Manager";

    /** A reference to BillingClient **/
    private BillingClient billingClient;

    /** True if billing service is connected now. */
    private boolean isServiceConnected;

    /** If true the subscriptions are enabled */
    private final boolean subsEnabled;

    /** Listener for billing events */
    private final BillingUpdatesListener billingUpdatesListener;

    /** The full list of known purchases, with updates appended at the end */
    private final List<Purchase> purchases = new ArrayList<>();

    /** List of pending actions to be executed after connection. Not null if connection is pending. */
    private List<Runnable> executeOnConnect;

    /** Purchase tokens to be consumed */
    private final Set<String> tokensToBeConsumed = new HashSet<>();

    /* base64PublicKey should be YOUR APPLICATION'S PUBLIC KEY
     * (that you got from the Google Play developer console). This is not your
     * developer public key, it's the *app-specific* public key.
     *
     * Instead of just storing the entire literal string here embedded in the
     * program,  construct the key at runtime from pieces or
     * use bit manipulation (for example, XOR with some other string) to hide
     * the actual key.  The key itself is not secret information, but we don't
     * want to make it easy for an attacker to replace the public key with one
     * of their own and then fake messages from the server.
     */
    private final String base64PublicKey;

    /**
     * Listener to the updates that happen when purchases list was updated or consumption of the
     * item was finished
     */
    public interface BillingUpdatesListener {

        /**
         * Called to report the result of the connection to the billing service. The billing
         * functionality should be considered (temporarily) unavailable. Pending commands will not
         * be executed.
         * @param responseCode The result code. In case of failure the billing functionality should be
         *                     considered (temporarily) unavailable. Pending commands will not
         *                     be executed.
         */
        void onBillingConnectResult(@BillingClient.BillingResponseCode int responseCode);

        /**
         * The billing service has been disconnected and will be reconnected at next request.
         * No updates will be received while disconnected. Try to reconnect if interested.
         */
        void onBillingDisconnected();

        /**
         * Called to report a change in the list of purchases. New items are added at the end
         * and could be overwriting previous ones.
         * @param responseCode The result code of the action triggering the update
         * @param purchases The current list of known purchases, independently of the reported result
         */
        void onBillingPurchasesUpdate(@BillingClient.BillingResponseCode int responseCode, List<Purchase> purchases);

        /**
         * Called to report a the end of sync of purchases' history from the server.
         * @param responseCode The result code of the action triggering the update
         * @param purchases The history of purchases or null in case of error
         */
        void onBillingPurchasesHistory(@BillingClient.BillingResponseCode int responseCode, List<PurchaseHistoryRecord> purchases);

        /**
         * Reports the result of the SKU query
         * @param skuType The type fo SKU queried
         * @param skuList The list of queried SKUs
         * @param responseCode The response code
         * @param skuDetailsList The details of the SKUs
         */
        void onSkuDetailsResponse(String skuType, List<String> skuList,
                                  @BillingClient.BillingResponseCode int responseCode, List<SkuDetails> skuDetailsList);

        /**
         * Called when a consume request has been recorded (or failed)
         * @param purchaseToken The token of the purchase to be consumed
         * @param responseCode The result of the action
         */
        void onConsumeFinished(String purchaseToken, @BillingClient.BillingResponseCode int responseCode);
    }

    /**
     * Creates a new BillingManager
     * @param application The Application owning it
     * @param base64PublicKey The application billing key in base64 encoding
     * @param subsEnabled If true the subscriptions support is enabled
     * @param updatesListener The listener called for callbacks
     */
    public BillingManager(Application application, String base64PublicKey, boolean subsEnabled,
                          BillingUpdatesListener updatesListener) {
        if (Misc.DEBUG) Log.d(TAG, "Creating Billing client.");
        this.base64PublicKey = base64PublicKey;
        this.subsEnabled = subsEnabled;
        this.billingUpdatesListener = updatesListener;
        this.billingClient = BillingClient.newBuilder(application).setListener(this).enablePendingPurchases().build();
    }

    /** Clear the resources. Call this when this object is no longer needed. */
    public void destroy() {
        if (Misc.DEBUG) Log.d(TAG, "Destroying the manager.");
        if (billingClient != null && billingClient.isReady()) {
            billingClient.endConnection();
            billingClient = null;
            isServiceConnected = false;
            executeOnConnect = null;
        }
    }

    /**
     * Internal logging method for billing results
     * @param billingResult The BillingResult to be logged
     * @return Its String representation
     */
    private static String logResult(BillingResult billingResult) {
        return billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK ? "OK" :
                billingResult.getResponseCode() + " (" + billingResult.getDebugMessage() + ")";
    }

    @Override
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, List<Purchase> purchases)
    {
        if (Misc.DEBUG) Log.d(TAG, "onPurchasesUpdated() result: " + logResult(billingResult));
        onPurchasesUpdated(billingResult.getResponseCode(), purchases);
    }

    private void onPurchasesUpdated(@BillingClient.BillingResponseCode int responseCode, List<Purchase> purchases)
    {
        if (Misc.DEBUG) Log.d(TAG, "onPurchasesUpdated() processing purchases");
        if (responseCode == BillingClient.BillingResponseCode.OK) {

            // loop for all the received purchases
            if (purchases != null) for (Purchase purchase : purchases) {

                // verify if it is valid
                if (!verifyPurchase(purchase)) {
                    if (Misc.DEBUG) Log.w(TAG, "Received invalid purchase: " + purchase);
                    continue;
                }

                // check if the purchase is completed
                if (purchase.getPurchaseState() != Purchase.PurchaseState.PURCHASED) {
                    if (Misc.DEBUG) Log.d(TAG, "Received pending purchase: " + purchase);
                    continue;
                }

                // purchase is OK
                if (Misc.DEBUG) Log.d(TAG, "Received purchase: " + purchase);
                this.purchases.add(purchase);

                // acknowledge the purchase if it hasn't already been acknowledged.
                if (!purchase.isAcknowledged()) {
                    if (Misc.DEBUG) Log.d(TAG, "Acknowledging purchase: " + purchase);
                    AcknowledgePurchaseParams acknowledgePurchaseParams =
                            AcknowledgePurchaseParams.newBuilder()
                                    .setPurchaseToken(purchase.getPurchaseToken())
                                    .build();
                    billingClient.acknowledgePurchase(acknowledgePurchaseParams, billingResult -> {
                        if (Misc.DEBUG) {
                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK)
                                Log.d(TAG, "Purchase successfully acknowledged");
                            else
                                Log.i(TAG, "Unable to acknowledge purchase with result: " + logResult(billingResult));
                        }
                    });
                }
            }
        } else if (responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
            if (Misc.DEBUG) Log.i(TAG, "onPurchasesUpdated() - user cancelled the purchase flow - skipping");
        } else {
            if (Misc.DEBUG) Log.w(TAG, "onPurchasesUpdated() got unknown resultCode: " + responseCode);
        }

        // notify current purchases
        billingUpdatesListener.onBillingPurchasesUpdate(responseCode, this.purchases);
    }

    /** Query purchases and deliver the result through the listener */
    public void queryPurchases() {
        connectAndExecute(() -> {

            List<Purchase> purchases = new ArrayList<>();

            if (Misc.DEBUG) Log.d(TAG, "Querying inventory.");
            billingClient.queryPurchasesAsync(SkuType.INAPP, (purchasesResult, purchasesList) -> {

                if (Misc.DEBUG) {
                    Log.i(TAG, "Query inventory result code: "
                            + purchasesResult.getResponseCode() + " - " + purchasesResult.getDebugMessage());
                    Log.i(TAG, "Query inventory list: " + purchasesList);
                }

                // something went wrong, report error and current list
                if (purchasesResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
                    if (Misc.DEBUG) Log.w(TAG, "queryPurchasesAsync() got an error result: "
                            + purchasesResult.getResponseCode());
                    billingUpdatesListener.onBillingPurchasesUpdate(purchasesResult.getResponseCode(), this.purchases);
                    return;
                }

                // add received purchases
                purchases.addAll(purchasesList);

                // if there are subscriptions supported, we add subscription rows as well
                if (subsEnabled && billingClient.isFeatureSupported(FeatureType.SUBSCRIPTIONS).getResponseCode() ==
                        BillingClient.BillingResponseCode.OK) {

                    if (Misc.DEBUG) Log.d(TAG, "Querying subscriptions.");
                    billingClient.queryPurchasesAsync(SkuType.SUBS, (subscriptionResult, subscriptionsList) -> {

                        if (Misc.DEBUG) {
                            Log.i(TAG, "Query subscriptions result code: "
                                    + subscriptionResult.getResponseCode() + " - " + subscriptionResult.getDebugMessage());
                            Log.i(TAG, "Query subscriptions list: " + subscriptionsList);
                        }

                        // something went wrong, report error and current list
                        if (subscriptionResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
                            if (Misc.DEBUG) Log.w(TAG, "queryPurchasesAsync() on subscriptions got an error result: "
                                    + subscriptionResult.getResponseCode());
                            billingUpdatesListener.onBillingPurchasesUpdate(subscriptionResult.getResponseCode(), this.purchases);
                            return;
                        }

                        // add subscriptions to the list
                        purchases.addAll(subscriptionsList);

                        // clear previous purchases and update with new
                        this.purchases.clear();
                        onPurchasesUpdated(BillingClient.BillingResponseCode.OK, purchases);
                    });

                    return;
                }

                if (Misc.DEBUG) Log.i(TAG, "Skipped subscription purchases query since they are not supported");

                // clear previous purchases and update with new
                this.purchases.clear();
                onPurchasesUpdated(BillingClient.BillingResponseCode.OK, purchases);
            });
        });
    }

    /** Queries the purchase history on the server */
    public void queryPurchaseHistoryAsync()
    {
        connectAndExecute(() -> billingClient.queryPurchaseHistoryAsync(SkuType.INAPP,
                (billingResult, purchaseHistoryRecordList) -> {

            // if we have been destroyed just skip
            if (billingClient == null) return;

            int responseCode = billingResult.getResponseCode();
            if (Misc.DEBUG) Log.d(TAG, "onPurchaseHistoryResponse() with response code: " + logResult(billingResult));
            if (responseCode != BillingClient.BillingResponseCode.OK) {
                billingUpdatesListener.onBillingPurchasesHistory(responseCode, null);
                return;
            }

            // check received purchases and report directly if subscriptions are not available
            final List<PurchaseHistoryRecord> purchases = new ArrayList<>();
            if (purchaseHistoryRecordList != null)
                for (PurchaseHistoryRecord p : purchaseHistoryRecordList)
                    if (verifyPurchase(p)) purchases.add(p);
            if (!subsEnabled || billingClient.isFeatureSupported(FeatureType.SUBSCRIPTIONS).getResponseCode() !=
                    BillingClient.BillingResponseCode.OK) {
                billingUpdatesListener.onBillingPurchasesHistory(responseCode, purchases);
                return;
            }

            // add subscriptions
            billingClient.queryPurchaseHistoryAsync(SkuType.SUBS, (subsBillingResult, subsPurchaseHistoryRecordList) -> {

                // if we have been destroyed just skip
                if (billingClient == null) return;

                int subsResponseCode = subsBillingResult.getResponseCode();
                if (Misc.DEBUG) Log.d(TAG, "onPurchaseHistoryResponse() subscriptions with response " + logResult(subsBillingResult));
                if (subsResponseCode != BillingClient.BillingResponseCode.OK) {
                    billingUpdatesListener.onBillingPurchasesHistory(subsResponseCode, null);
                    return;
                }

                // check received purchases and report them
                if (subsPurchaseHistoryRecordList != null)
                    for (PurchaseHistoryRecord p : subsPurchaseHistoryRecordList)
                        if (verifyPurchase(p)) purchases.add(p);
                billingUpdatesListener.onBillingPurchasesHistory(subsResponseCode, purchases);
            });
        }));
    }

    /**
     * Start a purchase flow
     * @param activity The Activity holding the purchase request
     * @param skuDetails The SKU details to purchase, could be null
     */
    public void initiatePurchaseFlow(Activity activity, SkuDetails skuDetails)
    {
        // check for null SKU
        if (skuDetails == null) {
            if (Misc.DEBUG) Log.i(TAG, "Attempted puchase for null SKU");
            return;
        }

        connectAndExecute(() -> {
            if (Misc.DEBUG) Log.d(TAG, "Launching in-app purchase flow for " + skuDetails);
            BillingFlowParams purchaseParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetails).build();
            billingClient.launchBillingFlow(activity, purchaseParams);
        });
    }

    /**
     * Retrieves SKU info from the server
     * @param skuType The SKU type to be queried
     * @param skuList The list of SKU to query
     */
    public void querySkuDetailsAsync(@SkuType final String skuType, final List<String> skuList)
    {
        connectAndExecute(() -> {

            // Query the purchase async
            SkuDetailsParams params = SkuDetailsParams.newBuilder().setSkusList(skuList).setType(skuType).build();
            billingClient.querySkuDetailsAsync(params, (billingResult, skuDetailsList) -> {

                // if we have been destroyed just skip
                if (billingClient == null) return;

                if (Misc.DEBUG) Log.d(TAG, "Received SKU response " + logResult(billingResult) +
                        " " + skuDetailsList);
                billingUpdatesListener.onSkuDetailsResponse(skuType, skuList, billingResult.getResponseCode(), skuDetailsList);
            });
        });
    }

    /**
     * Request the consume of a purchase
     * @param purchaseToken The token to be consumed
     */
    public void consumeAsync(String purchaseToken)
    {
        // If we've already scheduled to consume this token - no action is needed (this could happen
        // if you received the token when querying purchases inside onReceive() and later from
        // onActivityResult()
        if (tokensToBeConsumed.contains(purchaseToken)) {
            if (Misc.DEBUG) Log.i(TAG, "Token was already scheduled to be consumed - skipping...");
            return;
        }
        tokensToBeConsumed.add(purchaseToken);

        // Creating a runnable from the request to use it inside our connection retry policy below
        connectAndExecute(() -> {
            // Consume the purchase async
            ConsumeParams params = ConsumeParams.newBuilder().setPurchaseToken(purchaseToken).build();
            billingClient.consumeAsync(params, (billingResult, purchaseToken1) -> {

                // if we have been destroyed just skip
                if (billingClient == null) return;

                tokensToBeConsumed.remove(purchaseToken1);
                billingUpdatesListener.onConsumeFinished(purchaseToken1, billingResult.getResponseCode());
            });
        });
    }

    /**
     * Verifies the purchase
     * <p>Note: Notice that for each purchase, we check if signature is valid on the client.
     * It's recommended to move this check into your backend.
     * See {@link Security#verifyPurchase(String, String, String)}
     * </p>
     * @param purchase Purchase to be verified
     * @return true if it is a valid purchase
     */
    private boolean verifyPurchase(Purchase purchase)
    {
        if (!verifyValidSignature(purchase.getOriginalJson(), purchase.getSignature())) {
            if (Misc.DEBUG) Log.i(TAG, "Got a purchase: " + purchase + "; but signature is bad. Skipping...");
            return false;
        }

        if (Misc.DEBUG) Log.d(TAG, "Got a verified purchase: " + purchase);
        return true;
    }

    /**
     * Verifies the purchase history
     * <p>Note: Notice that for each purchase, we check if signature is valid on the client.
     * It's recommended to move this check into your backend.
     * See {@link Security#verifyPurchase(String, String, String)}
     * </p>
     * @param purchase Purchase to be verified
     * @return true if it is a valid purchase
     */
    private boolean verifyPurchase(PurchaseHistoryRecord purchase)
    {
        if (!verifyValidSignature(purchase.getOriginalJson(), purchase.getSignature())) {
            if (Misc.DEBUG) Log.i(TAG, "Got a purchase: " + purchase + "; but signature is bad. Skipping...");
            return false;
        }

        if (Misc.DEBUG) Log.d(TAG, "Got a verified purchase: " + purchase);
        return true;
    }

    /** Request connection to service if not yet done. */
    private void startServiceConnection()
    {
        // if connected or pending connection, just return
        if (isServiceConnected || executeOnConnect != null) return;

        // create the list of pending actions and start the connection process
        if (Misc.DEBUG) Log.d(TAG, "Connecting to billing service.");
        executeOnConnect = new ArrayList<>();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if (Misc.DEBUG) Log.d(TAG, "Setup finished. Result: " + billingResult);

                // if we have been destroyed just skip
                if (billingClient == null) return;

                // report result
                billingUpdatesListener.onBillingConnectResult(billingResult.getResponseCode());

                // stop here if not connected
                if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
                    isServiceConnected = false;
                    executeOnConnect = null;
                    return;
                }

                // report the service as connected and loop over pending actions
                isServiceConnected = true;
                if (executeOnConnect != null) for (Runnable r : executeOnConnect) r.run();
                executeOnConnect = null;
            }

            @Override
            public void onBillingServiceDisconnected() {
                if (Misc.DEBUG) Log.d(TAG, "Billing service disconnected.");
                isServiceConnected = false;
                billingUpdatesListener.onBillingDisconnected();
            }
        });
    }

    /**
     * Request service connection if not present and executed the specified runnable
     * @param runnable The Runnable to be executed when the connection is present
     */
    private void connectAndExecute(Runnable runnable)
    {
        // if we have been destroyed just skip
        if (billingClient == null) return;

        // request connection if not already connected
        if (!isServiceConnected) startServiceConnection();

        // if already connected just execute, else if possible put in the queue
        if (isServiceConnected) runnable.run();
        else if (executeOnConnect != null) executeOnConnect.add(runnable);
        else {
            // this should not happen, unless some connection failure case
            if (Misc.DEBUG) Log.d(TAG, "No connection available and no execution queue, just skipping");
        }
    }

    /**
     * Verifies that the purchase was signed correctly for this developer's public key.
     * <p>Note: It's strongly recommended to perform such check on your backend since hackers can
     * replace this method with "constant true" if they decompile/rebuild your app.
     * </p>
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean verifyValidSignature(String signedData, String signature) {
        try {
            return Security.verifyPurchase(base64PublicKey, signedData, signature);
        } catch (IOException e) {
            if (Misc.DEBUG) Log.e(TAG, "Got an exception trying to validate a purchase: " + e);
            return false;
        }
    }
}
