package net.gicar.util.googleplay.update;

/**
 * Status of app-updates.
 * Possible transitions:
 * UNKNOWN
 * UNKNOWN -> NOT_AVAILABLE
 * UNKNOWN -> AVAILABLE
 * NOT_AVAILABLE -> AVAILABLE
 * AVAILABLE -> REQUESTED -> AVAILABLE
 * AVAILABLE -> REQUESTED -> FAILED
 * AVAILABLE -> REQUESTED -> (DOWNLOADING) -> READY_TO_INSTALL
 * UNKNOWN -> (DOWNLOADING) -> READY_TO_INSTALL
 */
public enum AppUpdateStatus {

    /** Not known */
    UNKNOWN,

    /** No update is available */
    NOT_AVAILABLE,

    /** Update available */
    AVAILABLE,

    /** Update requested */
    REQUESTED,

    /** Something went wrong, could be user cancelled */
    FAILED,

    /** Update is being downloaded */
    DOWNLOADING,

    /** Update is ready to be installed */
    READY_TO_INSTALL
}
