package net.gicar.util.googleplay.update;

import android.app.Application;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import androidx.annotation.NonNull;
import net.gicar.util.Market;
import net.gicar.util.Misc;
import net.gicar.util.appcompat.RetainedInstance;
import net.gicar.util.appcompat.XAppCompatActivity;
import net.gicar.util.services.CrashReporting;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/** Retained fragment used to handle state for in-app updates */
public class AppUpdateHandler extends RetainedInstance {

    /** Set this to true to test this functionality without connecting with Google Play */
    final private static boolean APP_UPDATE_TEST = false;

    /** The log tag */
    final private static String TAG = "AppUpdateHandler";

    /** The keys for the fields in the bundles */
    private final static String REQUEST_CODE_KEY = "REQUEST_CODE_KEY",
            FORCED_VERSION_KEY = "FORCED_VERSION_KEY";

    public AppUpdateHandler(@NonNull Application application) {
        super(application);
    }

    /**
     * Returns the single instance of this fragment, creating it if necessary
     * @param activity The Activity performing the request
     * @param requestCode The requestCode which will be used for the update request
     * @param forcedVersion The minimum app version under which we will automatically request an update.
     *                      If set to 0 only flexible updates will be used.
     * @return The Fragment
     */
    public static AppUpdateHandler get(XAppCompatActivity activity, int requestCode, int forcedVersion)
    {
        if (!(activity instanceof AppUpdateCallback))
            throw new RuntimeException("Activity class shall implement AppUpdateCallback");

        // get the instance
        Bundle args = new Bundle();
        args.putInt(REQUEST_CODE_KEY, requestCode);
        args.putInt(FORCED_VERSION_KEY, forcedVersion);
        return getInstance(activity, AppUpdateHandler.class, args);
    }

    /** The requestCode which will be used for the update request */
    private int requestCode;

    /** The minimum app version under which we will automatically request an update */
    private int forcedVersion;

    /** The app update manager */
    private AppUpdateManager appUpdateManager;

    /** The pending update task, if any */
    private Task<AppUpdateInfo> appUpdateInfoTask;

    /** The current app update info, if available */
    private AppUpdateInfo appUpdateInfo;

    /** Current status of the app-update */
    private AppUpdateStatus status = AppUpdateStatus.UNKNOWN;

    @Override
    public void onCreate()
    {
        if (Misc.DEBUG) Log.d(TAG, "onCreate");
        super.onCreate();

        // recover the arguments
        Bundle args = requireArguments();
        requestCode = args.getInt(REQUEST_CODE_KEY);
        forcedVersion = args.getInt(FORCED_VERSION_KEY);
    }

    @Override
    public void onActivityCreate() {
        if (Misc.DEBUG) Log.d(TAG, "onActivityCreated");
        super.onActivityCreate();

        XAppCompatActivity activity = (XAppCompatActivity)requireActivity();

        // register with the app update manager at the first activity start
        if (appUpdateManager == null) {

            // creates instance of the manager
            appUpdateManager = AppUpdateManagerFactory.create(activity.getApplicationContext());

            // register the listener
            appUpdateManager.registerListener(listener);
        }

        // register for handling activity results
        activity.registerActivityResultHandler(requestCode, (activity1, requestCode, resultCode, resultData) -> {
            if (requestCode == this.requestCode) {
                if (resultCode == RESULT_OK) {
                    if (Misc.DEBUG) Log.d(TAG, "Update flow started");
                    if (status == AppUpdateStatus.REQUESTED) setStatus(AppUpdateStatus.DOWNLOADING);
                } else if (resultCode == RESULT_CANCELED) {
                    if (Misc.DEBUG) Log.w(TAG, "Update flow canceled");
                    setStatus(AppUpdateStatus.AVAILABLE);
                } else {
                    if (Misc.DEBUG) Log.w(TAG, "Update flow failed! Result code: " + resultCode);
                    setStatus(AppUpdateStatus.FAILED);
                }
            }
        });

        // notify the new activity of the current state
        ((AppUpdateCallback)activity).appUpdateStatus(status);
    }

    @Override
    public void onDestroy()
    {
        if (Misc.DEBUG) Log.d(TAG, "onDestroy");

        // unregister the listener
        if (appUpdateManager != null) appUpdateManager.unregisterListener(listener);

        super.onDestroy();
    }

    @Override
    public void onResume()
    {
        if (Misc.DEBUG) Log.d(TAG, "onResume");
        super.onResume();

        // update the current status
        requestAppUpdateInfo();
    }

    /** Listener to track request state updates */
    private final InstallStateUpdatedListener listener = state ->
    {
        if (Misc.DEBUG) Log.d(TAG, "Install update state: " + state);

        switch (state.installStatus()) {

            case InstallStatus.PENDING:
            case InstallStatus.DOWNLOADING:
                break;
            case InstallStatus.DOWNLOADED:
                setStatus(AppUpdateStatus.READY_TO_INSTALL);
                break;
            case InstallStatus.CANCELED:
            case InstallStatus.FAILED:
                setStatus(AppUpdateStatus.FAILED);
                break;
            case InstallStatus.INSTALLED:
            case InstallStatus.INSTALLING:
            //noinspection deprecation
            case InstallStatus.REQUIRES_UI_INTENT:
            case InstallStatus.UNKNOWN:
                break;
        }
    };

    private void requestAppUpdateInfo()
    {
        // avoid to perform requests if the app was not installed using Google Play
        if (!Market.canUseGooglePlay(getApplication())) return;

        // check if an update request is already ongoing
        if (appUpdateInfoTask != null) return;

        // request an intent object that you use to check for an update
        appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (Misc.DEBUG) Log.d(TAG, "Request completed successfully: " + appUpdateInfo);

            // if we are testing just stop here
            if (APP_UPDATE_TEST) return;

            // save the received update info
            this.appUpdateInfo = appUpdateInfo;

            // init the status if still unknown
            if (status == AppUpdateStatus.UNKNOWN) {
                switch (appUpdateInfo.updateAvailability()) {

                    case UpdateAvailability.UPDATE_AVAILABLE:
                        // if flexible updates are possible set as available, else deny
                        if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                            setStatus(AppUpdateStatus.AVAILABLE);

                            // if our version is below the forced version, start automatically the request
                            if (forcedVersion > Misc.getAppVersionNumber(getApplication()))
                                requestUpdate();
                        }
                        else setStatus(AppUpdateStatus.NOT_AVAILABLE);
                        break;

                    case UpdateAvailability.UPDATE_NOT_AVAILABLE:
                        // simple case here, just not available
                        setStatus(AppUpdateStatus.NOT_AVAILABLE);
                        break;

                    case UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS:
                        // this could happen if somehow we restarted while downloading
                        setStatus(AppUpdateStatus.DOWNLOADING);
                        break;

                    case UpdateAvailability.UNKNOWN:
                        break;
                }
            }
            else if (status == AppUpdateStatus.NOT_AVAILABLE &&
                    appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                // it could happen that an update becomes available while suspended,
                // if flexible updates are possible set as available, else deny
                if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE))
                    setStatus(AppUpdateStatus.AVAILABLE);
            }

            // check if download completed (additional checks could be implemented)
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED)
                setStatus(AppUpdateStatus.READY_TO_INSTALL);
        });
        appUpdateInfoTask.addOnFailureListener(e -> {
            if (Misc.DEBUG) Log.w(TAG, "Request failed with exception: " + e);
        });
        appUpdateInfoTask.addOnCompleteListener(task -> {
            if (Misc.DEBUG) Log.d(TAG, "Request task completed");
            appUpdateInfoTask = null;

            // if we are testing check if status change is needed
            if (APP_UPDATE_TEST && status == AppUpdateStatus.UNKNOWN)
                setStatus(AppUpdateStatus.AVAILABLE);
        });
    }

    /**
     * Sets a new status calling the appropriate callbacks if needed
     * @param newStatus The new status
     */
    private void setStatus(AppUpdateStatus newStatus)
    {
        if (Misc.DEBUG) Log.d(TAG, "Status change: " + newStatus);

        if (status != newStatus) {
            status = newStatus;
            AppUpdateCallback activity = (AppUpdateCallback)getActivity();
            if (activity != null) activity.appUpdateStatus(status);
        }
    }

    /**
     * The current status
     * @return AppUpdateStatus
     */
    public AppUpdateStatus getStatus() {
        return status;
    }

    /** Requests the download of the updated app */
    public void requestUpdate()
    {
        // if we are testing just stop here
        if (APP_UPDATE_TEST) {
            if (status == AppUpdateStatus.AVAILABLE) setStatus(AppUpdateStatus.READY_TO_INSTALL);
            return;
        }

        if (appUpdateInfo == null || status != AppUpdateStatus.AVAILABLE) {
            if (Misc.DEBUG) Log.w(TAG, "Update requested but not possible. Status: " + status);
            return;
        }

        if (Misc.DEBUG) Log.d(TAG, "Requesting app update");
        try {
            setStatus(AppUpdateStatus.REQUESTED);
            appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE,
                    requireActivity(), requestCode);
        } catch (IntentSender.SendIntentException e) {
            setStatus(AppUpdateStatus.FAILED);
            CrashReporting.logException(e);
        }

        // this can no longer be used
        appUpdateInfo = null;
    }

    /** Proceeds installing the downloaded app */
    public void install()
    {
        // if we are testing just stop here
        if (APP_UPDATE_TEST) {
            if (status == AppUpdateStatus.READY_TO_INSTALL) setStatus(AppUpdateStatus.AVAILABLE);
            return;
        }

        if (status != AppUpdateStatus.READY_TO_INSTALL) {
            if (Misc.DEBUG) Log.w(TAG, "Install requested but not possible. Status: " + status);
            return;
        }

        // proceed with the installation
        appUpdateManager.completeUpdate();
    }
}