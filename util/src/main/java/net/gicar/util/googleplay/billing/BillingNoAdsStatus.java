package net.gicar.util.googleplay.billing;

/** Possible statuses for the no ads billing interface */
public enum BillingNoAdsStatus {

    /** Not yet started */
    INITIALIZED(false, false, false),

    /** Not yet connected to the billing service */
    STARTING(false, false, false),

    /** Billing service not available. Might be temporary, we treat as permanent. */
    ERROR_UNAVAILABLE(true, false, false),

    /** Service successfully started, ads shall be shown but purchases not available */
    STARTED_ADS_CANNOT_PURCHASE(true, false, false),

    /** Purchases available, ads shall be shown */
    STARTED_ADS(true, false, true),

    /** No ads purchased */
    STARTED_NO_ADS(true, true, false);

    /** We are in a stable state */
    public final boolean isInitialized;

    /** The no ads has been purchased */
    public final boolean isNoAds;

    /** The no ads can be purchased */
    public final boolean isPurchaseAvailable;

    BillingNoAdsStatus(boolean isInitialized, boolean isNoAds, boolean isPurchaseAvailable) {
        this.isInitialized = isInitialized;
        this.isNoAds = isNoAds;
        this.isPurchaseAvailable = isPurchaseAvailable;
    }
}
