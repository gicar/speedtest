package net.gicar.util.googleplay.review;

import net.gicar.util.preferences.Settings;

/** Settings for caching the values */
class AppReviewSettings extends Settings {

    private final static String PREFS_NAME = "AppReviewSettings";

    AppReviewSettings() { super(PREFS_NAME); }

    /** Signals that we don't need to show the dialog */
    final SettingsItemBoolean dontShowAgain = new SettingsItemBoolean("dontshowagain", false);

    /** Counts the number of times the application has been launched */
    final SettingsItemInteger launchCount = new SettingsItemInteger("launch_count", 0);

    /** Records the first time the application has been launched */
    final SettingsItemLong dateFirstLaunch = new SettingsItemLong("date_first_launch", 0L);

    /** Counts the number of events */
    final SettingsItemInteger eventCount = new SettingsItemInteger("event_count", 0);
}
