package net.gicar.util.googleplay.billing;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.SkuDetails;

import java.util.Collections;
import java.util.List;
import java.util.Observable;

import net.gicar.util.Market;
import net.gicar.util.Misc;
import net.gicar.util.preferences.Settings;

import static net.gicar.util.googleplay.billing.BillingNoAdsStatus.ERROR_UNAVAILABLE;
import static net.gicar.util.googleplay.billing.BillingNoAdsStatus.INITIALIZED;
import static net.gicar.util.googleplay.billing.BillingNoAdsStatus.STARTED_ADS;
import static net.gicar.util.googleplay.billing.BillingNoAdsStatus.STARTED_ADS_CANNOT_PURCHASE;
import static net.gicar.util.googleplay.billing.BillingNoAdsStatus.STARTED_NO_ADS;
import static net.gicar.util.googleplay.billing.BillingNoAdsStatus.STARTING;

/** Manages ads removal purchases */
public class BillingNoAds extends Observable implements BillingManager.BillingUpdatesListener {

    private final static String TAG = "BillingClient.NoAds";

    /** Settings for caching the values */
    static class NoAdsSettings extends Settings {

        private final static String PREFS_NAME = "NoAds";

        NoAdsSettings() { super(PREFS_NAME); }

        /** Set to true if no ads status has been checked at least once */
        final SettingsItemBoolean noAdsInitialized = new SettingsItemBoolean("noAdsInitialized", false);

        /** No ads status */
        final SettingsItemBoolean noAds = new SettingsItemBoolean("noAds", false);

        /** No ads purchase token */
        final SettingsItemString noAdsToken = new SettingsItemString("noAdsToken", null);
    }

    /** The singleton instance of this object */
    private static BillingNoAds instance;

    /** The BillingManager */
    private final BillingManager billingManager;

    /** The SKU of the ads removal */
    private final String noAdsSku;

    /** Settings of the noAds */
    private final NoAdsSettings noAdsSettings;

    /** If true we have been installed by Google Play */
    private final boolean isInstalledByGooglePlay;

    /** The billing status */
    private BillingNoAdsStatus status = BillingNoAdsStatus.INITIALIZED;

    /** If true the service is connected */
    private boolean connected;

    /** The SKU details */
    private SkuDetails noAdsSkuDetails;

    public static BillingNoAds initialize(Application application, String base64PublicKey, String noAdsSku) {
        instance = new BillingNoAds(application, base64PublicKey, noAdsSku);
        return instance;
    }

    public static BillingNoAds getInstance() {
        return instance;
    }

    private BillingNoAds(Application application, String base64PublicKey, String noAdsSku)
    {
        this.noAdsSku = noAdsSku;
        isInstalledByGooglePlay = Market.canUseGooglePlay(application);

        noAdsSettings = new NoAdsSettings();
        noAdsSettings.onCreate(application);

        billingManager = new BillingManager(application, base64PublicKey, false, this);
    }

    /** Call this method to ensure the start */
    public void start()
    {
        if (status == INITIALIZED) {
            // querying the known purchases starts the service
            if (Misc.DEBUG) Log.i(TAG, "Starting");
            setStatus(STARTING);
            billingManager.queryPurchases();
        }
        else if (status != STARTING && !connected) {
            // querying the known purchases to try reconnection
            if (Misc.DEBUG) Log.i(TAG, "Reconnecting");
            billingManager.queryPurchases();
        }
    }

    /**
     * Returns the initialization status of no ads value.
     * @return true if no ads status has been received at least once from Google Play
     */
    public boolean isNoAdsInitialized() {
        return noAdsSettings.noAdsInitialized.getValue();
    }

    /**
     * Returns the current known status of the no ads license.
     * @return true if we own the no ads license
     */
    public boolean isNoAds() {
        return noAdsSettings.noAds.getValue();
    }

    /**
     * Returns the current status
     * @return The BillingNoAdsStatus
     */
    public BillingNoAdsStatus getStatus() {
        return status;
    }

    /**
     * Starts the no ads purchase flow
     * @param activity The activity starting the purchase flow
     */
    public void startPurchaseFlow(Activity activity) {
        billingManager.initiatePurchaseFlow(activity, noAdsSkuDetails);
    }

    /**
     * Consumes the purchase for test purposes
     * @return true is we have a valid token to be consumed
     */
    public boolean consumeTestPurchase() {
        String token = noAdsSettings.noAdsToken.getValue();
        if (token != null) billingManager.consumeAsync(token);
        return token != null;
    }

    /**
     * Sets the new status
     * @param status The status to be set
     */
    private void setStatus(BillingNoAdsStatus status)
    {
        // always set changed, even if the state is not really changed, other options could be modified
        if (Misc.DEBUG) Log.i(TAG, "New status: " + status);
        this.status = status;
        setChanged();
        notifyObservers(status);
    }

    @Override
    public void onBillingConnectResult(@BillingClient.BillingResponseCode int resultCode) {
        if (Misc.DEBUG) Log.i(TAG, "Billing service connection result: " + resultCode);

        // set the connect status depending on result
        connected = resultCode == BillingClient.BillingResponseCode.OK;

        // check if we failed on start, else keep previous status
        if (status == BillingNoAdsStatus.STARTING && resultCode != BillingClient.BillingResponseCode.OK)
            setStatus(ERROR_UNAVAILABLE);
    }

    @Override
    public void onBillingDisconnected() {
        if (Misc.DEBUG) Log.i(TAG, "Billing service disconnected");
        connected = false;
    }

    @Override
    public void onBillingPurchasesUpdate(@BillingClient.BillingResponseCode int resultCode, List<Purchase> purchases)
    {
        if (Misc.DEBUG) Log.i(TAG, "Purchase update: " + resultCode + ", " + purchases);

        // check if we failed on start
        if (status == BillingNoAdsStatus.STARTING && resultCode != BillingClient.BillingResponseCode.OK) {
            setStatus(ERROR_UNAVAILABLE);
            return;
        }

        // patch for simulating purchases with apps blocked in the country
        boolean noAds = false;
        if (Misc.DEBUG && status == STARTED_ADS && resultCode == BillingClient.BillingResponseCode.ITEM_UNAVAILABLE) {
            Log.e(TAG, "Simulating successful purchase with item unavailable");
            noAds = true;
        }

        // look for a valid no ads purchase
        for (Purchase p : purchases) {
            if (p.getSkus().contains(noAdsSku)) {
                if (Misc.DEBUG) Log.i(TAG, "No ads purchase present with token: " + p.getPurchaseToken());
                noAds = true;
                noAdsSettings.noAdsToken.setValue(p.getPurchaseToken());
                break;
            }
        }
        noAdsSettings.noAdsInitialized.setValue(true);
        noAdsSettings.noAds.setValue(noAds);
        noAdsSettings.commit();

        // check if we are in no ads mode
        if (noAds) setStatus(STARTED_NO_ADS);
        // else we are in ads mode: if we already have SKU details, just stop here
        else if (noAdsSkuDetails != null) setStatus(STARTED_ADS);
        // else if we are starting (or recovering from an error) fetch the SKU details
        else if ((status == STARTING || status == ERROR_UNAVAILABLE) && isInstalledByGooglePlay)
            billingManager.querySkuDetailsAsync(BillingClient.SkuType.INAPP, Collections.singletonList(noAdsSku));
        // else declare purchases not available
        else setStatus(STARTED_ADS_CANNOT_PURCHASE);
    }

    @Override
    public void onBillingPurchasesHistory(@BillingClient.BillingResponseCode int resultCode, List<PurchaseHistoryRecord> purchases)
    {
        if (Misc.DEBUG) Log.i(TAG, "Purchase history: " + resultCode + ", " + purchases);
    }

    @Override
    public void onSkuDetailsResponse(String skuType, List<String> skuList,
                                     @BillingClient.BillingResponseCode int responseCode, List<SkuDetails> skuDetailsList)
    {
        if (Misc.DEBUG) {
            Log.i(TAG, "SKU query for type " + skuType + " of SKUs " + skuList + " received with response " + responseCode);
            Log.i(TAG, "SKU details: " + skuDetailsList);
        }

        // look for our SKU
        noAdsSkuDetails = null;
        if (responseCode == BillingClient.BillingResponseCode.OK)
            for (SkuDetails d : skuDetailsList)
                if (noAdsSku.equals(d.getSku())) noAdsSkuDetails = d;

        // if we are in ads mode fix the status depending on the purchase availability
        if (!noAdsSettings.noAds.getValue())
            setStatus(noAdsSkuDetails == null ? STARTED_ADS_CANNOT_PURCHASE : STARTED_ADS);
    }

    @Override
    public void onConsumeFinished(String purchaseToken, @BillingClient.BillingResponseCode int result)
    {
        if (Misc.DEBUG) Log.i(TAG, "Consume finished with result " + result);

        // check cases which needs to be ignored
        if (result != BillingClient.BillingResponseCode.OK || !noAdsSettings.noAds.getValue() ||
                !purchaseToken.equals(noAdsSettings.noAdsToken.getValue())) return;

        // clear no ads (we keep the token for future reference)
        noAdsSettings.noAds.setValue(false);
        noAdsSettings.commit();

        // if we already have SKU details, stop here else query it
        if (noAdsSkuDetails != null) setStatus(STARTED_ADS);
        else billingManager.querySkuDetailsAsync(BillingClient.SkuType.INAPP, Collections.singletonList(noAdsSku));
    }
}
