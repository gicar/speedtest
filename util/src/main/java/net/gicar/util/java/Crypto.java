package net.gicar.util.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/** Cryptography related functions */
public class Crypto {

    /**
     * Calculate the SHA-1 hash of the specified file
     * @param file The file
     * @param data Additional optional data to be considered into the hashing, pass null if none
     * @return The SHA-1 hash
     */
    public static byte[] sha1(File file, ByteBuffer data) throws NoSuchAlgorithmException, IOException
    {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        InputStream fis = new FileInputStream(file);
        if (data != null) digest.update(data);
        int n = 0;
        byte[] buffer = new byte[4096];
        while (n != -1) {
            n = fis.read(buffer);
            if (n > 0) digest.update(buffer, 0, n);
        }
        return digest.digest();
    }

    /**
     * Calculate the SHA-1 hash of the specified data
     * @param buf The ByteBuffer of the data
     * @param data Additional optional data to be considered into the hashing, pass null if none
     * @return The SHA-1 hash
     */
    public static byte[] sha1(ByteBuffer buf, ByteBuffer data) throws NoSuchAlgorithmException
    {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        if (data != null) digest.update(data);
        digest.update(buf);
        return digest.digest();
    }
}
