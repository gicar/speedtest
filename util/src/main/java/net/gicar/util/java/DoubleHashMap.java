package net.gicar.util.java;

import java.util.HashMap;
import java.util.Map;

/** HashMap pair */
public class DoubleHashMap<T1, T2> {

    private final HashMap<T1, T2> map12 = new HashMap<T1, T2>();
    private final HashMap<T2, T1> map21 = new HashMap<T2, T1>();

    public void put(T1 k1, T2 k2)
    {
        map12.put(k1, k2);
        map21.put(k2, k1);
    }

    public Map<T1, T2> getMap1()
    {
        return map12;
    }

    public Map<T2, T1> getMap2()
    {
        return map21;
    }
}
