package net.gicar.util.oss;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import androidx.annotation.NonNull;
import net.gicar.util.IntentUtils;
import net.gicar.util.file.StreamUtils;
import net.gicar.util.services.CrashReporting;
import net.gicar.util.web.WebActivity;

import static net.gicar.util.oss.OssViewModel.LICENSE_TEXT_FILE;

/** An open source entry */
public class OssEntry implements Parcelable {

    /** Name */
    private final String name;

    /** Free description text, could be null */
    private final String description;

    /** File holding the license text in assets or URL to it, could be null */
    private final String license;

    /** If no explicit license provided, position and length in global file for it */
    private final int start, length;

    public OssEntry(String name, String description, String license) {
        this(name, description, license, 0, 0);
    }

    OssEntry(String name, String description, int start, int length) {
        this(name, description, null, start, length);
    }

    private OssEntry(String name, String description, String license, int start, int length) {
        this.name = name;
        this.description = description;
        this.license = license;
        this.start = start;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Shows the details for this entry
     * @param activity The activity calling this
     * @param theme The theme to be applied
     */
    void show(Activity activity, int theme)
    {
        // use the provided license in any
        if (license != null) {
            if (launchURL(activity, license)) return;
            OssDetailsActivity.startActivity(activity, theme, name, readFile(activity, license));
            return;
        }

        // load the text from the license file
        Resources res = activity.getResources();
        String s;
        try (InputStream is = res.openRawResource(res.getIdentifier(LICENSE_TEXT_FILE, "raw", activity.getPackageName()));
             ByteArrayOutputStream os = new ByteArrayOutputStream()) {
             StreamUtils.copy(is, os, start, length, new byte[1024]);
             s = new String(os.toByteArray());
        }
        catch (Exception e) {
             CrashReporting.logException(e);
             return;
        }

        // load the contained url or show the text
        if (launchURL(activity, s)) return;
        OssDetailsActivity.startActivity(activity, theme, name, s);
    }

    private boolean launchURL(Activity activity, String s)
    {
        String sl = s.toLowerCase();
        if (!sl.startsWith("http://") && !sl.startsWith("https://")) return false;
        if (s.endsWith("/")) IntentUtils.viewIntent(activity, s);
        else WebActivity.startActivity(activity, s, name);
        return true;
    }

    /**
     * Reads the specified file contents in a string
     * @param fileName The file name
     * @return The file contents or null if something went wrong
     */
    private static String readFile(Context context, String fileName)
    {
        try (InputStream is = context.getAssets().open(fileName)) {
            return StreamUtils.readToString(is);
        }
        catch (Exception e) {
            CrashReporting.logException(e);
            return null;
        }
    }

    protected OssEntry(Parcel in)
    {
        this(in.readString(),
             in.readString(),
             in.readString(),
             in.readInt(),
             in.readInt());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(license);
        dest.writeInt(start);
        dest.writeInt(length);
    }

    public static final Parcelable.Creator<OssEntry> CREATOR = new Parcelable.Creator<OssEntry>() {
        @Override
        public OssEntry createFromParcel(Parcel in) {
            return new OssEntry(in);
        }

        @Override
        public OssEntry[] newArray(int size) {
            return new OssEntry[size];
        }
    };
}
