package net.gicar.util.oss;

import android.app.Application;
import android.content.res.Resources;
import android.os.Parcelable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import net.gicar.util.Misc;
import net.gicar.util.services.CrashReporting;

public class OssViewModel extends AndroidViewModel {

    /** The log tag */
    final private static String TAG = OssViewModel.class.getSimpleName();

    /** The files created by the plugin */
    final static String LICENSE_METADATA_FILE = "third_party_license_metadata",
            LICENSE_TEXT_FILE = "third_party_licenses";

    /** The list of entries */
    final ArrayList<OssEntry> ossEntries = new ArrayList<>();

    /** Regular expression for the metadata */
    private static final Pattern METADATA_PATTERN = Pattern.compile("(\\d+):(\\d+) (.+)");

    /** Regular expression for splitting the name in the metadata */
    private static final Pattern METADATA_NAME_PATTERN = Pattern.compile("(\\S+):(\\S+)");

    public OssViewModel(@NonNull Application application) {
        super(application);
    }

    @SuppressWarnings("ConstantConditions")
    void load(Parcelable[] entries)
    {
        // add the entries from the parameters
        if (entries != null) for (Parcelable p : entries) ossEntries.add((OssEntry) p);

        // read the metadata file line by line
        Resources res = getApplication().getResources();
        try (BufferedReader metadata = new BufferedReader(new InputStreamReader(
                res.openRawResource(res.getIdentifier(LICENSE_METADATA_FILE, "raw", getApplication().getPackageName())))))
        {
            // read a line at a time
            String line;
            while ((line = metadata.readLine()) != null) {

                // parse the info in the metadata
                Matcher matcher = METADATA_PATTERN.matcher(line);
                if (matcher.find()) {
                    if (Misc.DEBUG) Log.d(TAG, "Processing line: " + line);
                    int start = Integer.parseInt(matcher.group(1));
                    int length = Integer.parseInt(matcher.group(2));
                    String name = matcher.group(3);

                    // try to split the name to get a description
                    String description = null;
                    Matcher matcher2 = METADATA_NAME_PATTERN.matcher(name);
                    if (matcher2.find()) {
                        description = matcher2.group(1);
                        name = matcher2.group(2);
                    }

                    // add the entry
                    ossEntries.add(new OssEntry(name, description, start, length));
                }
                else if (Misc.DEBUG) Log.e(TAG, "Ignoring line: " + line);
            }
        }
        catch (Exception e) {
            CrashReporting.logException(e);
        }
    }
}
