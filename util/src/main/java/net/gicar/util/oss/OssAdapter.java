package net.gicar.util.oss;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.gicar.util.R;
import net.gicar.util.recyclerlist.ActionModeRecyclerListViewHolder;
import net.gicar.util.recyclerlist.RecyclerListAdapter;

/** Adapter for RecyclerView */
public class OssAdapter extends RecyclerListAdapter<OssEntry, OssAdapter.ViewHolder> {

    /**
     * Provide a direct reference to each of the views within a data item.
     * Used to cache the views within the item layout for fast access
     */
    public static class ViewHolder extends ActionModeRecyclerListViewHolder {

        final TextView name;
        final TextView text;

        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.list_oss_name);
            text = itemView.findViewById(R.id.list_oss_text);
        }
    }

    /**
     * Build an adapter for licenses
     * @param context The Context
     */
    public OssAdapter(Context context)
    {
        super(context, false, 0, 0,
                0, 0, 0, 0);
    }

    @Override
    public OssAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        // inflate the custom layout
        View view = inflater.inflate(R.layout.oss_list_item, parent, false);

        // return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public int onBindViewHolder(OssAdapter.ViewHolder viewHolder, int position)
    {
        OssEntry entry = getItem(position);

        // set item views based on your views and data model
        viewHolder.name.setText(entry.getName());
        String text = entry.getDescription();
        viewHolder.text.setText(text);
        viewHolder.text.setVisibility(text != null ? View.VISIBLE : View.GONE);

        return ITEM_FLAG_CLICK;
    }
}
