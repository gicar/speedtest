package net.gicar.util.oss;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.gicar.util.Misc;
import net.gicar.util.R;
import net.gicar.util.recyclerlist.RecyclerListFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import static net.gicar.util.oss.OssActivity.INTENT_EXTRA_THEME;

public class OssListFragment extends RecyclerListFragment {

    private OssAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (Misc.DEBUG) Log.d(TAG, "onCreateView");
        // inflate the main fragment view
        return inflater.inflate(R.layout.oss_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (Misc.DEBUG) Log.d(TAG, "onViewCreated");

        // set the layout manager on the list
        setRecyclerListLayoutManager(new LinearLayoutManager(getContext()));

        // set the adapter on the list
        adapter = new OssAdapter(getContext());
        setRecyclerListAdapter(adapter);

        // add custom decorations
        //noinspection ConstantConditions
        addRecyclerListItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        // get the ViewModel
        OssViewModel ossViewModel = new ViewModelProvider(this).get(OssViewModel.class);

        // init the entries on first run
        if (savedInstanceState == null) //noinspection ConstantConditions
            ossViewModel.load(getArguments().getParcelableArray(OssActivity.INTENT_EXTRA_ENTRIES));

        // set the entries in the list
        adapter.addItems(ossViewModel.ossEntries);
    }

    @Override
    public void onItemClicked(int position)
    {
        OssEntry entry = adapter.getItem(position);
        //noinspection ConstantConditions
        entry.show(getActivity(), getArguments().getInt(INTENT_EXTRA_THEME));
    }
}
