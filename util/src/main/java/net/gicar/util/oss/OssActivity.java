package net.gicar.util.oss;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.R;
import net.gicar.util.appcompat.XAppCompatActivity;

import androidx.appcompat.app.ActionBar;

public class OssActivity extends XAppCompatActivity {

    /** Tag in the intent extra for the theme */
    static final String INTENT_EXTRA_THEME = "net.gicar.oss.theme";

    /** Tag in the intent extra for the title */
    static final String INTENT_EXTRA_TITLE = "net.gicar.oss.title";

    /** Tag in the intent extra for the entries */
    static final String INTENT_EXTRA_ENTRIES = "net.gicar.oss.entries";

    /**
     * Launches the oss activity
     * @param activity The caller activity
     * @param theme The theme to be used, or 0 for default
     * @param title The title to be shown
     * @param ossEntries The entries to be shown
     */
    public static void startActivity(Activity activity, int theme, String title, OssEntry[] ossEntries)
    {
        Intent intent = new Intent(activity, OssActivity.class);
        intent.putExtra(OssActivity.INTENT_EXTRA_THEME, theme);
        intent.putExtra(OssActivity.INTENT_EXTRA_TITLE, title);
        intent.putExtra(OssActivity.INTENT_EXTRA_ENTRIES, ossEntries);
        activity.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        if (Misc.DEBUG) Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        // get the parameters from the calling intent
        Intent intent = getIntent();

        // set the theme is provided
        int theme = intent.getIntExtra(INTENT_EXTRA_THEME, 0);
        if (theme != 0) setTheme(theme);

        // set content
        setContentView(R.layout.oss_activity);

        // set the back icon in the action bar and the title
        ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(intent.getCharSequenceExtra(INTENT_EXTRA_TITLE));

        // during initial setup, plug in the list fragment.
        if (savedInstanceState == null) {
            OssListFragment ossListFragment = new OssListFragment();
            ossListFragment.setArguments(intent.getExtras());
            getSupportFragmentManager().beginTransaction().add(R.id.oss_container, ossListFragment).commit();
        }
    }
}
