package net.gicar.util.oss;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import net.gicar.util.Misc;
import net.gicar.util.R;
import net.gicar.util.appcompat.XAppCompatActivity;

import androidx.appcompat.app.ActionBar;

public class OssDetailsActivity extends XAppCompatActivity {

    /** Tag in the intent extra for the theme */
    static final String INTENT_EXTRA_THEME = "net.gicar.oss.theme";

    /** Tag in the intent extra for the title */
    static final String INTENT_EXTRA_TITLE = "net.gicar.oss.title";

    /** Tag in the intent extra for the text */
    static final String INTENT_EXTRA_TEXT = "net.gicar.oss.text";

    /**
     * Launches the oss activity
     * @param activity The caller activity
     * @param theme The theme to be used, or 0 for default
     * @param title The title to be shown
     * @param text The text to be shown
     */
    public static void startActivity(Activity activity, int theme, String title, String text)
    {
        Intent intent = new Intent(activity, OssDetailsActivity.class);
        intent.putExtra(OssDetailsActivity.INTENT_EXTRA_THEME, theme);
        intent.putExtra(OssDetailsActivity.INTENT_EXTRA_TITLE, title);
        intent.putExtra(OssDetailsActivity.INTENT_EXTRA_TEXT, text);
        activity.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        if (Misc.DEBUG) Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        // get the parameters from the calling intent
        Intent intent = getIntent();

        // set the theme if provided
        int theme = intent.getIntExtra(INTENT_EXTRA_THEME, 0);
        if (theme != 0) setTheme(theme);

        // set the back icon in the action bar and the title
        ActionBar actionBar = getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(intent.getStringExtra(INTENT_EXTRA_TITLE));

        // set content
        setContentView(R.layout.oss_activity_details);
        TextView licenseText = findViewById(R.id.oss_license_text);
        licenseText.setText(intent.getStringExtra(INTENT_EXTRA_TEXT));
    }
}
