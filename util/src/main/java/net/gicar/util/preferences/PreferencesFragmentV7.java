package net.gicar.util.preferences;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

/** Provides the creation of customized dialogs */
public abstract class PreferencesFragmentV7 extends PreferenceFragmentCompat {

    /** Tag used for preferences dialog fragments */
    private static final String DIALOG_FRAGMENT_TAG = "android.support.v7.preference.PreferenceFragment.DIALOG";

    @Override
    public void onDisplayPreferenceDialog(Preference preference)
    {
        // check if the fragment is already there
        Fragment f = this.getParentFragmentManager().findFragmentByTag(DIALOG_FRAGMENT_TAG);
        DialogFragment f2 = null;

        // create our instances
        if (preference instanceof CustomListPreferenceV7) {
            if (f == null) f2 = CustomListPreferenceDialogFragmentCompatV7.newInstance(preference.getKey());
        } else {
            // revert to standard implementation
            super.onDisplayPreferenceDialog(preference);
            return;
        }

        // if we created a dialog show it
        if (f2 != null) {
            //noinspection deprecation
            f2.setTargetFragment(this, 0);
            f2.show(this.getParentFragmentManager(), DIALOG_FRAGMENT_TAG);
        }
    }
}
