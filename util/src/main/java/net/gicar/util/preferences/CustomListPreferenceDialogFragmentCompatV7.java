package net.gicar.util.preferences;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.TypedValue;

import net.gicar.util.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.preference.PreferenceDialogFragmentCompat;

/**
 * Provides customization possibility for a ListPreference dialog.
 * Main difference is on onPrepareDialogBuilder() method where our adapter is provided instead of
 * the default list one.
 */
public class CustomListPreferenceDialogFragmentCompatV7 extends PreferenceDialogFragmentCompat {

    private static final String SAVE_STATE_INDEX = "ListPreferenceDialogFragment.index";
    private static final String SAVE_STATE_ENTRIES = "ListPreferenceDialogFragment.entries";

    private int mClickedDialogEntryIndex;
    private ArrayList<CustomListPreferenceV7.ListPreferenceItem> mEntries;

    public static CustomListPreferenceDialogFragmentCompatV7 newInstance(String key) {
        final CustomListPreferenceDialogFragmentCompatV7 fragment =
                new CustomListPreferenceDialogFragmentCompatV7();
        final Bundle b = new Bundle(1);
        b.putString(ARG_KEY, key);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            final CustomListPreferenceV7 preference = getListPreference();

            if (preference.getEntries() == null || preference.getEntryValues() == null) {
                throw new IllegalStateException(
                        "CustomListPreferenceV7 requires an entries array and an entryValues array.");
            }

            // get the entries filtering them out
            mClickedDialogEntryIndex = -1;
            mEntries = new ArrayList<>();
            CharSequence[] entries = preference.getEntries();
            CharSequence[] entryValues = preference.getEntryValues();
            for (int i = 0; i < entryValues.length; i++) {
                if (preference.isEntryValid(entryValues[i])) {
                    if (entryValues[i].equals(preference.getValue())) mClickedDialogEntryIndex = mEntries.size();
                    mEntries.add(new CustomListPreferenceV7.ListPreferenceItem(
                            entries[i].toString(), entryValues[i].toString(),
                            preference.isEntryEnabled(entryValues[i])));
                }
            }

        } else {
            mClickedDialogEntryIndex = savedInstanceState.getInt(SAVE_STATE_INDEX, 0);
            //noinspection unchecked
            mEntries = (ArrayList<CustomListPreferenceV7.ListPreferenceItem>)
                    savedInstanceState.getSerializable(SAVE_STATE_ENTRIES);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_STATE_INDEX, mClickedDialogEntryIndex);
        outState.putSerializable(SAVE_STATE_ENTRIES, mEntries);
    }

    private CustomListPreferenceV7 getListPreference() {
        return (CustomListPreferenceV7) getPreference();
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        super.onPrepareDialogBuilder(builder);

        // *** Provide our adapter ***
        Context context = getContext();
        assert context != null;
        final TypedArray a =context.obtainStyledAttributes(null, R.styleable.AlertDialog,
                R.attr.alertDialogStyle, 0);
        int singleChoiceItemLayout = a.getResourceId(R.styleable.AlertDialog_singleChoiceItemLayout, 0);
        a.recycle();

        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorControlNormal, typedValue, true);
        int colorNormal = ContextCompat.getColor(context, typedValue.resourceId);

        colorNormal = ColorUtils.setAlphaComponent(colorNormal, 255);
        int colorDisabled = ColorUtils.setAlphaComponent(colorNormal, 96);;

        CustomListPreferenceV7.CheckedItemAdapter adapter =
                getListPreference().createListAdapter(getContext(), singleChoiceItemLayout, android.R.id.text1,
                        colorNormal, colorDisabled,
                        mEntries.toArray(new CustomListPreferenceV7.ListPreferenceItem[0]));

        builder.setSingleChoiceItems(adapter, mClickedDialogEntryIndex, (dialog, which) -> {
            mClickedDialogEntryIndex = which;

                    /*
                     * Clicking on an item simulates the positive button
                     * click, and dismisses the dialog.
                     */
            CustomListPreferenceDialogFragmentCompatV7.this.onClick(dialog,
                    DialogInterface.BUTTON_POSITIVE);
            dialog.dismiss();
        });

        /*
         * The typical interaction for list-based dialogs is to have
         * click-on-an-item dismiss the dialog instead of the user having to
         * press 'Ok'.
         */
        builder.setPositiveButton(null, null);
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        final CustomListPreferenceV7 preference = getListPreference();
        if (positiveResult && mClickedDialogEntryIndex >= 0) {
            String value = mEntries.get(mClickedDialogEntryIndex).entryValue;
            if (preference.callChangeListener(value)) {
                preference.setValue(value);
            }
        }
    }
}
