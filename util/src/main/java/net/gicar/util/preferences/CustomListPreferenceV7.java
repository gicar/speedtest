package net.gicar.util.preferences;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import net.gicar.util.R;

import java.io.Serializable;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleableRes;
import androidx.preference.ListPreference;

/** ListPreference providing customizations */
public class CustomListPreferenceV7 extends ListPreference {

    /** The summary value */
    protected String summary;

    public CustomListPreferenceV7(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        //Retrieve the Preference summary attribute since it's private in the Preference class.
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.Preference, defStyleAttr, defStyleRes);

        summary = typedArrayUtilsGetString(a, R.styleable.Preference_summary,
                R.styleable.Preference_android_summary);

        a.recycle();
    }

    public CustomListPreferenceV7(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public CustomListPreferenceV7(Context context, AttributeSet attrs) {
        this(context, attrs, typedArrayUtilsGetAttr(context, R.attr.dialogPreferenceStyle,
                android.R.attr.dialogPreferenceStyle));
    }

    public CustomListPreferenceV7(Context context) {
        this(context, null);
    }

    /**
     * @return The resource ID value in the {@code context} specified by {@code attr}. If it does
     * not exist, {@code fallbackAttr}.
     */
    public static int typedArrayUtilsGetAttr(@NonNull Context context, int attr, int fallbackAttr) {
        TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(attr, value, true);
        if (value.resourceId != 0) {
            return attr;
        }
        return fallbackAttr;
    }

    /**
     * @return a string value of {@code index}. If it does not exist, a string value of
     * {@code fallbackIndex}. If it still does not exist, {@code null}.
     */
    @Nullable
    private static String typedArrayUtilsGetString(@NonNull TypedArray a, @StyleableRes int index,
                                   @StyleableRes int fallbackIndex) {
        String val = a.getString(index);
        if (val == null) {
            val = a.getString(fallbackIndex);
        }
        return val;
    }

    @Override
    public void setSummary(CharSequence summary) {
        super.setSummary(summary);
        if (summary == null && this.summary != null) {
            this.summary = null;
        } else if (summary != null && !summary.equals(this.summary)) {
            this.summary = summary.toString();
        }
    }

    @Override
    public CharSequence getSummary() {
        if (getSummaryProvider() != null) {
            //noinspection unchecked
            return getSummaryProvider().provideSummary(this);
        }
        final CharSequence entry = getEntry();

        String summary = this.summary.replace("^u%s", "<u>%s</u>");
        return Html.fromHtml(String.format(summary, entry == null || TextUtils.isEmpty(entry) ?
                getContext().getString(R.string.not_set) : entry));
    }

    /**
     * Can be used to filter out entries from the list if needed
     * @param entryValue The entry value to be evaluated
     * @return true if it shall be presented, false to filter it out
     */
    protected boolean isEntryValid(CharSequence entryValue) { return true; }

    /**
     * Can be used to disable entries in the list if needed
     * @param entryValue The entry value to be evaluated
     * @return true if it enabled, false if it is disabled
     */
    protected boolean isEntryEnabled(CharSequence entryValue) { return true; }

    /**
     * Provides the ListAdapter. Override this method to provide your own which alters the views.
     * @param context The Context
     * @param resource The list resource item
     * @param textViewResourceId The id of the text resource in the list
     * @param colorNormal The color to be applied to enabled entries
     * @param colorDisabled The color to be applied to disabled entries
     * @param objects The array of items in the list
     * @return The adapter which will be used to show the list
     */
    protected CheckedItemAdapter createListAdapter(Context context, int resource, int textViewResourceId,
                                                   int colorNormal, int colorDisabled,
                                                   ListPreferenceItem[] objects)
    {
        return new CustomListPreferenceV7.CheckedItemAdapter(context, resource, textViewResourceId,
                colorNormal, colorDisabled,
                objects);
    }

    protected static class CheckedItemAdapter extends ArrayAdapter<ListPreferenceItem>
    {
        /** Colors for normal and disabled entries */
        private final int colorNormal, colorDisabled;

        public CheckedItemAdapter(Context context, int resource, int textViewResourceId,
                                  int colorNormal, int colorDisabled,
                                  ListPreferenceItem[] objects) {
            super(context, resource, textViewResourceId, objects);
            this.colorNormal = colorNormal;
            this.colorDisabled = colorDisabled;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return Objects.requireNonNull(getItem(position)).enabled;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent)
        {
            CheckedTextView v = (CheckedTextView) super.getView(position, convertView, parent);
            if (isEnabled(position)) v.setTextColor(colorNormal);
            else v.setTextColor(colorDisabled);
            return v;
        }
    }

    /** Preference item in the list */
    protected static class ListPreferenceItem implements Serializable {

        /** The entry */
        final String entry;

        /** The text of the entry */
        final String entryValue;

        /** true if the item is enabled */
        final boolean enabled;

        public ListPreferenceItem(String entry, String entryValue, boolean enabled) {
            this.entry = entry;
            this.entryValue = entryValue;
            this.enabled = enabled;
        }

        @NonNull
        @Override
        public String toString() {
            return entry;
        }
    }
}
