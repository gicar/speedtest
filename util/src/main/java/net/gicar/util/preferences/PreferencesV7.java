package net.gicar.util.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.preference.PreferenceManager;

/** Utility class for handling application preferences */
public abstract class PreferencesV7 extends Settings {

	/** XML holding the definition of the preferences */
	private final int xmlId;

	/**
	 * Creates a new preference handler 
	 * @param xmlId the ID of the XML of the preferences, from where the defaults will be got
	 */
	protected PreferencesV7(int xmlId)
	{
        super(null);
        this.xmlId = xmlId;
	}

    @Override
    protected SharedPreferences getSharedPreferences(Context context) {
        PreferenceManager.setDefaultValues(context, xmlId, true);
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    protected boolean updateVersion(Context context, int createVersion, int lastVersion, int currentVersion) {
        // this is the shared preference file, we cannot erase anything here
        return false;
    }
}
