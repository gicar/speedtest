package net.gicar.util.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import androidx.annotation.NonNull;
import net.gicar.util.Misc;

/** Base class for handling settings for an activity */
@SuppressWarnings("rawtypes")
public abstract class Settings implements SharedPreferences.OnSharedPreferenceChangeListener {

    /** The log tag */
    public final String TAG;

    /** Name of the preferences file */
    protected final String preferencesName;

    /** The setting items we are handling */
    private final HashMap<String, SettingsItem> settingItems = new HashMap<>();

    /** The registered observers */
    private final ArrayList<SettingsObserver> observers = new ArrayList<>();

    /** Saved versions before update */
    private int savedLastVersion;

    /** Signals if this is the first run of the application */
    private boolean firstRun;

    /** Signal if this is the first time the application is run with this version */
    private boolean firstVersionRun;

    /** The Context */
    private Context context;

    /** The settings */
    private SharedPreferences settings;

    /** If true there are modifications not yet saved */
    private boolean dirty;

    /**
     * Creates a Settings with the specified user items
     * @param preferencesName Name of the preferences file. Could be null if determined by other means
     */
    protected Settings(String preferencesName)
    {
        this.TAG = "Settings-" + preferencesName;
        this.preferencesName = preferencesName;
    }

    /**
     * Call this method on onCreate
     * @param context The Context
     */
    public final void onCreate(Context context)
    {
        if (Misc.DEBUG) Log.d(TAG, "onCreate " + this);

        // save the context
        this.context = context;

        // get our settings object
        settings = getSharedPreferences(context);

        // load all
        for (SettingsItem i : settingItems.values()) {
            i.loadDefaultValue(context);
            i.load();
            if (Misc.DEBUG) Log.d(TAG, "Loaded: " + i);
        }

        // handle versions
        handleVersions();

        // register as preferences change listener
        settings.registerOnSharedPreferenceChangeListener(this);
    }

    private void handleVersions()
    {
        // get the current version
        int currentVersion = Misc.getAppVersionNumber(context);

        // update data if needed
        boolean clearOnFirstVersionRun = updateVersion(context, createVersion.getValue(), lastVersion.getValue(), currentVersion);

        // save version values
        savedLastVersion = lastVersion.getValue();

        // check for no create version stored
        if (createVersion.getValue() == 0) {
            if (Misc.DEBUG)
                Log.i(TAG, "Setting the preferences create version to the current version: " + currentVersion);
            createVersion.setValue(currentVersion);
            firstRun = true;
        }

        // update the last run version to the current one if needed
        if (lastVersion.getValue() != currentVersion) {
            if (Misc.DEBUG)
                Log.i(TAG, "Updating the last run version to the current version: " + currentVersion);
            lastVersion.setValue(currentVersion);
            firstVersionRun = true;
        }

        // we could have something to commit from version handling, do it now;
        // on first version run this clears also leftovers from previous app versions, if required
        commit(true, clearOnFirstVersionRun && firstVersionRun);
    }

    /** Call this method on onPause (optional if commits are explicitly performed) */
    public final void onPause() {
        if (Misc.DEBUG) Log.d(TAG, "onPause " + this);

        // save pending writes
        commit();
    }

    /** Call this method on onDestroy (optional if class is hosted in Application) */
    public final void onDestroy() {
        if (Misc.DEBUG) Log.d(TAG, "onDestroy " + this);
        commit();
        settings.unregisterOnSharedPreferenceChangeListener(this);
    }

    /** Reset the setting to the default values (if present). Includes the commit */
    public void reset()
    {
        // reset all
        for (SettingsItem i : settingItems.values()) {
            i.reset();
            if (Misc.DEBUG) Log.d(TAG, "Reset: " + i);
        }

        // re-init the versions
        handleVersions();
    }

    /** Writes async pending data to the SharedPreference, clearing unregistered settings */
    public final void commit()
    {
        commit(true, false);
    }

    /**
     * Writes pending data to the SharedPreference
     * @param async The write can be performed async
     * @param clear The unregistered settings will be removed
     */
    public final void commit(boolean async, boolean clear)
    {
        // save current values if dirty
        if (dirty) {
            if (Misc.DEBUG) Log.i(TAG, "Saving values");
            SharedPreferences.Editor editor = settings.edit();
            if (clear) editor.clear();
            for (SettingsItem i : settingItems.values()) {
                if (i.getValue() != null) i.save(editor);
                else if (!clear) editor.remove(i.getTag());
            }
            if (async) editor.apply();
            else editor.commit();
            dirty = false;
        }
    }

    /**
     * Returns the configured SettingItems in this Settings object
     * @return List of SettingItems
     */
    public Collection<SettingsItem> getSettingItems() {
        return settingItems.values();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        if (Misc.DEBUG) Log.d(TAG, "onSharedPreferenceChanged: " + key);

        // reload changed preference
        SettingsItem i = settingItems.get(key);
        if (i != null) {
            Object v1 = i.getValue();
            i.load();
            Object v2 = i.getValue();
            if (v1 != v2 && (v1 == null || !v1.equals(v2))) {
                if (Misc.DEBUG) Log.d(TAG, "Changed: " + i);
                for (SettingsObserver o : observers) o.settingChange(i);
            }
        }
    }

    /** Getter method for the context */
    protected Context getContext() { return context; }

    /**
     * Override this method for providing specific preferences or to set default values
     * @param context The application Context
     * @return The SharedPreferences object to be used
     */
    protected SharedPreferences getSharedPreferences(Context context)
    {
        return context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE);
    }

    /**
     * Override this method to provide update functionality when first running the app on a new version
     * @param context Our Context
     * @param createVersion The first version of the app that was run, or 0 if none
     * @param lastVersion The last version of the app that was run, or 0 if none
     * @param currentVersion The current app version
     * @return If true on version change the preference items no longer present will be deleted.
     *         This should not be performed on the the default shared preference, as some library
     *         could store its data there.
     */
    protected boolean updateVersion(Context context, int createVersion, int lastVersion, int currentVersion) {
        return true;
    }

    /**
     * Returns true if this is the first time that this app is run.
     * @return <code>true</code> if this is the first time the application is run
     */
    public final boolean isFirstRun() {
        return firstRun;
    }

    /**
     * Returns true if this is the first time that this app is run with the current version
     * @return <code>true</code> if the version changed from the last application run
     */
    public final boolean isFirstVersionRun() {
        return firstVersionRun;
    }

    /**
     * Returns the last version of the app
     * @return The last version of the app run from these preferences
     */
    public final int getLastVersion() {
        return savedLastVersion;
    }

    /** The version in which these settings were first stored */
    private final SettingsItemInteger createVersion = new SettingsItemInteger("settings_create_version", 0);

    /** The last version in which these settings were stored */
    private final SettingsItemInteger lastVersion = new SettingsItemInteger("settings_last_version", 0);

    /**
     * Registers an observer for the settings
     * @param observer The observer to add
     */
    public final void registerObserver(SettingsObserver observer)
    {
        observers.add(observer);
    }

    /**
     * Unregisters an observer for the settings
     * @param observer The observer to remove
     */
    public final void unregisterObserver(SettingsObserver observer)
    {
        observers.remove(observer);
    }

    /** Interface for getting notified in changes of the settings */
    public interface SettingsObserver {

        /**
         * Signals a change in a setting
         * @param settingsItem The the setting which changed
         */
        void settingChange(SettingsItem settingsItem);
    }

    /** Generic settings item */
    public abstract class SettingsItem<T> {

        /** The tag used for saving it */
        protected final String tag;

        /** The default value */
        protected T defaultValue;

        /** The current value */
        protected volatile T value;

        protected SettingsItem(String tag) {
            this(tag, null);
        }

        protected SettingsItem(String tag, T defaultValue) {
            this.tag = tag;
            this.defaultValue = defaultValue;
            if (settingItems.put(tag, this) != null)
                throw new RuntimeException("Already mapped preference " + tag + " in " + preferencesName);
        }

        public final String getTag() {
            return tag;
        }

        public final T getValue() {
            return value;
        }

        public final void setValue(T value) {
            if (value == this.value || (value != null && value.equals(this.value))) return;
            if (Misc.DEBUG) Log.i(TAG, "Setting: " + tag + " = " + value);
            this.value = value;
            setDirty();
            for (SettingsObserver o : observers) o.settingChange(this);
        }

        public final void reset() {
            setValue(defaultValue);
        }

        public final Settings getSettings() { return Settings.this; }

        protected void loadDefaultValue(Context context) {}

        abstract protected void load();
        abstract protected void save(SharedPreferences.Editor editor);

        protected void remove(SharedPreferences.Editor editor) {
            editor.remove(tag);
        }

        protected void setDirty() { dirty = true; }

        @NonNull
        @Override
        public String toString() {
            return "SettingsItem{" +
                    "tag='" + tag + '\'' +
                    ", value=" + value +
                    '}';
        }
    }

    /** A Boolean settings item */
    public class SettingsItemBoolean extends SettingsItem<Boolean> {
        public SettingsItemBoolean(String tag) {
            super(tag);
        }
        public SettingsItemBoolean(String tag, Boolean defaultValue) {
            super(tag, defaultValue);
        }
        @Override
        protected void load() {
            value = getBoolean(tag, defaultValue);
        }
        @Override
        protected void save(SharedPreferences.Editor editor) {
            editor.putBoolean(tag, value);
        }
    }

    /** An Integer settings item */
    public class SettingsItemInteger extends SettingsItem<Integer> {
        public SettingsItemInteger(String tag) {
            super(tag);
        }
        public SettingsItemInteger(String tag, Integer defaultValue) {
            super(tag, defaultValue);
        }
        @Override
        protected void load() {
            value = getInt(tag, defaultValue);
        }
        @Override
        protected void save(SharedPreferences.Editor editor) {
            editor.putInt(tag, value);
        }
    }

    /** A Long settings item */
    public class SettingsItemLong extends SettingsItem<Long> {
        public SettingsItemLong(String tag) {
            super(tag);
        }
        public SettingsItemLong(String tag, Long defaultValue) {
            super(tag, defaultValue);
        }
        @Override
        protected void load() {
            value = getLong(tag, defaultValue);
        }
        @Override
        protected void save(SharedPreferences.Editor editor) {
            editor.putLong(tag, value);
        }
    }

    /** A String settings item */
    public class SettingsItemString extends SettingsItem<String> {
        public SettingsItemString(String tag) {
            super(tag);
        }
        public SettingsItemString(String tag, String defaultValue) {
            super(tag, defaultValue);
        }
        @Override
        protected void load() {
            value = getString(tag, defaultValue);
        }
        @Override
        protected void save(SharedPreferences.Editor editor) {
            editor.putString(tag, value);
        }
    }

    /** An Integer stored in a String settings item (use it for list preferences) */
    public class SettingsItemStringInteger extends SettingsItem<Integer> {
        public SettingsItemStringInteger(String tag) {
            super(tag);
        }
        public SettingsItemStringInteger(String tag, Integer defaultValue) {
            super(tag, defaultValue);
        }
        @Override
        protected void load() {
            value = getStringInt(tag, defaultValue);
        }
        @Override
        protected void save(SharedPreferences.Editor editor) {
            editor.putString(tag, value == null ? null : value.toString());
        }
    }

    /** A String set settings item */
    public class SettingsItemStringSet extends SettingsItem<Set<String>> {
        public SettingsItemStringSet(String tag) {
            super(tag);
        }
        public SettingsItemStringSet(String tag, Set<String> defaultValue) {
            super(tag, defaultValue);
        }
        @Override
        protected void load() {
            value = getStringSet(tag, defaultValue);
        }
        @Override
        protected void save(SharedPreferences.Editor editor) {
            editor.putStringSet(tag, value);
        }
    }

    /** A int array settings item */
    public class SettingsItemIntArray extends SettingsItem<int[]> {
        public SettingsItemIntArray(String tag) {
            super(tag);
        }
        public SettingsItemIntArray(String tag, int[] defaultValue) {
            super(tag, defaultValue);
        }
        @Override
        protected void load() {
            value = getIntArray(tag, defaultValue);
        }
        @Override
        protected void save(SharedPreferences.Editor editor) {
            editor.putString(tag, convertToString(value));
        }
    }

    /** A generic numeric array list settings item */
    protected abstract class SettingsItemArrayList<T extends Number> extends SettingsItem<ArrayList<T>> {
        public SettingsItemArrayList(String tag) {
            super(tag);
        }
        public SettingsItemArrayList(String tag, ArrayList<T> defaultValue) {
            super(tag, defaultValue);
        }

        @Override
        protected void load()
        {
            if (!settings.contains(tag)) value = defaultValue;
            else {
                String s = settings.getString(tag, null);
                if (s == null) value = null;
                else {
                    try {
                        value = new ArrayList<>();
                        if (s.length() != 0) {
                            String[] sp = s.split(" ");
                            for (String spi : sp) value.add(convert(spi));
                        }
                    }
                    catch (Exception e) {
                        value = defaultValue;
                    }
                }
            }
        }

        protected abstract T convert(String s);

        @Override
        protected void save(SharedPreferences.Editor editor)
        {
            StringBuilder s  = new StringBuilder();
            boolean comma = false;
            for (T i : value) {
                if (comma) s.append(" ");
                else comma = true;
                s.append(i);
            }
            editor.putString(tag, s.toString());
        }
    }

    /** A long array list settings item */
    public class SettingsItemArrayListLong extends SettingsItemArrayList<Long> {
        public SettingsItemArrayListLong(String tag) { super(tag); }
        public SettingsItemArrayListLong(String tag, ArrayList<Long> defaultValue) {
            super(tag, defaultValue);
        }

        @Override
        protected Long convert(String s) {
            return Long.parseLong(s);
        }
    }

    /**
     * Read a String value
     * @param key The key
     * @param defaultValue The default value
     * @return The value associated with the key or default one if not present/error
     */
    protected final String getString(String key, String defaultValue)
    {
        try {
            if (!settings.contains(key)) return defaultValue;
            return settings.getString(key, null);
        } catch (Exception ignored) {}
        return defaultValue;
    }

    /**
     * Read a String set value
     * @param key The key
     * @param defaultValue The default value
     * @return The value associated with the key or default one if not present/error
     */
    protected final Set<String> getStringSet(String key, Set<String> defaultValue)
    {
        try {
            if (!settings.contains(key)) return defaultValue;
            return settings.getStringSet(key, null);
        } catch (Exception ignored) {}
        return defaultValue;
    }

    /**
     * Read a Boolean value
     * @param key The key
     * @param defaultValue The default value
     * @return The value associated with the key or default one if not present/error
     */
    protected final Boolean getBoolean(String key, Boolean defaultValue)
    {
        try {
            if (!settings.contains(key)) return defaultValue;
            return settings.getBoolean(key, false);
        } catch (Exception ignored) {}
        return defaultValue;
    }

    /**
     * Read a Integer value
     * @param key The key
     * @param defaultValue The default value
     * @return The value associated with the key or default one if not present/error
     */
    protected final Integer getInt(String key, Integer defaultValue)
    {
        try {
            if (!settings.contains(key)) return defaultValue;
            return settings.getInt(key, 0);
        } catch (Exception ignored) {}
        return defaultValue;
    }

    /**
     * Read a Long value
     * @param key The key
     * @param defaultValue The default value
     * @return The value associated with the key or default one if not present/error
     */
    protected final Long getLong(String key, Long defaultValue)
    {
        try {
            if (!settings.contains(key)) return defaultValue;
            return settings.getLong(key, 0);
        } catch (Exception ignored) {}
        return defaultValue;
    }

    /**
     * Read a Integer value stored in a String entry
     * @param key The key
     * @param defaultValue The default value
     * @return The value associated with the key or default one if not present/error
     */
    protected final Integer getStringInt(String key, Integer defaultValue)
    {
        try {
            if (!settings.contains(key)) return defaultValue;
            String s = settings.getString(key, null);
            return s == null ? null : Integer.valueOf(s);
        } catch (Exception ignored) {}
        return defaultValue;
    }

    /**
     * Read a int[] value stored in a String entry
     * @param key The key
     * @param defaultValue The default value
     * @return The value associated with the key or default one if not present/error
     */
    protected final int[] getIntArray(String key, int[] defaultValue)
    {
        try {
            if (!settings.contains(key)) return defaultValue;
            String s = settings.getString(key, null);
            if (s == null) return null;
            if (s.length() == 0) return new int[0];
            String[] sp = s.split(" ");
            int[] value = new int[sp.length];
            int i = 0;
            for (String spi : sp) value[i++] = Integer.parseInt(spi);
            return value;
        } catch (Exception ignored) {}
        return defaultValue;
    }

    /**
     * Converter to string
     * @param value The value to be converted
     * @return The value stored in a string
     */
    protected final String convertToString(int[] value)
    {
        StringBuilder s  = new StringBuilder();
        boolean comma = false;
        for (int i : value) {
            if (comma) s.append(" ");
            else comma = true;
            s.append(i);
        }
        return s.toString();
    }
}
