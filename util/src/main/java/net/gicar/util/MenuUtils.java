package net.gicar.util;

import android.view.Menu;
import android.view.MenuItem;

/** Menu related utilities */
public class MenuUtils {

    /**
     * Finds a menu entries and sets both its Active and Visible status
     * @param menu The Menu where to find the item
     * @param id The id of the menu item
     * @param status Its new active and visible status
     * @return The MenuItem
     */
    public static MenuItem setMenuItemActiveVisible(Menu menu, int id, boolean status)
    {
        MenuItem item = menu.findItem(id);
        item.setEnabled(status);
        item.setVisible(status);
        return item;
    }
    /**
     * Finds a menu entries and sets its Active and Visible status
     * @param menu The Menu where to find the item
     * @param id The id of the menu item
     * @param active Its new active status
     * @param visible Its new visible status
     * @return The MenuItem
     */
    public static MenuItem setMenuItemActiveVisible(Menu menu, int id, boolean active, boolean visible)
    {
        MenuItem item = menu.findItem(id);
        item.setEnabled(active);
        item.setVisible(visible);
        return item;
    }
}
