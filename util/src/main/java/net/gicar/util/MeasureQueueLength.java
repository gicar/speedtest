package net.gicar.util;

import android.util.Log;

/** Measures the execution time */
public class MeasureQueueLength {

    /** The log tag */
    private final String tag;

    /** The log message */
    private final String message;

    /** Number of seconds to wait between logs */
    private final int logSecondsDelay;

    /** Minimum number of samples to be collected to perform logging */
    private final int minSamplesToLog;

    /** Next output time */
    private long tOut;

    /** Min and max */
    private int min = -1, max = -1;

    /** Sum of samples */
    private long s;

    /** Number of samples */
    private int n;

    public MeasureQueueLength(String tag, String message)
    {
        this(tag, message, 0, 0);
    }

    public MeasureQueueLength(String tag, String message, int logSecondsDelay, int minSamplesToLog) {
        this.tag = tag;
        this.message = message;
        this.logSecondsDelay = logSecondsDelay;
        this.minSamplesToLog = minSamplesToLog;
        if (Misc.DEBUG) tOut = System.currentTimeMillis() + 1000 * logSecondsDelay;
    }

    /** Loges this measurement for the queue size */
    public void sample(int x)
    {
        // update sum, min and max
        s += x;
        if (min == -1 || x < min) min = x;
        if (max == -1 || x > max) max = x;
        n++;

        // produce output if in debug mode
        if (Misc.DEBUG) {
            long t = System.currentTimeMillis();
            if (t >= tOut && n >= minSamplesToLog) {
                tOut = System.currentTimeMillis() + 1000 * logSecondsDelay;
                Log.d(tag, message + " " + toString());
                s = n = 0;
                min = max = -1;
            }
        }
    }

    @Override
    public String toString() {
        return "MeasureQueueLength{" +
                "min=" + min +
                ", max=" + max +
                ", avg=" + ((float)s / n) +
                ", n=" + n +
                '}';
    }
}
