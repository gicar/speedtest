package net.gicar.util;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewConfiguration;

import java.lang.reflect.Method;

/** Wrapper class for methods which could not be supported */
public class Reflect {
	
	// methods
	private static Method view_setLayerType;
	private static Method viewConfiguration_hasPermanentMenuKey;

	static {
		try {
			view_setLayerType = View.class.getMethod("setLayerType", int.class, Paint.class);
		} catch (NoSuchMethodException nsme) { /* failure, must be older device */ }
		try {
			viewConfiguration_hasPermanentMenuKey = ViewConfiguration.class.getMethod("hasPermanentMenuKey");
		} catch (NoSuchMethodException nsme) { /* failure, must be older device */ }
	}

	public static void viewSetLayerType(View v, int layerType, Paint paint)
	{
		// if unsupported operation just return
		if (view_setLayerType == null) return;
		
		try {
			view_setLayerType.invoke(v, Integer.valueOf(layerType), paint);
		} catch (Exception e) {}
	}

	public static boolean viewConfigurationHasPermanentMenuKey(ViewConfiguration v)
	{
		// if the operation is supported try to invoke it
		if (viewConfiguration_hasPermanentMenuKey != null) {
			try {
				Object value = viewConfiguration_hasPermanentMenuKey.invoke(v);
				return ((Boolean)value).booleanValue();
			} catch (Exception e) {}
		}
		
		// fallback solution: look at the Android version
		return Misc.getOsVersionNumber() <= 10;
	}
}