package net.gicar.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.core.net.ConnectivityManagerCompat;

import static net.gicar.util.Network.NetworkAllowedUsage.FREE;
import static net.gicar.util.Network.NetworkAllowedUsage.METERED;
import static net.gicar.util.Network.NetworkAllowedUsage.RESTRICTED;

/**
 * Network utilities
 */
public class Network {

    /**
     * Verifies if the Internet connection is currently using the WiFi
     * @param context The context to be used
     * @return <code>true</code> if the Internet connection is using the WiFi interface
     */
    public static boolean wifiConnected(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null)
            networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return networkInfo != null && networkInfo.isConnected();
    }

    /**
     * Verifies if the Internet connection is currently available
     * @param context The context to be used
     * @return <code>true</code> if the Internet connection is available
     */
    public static boolean isAvailable(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /** Allowed network usage by this app */
    public enum NetworkAllowedUsage {
        /** No limit imposed */
        FREE,
        /** Connection is metered, limit its usage */
        METERED,
        /** Background usage has been restricted */
        RESTRICTED
    }

    /**
     * Checks and returns the current allowed network usage
     * @param context The context to be used
     * @return Allowed network usage by this app
     */
    public static NetworkAllowedUsage getNetworkAllowedUsage(Context context)
    {
        // check how much data we are allowed to consume
        ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (ConnectivityManagerCompat.isActiveNetworkMetered(connMgr)) {
            switch (ConnectivityManagerCompat.getRestrictBackgroundStatus(connMgr)) {
                case ConnectivityManagerCompat.RESTRICT_BACKGROUND_STATUS_ENABLED:
                case ConnectivityManagerCompat.RESTRICT_BACKGROUND_STATUS_WHITELISTED:
                    return METERED;
                case ConnectivityManagerCompat.RESTRICT_BACKGROUND_STATUS_DISABLED:
                default:
                    return RESTRICTED;
            }
        }
        return FREE;
    }
}
