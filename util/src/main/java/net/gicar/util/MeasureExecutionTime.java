package net.gicar.util;

import android.util.Log;

/** Measures the execution time */
public class MeasureExecutionTime {

    /** One second in nanos */
    private final static long ONE_SECOND = 1_000_000_000;

    /** The log tag */
    private final String tag;

    /** The log message */
    private final String message;

    /** Number of seconds to wait between logs */
    private final int logSecondsDelay;

    /** Minimum number of samples to be collected to perform logging */
    private final int minSamplesToLog;

    /** Next output time */
    private long tOut;

    /** Start measurement time */
    private long t1;

    /** Sum of execution times */
    private long t;

    /** Number of samples */
    private int n;

    public MeasureExecutionTime(String tag, String message)
    {
        this(tag, message, 0, 0);
    }

    public MeasureExecutionTime(String tag, String message, int logSecondsDelay, int minSamplesToLog) {
        this.tag = tag;
        this.message = message;
        this.logSecondsDelay = logSecondsDelay;
        this.minSamplesToLog = minSamplesToLog;
        if (Misc.DEBUG) tOut = System.nanoTime() + ONE_SECOND * logSecondsDelay;
    }

    /** Measurement start */
    public MeasureExecutionTime start()
    {
        // save the start time
        t1 = System.nanoTime();

        return this;
    }

    /** Measurement end */
    public long end()
    {
        // update the total time
        long t2 = System.nanoTime();
        long tt = t2 - t1;
        t += tt;
        n++;

        // check if we need to output
        if (Misc.DEBUG && t2 >= tOut) {
            tOut = t2 + ONE_SECOND * logSecondsDelay;
            if (minSamplesToLog == 0)
                Log.d(tag, message + " t[ms]=" + (getAverageTime() / 1_000_000));
            else if (n >= minSamplesToLog)
                Log.d(tag, message + " n=" + n + ", avg[ms]=" + (getAverageTime() / 1_000_000));
        }

        return tt;
    }

    /**
     * Returns and clears the average execution time
     * @return The average execution time in ns
     */
    private float getAverageTime()
    {
        //noinspection IntegerDivisionInFloatingPointContext
        float a = t / n;
        t = 0;
        n = 0;

        return a;
    }
}
