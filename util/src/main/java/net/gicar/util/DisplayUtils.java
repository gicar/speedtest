package net.gicar.util;

import android.app.Activity;
import android.app.UiModeManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/** Display related utilities */
public class DisplayUtils {

    /**
     * Returns if the application is running in landscape mode
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if the application is running in landscape mode
     */
    public static boolean isLandscape(Context context)
    {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    /**
     * Returns the current display metrics
     * @param context The <code>Context</code> to be used for the operation
     * @return The display metrics
     */
    public static DisplayMetrics getDisplayMetrics(Context context)
    {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    /**
     * Returns the display size (with the current rotation) in pixels
     * @param context The <code>Context</code> to be used for the operation
     * @return The current display size in pixels
     */
    @SuppressWarnings("deprecation")
    public static Point getDisplaySizePx(Context context)
    {
        android.view.Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        return new Point(display.getWidth(), display.getHeight());
    }

    /**
     * Returns the display size (with the current rotation) in dp
     * @param context The <code>Context</code> to be used for the operation
     * @return The current display size in dp
     */
    @SuppressWarnings("deprecation")
    public static PointF getDisplaySizeDp(Context context)
    {
        DisplayMetrics metrics = new DisplayMetrics();
        android.view.Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        display.getMetrics(metrics);
        return new PointF(display.getWidth() / metrics.density, display.getHeight() / metrics.density);
    }

    /**
     * Returns the screen size (with the current rotation) in dp
     * @param context The <code>Context</code> to be used for the operation
     * @return The current display size in dp
     */
    public static Point getScreenSizeDp(Context context)
    {
        Configuration config = context.getResources().getConfiguration();
        return new Point(config.screenWidthDp, config.screenHeightDp);
    }

    /**
     * Sets the activity window in full screen depending on the flags
     * @param activity The <code>Activity</code> to be used for the operation
     * @param fullScreen If true the full screen will be set
     */
    public static void setFullScreen(Activity activity, boolean fullScreen)
    {
        activity.getWindow().setFlags(fullScreen ? WindowManager.LayoutParams.FLAG_FULLSCREEN : 0,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * Sets the activity window in full screen depending on the flags
     * @param activity The <code>Activity</code> to be used for the operation
     * @param fullScreenOnPortrait If true and we are in portrait mode the full screen will be set
     * @param fullScreenOnLandscape If true and we are in landscape mode the full screen will be set
     */
    public static void setFullScreen(Activity activity, boolean fullScreenOnPortrait, boolean fullScreenOnLandscape)
    {
        boolean fullScreen = (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ?
                fullScreenOnLandscape : fullScreenOnPortrait);
        setFullScreen(activity, fullScreen);
    }

    /**
     * Checks if we are running on a television
     * @param context The <code>Context</code> to be used for the operation
     * @return true if we are running on a TV, false if not or if we cannot detect this condition
     */
    public static boolean isTelevision(Context context)
    {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) return false;
        UiModeManager uiModeManager = (UiModeManager) context.getSystemService(Context.UI_MODE_SERVICE);
        return uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION;
    }
}
