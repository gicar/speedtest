package net.gicar.util;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

/** A MutableLiveData that does not allow null values */
public class MutableNonNullLiveData<T> extends MutableLiveData<T> {

    public MutableNonNullLiveData(@NonNull T value) {
        super(value);
    }

    @Override
    public void postValue(@NonNull T value) {
        super.postValue(value);
    }

    @Override
    public void setValue(@NonNull T value) {
        super.setValue(value);
    }

    @NonNull
    @Override
    public T getValue() {
        T value = super.getValue();
        assert value != null;
        return value;
    }
}
