package net.gicar.util.gdpr;

import android.app.Application;

/** Notifier interface for data collectors */
public interface GdprDataCollector {

    /**
     * Notifies changes in the data collection enabling. Users shall start disabled.
     * @param application Our application
     * @param canCollectData If true user data can be collected
     */
    void gdprDataCollection(Application application, boolean canCollectData);
}
