package net.gicar.util.recyclerlist;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/** ViewHolder for RecyclerList */
@SuppressWarnings("rawtypes")
public class RecyclerListViewHolder extends RecyclerView.ViewHolder {

    /** Touch listener for drag views */
    @SuppressLint("ClickableViewAccessibility")
    private final static View.OnTouchListener ON_TOUCH_LISTENER = (v, event) -> {
        // check for action down
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {

            // find our recycler view and start dragging
            RecyclerList.ViewItem viewItem = RecyclerList.getRecyclerViewItem(v);
            if (viewItem == null) return false;
            viewItem.recyclerList.recyclerListAdapter.startDrag(viewItem.viewHolder);
        }

        // go with usual handling
        return false;
    };

    /** The flags of the view */
    int flags;

    public RecyclerListViewHolder(View itemView) {
        super(itemView);
    }

    /**
     * Starts drag when the specified view if touched
     * @param view The View which will start drag
     */
    protected void setDragOnTouch(View view)
    {
        view.setOnTouchListener(ON_TOUCH_LISTENER);
    }

    /**
     * Searches the adapter to which this ViewHolder belongs to
     * @return The adapter of null if for some reason it wasn't found
     */
    protected RecyclerListAdapter getAdapter()
    {
        RecyclerList.ViewItem viewItem = RecyclerList.getRecyclerViewItem(itemView);
        if (viewItem == null) return null;
        return viewItem.recyclerList.recyclerListAdapter;
    }

    /**
     * Sends the specified event
     * @param event The event to be sent to the listener
     */
    public void sendEvent(Object event)
    {
        // send the event
        RecyclerListAdapter adapter = getAdapter();
        if (adapter == null) return;
        adapter.getListener().onViewHolderEvent(getAdapterPosition(), event);
    }
}
