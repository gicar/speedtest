package net.gicar.util.recyclerlist;

import androidx.appcompat.view.ActionMode;

/** Notifies events on items in a Action Mode RecyclerList */
public interface ActionModeRecyclerListListener extends RecyclerListListener {

    /**
     * Signals the start of the action mode
     * @return The ActionMode object
     */
    ActionMode onStartActionMode();

    /**
     * Called when an item is selected or unchecked during selection mode.
     *
     * @param position Adapter position of the item that was selected or not
     * @param selected <code>true</code> if the item is now selected, <code>false</code>
     *                if the item is not selected.
     */
    void onItemSelectedStateChanged(int position, boolean selected);

    /** Called when more than one item changed its selected state */
    void onItemsSelectedStateChanged();
}
