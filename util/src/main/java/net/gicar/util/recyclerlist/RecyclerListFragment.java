package net.gicar.util.recyclerlist;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.gicar.util.Misc;
import net.gicar.util.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Fragment similar to ListFragment but working with RecyclerViews.
 * Rules for its usage:<ul>
 * <li>RecyclerView initialization must happen in onActivityCreated() after having called the super.</li>
 * <li>This fragment will set the listener to the adapter to itself.</li>
 * </ul>
 */
@SuppressWarnings("rawtypes")
public abstract class RecyclerListFragment extends Fragment implements RecyclerListListener {

    /** The log tag */
    static public String TAG = "RecyclerListFragment";

    /** The save instance keys */
    private final static String POSITION_KEY = "net.gicar.util.POSITION";

    /** The RecyclerList */
    private RecyclerList recyclerList;

    /** The optional view which will be shown if the list is empty */
    private View emptyView;

    /** If true the data observer has been registered */
    private boolean dataObserverRegistered;

    /** If true the list is empty */
    private Boolean empty;

    private final RecyclerView.AdapterDataObserver observer = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            if (Misc.DEBUG) Log.d(TAG, "AdapterDataObserver onChanged");
            checkEmpty();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            if (Misc.DEBUG) Log.d(TAG, "AdapterDataObserver onItemRangeChanged " + positionStart + "," + itemCount);
            checkEmpty();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            if (Misc.DEBUG) Log.d(TAG, "AdapterDataObserver onItemRangeChanged " + positionStart + "," + itemCount + "," + payload);
            checkEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            if (Misc.DEBUG) Log.d(TAG, "AdapterDataObserver onItemRangeInserted " + positionStart + "," + itemCount);
            checkEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            if (Misc.DEBUG) Log.d(TAG, "AdapterDataObserver onItemRangeRemoved " + positionStart + "," + itemCount);
            checkEmpty();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            if (Misc.DEBUG) Log.d(TAG, "AdapterDataObserver onItemRangeMoved " + fromPosition + "," + toPosition + "," + itemCount);
            checkEmpty();
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // inflate the default main fragment view
        return inflater.inflate(R.layout.container_recyclerlist_default, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // find our views
        recyclerList = view.findViewById(android.R.id.list);
        emptyView = view.findViewById(android.R.id.empty);
    }

    @Override
    public void onStart() {
        super.onStart();

        // don't check for emptiness until the first update happens
        if (empty != null) checkEmpty();

        // register our observer if not yet done
        if (!dataObserverRegistered) {
            RecyclerView.Adapter adapter = recyclerList.getAdapter();
            assert adapter != null;
            adapter.registerAdapterDataObserver(observer);
            dataObserverRegistered = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        // unregister our observer
        RecyclerView.Adapter adapter = recyclerList.getAdapter();
        assert adapter != null;
        adapter.unregisterAdapterDataObserver(observer);
        dataObserverRegistered = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // needed to trigger the onDetachedFromRecyclerView() in the adapter
        recyclerList.setAdapter((RecyclerListAdapter)null);
    }

    /** Makes the empty view visible if needed */
    private void checkEmpty()
    {
        RecyclerView.Adapter adapter = recyclerList.getAdapter();
        assert adapter != null;
        boolean e = adapter.getItemCount() == 0;
        if (empty != null && empty == e) return;
        empty = e;
        if (emptyView != null)
            emptyView.setVisibility(empty ? View.VISIBLE : View.GONE);
    }

    /**
     * Getter method for the RecyclerList of this fragment
     * @return The RecyclerList
     */
    public RecyclerList getRecyclerList() {
        return recyclerList;
    }

    /**
     * Getter method for the empty view of this fragment
     * @return The RecyclerView
     */
    public View getEmptyView() {
        return emptyView;
    }

    /**
     * Sets the text of the empty view, if any and it possible
     * @param text The text to be set
     */
    public void setEmptyText(CharSequence text) {
        if (emptyView instanceof TextView) ((TextView)emptyView).setText(text);
    }

    /**
     * Sets the LayoutManager for the list
     * @param layout The LayoutManager which will be used
     */
    protected void setRecyclerListLayoutManager(RecyclerView.LayoutManager layout)
    {
        recyclerList.setLayoutManager(layout);
    }

    /**
     * Sets the adapter for the list. Note that the listener will be overridden.
     * @param adapter The adapter which will be used
     */
    protected void setRecyclerListAdapter(RecyclerListAdapter adapter)
    {
        adapter.setListener(this);
        recyclerList.setAdapter(adapter);

        // register as soon as possible to get initialization trigger
        adapter.getRecyclerViewAdapter().registerAdapterDataObserver(observer);
        dataObserverRegistered = true;
    }

    protected void addRecyclerListItemDecoration(RecyclerView.ItemDecoration decoration)
    {
        recyclerList.addItemDecoration(decoration);
    }

    @Override
    public void onItemClicked(int position) {}

    @Override
    public void onItemLongClicked(int position) {}

    @Override
    public void onDragged(int source, int target) {}

    @Override
    public boolean onSwiped(int position, boolean left) {
        return false;
    }

    @Override
    public void onViewHolderEvent(int position, Object event) {}
}
