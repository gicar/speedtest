package net.gicar.util.recyclerlist;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_DRAG;
import static androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_SWIPE;
import static androidx.recyclerview.widget.ItemTouchHelper.LEFT;

/**
 * The base class which shall be used as Adapter in lists of RecyclerViews.
 * @param <I> The object type of the items
 * @param <VH> The object type of the view holder
 */
@SuppressWarnings("rawtypes")
public abstract class RecyclerListAdapter<I, VH extends RecyclerListViewHolder> extends RecyclerListAdapterBase<I>
        implements View.OnClickListener, View.OnLongClickListener {

    /** Our Context */
    protected final Context context;

    /** Cached inflater */
    protected final LayoutInflater inflater;

    /** Our RecyclerList */
    private RecyclerList recyclerList;

    /** The item events listener */
    private RecyclerListListener listener;

    /** The background colors used during actions */
    private final ColorDrawable dragBackgroundColor, swipeBackgroundColor;

    /** The colors used to highlight swipes */
    private final Integer swipeLeftColor, swipeRightColor;

    /** The icons used in swiping */
    private final Drawable swipeLeftIcon, swipeRightIcon;

    /** The available flags */
    int flagMask = -1;

    /** The Adapter for the RecyclerView */
    private final RecyclerView.Adapter<VH> adapter = new RecyclerView.Adapter<VH>() {

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return RecyclerListAdapter.this.onCreateViewHolder(parent, viewType);
        }

        @Override
        public void onBindViewHolder(VH holder, int position)
        {
            // call the binding
            holder.flags = RecyclerListAdapter.this.onBindViewHolder(holder, position) & flagMask;

            // adjust for clickable properties
            boolean clickable = (holder.flags & ITEM_FLAG_CLICK) != 0;
            boolean longClickable = (holder.flags & ITEM_FLAG_LONG_CLICK) != 0;
            holder.itemView.setClickable(clickable | longClickable);
            holder.itemView.setFocusable(clickable | longClickable);
            holder.itemView.setOnClickListener(clickable ? RecyclerListAdapter.this : null);
            holder.itemView.setOnLongClickListener(longClickable ? RecyclerListAdapter.this : null);
        }

        @Override
        public void onViewRecycled(VH holder) {
            // remove listeners as they leak references to the adapter
            holder.itemView.setOnClickListener(null);
            holder.itemView.setOnLongClickListener(null);
            RecyclerListAdapter.this.onViewRecycled(holder);
        }

        @Override
        public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
            // set the recyclerList
            if (RecyclerListAdapter.this.recyclerList != null)
                throw new RuntimeException("Using a RecyclerListAdapter with more than one RecyclerList is currently unsupported");
            RecyclerList recyclerList = (RecyclerList) recyclerView;
            RecyclerListAdapter.this.recyclerList = recyclerList;

            // attach our touch helper
            touchHelper = new ItemTouchHelper(touchCallback);
            touchHelper.attachToRecyclerView(recyclerList);
        }

        @Override
        public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
            // clear the recyclerList and the touch helper
            RecyclerListAdapter.this.recyclerList = null;
            touchHelper.attachToRecyclerView(null);

            RecyclerListAdapter.this.onDestroy();
        }

        @Override
        public int getItemCount() {
            return RecyclerListAdapter.this.getItemCount();
        }

        @Override
        public long getItemId(int position) {
            return RecyclerListAdapter.this.getItemId(position);
        }

        @Override
        public int getItemViewType(int position) {
            return RecyclerListAdapter.this.getItemViewType(position);
        }
    };

    @Override
    RecyclerView getRecyclerView() { return recyclerList; }

    @Override
    RecyclerView.Adapter getRecyclerViewAdapter() { return adapter; }

    /** Our callback for the ItemTouchHelper */
    @SuppressWarnings("unchecked")
    private class TouchCallback extends ItemTouchHelper.Callback {

        /** The action */
        private int action = -1;

        /** Currently selected ViewHolder */
        private VH selected;

        /** The drag positions */
        private int dragSource = -1, dragTarget = -1;

        @Override
        public boolean isLongPressDragEnabled() {
            // we need to override automatic drag on long press as it seems to be faulty: if a view
            // declares to disallow parents to get click events, dragging will start anyway.
            // we will start dragging on our long touch handler
            return false;
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder)
        {
            // get the flags
            int flags = ((RecyclerListViewHolder)viewHolder).flags;

            // set movement flags based on the layout manager
            int dragFlags, swipeFlags;
            if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                swipeFlags = 0;
            } else {
                dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
            }

            // clear unsupported operations
            if ((flags & ITEM_FLAG_SWIPE_LEFT) == 0) swipeFlags &= ~ItemTouchHelper.LEFT;
            if ((flags & ITEM_FLAG_SWIPE_RIGHT) == 0) swipeFlags &= ~ItemTouchHelper.RIGHT;

            // return allowed movements
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean canDropOver(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder current,
                                   @NonNull RecyclerView.ViewHolder target) {
            return RecyclerListAdapter.this.canDropOver((VH)current, (VH)target);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder source,
                              @NonNull RecyclerView.ViewHolder target)
        {
            // this check should not be needed, but we have it just to be sure
            if (!canDropOver(recyclerView, source, target)) return false;

            // move the item
            moveItem(source.getAdapterPosition(), dragTarget = target.getAdapterPosition());
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int i)
        {
            // notify the event
            int position = viewHolder.getAdapterPosition();
            if (!listener.onSwiped(position, (i & LEFT) != 0))
                adapter.notifyItemChanged(position);
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView,
                                @NonNull RecyclerView.ViewHolder viewHolder,
                                float dX, float dY, int actionState, boolean isCurrentlyActive)
        {
            boolean handled = false;
            switch (actionState) {
                case ACTION_STATE_DRAG:
                    handled = onDragDraw(c, (VH) viewHolder, dX, dY, isCurrentlyActive);
                    break;
                case ACTION_STATE_SWIPE:
                    handled = onSwipeDraw(c, (VH) viewHolder, dX, dY, isCurrentlyActive);
            }
            if (!handled) super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }

        @Override
        public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState)
        {
            // check for end of selection (the first check could be enough but we want to be sure)
            if (viewHolder == null || actionState == ItemTouchHelper.ACTION_STATE_IDLE) {

                // if the item has been dragged, mark the change and report to the user
                if (dragSource != dragTarget) {
                    setChanged(true);
                    listener.onDragged(dragSource, dragTarget);
                }

                // clear the status
                selected = null;
                dragSource = dragTarget = -1;
                return;
            }

            // save selected and notify adapter
            action = actionState;
            selected = (VH) viewHolder;
            switch (actionState) {
                case ACTION_STATE_DRAG:
                    dragSource = dragTarget = selected.getAdapterPosition();
                    onStartDrag(selected);
                    break;
                case ACTION_STATE_SWIPE:
                    onStartSwipe(selected);
                    break;
            }

            super.onSelectedChanged(viewHolder, actionState);
        }

        @Override
        public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder)
        {
            super.clearView(recyclerView, viewHolder);

            // end action
            switch (action) {
                case ACTION_STATE_DRAG:
                    onEndDrag((VH) viewHolder);
                    break;
                case ACTION_STATE_SWIPE:
                    onEndSwipe((VH) viewHolder);
                    break;
            }
            action = -1;
        }
    }

    /** The touch helper instance */
    private ItemTouchHelper touchHelper;

    /** Our callback for the ItemTouchHelper */
    private final TouchCallback touchCallback = new TouchCallback();

    /**
     * Starts dragging the specified item
     * @param viewHolder The view holder of the item which will be dragged
     */
    protected void startDrag(RecyclerListViewHolder viewHolder)
    {
        touchHelper.startDrag(viewHolder);
    }

    /**
     * Gets the view holder currently dragging if any
     * @return The view holder currently dragging or null if none
     */
    protected VH getDragging()
    {
        return touchCallback.action == ACTION_STATE_DRAG ? touchCallback.selected : null;
    }

    /**
     * Gets the view holder currently swiping if any
     * @return The view holder currently swiping or null if none
     */
    protected VH getSwiping()
    {
        return touchCallback.action == ACTION_STATE_SWIPE ? touchCallback.selected : null;
    }

    /**
     * Returns the position in the adapter of the passed view
     * @param v The view
     * @return The position in the adapter
     */
    int getViewAdapterPosition(View v)
    {
        return RecyclerList.getRecyclerViewItem(v).viewHolder.getAdapterPosition();
    }

    @Override
    public void onClick(View v) {
        // in some wrong cases -1 could be returned
        int position = getViewAdapterPosition(v);
        if (position != -1) listener.onItemClicked(position);
    }

    @Override
    public boolean onLongClick(View v)
    {
        // start dragging if configured
        RecyclerListViewHolder vh = RecyclerList.getRecyclerViewItem(v).viewHolder;
        if ((vh.flags & ITEM_FLAG_DRAG_LONG_CLICK) == ITEM_FLAG_DRAG_LONG_CLICK) {
            startDrag(vh);
            return true;
        }

        // prefer standard handler if both are configured
        listener.onItemLongClicked(getViewAdapterPosition(v));
        return true;
    }

    /**
     * Build an adapter for RecyclerList
     * @param context The Context
     * @param hasStableIds if true the items have stable IDs
     * @param dragBackgroundColorId The id of the background color which will be used for dragged items or 0 if none
     * @param swipeBackgroundColorId The id of the background color which will be used for swiped items or 0 if none
     * @param swipeLeftColorId The id of the color which will be used for swipe left or 0 if none
     * @param swipeLeftIconId The id of the icon which will be used for swipe left or 0 if none
     * @param swipeRightColorId The id of the color which will be used for swipe right or 0 if none
     * @param swipeRightIconId The id of the icon which will be used for swipe left or 0 if none
     */
    public RecyclerListAdapter(Context context, boolean hasStableIds, int dragBackgroundColorId,
                               int swipeBackgroundColorId,
                               int swipeLeftColorId, int swipeLeftIconId,
                               int swipeRightColorId, int swipeRightIconId)
    {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        adapter.setHasStableIds(hasStableIds);
        dragBackgroundColor = dragBackgroundColorId != 0 ?
                new ColorDrawable(ContextCompat.getColor(context, dragBackgroundColorId)) : null;
        swipeBackgroundColor = swipeBackgroundColorId != 0 ?
                new ColorDrawable(ContextCompat.getColor(context, swipeBackgroundColorId)) : null;
        swipeLeftColor = swipeLeftColorId != 0 ? ContextCompat.getColor(context, swipeLeftColorId) : null;
        swipeRightColor = swipeRightColorId != 0 ? ContextCompat.getColor(context, swipeRightColorId) : null;
        swipeLeftIcon = swipeLeftIconId != 0 ? AppCompatResources.getDrawable(context, swipeLeftIconId) : null;
        swipeRightIcon = swipeRightIconId != 0 ?AppCompatResources.getDrawable(context, swipeRightIconId) : null;
    }

    /**
     * Sets the listener for this adapter
     * @param listener The listener for item events
     */
    public void setListener(RecyclerListListener listener)
    {
        this.listener = listener;
    }

    /**
     * Gets the listener for this adapter
     * @return The listener for item events
     */
    public RecyclerListListener getListener() {
        return listener;
    }

    /**
     * Called when RecyclerList needs a new {@link RecyclerView.ViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     *
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     *
     * @return A new ViewHolder that holds a View of the given view type.
     */
    abstract protected VH onCreateViewHolder(ViewGroup parent, int viewType);

    /** Click events are reported */
    protected static final int ITEM_FLAG_CLICK = 1;

    /** Long click events are reported */
    protected static final int ITEM_FLAG_LONG_CLICK = 2;

    /** Drag on long click is enabled. Cannot be set with long click events reporting */
    protected static final int ITEM_FLAG_DRAG_LONG_CLICK = ITEM_FLAG_LONG_CLICK + 4;

    /** Left swiping is supported */
    protected static final int ITEM_FLAG_SWIPE_LEFT = 8;

    /** Right swiping is supported */
    protected static final int ITEM_FLAG_SWIPE_RIGHT = 16;

    /**
     * Called by RecyclerList to display the data at the specified position. This method should
     * update the contents of the {@link RecyclerView.ViewHolder#itemView} to reflect the item at the given
     * position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     * @return Mask of ITEM_FLAG_* attributes
     */
    abstract protected int onBindViewHolder(VH holder, int position);

    /**
     * Called when a view created by this adapter has been recycled.
     *
     * <p>A view is recycled when a LayoutManager decides that it no longer
     * needs to be attached to its parent {@link RecyclerView}. This can be because it has
     * fallen out of visibility or a set of cached views represented by views still
     * attached to the parent RecyclerView. If an item view has large or expensive data
     * bound to it such as large bitmaps, this may be a good place to release those
     * resources.</p>
     * <p>
     * RecyclerView calls this method right before clearing ViewHolder's internal data and
     * sending it to RecycledViewPool. This way, if ViewHolder was holding valid information
     * before being recycled, you can call {@link RecyclerView.ViewHolder#getAdapterPosition()} to get
     * its adapter position.
     *
     * @param holder The ViewHolder for the view being recycled
     */
    protected void onViewRecycled(VH holder) {}

    /** This adapter has been detached and will be destroyed */
    protected void onDestroy() {}

    /** Saved background from item */
    private Drawable oldBackground;

    /**
     * Called when drag starts
     * @param holder The view holder which is dragged
     */
    protected void onStartDrag(VH holder) {
        if (dragBackgroundColor != null) {
            oldBackground = holder.itemView.getBackground();
            holder.itemView.setBackground(dragBackgroundColor);
        }
    }

    /**
     * Called when drag ends
     * @param holder The view holder which is dragged
     */
    protected void onEndDrag(VH holder) {
        if (oldBackground != null) {
            holder.itemView.setBackground(oldBackground);
            oldBackground = null;
        }
    }

    /**
     * Called when swipe starts
     * @param holder The view holder which is swiped
     */
    protected void onStartSwipe(VH holder) {
        if (swipeBackgroundColor != null) {
            oldBackground = holder.itemView.getBackground();
            holder.itemView.setBackground(swipeBackgroundColor);
        }
    }

    /**
     * Called when swipe ends
     * @param holder The view holder which is swiped
     */
    protected void onEndSwipe(VH holder) {
        if (oldBackground != null) {
            holder.itemView.setBackground(oldBackground);
            oldBackground = null;
        }
    }

    /** Paint used for drawing */
    private final Paint p = new Paint();

    /**
     * If you would like to customize how your View's respond to swipe interactions, this is
     * a good place to override.
     * <p>
     * Default implementation translates the child by the given <code>dX</code>,
     * <code>dY</code>.
     *
     * @param c                 The canvas which RecyclerView is drawing its children
     * @param viewHolder        The ViewHolder which is being interacted by the User or it was
     *                          interacted and simply animating to its original position
     * @param dX                The amount of horizontal displacement caused by user's action
     * @param dY                The amount of vertical displacement caused by user's action
     * @param isCurrentlyActive True if this view is currently being controlled by the user or
     *                          false it is simply animating back to its original state.
     * @return If false the super implementation is called
     */
    protected boolean onSwipeDraw(Canvas c, VH viewHolder, float dX, float dY, boolean isCurrentlyActive)
    {
        // get some parameters
        View itemView = viewHolder.itemView;
        int height = itemView.getBottom() - itemView.getTop();
        int width = height / 3;

        // right swipe
        if (dX > 0) {
            c.save();
            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
            c.clipRect(background);
            if (swipeRightColor != null) {
                p.setColor(swipeRightColor);
                c.drawRect(background, p);
            }
            if (swipeRightIcon != null) {
                int h = swipeRightIcon.getIntrinsicHeight();
                int w = swipeRightIcon.getIntrinsicWidth();
                swipeRightIcon.setBounds(itemView.getLeft() + w / 3, itemView.getTop() + (height - h) / 2,
                        itemView.getLeft() + w + w / 3, itemView.getTop() + (height + h) / 2);
                swipeRightIcon.setBounds(itemView.getLeft() + width, itemView.getTop() + width, itemView.getLeft() + 2 * width, itemView.getBottom() - width);
                swipeRightIcon.draw(c);
            }
            c.restore();
        // left swipe
        } else if (dX < 0) {
            c.save();
            RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
            c.clipRect(background);
            if (swipeLeftColor != null) {
                p.setColor(swipeLeftColor);
                c.drawRect(background, p);
            }
            if (swipeLeftIcon != null) {
                int h = swipeLeftIcon.getIntrinsicHeight();
                int w = swipeLeftIcon.getIntrinsicWidth();
                swipeLeftIcon.setBounds(itemView.getRight() - w - w / 3, itemView.getTop() + (height - h) / 2,
                        itemView.getRight() - w / 3, itemView.getTop() + (height + h) / 2);
                swipeLeftIcon.draw(c);
            }
            c.restore();
        }

        return false;
    }

    /**
     * If you would like to customize how your View's respond to drag interactions, this is
     * a good place to override.
     * <p>
     * Default implementation translates the child by the given <code>dX</code>,
     * <code>dY</code>.
     *
     * @param c                 The canvas which RecyclerView is drawing its children
     * @param viewHolder        The ViewHolder which is being interacted by the User or it was
     *                          interacted and simply animating to its original position
     * @param dX                The amount of horizontal displacement caused by user's action
     * @param dY                The amount of vertical displacement caused by user's action
     * @param isCurrentlyActive True if this view is currently being controlled by the user or
     *                          false it is simply animating back to its original state.
     * @return If false the super implementation is called
     */
    protected boolean onDragDraw(Canvas c, VH viewHolder, float dX, float dY, boolean isCurrentlyActive) {
        return false;
    }

    /**
     * Return true if the current ViewHolder can be dropped over the the target ViewHolder.
     * <p>
     * This method is used when selecting drop target for the dragged View.
     * <p>
     * Default implementation returns true if they are of the same type.
     *
     * @param current The ViewHolder that user is dragging.
     * @param target The ViewHolder which is below the dragged ViewHolder.
     * @return True if the dragged ViewHolder can be replaced with the target ViewHolder, false
     * otherwise.
     */
    protected boolean canDropOver(VH current, VH target) {
        return current.getItemViewType() == target.getItemViewType();
    }

    /**
     * Reports if a decorations shall be present for the specified position
     * @param decoration The decoration
     * @param position The position
     * @return true if the specified decoration shall be appiled for that position
     */
    protected boolean useDecoration(RecyclerView.ItemDecoration decoration, int position) {
        return true;
    }
}
