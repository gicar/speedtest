package net.gicar.util.recyclerlist;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

/**
 * Smarter DividerItemDecoration enabling divider disable on request.
 * To have this work, you need to set a solid background on the list view.
 */
@SuppressWarnings("rawtypes")
public class RecyclerListDividerItemDecoration extends DividerItemDecoration {

    /**
     * Creates a divider RecyclerView.ItemDecoration that can be used with a LinearLayoutManager.
     *
     * @param context     Current context, it will be used to access resources.
     * @param orientation Divider orientation. Should be {@link #HORIZONTAL} or {@link #VERTICAL}.
     */
    public RecyclerListDividerItemDecoration(Context context, int orientation) {
        super(context, orientation);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
    {
        // query the adapter to check if the decoration shall be applied or not
        RecyclerListAdapter adapter = ((RecyclerList)parent).getRecyclerListAdapter();

        // get position and filter out errors
        int position = parent.getChildAdapterPosition(view);
        if (position == NO_POSITION) return;

        // check if decoration shall be present and draw it if yes
        if (adapter.useDecoration(this, position))
            super.getItemOffsets(outRect, view, parent, state);
        else outRect.setEmpty();
    }
}
