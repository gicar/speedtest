package net.gicar.util.recyclerlist;

import android.view.View;
import android.widget.CompoundButton;

/** ViewHolder for Action Mode RecyclerList */
@SuppressWarnings("rawtypes")
public class ActionModeRecyclerListViewHolder extends RecyclerListViewHolder {

    /** Checked state listener */
    private final static CompoundButton.OnCheckedChangeListener ON_CHECKED_CHANGE_LISTENER =
            (buttonView, isChecked) -> {
                // find our recycler view and the adapter
                RecyclerList.ViewItem viewItem = RecyclerList.getRecyclerViewItem(buttonView);
                if (viewItem == null) return;
                ActionModeRecyclerListAdapter listAdapter = (ActionModeRecyclerListAdapter) viewItem.recyclerList.recyclerListAdapter;

                // set the selected state
                int position = viewItem.viewHolder.getAdapterPosition();
                listAdapter.setSelected(position, isChecked, false);
            };

    public ActionModeRecyclerListViewHolder(View itemView) {
        super(itemView);
    }

    /**
     * Configures a CompoundButton to affect the selected state of the item
     * @param view The CompoundButton which will start change the selected state
     */
    protected void setSelectedCompoundButton(CompoundButton view)
    {
        view.setOnCheckedChangeListener(ON_CHECKED_CHANGE_LISTENER);
    }

}
