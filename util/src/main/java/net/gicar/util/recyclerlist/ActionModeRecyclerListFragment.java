package net.gicar.util.recyclerlist;

import android.content.res.Resources;
import android.os.Bundle;

import java.util.TreeSet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

/**
 * A RecyclerListFragment which supports ActionMode.
 * Note the current limitation that structural changes are not allowed while in ActionMode,
 * exception the deletion controlled by our interface.
 */
@SuppressWarnings("rawtypes")
abstract public class ActionModeRecyclerListFragment extends RecyclerListFragment
        implements ActionMode.Callback, ActionModeRecyclerListListener {

    /** The save instance keys */
    private final static String ACTION_MODE_KEY = "net.gicar.util.ACTION_MODE",
        SELECTED_ITEMS_KEY = "net.gicar.util.SELECTED_ITEMS";

	/**  Our Adapter */
	private ActionModeRecyclerListAdapter adapter;

    /**
     * Sets the adapter for the list. Note that the listener will be overridden.
     * @param adapter The adapter which will be used
     */
    protected void setRecyclerListAdapter(ActionModeRecyclerListAdapter adapter)
    {
        // save our adapter reference and call the super implementation
        this.adapter = adapter;
        super.setRecyclerListAdapter(adapter);
    }

    @Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// declare that we have menus (this is default behavior for classes extending this)
		setHasOptionsMenu(true);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		// save if in ActionMode value and the current selection; note that the adapter copuld not
        // have been initialized if the fragment is not currently visible
        if (adapter != null) {
            outState.putBoolean(ACTION_MODE_KEY, adapter.actionMode != null);
            outState.putSerializable(SELECTED_ITEMS_KEY, adapter.selectedItems);
        }

		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		super.onViewStateRestored(savedInstanceState);

		if (savedInstanceState != null)
        {
            // restore the checked items
            Object s = savedInstanceState.getSerializable(SELECTED_ITEMS_KEY);
            if (s instanceof TreeSet) //noinspection unchecked
                adapter.selectedItems.addAll((TreeSet)s);

            // start the ActionMode if it was there before
            if (savedInstanceState.getBoolean(ACTION_MODE_KEY, false))
                adapter.startActionMode();
        }
	}

 	@Override
	public ActionMode onStartActionMode()
	{
		// create the ActionMode
        return ((AppCompatActivity)getActivity()).startSupportActionMode(this);
	}

    @Override
    public void onItemSelectedStateChanged(int position, boolean selected)
    {
        // default behaviour is updating the ActionMode
        if (adapter.actionMode != null)
            onPrepareActionMode(adapter.actionMode, adapter.actionMode.getMenu());
    }

    @Override
    public void onItemsSelectedStateChanged()
    {
        // default behaviour is updating the ActionMode
        if (adapter.actionMode != null)
            onPrepareActionMode(adapter.actionMode, adapter.actionMode.getMenu());
    }

    /**
	 * Utility function to build the string with the count of items
	 * @param n The number of items
	 * @param id_selected_0 The id of the string for 0 items
	 * @param id_selected_1 The id of the string for 1 item
	 * @param id_selected_n The id of the string for n items
	 * @return The string describing the number of items
	 */
	protected String buildCountString(int n, int id_selected_0, int id_selected_1, int id_selected_n)
	{
        Resources r = getActivity().getResources();
		int id;
		switch (n) {
			case 0:
				id = id_selected_0;
				break;
			case 1:
				id = id_selected_1;
				break;
			default:
				id = id_selected_n;
		}
		if (id == 0) return Integer.toString(n);
		return String.format(r.getString(id), n);
	}

	@Override
	public void onDestroyActionMode(ActionMode mode)
	{
	    adapter.onDestroyActionMode();
	}
}
