package net.gicar.util.recyclerlist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static androidx.recyclerview.widget.RecyclerView.NO_ID;
import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

/** Contains the item's handling logic for the RecyclerListAdapter */
@SuppressWarnings("rawtypes")
abstract class RecyclerListAdapterBase<I> {

    /**
     * Retrieves the view instance from the subclass to here
     * @return The view
     */
    abstract RecyclerView getRecyclerView();

    /**
     * Retrieves the adapter instance from the subclass to here
     * @return The adapter
     */
    abstract RecyclerView.Adapter getRecyclerViewAdapter();

    /** List of items */
    final private List<I> items = new ArrayList<>();

    /** If true the items has been changed */
    private boolean changed = false;

    /**
     * Getter method for the number of items
     * @return The number of items
     */
    public int numItems()
    {
        return items.size();
    }

    /**
     * Returns the item at the specified position
     * @param position The item's position
     * @return The item
     */
    public I getItem(int position)
    {
        if (position == NO_POSITION) return null;
        return items.get(position);
    }

    /**
     * Replaces current items list with the provided one
     * @param list The new list of items or null to clear it
     */
    public void setItems(List<I> list)
    {
        items.clear();
        if (list != null) items.addAll(list);
        changed = true;
        executeWhenRecyclerViewReady(() -> getRecyclerViewAdapter().notifyDataSetChanged());
    }

    /**
     * Add the specified item at the end of the list
     * @param i The item to be added
     */
    public void addItem(I i)
    {
        items.add(i);
        changed = true;
        executeWhenRecyclerViewReady(() -> getRecyclerViewAdapter().notifyItemInserted(items.size() - 1));
    }

    /**
     * Add the specified items at the end of the list
     * @param i The items to be added
     */
    public void addItems(Collection<I> i)
    {
        if (i != null) {
            items.addAll(i);
            changed = true;
            executeWhenRecyclerViewReady(() ->
                    getRecyclerViewAdapter().notifyItemRangeInserted(items.size() - i.size(), i.size()));
        }
    }

    /**
     * Deletes the item at the specified position
     * @param position The item's position
     * @return The deleted item
     */
    public I deleteItem(int position) {
        I item = items.remove(position);
        changed = true;
        executeWhenRecyclerViewReady(() -> getRecyclerViewAdapter().notifyItemRemoved(position));
        return item;
    }

    /**
     * Replaces the item at the specified position
     * @param position The item's position
     * @param i The new item value
     * @return The item previously stored at the specified position
     */
    public I replaceItem(int position, I i) {
        I item = items.set(position, i);
        changed = true;
        executeWhenRecyclerViewReady(() -> getRecyclerViewAdapter().notifyItemChanged(position));
        return item;
    }

    /**
     * Moves an items from a source position to a new destination.
     * This is for internal use during dragging.
     * @param fromPosition The source position
     * @param toPosition The destination
     */
    void moveItem(int fromPosition, int toPosition)
    {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(items, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(items, i, i - 1);
            }
        }
        executeWhenRecyclerViewReady(() -> getRecyclerViewAdapter().notifyItemMoved(fromPosition, toPosition));
    }

    /**
     * Getter method for the number of items in the list
     * @return The number of items in the list
     */
    public int getItemCount() {
        return items.size();
    }

    /** Returns a copy of the list of items */
    public ArrayList<I> getItems() {
        return new ArrayList<>(items);
    }

    /**
     * Return the stable ID for the item at <code>position</code>. If has stable ids
     * would return false this method should return NO_ID. The default implementation
     * of this method returns NO_ID.
     *
     * @param position Adapter position to query
     * @return the stable ID of the item at position
     */
    protected long getItemId(int position) {
        return NO_ID;
    }

    /**
     * Return the view type of the item at <code>position</code> for the purposes
     * of view recycling.
     *
     * <p>The default implementation of this method returns 0, making the assumption of
     * a single view type for the adapter. Unlike ListView adapters, types need not
     * be contiguous. Consider using id resources to uniquely identify item view types.
     *
     * @param position position to query
     * @return integer value identifying the type of the view needed to represent the item at
     *                 <code>position</code>. Type codes need not be contiguous.
     */
    protected int getItemViewType (int position) {
        return 0;
    }

    /** Notify any registered observers that the data set has changed. */
    public void notifyDataSetChanged()
    {
        changed = true;
        executeWhenRecyclerViewReady(() -> getRecyclerViewAdapter().notifyDataSetChanged());
    }

    /**
     * Notify any registered observers that the item at <code>position</code> has changed.
     * @param position Position of the item that has changed
     */
    public void notifyItemChanged(int position)
    {
        changed = true;
        executeWhenRecyclerViewReady(() -> getRecyclerViewAdapter().notifyItemChanged(position));
    }

    /**
     * Notify any registered observers that the item at <code>position</code> has changed.
     * Note that by passing a non null object, no animation will be performed for the update.
     * @param position Position of the item that has changed
     * @param payload Data passed to the onBindViewHolder
     */
    public void notifyItemChanged(int position, Object payload)
    {
        changed = true;
        executeWhenRecyclerViewReady(() -> getRecyclerViewAdapter().notifyItemChanged(position, payload));
    }

    /**
     * Executes the passed Runnable when the RecyclerView associated with us is not computing layout.
     * This is needed for cases in which the changes in the items is not coordinated with the rest of
     * the RecyclerView operations
     * @param runnable The operation to be performed
     */
    private void executeWhenRecyclerViewReady(Runnable runnable)
    {
        RecyclerView recyclerView = getRecyclerView();
        if (recyclerView == null) return;
        if (recyclerView.isComputingLayout())
            recyclerView.post(runnable);
        else runnable.run();
    }

    /**
     * Getter for the changed state flag
     * @return If true the data has been changed
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * Setter for the changed state flag
     * @param changed If true the data has been changed
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }
}
