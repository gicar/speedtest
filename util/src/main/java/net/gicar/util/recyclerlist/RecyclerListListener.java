package net.gicar.util.recyclerlist;

/** Notifies events on items in a RecyclerList */
public interface RecyclerListListener {

    /**
     * Called when an item has been clicked
     * @param position The position of the item
     */
    void onItemClicked(int position);

    /**
     * Called when an item has a long clicked
     * @param position The position of the item
     */
    void onItemLongClicked(int position);

    /**
     * Signals a drag action on an item. Note the underlying list is already changed with the ordering
     * when this callback is called, and thus the reported positions are no longer the current ones.
     * @param source The source position of the item within the adapter's data set at the start of dragging
     * @param target The target position of the item within the adapter's data set at the end of dragging
     */
    void onDragged(int source, int target);

    /**
     * Signals a swipe action on an item
     * @param position The position of the item within the adapter's data set.
     * @param left If true it is a left swipe, else a right one
     * @return true if the item has been invalidated by the swipe code
     */
    boolean onSwiped(int position, boolean left);

    /**
     * Signals a custom ViewHolder event at the specified position
     * @param position The event position
     * @param event The event
     */
    void onViewHolderEvent(int position, Object event);
}
