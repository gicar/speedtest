package net.gicar.util.recyclerlist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/** */
@SuppressWarnings("rawtypes")
public class RecyclerList extends RecyclerView {

    public RecyclerList(Context context) {
        this(context, null);
    }

    public RecyclerList(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecyclerList(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /** The adapter set on this list */
    RecyclerListAdapter recyclerListAdapter;

    /** If true user interactions are listTouchDisabled */
    private boolean listTouchDisabled;

    /** Info recovered from a View in the RecyclerView hierarchy */
    public static class ViewItem {

        /** Its RecyclerView */
        public final RecyclerList recyclerList;

        /** Its ViewHolder */
        public final RecyclerListViewHolder viewHolder;

        public ViewItem(RecyclerList recyclerList, RecyclerListViewHolder viewHolder) {
            this.recyclerList = recyclerList;
            this.viewHolder = viewHolder;
        }
    }

    /**
     * Gets info about a view in the RecyclerView hierarchy.
     * @param view Child view of the RecyclerView's item
     * @return Info about the view item or null if not found
     */
    static ViewItem getRecyclerViewItem(@Nullable View view)
    {
        if (view == null) return null;

        while (true) {
            ViewParent parent = view.getParent();
            if (parent instanceof RecyclerList) {
                RecyclerList rv = (RecyclerList) parent;
                return new ViewItem(rv, (RecyclerListViewHolder) rv.getChildViewHolder(view));
            }
            if (parent instanceof View) {
                view = (View) parent;
                continue;
            }
            return null;
        }
    }

    /**
     * Set a new adapter to provide child views on demand.
     * @param recyclerListAdapter The adapter to be set or null to remove
     */
    public void setAdapter(RecyclerListAdapter recyclerListAdapter)
    {
        // set the adapter and save the list
        this.recyclerListAdapter = recyclerListAdapter;
        setAdapter(recyclerListAdapter != null ? recyclerListAdapter.getRecyclerViewAdapter() : null);
    }

    public RecyclerListAdapter getRecyclerListAdapter() {
        return recyclerListAdapter;
    }

    public boolean isListTouchDisabled() {
        return listTouchDisabled;
    }

    public void setListTouchDisabled(boolean listTouchDisabled) {
        this.listTouchDisabled = listTouchDisabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        if (listTouchDisabled) return true;
        return super.onInterceptTouchEvent(e);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (listTouchDisabled) return true;
        return super.onTouchEvent(e);
    }
}
