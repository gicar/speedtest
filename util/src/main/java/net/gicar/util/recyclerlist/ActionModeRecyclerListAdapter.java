package net.gicar.util.recyclerlist;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import androidx.appcompat.view.ActionMode;

/** RecyclerListAdapter supporting action mode */
public abstract class ActionModeRecyclerListAdapter<I, VH extends ActionModeRecyclerListViewHolder>
        extends RecyclerListAdapter<I, VH> {

    /** Start action mode on long click */
    protected static final int ITEM_FLAG_ACTION_MODE_LONG_CLICK = ITEM_FLAG_LONG_CLICK + 256;

    /** Notify clicks on item instead of changing selection */
    protected static final int ITEM_FLAG_ACTION_MODE_CLICK = 512;

    /** Provides selection state */
    public interface SelectionSetter<I> {

        /**
         * Provides selection state for an item
         * @param position The position of the item
         * @param item The item
         * @return true if the item shall be selected, false if not
         */
        boolean isSelected(int position, I item);
    }

    /** The item events listener */
    private ActionModeRecyclerListListener listener;

    /** The ActionMode or null if not active */
    ActionMode actionMode;

    /** Position of selected items */
    TreeSet<Integer> selectedItems = new TreeSet<>();

    /**
     * Build an adapter for RecyclerList
     *
     * @param context The Context
     * @param hasStableIds if true the items have stable IDs
     * @param dragBackgroundColorId The id of the background color which will be used for dragged items or 0 if none
     * @param swipeBackgroundColorId The id of the background color which will be used for swiped items or 0 if none
     * @param swipeLeftColorId The id of the color which will be used for swipe left or 0 if none
     * @param swipeLeftIconId The id of the icon which will be used for swipe left or 0 if none
     * @param swipeRightColorId The id of the color which will be used for swipe right or 0 if none
     * @param swipeRightIconId The id of the icon which will be used for swipe left or 0 if none
     */
    public ActionModeRecyclerListAdapter(Context context, boolean hasStableIds, int dragBackgroundColorId,
                                         int swipeBackgroundColorId,
                                         int swipeLeftColorId, int swipeLeftIconId,
                                         int swipeRightColorId, int swipeRightIconId) {
        super(context, hasStableIds, dragBackgroundColorId, swipeBackgroundColorId,
                swipeLeftColorId, swipeLeftIconId, swipeRightColorId, swipeRightIconId);
    }

    /**
     * Sets the listener for this adapter
     * @param listener The listener for item events
     */
    @Override
    public void setListener(RecyclerListListener listener)
    {
        if (!(listener instanceof ActionModeRecyclerListListener))
            throw new UnsupportedOperationException("Please provide the correct listener");

        this.listener = (ActionModeRecyclerListListener)listener;
        super.setListener(listener);
    }

    @Override
    public void onClick(View v)
    {
        // recover the view item if needed
        RecyclerListViewHolder vh = null;
        if (actionMode != null) vh = RecyclerList.getRecyclerViewItem(v).viewHolder;

        // if we are not with action mode report the selection and exit here
        if (vh == null || (vh.flags & ITEM_FLAG_ACTION_MODE_CLICK) != 0) {
            super.onClick(v);
            return;
        }

        // change the selection state
        int position = vh.getAdapterPosition();
        setSelected(position, !isSelected(position));
    }

    @Override
    public boolean onLongClick(View v)
    {
        // revert to normal handling if not configured to
        RecyclerListViewHolder vh = RecyclerList.getRecyclerViewItem(v).viewHolder;
        if ((vh.flags & ITEM_FLAG_ACTION_MODE_LONG_CLICK) != ITEM_FLAG_ACTION_MODE_LONG_CLICK)
            return super.onLongClick(v);

        // avoid starting action mode if other actions are ongoing
        if (getDragging() != null || getSwiping() != null) return false;

        // start selecting this item
        int position = getViewAdapterPosition(v);
        selectedItems.add(position);

        // start the ActionMode
        startActionMode();

        return true;
    }

    /** Starts the ActionMode on this adapter. */
    public void startActionMode()
    {
        // start the ActionMode and disable long clicks
        actionMode = listener.onStartActionMode();
        flagMask &= ~ITEM_FLAG_LONG_CLICK;
        getRecyclerViewAdapter().notifyDataSetChanged();
    }

    /** Terminates the action mode */
    public void finishActionMode()
    {
        // finish the action mode
        if (actionMode != null) actionMode.finish();
    }

    /** Called to notify action mode destroy */
    void onDestroyActionMode()
    {
        // forget about the ActionMode
        actionMode = null;

        // restore normal view status
        flagMask |= ITEM_FLAG_LONG_CLICK;
        selectedItems.clear();
        getRecyclerViewAdapter().notifyDataSetChanged();
    }

    /**
     * Returns the status of the action mode
     * @return true is the ActionMode is active
     */
    public boolean isInActionMode() {
        return actionMode != null;
    }

    /**
     * Returns the current action mode
     * @return The current ActionMode, if any
     */
    public ActionMode getActionMode() {
        return actionMode;
    }

    /**
     * Reports if the specified position is selected
     * @param position The position
     * @return true it is selected
     */
    public boolean isSelected(int position)
    {
        return selectedItems.contains(position);
    }

    /**
     * Changes the selected state of a position
     * @param position The position to be changed
     * @param selected The new selected state
     */
    public void setSelected(int position, boolean selected)
    {
        setSelected(position, selected, true);
    }

    /**
     * Changes the selected state of a position
     * @param position The position to be changed
     * @param selected The new selected state
     * @param notifyAdapter Controls if the adapter shall be notified or not
     */
    void setSelected(int position, boolean selected, boolean notifyAdapter)
    {
        // avoid actions if state is already correct
        if (isSelected(position) == selected) return;

        // add or remove as requested
        if (selected) selectedItems.add(position);
        else selectedItems.remove(position);

        // notify the change to the adapter
        if (notifyAdapter) getRecyclerViewAdapter().notifyItemChanged(position);

        // notify the listener
        listener.onItemSelectedStateChanged(position, selected);
    }

    /**
     * Reports the number of selected items
     * @return Number of selected items
     */
    public int getSelectedCount()
    {
        return selectedItems.size();
    }

    /**
     * Returns the first selected item
     * @return position of the first selected item, -1 if none
     */
    public int getFirstSelectedPosition() {
        if (selectedItems.size() == 0) return -1;
        return selectedItems.first();
    }

    /**
     * Returns a set with all the selected positions
     * @return A SortedSet holding the selected positions
     */
    public SortedSet<Integer> getSelectedPositions() {
        return new TreeSet<>(selectedItems);
    }

    /** Clears the selection */
    public void clearSelected()
    {
        if (selectedItems.size() > 0) {
            selectedItems.clear();
            listener.onItemsSelectedStateChanged();
            getRecyclerViewAdapter().notifyDataSetChanged();
        }
    }

    /**
     * Set the selection state of the items as reported by the setter
     * @param setter Provides new selection state for all the items
     */
    public void select(SelectionSetter<I> setter)
    {
        // get the list of items
        List<I> list = getItems();
        if (list.size() > 0) {

            // clear previous selection and perform new selection
            selectedItems.clear();
            for (int i = 0; i < list.size(); i++)
                if (setter.isSelected(i, list.get(i))) selectedItems.add(i);

            // notify the change
            listener.onItemsSelectedStateChanged();
            getRecyclerViewAdapter().notifyDataSetChanged();
        }
    }

    /** Select all items */
    public void selectAll()
    {
        List<I> list = getItems();
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) selectedItems.add(i);
            listener.onItemsSelectedStateChanged();
            getRecyclerViewAdapter().notifyDataSetChanged();
        }
    }

    /**
     * Deletes the items which are currently selected, returning them.
     * The selection is cleared.
     * @return The list of deleted items
     */
    public List<I> deleteSelectedItems()
    {
        // delete the items and build its list
        List<I> list = new ArrayList<>(selectedItems.size());
        int removed = 0;
        for (Integer position : selectedItems) {
            int i = position - removed++;
            list.add(deleteItem(i));
            getRecyclerViewAdapter().notifyItemRemoved(i);
        }

        // we notified the deleted items one by one, so we don't need to notify again
        if (selectedItems.size() > 0) {
            selectedItems.clear();
            listener.onItemsSelectedStateChanged();
        }

        return list;
    }

    /**
     * Utility function to show a drag handle or a checkbox with the selection depending on the state
     * @param position The position
     * @param checkBox The check box where to show the selection
     * @param dragHandle The drag handle to show if not in action mode
     */
    protected void showCheckBoxOrDragHandle(int position, CheckBox checkBox, ImageView dragHandle)
    {
        boolean isInActionMode = isInActionMode();
        if (isInActionMode) {
            boolean isSelected = isSelected(position);
            boolean skipAnimation = !isSelected && checkBox.isChecked() && checkBox.getVisibility() != View.VISIBLE;
            checkBox.setChecked(isSelected);
            if (skipAnimation) checkBox.jumpDrawablesToCurrentState();
        }
        checkBox.setVisibility(isInActionMode ? View.VISIBLE : View.GONE);
        dragHandle.setVisibility(isInActionMode ? View.GONE : View.VISIBLE);
    }
}
