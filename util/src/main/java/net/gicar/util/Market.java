package net.gicar.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

/** Utility functions to check and interact with markets */
public class Market {

    private Market() {}

    /**
     * Provides information about the possibility to use services from the Google Play market
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if Google Play market app services can be used
     */
    public static boolean canUseGooglePlay(Context context)
    {
        return Misc.DEBUG || !isInstalledByAmazon(context);
    }

    /**
     * Returns the package name which installed this app
     * @param context The <code>Context</code> to be used for the operation
     * @return The installer's package name or null if not available
     */
    public static String getInstallerPackageName(Context context)
    {
        // on older android versios use the info from the package manager
        if (android.os.Build.VERSION.SDK_INT < 30)
            //noinspection deprecation
            return context.getPackageManager().getInstallerPackageName(context.getPackageName());

        try {
            return context.getPackageManager().getInstallSourceInfo(context.getPackageName()).getInitiatingPackageName();
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    /**
     * Checks if it has been installed with the Google Play market
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if Google Play was used to install the app
     */
    public static boolean isInstalledByGooglePlay(Context context)
    {
        return "com.android.vending".equals(getInstallerPackageName(context));
    }

    /**
     * Checks if it has been installed with the Amazon market
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if Amazon was used to install the app
     */
    public static boolean isInstalledByAmazon(Context context)
    {
        return "com.amazon.venezia".equals(getInstallerPackageName(context));
    }

    /**
     * Launches Google Play or Amazon Market on the current application depending on the device
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if market launched successfully
     */
    public static boolean launchMarket(Context context)
    {
        if (isInstalledByAmazon(context)) return launchAmazonMarket(context);
        if (isInstalledByGooglePlay(context)) return launchGooglePlay(context);
        if (launchGalaxyApps(context)) return true;
        return launchGooglePlay(context);
    }

    /**
     * Launches Google Play on the current application
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if market launched successfully
     */
    private static boolean launchGooglePlay(Context context)
    {
        return IntentUtils.viewIntent(context, "market://details?id=" + context.getPackageName());
    }

    /**
     * Launches Amazon Market on the current application
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if market launched successfully
     */
    private static boolean launchAmazonMarket(Context context)
    {
        return IntentUtils.viewIntent(context, "amzn://apps/android?p=" + context.getPackageName());
    }

    /**
     * Launches Galaxy Apps on the current application
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if market launched successfully
     */
    private static boolean launchGalaxyApps(Context context)
    {
        return IntentUtils.viewIntent(context, "samsungapps://ProductDetail/" + context.getPackageName(),
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
}
