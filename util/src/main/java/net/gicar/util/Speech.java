package net.gicar.util;

import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

import net.gicar.util.appcompat.XAppCompatActivity;

/**
 * Helper class for text to speech
 */
@SuppressWarnings("deprecation")
public class Speech implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {

	/** The log tag */
	public final static String TAG = "Speech";
	
	/** Our context */
	private final Context context;
	
    /** Request code for the TTS availability check */
    private final int requestIntent;
    
    /** The callback interface */
    private final SpeechCallback callback;

    /** Status of the speech */
    public enum SpeechStatus {
    	/** We need to be initialized */
    	UNINITIALIZED,
    	/** The speech component is initializing */
    	INITIALIZING,
    	/** The speech component is ready and not speaking */
    	READY,
    	/** Speech is ongoing, please listen to it */
    	SPEAKING,
    	/** Instance is shutdown, cannot be used any longer */
    	SHUTDOWN
    }
    
    /** Current speech status */
    private SpeechStatus status;

    /** The text to speech object */
    private TextToSpeech tts;
    
    /** The Locale of the current speech */
    private Locale locale;
    
    /** The text for the speech */
    private String text;
    
    /** The ID of this speech request */
    private int speechId;
    
	/**
	 * Initializes the text to speech functionality. Must be called from the main application's activity.
	 * The TTS will be no longer available when it will be destroyed.
	 * @param context The context
	 * @param requestIntent Request code for the TTS availability check flow
	 * @param callback The callback interface for reporting events
	 */
	public Speech(Context context, int requestIntent, SpeechCallback callback)
	{
		if (Misc.DEBUG) Log.i(TAG, "Executing initialization");
		
		// save our parameters
		this.context = context;
		this.requestIntent = requestIntent;
		this.callback = callback;
		
		// set the status to UNINITIALIZED
		status = SpeechStatus.UNINITIALIZED;
	}
	
	/**
	 * Call this in the onDestroy() of the activity
	 */
	public void shutdown()
	{
		if (Misc.DEBUG) Log.i(TAG, "Executing shutdown");

		// set the status to shutdown, do not notify in this case
		status = SpeechStatus.SHUTDOWN;

		// if we have a tts instance, destroy it
		if (tts != null) {
			tts.shutdown();
			tts = null;
		}
	}

	/**
	 * Starts the process which will lead to speaking the specified text in the specified locale.
	 * Any previous speech will be discarded/aborted.
     * @param activity The activity requesting the speech
     * @param locale The locale of the speech
	 * @param text The text to be speech
	 */
	public void speak(XAppCompatActivity activity, Locale locale, String text)
	{
		if (Misc.DEBUG) Log.i(TAG, "Speech request with locale " + locale + ", text:\n" + text);

		// save the speech request
		this.locale = locale;
		this.text = text;
		
		// update the speech id
		speechId++;

		switch (status) {
		case UNINITIALIZED:
			// if we are not initialized we need to start this process
			setStatus(SpeechStatus.INITIALIZING);
			Intent checkIntent = new Intent();
			checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
			try {
				// we run in try-catch block as on some devices TTS is not supported at all
                activity.startActivityForResult(checkIntent, requestIntent);
			}
			catch (Exception e) {
	            // report the error and go back to the uninitialized state
	            setStatus(SpeechStatus.UNINITIALIZED, SpeechError.FAILURE);
			}
			break;
			
		case INITIALIZING:
			// the request will be handled when initialization will be completed
			break;
			
		case READY:
		case SPEAKING:
			// just start speaking
			startSpeech();
			break;

		case SHUTDOWN:
			if (Misc.DEBUG) Log.e(TAG, "Speech requested when in shutdown status");
			break;
		}
	}

	/** Literally shut up... */
	public void silence()
	{
		if (Misc.DEBUG) Log.i(TAG, "Silencing");
		
		// workaround for activity requesting silencing while initializing due to pause
		if (status == SpeechStatus.INITIALIZING) {
			if (Misc.DEBUG) Log.i(TAG, "Silencing ignored while initializing");
			return;
		}

		// clear the speech request if any
		this.locale = null;
		this.text = null;
		
		// reset the status and flush the speech 
		if (status == SpeechStatus.SPEAKING) {
			tts.stop();
			setStatus(SpeechStatus.READY);
		}
	}
	
	/**
	 * Getter method for the current speech status
	 * @return The current speech status
	 */
	public SpeechStatus getStatus()
	{
		return status;
	}

	/**
	 * Checks is there is any speak pending
	 * @return true if speaking, or if going to
	 */
	public boolean isSpeaking()
	{
		return status == SpeechStatus.INITIALIZING || status == SpeechStatus.SPEAKING;
	}

    /**
     * This method must be called in the main activity activity result handling
     * @param requestCode The requestCode as you received it.
     * @param resultCode The resultCode as you received it.
     * @param data The data (Intent) as you received it.
     * @return Returns true if the result was related to a TTS flow and was handled;
     *     false if the result was not related it, in which case you should
     *     handle it normally.
     */
	public boolean handleActivityResult(int requestCode, int resultCode, Intent data)
    {
		// if this is not for us just return false
	    if (requestCode != requestIntent) return false;
	    
        if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
            // success, create the TTS instance and rely on the onInit to be called somewhere in the future
            tts = new TextToSpeech(context, this);
        } else {
        	try {
	            // missing data, install it
	            Intent installIntent = new Intent();
	            installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
	            context.startActivity(installIntent);
        	}
        	catch (Exception e) { /* could happen, just ignore */ }
            
            // report the error and go back to the uninitialized state
            setStatus(SpeechStatus.UNINITIALIZED, SpeechError.TTS_DATA_NOT_INSTALLED);
        }

        return true;
    }

	@Override
	public void onInit(final int status)
	{
//		// post a Runnable on the main thread as we are not sure where we are
//        context.runOnUiThread(() -> {
            // if we are shutdown it is too late for everything
            if (Speech.this.status == SpeechStatus.SHUTDOWN) return;

            // check for failures
            if (status == TextToSpeech.ERROR) {
                setStatus(SpeechStatus.UNINITIALIZED, SpeechError.FAILURE);
                return;
            }

            // set the Speech object as completed listener
            tts.setOnUtteranceCompletedListener(Speech.this);

            // start speaking if anything is still pending
            startSpeech();
//        });
	}
	
	@Override
	public void onUtteranceCompleted(final String utteranceId)
	{
//		// post a Runnable on the main thread as we are not sure where we are
//        context.runOnUiThread(() -> {
            // if we are shutdown it is too late for everything
            if (Speech.this.status == SpeechStatus.SHUTDOWN) return;

            // if the current speech id if different from what we do expect, a new speech already started
            if (!utteranceId.equals(String.valueOf(speechId))) return;

            // go back to ready status
            setStatus(SpeechStatus.READY);
//        });
	}
	
	private void startSpeech()
	{
		// set the locale if specified
		if (locale != null) {
			int result = tts.setLanguage(locale);
			if (result < 0) switch (result) {
				case TextToSpeech.LANG_MISSING_DATA:
					setStatus(SpeechStatus.READY, SpeechError.LANG_MISSING_DATA);
					return;
				case TextToSpeech.LANG_NOT_SUPPORTED:
					setStatus(SpeechStatus.READY, SpeechError.LANG_NOT_SUPPORTED);
					return;
				default:
					setStatus(SpeechStatus.READY, SpeechError.FAILURE);
					return;
			}
		}
		
		// abort if no text specified
		if (text == null) {
			setStatus(SpeechStatus.READY);
			return;
		}
		
		// start the speech
		HashMap<String, String> myHashAlarm = new HashMap<>();
		myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(speechId));
		tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);		
		setStatus(SpeechStatus.SPEAKING);
	}

	/**
	 * Changes the speech status, reporting it through the callback interface
	 * @param status The new status
	 */
	private void setStatus(SpeechStatus status)
	{
		setStatus(status, null);
	}
	
	/**
	 * Changes the speech status, reporting it through the callback interface, together with the provided error.
	 * Must be called in UI thread.
	 * @param status The new status
	 * @param error The error
	 */
	private void setStatus(SpeechStatus status, SpeechError error)
	{
		if (Misc.DEBUG) Log.i(TAG, "New speech status: " + status + (error != null ? ", due to error: " + error : ""));
		this.status = status;
		if (callback != null) {
			if (error != null) callback.onSpeechError(error);
			callback.onNewSpeechStatus(status);
		}
	}	
	
	public enum SpeechError {
		/**Denotes a generic operation failure */
		FAILURE,
		/** TTS data needs to be initialized, operation has been requested */
		TTS_DATA_NOT_INSTALLED,
		/** Data for the specified language needs to be installed */
		LANG_MISSING_DATA,
		/** The selected language is not available */
		LANG_NOT_SUPPORTED 
	}
	
	/** Callback interface for the activity. */
	public interface SpeechCallback {
		
		/**
		 * The speech component changed its status
		 * @param status The new status
		 */
		void onNewSpeechStatus(SpeechStatus status);
		
		/**
		 * An error occurred during the speech. This callback is independent from the status change which will happen
		 * anyway, but is meant to provide the possibility to report a message to the user.
		 * @param error The error
		 */
		void onSpeechError(SpeechError error);
	}
}
