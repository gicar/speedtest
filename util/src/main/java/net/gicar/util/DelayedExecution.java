package net.gicar.util;

import java.util.ArrayList;

import android.os.Handler;

/** Allows the delayed execution of runnables, triggered from an event */
public class DelayedExecution {

	/** The delay to be used after the trigger */
	private final int delay;
	
	/** The Handler which will be used to post the runnables */
	private final Handler handler;
	
	/** The list of runnables to be executed */
	private final ArrayList<Runnable> runnables = new ArrayList<Runnable>();
	
	/** If true the execution has already been triggered */
	boolean triggered = false;
	
	/**
	 * Creates a delayed execution handler, with the specified delay from the trigger
	 * @param delayMillis The delay to be used after the trigger
	 */
	public DelayedExecution(int delayMillis)
	{
		this.delay = delayMillis;
		this.handler = new Handler();
	}
	
	/**
	 * Registers a runnable for its execution after the trigger
	 * @param runnable The Runnable to be registered
	 */
	public void register(Runnable runnable)
	{
		// add the runnable to the list
		runnables.add(runnable);
		
		// if already triggered perform its execution
		if (triggered) trigger();
	}
	
	/**
	 * Triggers the execution of the registered runnables
	 */
	public void trigger()
	{
		// set the already triggered flag
		triggered = true;
		
		// post the execution of the registered runnables
		for (Runnable r : runnables)
			handler.postDelayed(r, delay);
		
		// clear the list of registered runnables
		runnables.clear();
	}
}
