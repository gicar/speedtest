//package net.gicar.util.services;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.util.Log;
//
//import com.google.android.gms.appinvite.AppInviteInvitation;
//import com.google.firebase.appinvite.FirebaseAppInvite;
//import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentManager;
//import net.gicar.util.Misc;
//import net.gicar.util.appcompat.XAppCompatActivity;
//
//import static android.app.Activity.RESULT_OK;
//
///** App invites service */
//public abstract class InvitesService extends Fragment {
//
//    /** The log tag */
//    protected static final String TAG = "Invites";
//
//    /** The tag for this fragment */
//    protected static final String FRAGMENT_TAG = "net.gicar.util.invites";
//
//    /** Intent request code */
//    private final int requestCode;
//
//    /**
//     * Call this in the main activity onCreate method
//     * @param activity The hosting activity
//     * @param invitesClass The non abstract fragment class
//     * @param <T> The invitesClass type
//     * @return The InvitesService instance or null if the service is not available
//     */
//    public static <T extends InvitesService> T create(@NonNull XAppCompatActivity activity,
//                                                      Class<T> invitesClass)
//    {
//        // we need the availability of Google Play service to let this work
//        if (!GooglePlayService.isAvailable(activity)) return null;
//
//        FragmentManager fragmentManager = activity.getSupportFragmentManager();
//
//        //noinspection unchecked
//        T fragment = (T) fragmentManager.findFragmentByTag(FRAGMENT_TAG);
//
//        if (fragment == null) {
//            // during initial setup, plug in the fragment
//            if (Misc.DEBUG) Log.d(TAG, "Fragment not found, creating it");
//            try {
//                fragment = invitesClass.newInstance();
//            } catch (Exception e) {
//                // we are not prepared for this to happen
//                CrashReporting.logException(e);
//                return null;
//            }
//            fragmentManager.beginTransaction().add(fragment, FRAGMENT_TAG).commit();
//        }
//
//        return fragment;
//    }
//
//    /**
//     * Creates an invites service object
//     * @param requestCode Intent request code
//     */
//    protected InvitesService(int requestCode)
//    {
//        // save the parameters
//        this.requestCode = requestCode;
//    }
//
//    /**
//     * Sends the provided invitation
//     * @param invitation The invitation
//     */
//    public void sendInvitation(@NonNull AppInviteInvitation.IntentBuilder invitation)
//    {
//        if (Misc.DEBUG) Log.d(TAG, "Sending invitation " + invitation);
//        startActivityForResult(invitation.build(), requestCode);
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data)
//    {
//        if (requestCode == this.requestCode) {
//            if (resultCode == RESULT_OK) {
//                // Get the invitation IDs of all sent messages
//                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
//                if (Misc.DEBUG) for (String id : ids) Log.d(TAG, "onActivityResult: sent invitation " + id);
//                inviteSuccess(ids);
//            } else {
//                // Sending failed or it was canceled, show failure message to the user
//                if (Misc.DEBUG) Log.i(TAG, "onActivityResult: invitation failure " + resultCode);
//                inviteFailure(resultCode);
//            }
//        }
//        else super.onActivityResult(requestCode, resultCode, data);
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        if (Misc.DEBUG) Log.d(TAG, "onActivityCreated, savedInstanceState=" + savedInstanceState);
//        super.onActivityCreated(savedInstanceState);
//
//        // Check for App Invite invitations and launch deep-link activity if possible.
//        // Requires that an Activity is registered in AndroidManifest.xml to handle
//        // deep-link URLs.
//        // The check for null should not be needed, but on some devices it will happen.
//        Activity activity = getActivity();
//        FirebaseDynamicLinks firebaseDynamicLinks = FirebaseDynamicLinks.getInstance();
//        if (firebaseDynamicLinks != null) firebaseDynamicLinks.getDynamicLink(activity.getIntent())
//                .addOnSuccessListener(activity, data -> {
//                    if (data == null) {
//                        if (Misc.DEBUG) Log.d(TAG, "getInvitation: no data");
//                        return;
//                    }
//
//                    // Get the deep link
//                    Uri deepLink = data.getLink();
//                    if (Misc.DEBUG) Log.d(TAG, "Got deep link: " + deepLink);
//
//                    // Extract invite
//                    FirebaseAppInvite invite = FirebaseAppInvite.getInvitation(data);
//                    if (invite != null) {
//                        String invitationId = invite.getInvitationId();
//                        if (Misc.DEBUG) Log.d(TAG, "Received invitation with invitationId: " + invitationId);
//                        inviteReceived(deepLink, invitationId);
//                        //noinspection UnnecessaryReturnStatement
//                        return;
//                    }
//
//                    // Handle the deep link TBD
//                    // ...
//                })
//                .addOnFailureListener(activity, e -> {
//                    if (Misc.DEBUG) Log.w(TAG, "getDynamicLink:onFailure", e);
//                });
//    }
//
//    /**
//     * Reports successful sending of invitations
//     * @param ids The IDs of the sent invitations
//     */
//    protected abstract void inviteSuccess(String[] ids);
//
//    /**
//     * Reports unsuccessful sending of invitations
//     * @param resultCode The result error code
//     */
//    protected abstract void inviteFailure(int resultCode);
//
//    /**
//     * We have received an invitation for our app
//     * @param deepLink The deep link embedded in the invitation, if any
//     * @param invitationId The invitation ID
//     */
//    protected abstract void inviteReceived(Uri deepLink, String invitationId);
//}
