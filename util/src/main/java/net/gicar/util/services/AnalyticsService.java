package net.gicar.util.services;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import net.gicar.util.Misc;

/** Provides analytics capability through Firebase */
public class AnalyticsService {

    /** The log tag */
    public static final String TAG = "Analytics";

    /** The analytics istance */
    private final FirebaseAnalytics firebaseAnalytics;

    /** If true automatic track screen is enabled */
    private final boolean automaticTrackScreen;

    /**
     * Creates an instance of the analytics service
     * @param context The Context which shall be used
     */
    public AnalyticsService(Context context)
    {
        // connect to analytics only if not debugging
        firebaseAnalytics = /*Misc.DEBUG ? null :*/ FirebaseAnalytics.getInstance(context);

        // check if the automatic track screen is enabled
        Bundle metadata = Misc.getApplicationMetadata(context);
        automaticTrackScreen = metadata == null ||
                metadata.getBoolean("google_analytics_automatic_screen_reporting_enabled", true);
        if (Misc.DEBUG) Log.i(TAG, "Automatic track screen: " + automaticTrackScreen);

        // if we are in an invalid instance, always log
        if (!Misc.VALID) firebaseAnalytics.setAnalyticsCollectionEnabled(true);
    }

	/**
	 * Sets a custom dimension (the Google Analytics way, except that we want a name instead of the index)
	 * @param name The custom dimension name
	 * @param value The custom dimension value
	 */
	public void setCustomDimension(String name, String value)
	{
        if (Misc.DEBUG) Log.d(TAG, "Tracking custom dimension name=" + name + ", value=" + value);

        // set the dimension with a property
        if (firebaseAnalytics != null) firebaseAnalytics.setUserProperty(name, value);
	}

	/**
	 * Logs a specific event (the Google Analytics way)
	 * @param category A category (required) this String defines the event category. You might define event categories
	 *                 based on the class of user actions, like clicks or gestures or voice commands, or you might
	 *                 define them based upon the features available in your application (play, pause, fast forward, etc.).
	 * @param action An action (required) this String defines the specific event action within the category specified.
	 *               In the example, we are basically saying that the category of the event is user clicks, and the
	 *               action is a button click.
	 * @param label A label (optional) this String defines a label associated with the event. For example, if you have
	 *              multiple Button controls on a screen, you might use the label to specify the specific View control
	 *              identifier that was clicked.
	 * @param value A value (optional) this integer defines a numeric value associated with the event. For example, if
	 *              you were tracking "Buy" button clicks, you might log the number of items being purchased, or their
	 *              total cost. Pass null if no value needs to be set.
	 */
	public void event(final String category, final String action, final String label, final Long value)
	{
		if (Misc.DEBUG) Log.d(TAG, "Tracking event category=" + category + ", action=" + action + ", label=" + label + ", value=" + value);

		// Build and send an Event
        Bundle params = new Bundle();
        params.putString("action", action);
        if (label != null) params.putString("label", label);
        if (value != null) params.putLong("value", value);
        logEvent(new Event(category, params));
	}

	/**
     * Logs the specified event
     * @param event The event which shall be logged
	 */
    public void logEvent(Event event) {
        if (Misc.DEBUG) Log.d(TAG, "Logging event=" + event.name + ", params=" + event.params);
        if (firebaseAnalytics != null) firebaseAnalytics.logEvent(event.name, event.params);
    }

    /**
	 * Can be called to track a screen (the Google Analytics way).
     * Note that to be effective, this call must be performed in the onResume() method of the
     * activity.
     * @param activity The current activity
     * @param screenName The screen name to be logged
	 */
	public void trackScreen(final Activity activity, final String screenName)
	{
	    if (automaticTrackScreen) return;
        if (Misc.DEBUG) Log.d(TAG, "Tracking screen: " + screenName);
        if (firebaseAnalytics != null) {
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, screenName);
            bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, activity.getClass().getSimpleName());
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
        }
	}

    /** GDPR data collector status handler */
    public static class GdprDataCollector implements net.gicar.util.gdpr.GdprDataCollector {
        @Override
        public void gdprDataCollection(Application application, boolean canCollectData) {
            if (Misc.DEBUG) Log.d(TAG, "Setting GDPR app opt out to " + !canCollectData);

            // change the collection enabling, but only if we are in a valid instance
            if (Misc.VALID) FirebaseAnalytics.getInstance(application).setAnalyticsCollectionEnabled(canCollectData);
        }
    }

    /** An event which can be logged */
    public static class Event {

        public final String name;
        public final Bundle params;

        public Event(String name, Bundle params) {
            this.name = name;
            this.params = params;
        }
    }
}
