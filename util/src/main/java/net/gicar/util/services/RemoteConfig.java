package net.gicar.util.services;

import android.util.Log;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import net.gicar.util.Misc;
import net.gicar.util.preferences.Settings;

import static com.google.firebase.remoteconfig.FirebaseRemoteConfig.VALUE_SOURCE_REMOTE;

/** Interfaces with the remote config feature of Firebase for updating local settings */
public final class RemoteConfig {

    /** The log tag */
    private static final String TAG = "RemoteConfig";

    /** The fetch intervals */
    private final long FETCH_DEFAULT = 12*60*60, FETCH_DEBUG = 5*60;

    /** The remote config singleton */
    private final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();

    /** The setting items that will be updated */
    private final Collection<Settings.SettingsItem> settingItems;

    /** Values before and after activation */
    private static Map<String, FirebaseRemoteConfigValue> oldValues, newValues;

    /**
     * Creates a RemoteConfig instance connected with the specified settings
     * @param settingItems The SettingsItems that will be updated
     */
    public RemoteConfig(Collection<Settings.SettingsItem> settingItems)
    {
        this.settingItems = settingItems;

        // fetch and activate, can't use the single call because it does not have the fetch interval param
        remoteConfig.fetch(Misc.DEBUG ? FETCH_DEBUG : FETCH_DEFAULT)
                .addOnFailureListener(e -> {
                    if (Misc.DEBUG) Log.w("Unable to fetch", e);
                })
                .addOnSuccessListener(task -> {
                    if (Misc.DEBUG) Log.d(TAG, "Fetch performed");
                    remoteConfig.activate()
                            .addOnFailureListener(e -> {
                                if (Misc.DEBUG) Log.w("Unable to activate", e);
                            })
                            .addOnSuccessListener(task2 -> {
                                if (Misc.DEBUG) Log.d(TAG, "Activate performed");

                                // save the values after the activation in our settings
                                Map<String, FirebaseRemoteConfigValue> values = remoteConfig.getAll();
                                HashSet<Settings> settings = new HashSet<>();
                                for (Settings.SettingsItem i : settingItems) {
                                    settings.add(i.getSettings());
                                    FirebaseRemoteConfigValue v = values.get(i.getTag());
                                    if (v == null || v.getSource() != VALUE_SOURCE_REMOTE) {
                                        if (Misc.DEBUG) Log.d(TAG, "No remote value received for " + i.getTag());
                                        i.reset();
                                    }
                                    else {
                                        try {
                                            String s = v.asString();
                                            if (Misc.DEBUG)
                                                Log.d(TAG, "Value received for '" + i.getTag() + "' : " + s);
                                            if (i instanceof Settings.SettingsItemBoolean)
                                                ((Settings.SettingsItemBoolean) i).setValue(Boolean.valueOf(s));
                                            else if (i instanceof Settings.SettingsItemInteger)
                                                ((Settings.SettingsItemInteger) i).setValue(Integer.valueOf(s));
                                            else if (i instanceof Settings.SettingsItemLong)
                                                ((Settings.SettingsItemLong) i).setValue(Long.valueOf(s));
                                            else if (i instanceof Settings.SettingsItemString)
                                                ((Settings.SettingsItemString) i).setValue(s);
                                            else if (Misc.DEBUG)
                                                Log.w(TAG, "Unsupported settings item for " + i);
                                        }
                                        catch (Exception e) {
                                            CrashReporting.logException(e);
                                        }
                                    }
                                }

                                // commit the modified settings
                                for (Settings s : settings) s.commit();
                            });
                });
    }
}
