package net.gicar.util.services;


import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/** Utility class to acces Google Play Services */
public class GooglePlayService {

    public static boolean isAvailable(Context context) {
        return GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS;
    }

    /**
     * Utility function for logging the current installed Google Play Services version on the target
     * @param context Context which will be used for the operation
     * @return The version of the installed Google Play Services, or null if i was impossible to get
     */
    public static String getInstalledVersionName(Context context)
    {
    	try {
			return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionName;
		} catch (Exception e) {
			return null;
		}
    }
}
