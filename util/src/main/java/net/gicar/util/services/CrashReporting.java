package net.gicar.util.services;

import android.app.Application;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

import net.gicar.util.Misc;

/** Crash reporting using Crashlytics */
public class CrashReporting {

    /** The log tag */
    private static final String TAG = "CrashReporting";

    /** If true the crash reporting is enabled */
    private static boolean canCollectData = false;

    /**
     * Checks if we are allowed to use Crashlytics
     * Note: if you want to test crashes, remember to enable crash reporting in debug also in
     * gradle file
     * @return true if we are allowed to use Crashlytics
     */
    private static boolean allowed() {
        // do not log when debugging
        return !Misc.DEBUG;
//        return true;
    }

    /**
     * Sets the enabling of the data collection for crashes.
     * Note that when switching to false the new value does not apply until the next run of the app.
     * @param canCollectData If true the crash collection can be performed.
     */
    public static void setCollectionEnabled(boolean canCollectData)
    {
        if (Misc.DEBUG) Log.d(TAG, "Data collection set to: " + canCollectData);
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(canCollectData && allowed());
        CrashReporting.canCollectData = canCollectData;
    }

    /** GDPR data collector status handler */
    public static class GdprDataCollector implements net.gicar.util.gdpr.GdprDataCollector {
        @Override
        public void gdprDataCollection(Application application, boolean canCollectData)
        {
            setCollectionEnabled(canCollectData);
        }
    }

    /**
     * Logs the specified exception
     * @param throwable The exception which shall be logged
     */
    public static void logException(Throwable throwable)
    {
        if (Misc.DEBUG) Log.e(TAG, "Logging exception", throwable);
        if (!canCollectData) return;
        if (allowed()) FirebaseCrashlytics.getInstance().recordException(throwable);
    }

    /**
     * Logs the specified message
     * @param message The message which shall be logged
     */
    public static void log(String message)
    {
        if (Misc.DEBUG) Log.e(TAG, "Logging message: " + message);
        if (!canCollectData) return;
        if (allowed()) FirebaseCrashlytics.getInstance().log(message);
    }

    /**
     * Sets a value which will be passed with crashes
     * @param key The key
     * @param value The value
     */
    public static void setInt(String key, int value) {
        if (Misc.DEBUG) Log.i(TAG, "Setting key='" + key + "', value=" + value);
        if (!canCollectData) return;
        if (allowed()) FirebaseCrashlytics.getInstance().setCustomKey(key, value);
    }

    /**
     * Sets a value which will be passed with crashes
     * @param key The key
     * @param value The value
     */
    public static void setString(String key, String value) {
        if (Misc.DEBUG) Log.i(TAG, "Setting key='" + key + "', value=" + value);
        if (!canCollectData) return;
        if (allowed()) FirebaseCrashlytics.getInstance().setCustomKey(key, value);
    }

    /** Force a crash for testing purposes */
    public static void crash() {
        if (Misc.DEBUG) Log.i(TAG, "Performing a crash test");
        if (!canCollectData) {
            if (Misc.DEBUG) Log.e(TAG, "Sorry, crash reporting was not started");
            return;
        }
        if (allowed()) throw new RuntimeException("Test crash");
    }
}
