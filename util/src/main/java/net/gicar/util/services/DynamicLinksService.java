package net.gicar.util.services;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;

import net.gicar.util.Misc;

/** App dynamic links service */
public class DynamicLinksService {

    /** The log tag */
    private static final String TAG = "DynamicLinks";

    /** Our activity */
    private final Activity activity;

    public DynamicLinksService(Activity activity) {
        this.activity = activity;
    }

    /**
     * Utility function to send dynamic links
     * @param shareTitle The title to be used for sharing
     * @param link The link
     */
    public void sendDynamicLink(String shareTitle, String link)
    {
        // create the intent and share it
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, link);
        activity.startActivity(Intent.createChooser(intent, shareTitle));
    }

    /**
     * Retrieves the dynamic link from the launch intent or from Firebase
     * @param intent The activity launch intent
     * @param dynamicLinkHandler The handler for the received link
     */
    public void handleIntent(Intent intent, DynamicLinkHandler dynamicLinkHandler)
    {
        // Check for launch deep-link activity if possible.
        // Requires that an Activity is registered in AndroidManifest.xml to handle
        // deep-link URLs.
        FirebaseDynamicLinks firebaseDynamicLinks = FirebaseDynamicLinks.getInstance();
        firebaseDynamicLinks.getDynamicLink(intent)
            .addOnSuccessListener(activity, data -> {
                if (data == null) {
                    if (Misc.DEBUG) Log.d(TAG, "getDynamicLink: no data");
                    return;
                }

                // Get the link
                Uri link = data.getLink();
                if (Misc.DEBUG) Log.d(TAG, "Got dynamic link: " + link);

                // Handle the deep link
                if (dynamicLinkHandler != null) dynamicLinkHandler.dynamicLinkReceived(link);
            })
            .addOnFailureListener(activity, e -> {
                if (Misc.DEBUG) Log.w(TAG, "getDynamicLink: onFailure", e);
            });
    }

    public interface DynamicLinkHandler {

        /**
         * We have received an dynamic link for our app
         * @param link The link
         */
        void dynamicLinkReceived(Uri link);
    }
}
