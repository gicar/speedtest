package net.gicar.util;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import androidx.annotation.NonNull;
import androidx.collection.LruCache;

/**
 * Coordinates the async loading of items in target objects (mainly views).
 * Note that currently there is no optimization to handle the case where the same id needs to be loaded on
 * multiple views.
 */
public class AsyncLoader<T,V> {

	/** Observer which receives notifications of changes in the loading status. Could be null */
	private final AsyncLoaderObserver observer;

    /** The loaders which will be used to retrieve missing data */
    private final LinkedList<AsyncHandler<T,V>> handlers;

	/** Views waiting to be loaded */
	private final LinkedHashMap<V, T> pending = new LinkedHashMap<>();

	/** Views currently loading */
	private final LinkedList<V> viewsLoading = new LinkedList<>();

	/** Loader tasks currently executing; they are aligned with viewsLoading */
	private final LinkedList<LoaderTask> loaderTasks = new LinkedList<>();

	/** Cache for already loaded items */
	private final LruCache<T, AsyncData<V>> cache;

	/** If true the loader is available */
	private boolean running = true;

	/**
	 * Create an AsyncViewLoader
	 * @param handlers The handlers which will be used to fetch new data
	 * @param maxCacheSize Maximum size of the LRU cache, in units as returned by {@link AsyncData#sizeOf}
	 * @param observer Observer which receives notifications of changes in the loading status. Could be null
	 */
	public AsyncLoader(List<AsyncHandler<T,V>> handlers, int maxCacheSize, AsyncLoaderObserver observer)
	{
		this.handlers = new LinkedList<>(handlers);
		cache = new LruCache<T, AsyncData<V>>(maxCacheSize) {
			@Override
			protected int sizeOf(@NonNull T key, @NonNull AsyncData<V> value) {
				return value.sizeOf();
			}
		};
		this.observer = observer;
	}
	
	/**
	 * Starts the load of the specified id data on the specified view.
	 * @param id The identifier of the data which needs to be loaded
	 * @param view The view on which the data will be displayed
	 */
	public void loadRequest(T id, V view)
	{
		// check if we are still running
		if (!running) return;
		
		// stop the loading of the view if it was still performing an earlier request
		loadAbort(view);
		
		// verify if we already have it in the cache; if yes just update the view
		AsyncData<V> data = cache.get(id);
		if (data != null) {
			data.display(view);
			return;
		}
		
		// place the request in the queue and start the scheduler
		pending.put(view, id);
		scheduleNext();
	}

	/**
	 * Aborts the load of the specified view if still executing
	 * @param view The view load which needs to be aborted
	 */
	public void loadAbort(V view)
	{
		// check if we are still running
		if (!running) return;

		// try to remove the view from the pending ones
		if (pending.remove(view) != null) return;
		
		// if it is currently loading try to stop it
		int i = viewsLoading.indexOf(view);
		if (i == -1) return;

		// remove and cancel it
		viewsLoading.remove(i);
		loaderTasks.remove(i).cancel();
	}
	
	/** Aborts pending and future requests, clearing cached values */
	public void reset()
	{
		// delete all the pending requests
		pending.clear();

		// abort all the executing requests
		for (LoaderTask loaderTask : loaderTasks) loaderTask.cancel();
		viewsLoading.clear();
		loaderTasks.clear();

		// clear the cache
		cache.evictAll();
	}
	
	/**
	 * Stops all the pending and executing loads
	 */
	public void shutdown()
	{
		// mark us as no longer running
		running = false;

		// stop pending activities
		reset();
		
		// shutdown all the available handlers
		for (AsyncHandler<T,V> handler : handlers)
			handler.shutdown();
	}

	private void scheduleNext()
	{
		// loop until we do not have a free handler for loading or nothing more to schedule
		while (!pending.isEmpty() && !handlers.isEmpty()) {
			
			// get the first handler and remove it
			AsyncHandler<T,V> handler = handlers.removeFirst();
			
			// get the first pending entry and remove it
			Entry<V,T> x = pending.entrySet().iterator().next();
			pending.remove(x.getKey());
			
			// create the loader task, add it to its list and start its execution
			LoaderTask loaderTask = new LoaderTask(handler, x.getValue());
			viewsLoading.add(x.getKey());
			loaderTasks.add(loaderTask);
			loaderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}

		// report the number of loads currently executing
		if (observer != null) observer.updateLoadingCount(pending.size() + viewsLoading.size());
	}

    /** Fetcher task for view contents */
    @SuppressLint("StaticFieldLeak")
    private class LoaderTask extends AsyncTask<Void, AsyncData<V>, AsyncData<V>> {
    	
    	/** The handler which will be used for this request */
    	private final AsyncHandler<T,V> handler;
    	
    	/** Id which needs to be loaded */
    	private final T id;
    	
		LoaderTask(AsyncHandler<T,V> handler, T id)
    	{
    		this.handler = handler;
    		this.id = id;
    	}
		
		/** Cancels the execution of this task */
		void cancel()
		{
			cancel(handler.cancel());
		}
    	
		@Override
    	protected AsyncData<V> doInBackground(Void... params)
    	{
			// start the load
			return handler.backgroundLoad(id);
    	}

	    @Override
    	protected void onPostExecute(AsyncData<V> data)
    	{
	    	try {
				// find ourselves in the list of executing threads, if we are still there (not cancelled)
				int i = loaderTasks.indexOf(this);
				if (i == -1) return;
				
				// remove the thread and the view
				loaderTasks.remove(i);
				V view = viewsLoading.remove(i);
				
				// check if we actually got any useful data
				if (data != null) {
				
					// display the contents in the view
					data.display(view);
					
					// save the data in the cache
					if (data.isCachable())
						cache.put(id, data);
				}
	    	}
	    	finally {
				// put back the handler and schedule the next load
	    		onCancelled();
	    	}
    	}

		@Override
		protected void onCancelled()
		{
			// if we are no longer running, just shutdown the handler
			if (!running) {
				handler.shutdown();
				return;
			}
			
			// put back the handler and schedule the next load
			handlers.add(handler);
			scheduleNext();
		}
    }

    /** Wraps a data request that need to be loaded asynchronously */
	public interface AsyncHandler<T,V> {
		
		/**
		 * Will be called in a background thread to perform the actual loading of data
		 * @param id The identifier of the data which needs to be loaded
		 * @return The data corresponding to this request or null if not available
		 */
		AsyncData<V> backgroundLoad(T id);
		
		/**
		 * During the beckgroundLoad() execution will be called from another thread to cancel its execution.
		 * Note that the calling thread could be the UI thread.
		 * @return true if an interrupt request needs to be sent to the thread executing the backgroundLoad()
		 */
		boolean cancel();
		
		/**
		 * Will be called as the last request to this handler, to let it release its resources.
 		 * Note that the calling thread could be the UI thread.
		 */
		void shutdown();
	}

	/** The result of a loading */
	public interface AsyncData<V> {
		
		/**
		 * Verifies if this data is cachable
		 * @return true if this item can be put in the LRU cache
		 */
		boolean isCachable();
		
		/**
		 * Size of this item
		 * @return The size of this item, in LRU cache units
		 */
		int sizeOf();

		/**
		 * Displays data on a view
		 * @param view The view where the data shall be displayed
		 */
		void display(V view);
	}

	/** Reports status changes of the AsyncViewLoader */
	public interface AsyncLoaderObserver {

		/**
		 * Implement this method to give visual indication on loading progress
		 * @param n Total number of loads currently executing plus the pending ones
		 */
		void updateLoadingCount(int n);
	}
}
