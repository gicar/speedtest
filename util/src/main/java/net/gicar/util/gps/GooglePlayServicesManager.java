//package net.gicar.util.gps;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentSender.SendIntentException;
//import android.os.Bundle;
//import androidx.annotation.NonNull;
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentManager;
//import android.util.Log;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GoogleApiAvailability;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
//import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import net.gicar.util.Misc;
//import net.gicar.util.appcompat.XAppCompatActivity;
//
///** Main access class for Google Play services. */
//public class GooglePlayServicesManager extends Fragment implements GooglePlayServices,
//        ConnectionCallbacks, OnConnectionFailedListener {
//
//	/** The log tag */
//    public static final String TAG = "GooglePlayServices";
//
//    /** The state resolving string name for the saved instance */
//	private static final String STATE_RESOLVING_ERROR = "gplays_resolving_error";
//
//    /** Name of this fragment */
//    static private final String GPS_FRAGMENT = "GPS_FRAGMENT";
//
//    /** The services we need to link to */
//    private final List<GPService> services = new ArrayList<>();
//
//    /** The GooglePlayAvailability helper class */
//    private GoogleApiAvailability googleApiAvailability;
//
//	/** The preferences handling object */
//	private GpsPreferences preferences;
//
//	/** The listener for events */
//	private GooglePlayServicesListener listener;
//
//    /** Request code to use when launching the resolution activity */
//    private int resolveErrorIntent;
//
//	/** The Google Api client, could be null if no services needs connection */
//	private GoogleApiClient client;
//
//    /** Bool to track whether the app is already resolving an error */
//    private boolean resolvingError;
//
//    /** If false the Google Play Services are not available on this device */
//    private boolean available;
//
//    /**
//     * Utility function for logging the current installed Google Play Services version on the target
//     * @param context Context which will be used for the operation
//     * @return The version of the installed Google Play Services, or null if i was impossible to get
//     */
//    public static String getInstalledVersionName(Context context)
//    {
//    	try {
//			return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionName;
//		} catch (Exception e) {
//			return null;
//		}
//    }
//
//    /**
//     * Call this on the onCreate() method of the Activity
//     * @param activity The Activity
//     * @param savedInstanceState The savedInstanceState Bundle received by onCreate()
//     * @param resolveErrorIntent Request code for the resolve error flow
//     * @param listener The listener interface
//     * @return The GPS interface
//     */
//    public static GooglePlayServicesManager create(XAppCompatActivity activity, Bundle savedInstanceState,
//                                                   int resolveErrorIntent, GooglePlayServicesListener listener)
//    {
//        // find our fragment
//        FragmentManager fm = activity.getSupportFragmentManager();
//        GooglePlayServicesManager fragment;
//        if (savedInstanceState == null) {
//            // during initial setup, plug in the fragment.
//            if (Misc.DEBUG) Log.d(TAG, "Creating new fragment");
//            fragment = new GooglePlayServicesManager();
//            fm.beginTransaction().add(fragment, GPS_FRAGMENT).commitNow();
//        }
//        else fragment = (GooglePlayServicesManager) fm.findFragmentByTag(GPS_FRAGMENT);
//
//        // perform initialization
//        fragment.init(activity, savedInstanceState, resolveErrorIntent, listener);
//
//        return fragment;
//    }
//
//    /**
//     * Limited version for creating a new GooglePlayServices object. Call this in the onCreate()
//     * With this setup no error resolution is available: simpler setup but you shouldn't use it on the main activity.
//     * @param activity The activity using the services
//     * @param savedInstanceState The savedInstanceState Bundle received by onCreate()
//     */
//    public static GooglePlayServicesManager create(XAppCompatActivity activity, Bundle savedInstanceState)
//    {
//        return create(activity, savedInstanceState, 0, null);
//    }
//
//    /** Default constructor for the Fragment */
//    public GooglePlayServicesManager() {}
//
//	/**
//	 * Creates a new GooglePlayServices object. Call this in the onCreate()
//	 * @param activity The activity using the services
//	 * @param resolveErrorIntent Request code for the resolve error flow
//	 * @param savedInstanceState The saved Bundle instance, pass null if none
//	 * @param listener The listener interface
//	 */
//	private void init(XAppCompatActivity activity, Bundle savedInstanceState,
//                      int resolveErrorIntent, GooglePlayServicesListener listener)
//	{
//		// save the parameters
//		this.resolveErrorIntent = resolveErrorIntent;
//		this.listener = listener;
//
//        // Get the GoogleApiAvailability instance
//        googleApiAvailability = GoogleApiAvailability.getInstance();
//
//		// get hold of the preferences
//		preferences = ((GpsApplication)activity.getApplication()).getGpsPreferences();
//
//		// initialize the resolvingError status
//		resolvingError = savedInstanceState != null && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);
//
//        // Although the API considers SERVICE_INVALID to be resolvable, it can cause crashes
//        // on devices with no GmsCore installed at all (like emulators) and therefore we will
//        // avoid trying to resolve it.
//        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
//        available = status == ConnectionResult.SUCCESS ||
//                (status != ConnectionResult.SERVICE_INVALID && googleApiAvailability.isUserResolvableError(status));
//
//		if (Misc.DEBUG) {
//            Log.d(TAG, "Creating Google Play services client: " + activity.getClass().getSimpleName());
//            if (!available) Log.i(TAG, "Google Play services are not available on this device");
//        }
//	}
//
//    @Override
//    public void add(GPService service)
//    {
//        services.add(service);
//        service.gps = this;
//    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        if (Misc.DEBUG) Log.d(TAG, "onActivityCreated: " + getActivity().getClass().getSimpleName());
//        super.onActivityCreated(savedInstanceState);
//
//		// create the client if the services need it
//		boolean needsConnect = false;
//		GoogleApiClient.Builder builder = new GoogleApiClient.Builder(getActivity());
//		for (GPService service : services) needsConnect |= service.addService(builder);
//		if (needsConnect && available)
//			client = builder.addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
//		else
//			client = null;
//
//        // notify the services of the event
//        for (GPService service : services) service.onActivityCreated();
//	}
//
//    @Override
//    public boolean isAvailable() {
//        return available;
//    }
//
//    @Override
//    public void setConnectOnStart(boolean connectOnStart)
//    {
//		if (Misc.DEBUG) Log.d(TAG, "Setting connectOnStart=" + connectOnStart);
//		preferences.connectOnStart.setValue(connectOnStart);
//        preferences.commit();
//    }
//
//    @Override
//    public GoogleApiClient getClient()
//    {
//    	return client;
//    }
//
//    /**
//     * Returns the current connection status
//     * @return true if we are currently connected
//     */
//    public boolean isConnected()
//    {
//    	return client != null && client.isConnected();
//    }
//
//	/** Starts the connection process */
//	public void requestConnect()
//	{
//		if (Misc.DEBUG) Log.d(TAG, "Requested connection by activity");
//		connect();
//	}
//
//	/** Starts the disconnection process, this implies also signing out */
//	public void requestDisconnect()
//	{
//		if (Misc.DEBUG) Log.d(TAG, "Requested disconnection by activity");
//
//		// we do not want to automatically connect since next start
//		setConnectOnStart(false);
//
//		// if we are connected request all the services to sign out
//		if (isConnected()) for (GPService service : services) service.signOut();
//
//		// disconnect the client
//		disconnect();
//	}
//
//	/** Starts a connection if the conditions are suitable */
//	void connect()
//	{
//		if (client != null && available && !resolvingError && !client.isConnecting() && !client.isConnected()) {
//			if (Misc.DEBUG) Log.i(TAG, "Connecting to Google Play services");
//			client.connect();
//			if (listener != null) listener.connectStart();
//		}
//	}
//
//	/** Performs a client disconnection */
//	private void disconnect()
//	{
//		if (client != null) client.disconnect();
//	}
//
//    @Override
//	public void onStart()
//	{
//		if (Misc.DEBUG) Log.d(TAG, "onStart: " + getActivity().getClass().getSimpleName());
//        super.onStart();
//
//		// if we want to connect on start, just do it
//		if (preferences.connectOnStart.getValue()) connect();
//
//		// notify the services of the event
//		for (GPService service : services) service.onStart();
//	}
//
//    @Override
//	public void onResume()
//	{
//		if (Misc.DEBUG) Log.d(TAG, "onResume: " + getActivity().getClass().getSimpleName());
//        super.onResume();
//
//		// check the availability of the Google Play Services (if we need to connect)
//		if (client != null) {
//			int result = googleApiAvailability.isGooglePlayServicesAvailable(getActivity());
//			if (result != ConnectionResult.SUCCESS) {
//
//				if (Misc.DEBUG) Log.d(TAG, "Service not available: " + result);
//
//	        	// show the error resolve dialog if we are not already resolving it
//	            if (!resolvingError) showErrorDialog(result);
//			}
//		}
//
//		// notify the services of the event
//		for (GPService service : services) service.onResume();
//	}
//
//    @Override
//	public void onStop()
//	{
//		if (Misc.DEBUG) Log.d(TAG, "onStop: " + getActivity().getClass().getSimpleName());
//        super.onStop();
//
//		// notify the services of the event
//		for (GPService service : services) service.onStop();
//
//		// disconnect
//		if (client != null) {
//			if (Misc.DEBUG) Log.i(TAG, "Disconnecting from Google Play services");
//			client.disconnect();
//		}
//	}
//
//    @Override
//	public void onPause()
//	{
//		if (Misc.DEBUG) Log.d(TAG, "onPause: " + getActivity().getClass().getSimpleName());
//        super.onPause();
//
//		// notify the services of the event
//		for (GPService service : services) service.onPause();
//	}
//
//    @Override
//	public void onDestroy()
//	{
//		if (Misc.DEBUG) Log.d(TAG, "onDestroy: " + getActivity().getClass().getSimpleName());
//        super.onDestroy();
//
//		// notify the services of the event
//		for (GPService service : services) service.onDestroy();
//	}
//
//    @Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data)
//    {
//		// if it is not our intent query the services
//		if (requestCode != resolveErrorIntent) {
//
//			// stop at first match
//			for (GPService service : services)
//				if (service.handleActivityResult(requestCode, resultCode, data))
//					return;
//
//			// if no service claimed go to super class
//			super.onActivityResult(requestCode, resultCode, data);
//            return;
//		}
//
//        resolvingError = false;
//        if (resultCode == Activity.RESULT_OK) {
//    		if (Misc.DEBUG) Log.d(TAG, "Problem resolved, go connect");
//    		connect();
//        }
//		else if (resultCode == Activity.RESULT_CANCELED) {
//
//			// User cancelled.
//    		if (Misc.DEBUG) Log.d(TAG, "Got a cancellation result, so disconnecting");
//
//    		// notify the listener
//    		if (listener != null) listener.connectCancel();
//
//    		// disconnect
//    		disconnect();
//		}
//		else {
//    		if (Misc.DEBUG) Log.i(TAG, "Problem still present: " + resultCode);
//
//    		// notify the listener
//    		if (listener != null) listener.connectFail(resultCode);
//        }
//    }
//
//    @Override
//	public void onSaveInstanceState(Bundle outState) {
//	    outState.putBoolean(STATE_RESOLVING_ERROR, resolvingError);
//	}
//
//	@Override
//	public void onConnectionFailed(@NonNull ConnectionResult result)
//	{
//		if (Misc.DEBUG) Log.i(TAG, "Connection failed: " + result);
//
//        if (resolvingError) {
//            // Already attempting to resolve an error.
//            return;
//        } else if (result.hasResolution() && resolveErrorIntent != 0) {
//            try {
//            	resolvingError = true;
//                result.startResolutionForResult(getActivity(), resolveErrorIntent);
//            } catch (SendIntentException e) {
//                // There was an error with the resolution intent. Try again forcing it
//                client.connect();
//            }
//        } else {
//
//        	// get the error code
//        	int errorCode = result.getErrorCode();
//
//        	// show the error resolve dialog
//            showErrorDialog(errorCode);
//
//    		// notify the listener
//    		if (listener != null) listener.connectFail(errorCode);
//        }
//	}
//
//	@Override
//	public void onConnected(Bundle connectionHint)
//	{
//		if (Misc.DEBUG) Log.i(TAG, "Connection success: " + connectionHint);
//
//		// we want to automatically connect since next start
//		setConnectOnStart(true);
//
//		// notify the services of the event
//		for (GPService service : services) service.onConnected(connectionHint);
//
//		// notify the listener
//		if (listener != null) listener.connectSuccess();
//	}
//
//	@Override
//	public void onConnectionSuspended(int cause)
//	{
//		if (Misc.DEBUG) Log.i(TAG, "Connection suspended: " + cause);
//
//		// notify the services of the event
//		for (GPService service : services) service.onConnectionSuspended(cause);
//
//		// notify the listener
//		if (listener != null) listener.connectFail(cause);
//	}
//
//    /* Creates a dialog for an error message */
//    private void showErrorDialog(int errorCode)
//    {
//        // if no user resolution possible just give up; we shall never arrive to this condition here,
//        // this is just for maximum safety
//        if (!googleApiAvailability.isUserResolvableError(errorCode)) {
//            available = false;
//            return;
//        }
//
//    	// create and show the dialog
//		Dialog dialog = googleApiAvailability.getErrorDialog(getActivity(), errorCode, resolveErrorIntent);
//		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//
//			@Override
//			public void onDismiss(DialogInterface dialog) {
//				resolvingError = false;
//			}
//		});
//		dialog.show();
//
//		// mark that we are already trying to resolve the error
//		resolvingError = true;
//    }
//}
