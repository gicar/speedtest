//package net.gicar.util.gps;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.net.Uri;
//import androidx.annotation.NonNull;
//import android.util.Log;
//
//import com.google.android.gms.appinvite.AppInviteInvitation;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.firebase.appinvite.FirebaseAppInvite;
//import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
//import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
//
//import net.gicar.util.Misc;
//
//import static android.app.Activity.RESULT_OK;
//
///** App invites service */
//public class InvitesService extends GPService {
//
//    /** The log tag */
//    private static final String TAG = "InvitesService";
//
//    /** Intent request code */
//    private final int requestCode;
//
//    /** If true the deep link embedded in the invitation will be launched */
//    private final boolean launchDeepLink;
//
//    /** InvitesService result handler */
//    private final InvitesResultHandler resultHandler;
//
//    /**
//     * Creates an invites service object
//     * @param requestCode Intent request code
//     * @param launchDeepLink If true the deep link embedded in the invitation will be launched
//     * @param resultHandler InvitesService result handler or null if none
//     */
//    public InvitesService(int requestCode, boolean launchDeepLink, InvitesResultHandler resultHandler)
//    {
//        // save the parameters
//        this.requestCode = requestCode;
//        this.launchDeepLink = launchDeepLink;
//        this.resultHandler = resultHandler;
//    }
//
//    /**
//     * Sends the provided invitation
//     * @param invitation The invitation
//     * @return True if it was possible to sent the invitation intent, false in case of error
//     */
//    public boolean sendInvitation(AppInviteInvitation.IntentBuilder invitation)
//    {
//        if (Misc.DEBUG) Log.d(TAG, "Sending invitation " + invitation);
//
//        try {
//            gps.startActivityForResult(invitation.build(), requestCode);
//            return true;
//        }
//        catch (Exception e) {
//            if (Misc.DEBUG) Log.w(TAG, "Unable to send invitation: " + e);
//            return false;
//        }
//    }
//
//    @Override
//    protected boolean handleActivityResult(int requestCode, int resultCode, Intent data)
//    {
//        // discard events not related to us
//        if (requestCode != this.requestCode) return false;
//
//        if (resultCode == RESULT_OK) {
//            // Get the invitation IDs of all sent messages
//            String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
//            if (Misc.DEBUG) for (String id : ids) Log.d(TAG, "onActivityResult: sent invitation " + id);
//            if (resultHandler != null) resultHandler.inviteSuccess(ids);
//        } else {
//            // Sending failed or it was canceled, show failure message to the user
//            if (Misc.DEBUG) Log.i(TAG, "onActivityResult: invitation failure " + resultCode);
//            if (resultHandler != null) resultHandler.inviteFailure(resultCode);
//        }
//
//        return true;
//    }
//
//    @Override
//    protected void onActivityCreated()
//    {
//        // do not proceed if GPS not available
//        if (!gps.isAvailable()) return;
//
//        // Check for App Invite invitations and launch deep-link activity if possible.
//        // Requires that an Activity is registered in AndroidManifest.xml to handle
//        // deep-link URLs.
//        Activity activity = gps.getActivity();
//        FirebaseDynamicLinks.getInstance().getDynamicLink(activity.getIntent())
//                .addOnSuccessListener(activity, new OnSuccessListener<PendingDynamicLinkData>() {
//                    @Override
//                    public void onSuccess(PendingDynamicLinkData data) {
//                        if (data == null) {
//                            if (Misc.DEBUG) Log.d(TAG, "getInvitation: no data");
//                            return;
//                        }
//
//                        // Get the deep link
//                        Uri deepLink = data.getLink();
//                        if (Misc.DEBUG) Log.d(TAG, "Got deep link: " + deepLink);
//
//                        // Extract invite
//                        FirebaseAppInvite invite = FirebaseAppInvite.getInvitation(data);
//                        if (invite != null) {
//                            String invitationId = invite.getInvitationId();
//                            if (Misc.DEBUG) Log.d(TAG, "Received invitation with invitationId: " + invitationId);
//                            if (resultHandler != null)
//                                resultHandler.inviteReceived(deepLink, invitationId);
//                            return;
//                        }
//
//                        // Handle the deep link TBD
//                        // ...
//                    }
//                })
//                .addOnFailureListener(activity, new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        if (Misc.DEBUG) Log.w(TAG, "getDynamicLink:onFailure", e);
//                    }
//                });
//    }
//
//    /** Receives notifications about invites result */
//    public interface InvitesResultHandler {
//
//        /**
//         * Reports successful sending of invitations
//         * @param ids The IDs of the sent invitations
//         */
//        void inviteSuccess(String[] ids);
//
//        /**
//         * Reports unsuccessful sending of invitations
//         * @param resultCode The result error code
//         */
//        void inviteFailure(int resultCode);
//
//        /**
//         * We have received an invitation for our app
//         * @param deepLink The deep link embedded in the invitation, if any
//         * @param invitationId The invitation ID
//         */
//        void inviteReceived(Uri deepLink, String invitationId);
//    }
//}
