//package net.gicar.util.gps;
//
//import net.gicar.util.preferences.Settings;
//
///** Class holding configuration preferences for the Google Play services */
//class GpsPreferences extends Settings {
//
//    /** The preferences file */
//    private final static String GPS_SHARED_PREFS = "Gicci_Gps_Prefs";
//
//    /** Preference key for the connect on start request */
//    private final static String GPS_KEY_CONNECT_ON_START = "Key_Connect_on_start";
//
//    GpsPreferences() {
//        super(GPS_SHARED_PREFS);
//    }
//
//    /** Connect on start */
//    public final SettingsItemBoolean connectOnStart = new SettingsItemBoolean(GPS_KEY_CONNECT_ON_START, false);
//}
