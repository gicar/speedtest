//package net.gicar.util.gps;
//
//import android.app.Activity;
//import android.content.Intent;
//
//import com.google.android.gms.common.api.GoogleApiClient;
//
///**
// * Main access class for Google Play services.
// */
//public interface GooglePlayServices {
//
//    /**
//     * Adds a service. Call this only during onCreate()
//     * @param service The service to be added
//     */
//    void add(GPService service);
//
//    /**
//     * Returns the state of the available flag
//     * @return If false the Google Play Services are not available on this device
//     */
//    boolean isAvailable();
//
//    /**
//     * Sets the wanted connection status
//     * @param connectOnStart Pass true to request connection on activity start
//     */
//    void setConnectOnStart(boolean connectOnStart);
//
//    /**
//     * Getter method for the API client
//     * @return The API client, null if not present
//     */
//    GoogleApiClient getClient();
//
//    // The following methods of the manager are visible to help services
//    Activity getActivity();
//    void startActivityForResult(Intent intent, int requestCode);
//}
