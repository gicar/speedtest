//package net.gicar.util.gps;
//
//import android.util.Log;
//
//import net.gicar.util.Misc;
//import net.gicar.util.appcompat.XApplication;
//
///** To use Google Play Services the app needs to use this Application */
//public class GpsApplication extends XApplication {
//
//    /** The log tag */
//    static public String TAG = "GpsApplication";
//
//    /** GPS settings */
//    private GpsPreferences gpsPreferences;
//
//    @Override
//    public void onCreate()
//    {
//        super.onCreate();
//
//        if (Misc.DEBUG) Log.d(TAG, "onCreate");
//
//        // get the GPS preferences
//        gpsPreferences = new GpsPreferences();
//        gpsPreferences.onCreate(getApplicationContext());
//    }
//
//    GpsPreferences getGpsPreferences() {
//        return gpsPreferences;
//    }
//}
