//package net.gicar.util.gps;
//
//import android.content.Intent;
//import android.os.Bundle;
//
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
//
///** Common base class for each one of the supported Api */
//public abstract class GPService implements ConnectionCallbacks {
//
//	/** Our services class */
//	protected GooglePlayServices gps;
//
//	/**
//	 * Adds this service to the configured ones
//	 * @param builder The builder object to use
//	 * @return Indication if the service needs connection to be performed or not
//	 */
//	protected boolean addService(GoogleApiClient.Builder builder) { return false; }
//
//    protected void onActivityCreated() {}
//    protected void onStart() {}
//    protected void onResume() {}
//    protected void onStop() {}
//    protected void onPause() {}
//    protected void onDestroy() {}
//
//    /**
//     * This method will be called in the activity intent result handling
//     * @param requestCode The requestCode of the intent.
//     * @param resultCode The resultCode of the intent.
//     * @param data The data (Intent) as you received it.
//     * @return Returns true if the result was related to this service flow and was handled;
//     *     false if the result was not related to that.
//     */
//    protected boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
//		return false;
//	}
//
//	/** An activity requested disconnection, so we need to sign out as well */
//    protected void signOut() {}
//
//	@Override
//	public void onConnected(Bundle connectionHint) {}
//
//	@Override
//	public void onConnectionSuspended(int cause) {}
//}
