//package net.gicar.util.gps;
//
///** Call-backs for GooglePlayService events */
//public interface GooglePlayServicesListener {
//
//	/**
//	 * A connection request has been started. Will be followed by fail, success or cancel events.
//	 * The start notification could be repeated if multiple re-attempts are performed.
//	 */
//	void connectStart();
//
//	/**
//	 * The connect has failed. Could also be triggered spontaneously in case of services failures during connection.
//	 * @param errorCode The error code. Note that the error code could belong to the main GooglePlayServices
//	 * 					 range, or to one of the attached services.
//	 */
//	void connectFail(int errorCode);
//
//	/** The connect request was successfully performed */
//	void connectSuccess();
//
//	/** The connect request was cancelled */
//	void connectCancel();
//}
