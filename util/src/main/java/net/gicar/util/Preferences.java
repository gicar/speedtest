package net.gicar.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Utility class for handling application preferences
 */
public abstract class Preferences implements OnSharedPreferenceChangeListener {

	/** The log tag */
	protected static final String TAG = "Preferences";
	
	/** The version with which the preferences were created */
	private final static String CREATE_VERSION = "create_version";

	/** The first version we run */
	protected int createVersion;
	
	/** The version stored in the preferences */
	private final static String LAST_VERSION = "last_version";

	/** The last version we run */
	protected int lastVersion;
	
	/** Signals if this is the first run of the application */
	protected boolean firstRun;
	
	/** Signal if this is the first time the application is run with this version */
	protected boolean firstVersionRun;
		
	/** The settings */
	final protected SharedPreferences settings;
	
	/** handler for changes in the preferences */
	final private PreferenceChangeHandler handler;
	
	/** if <code>true</code> the preferences notification is paused */
	private boolean paused = true;

	/** Flag to notify changes in preferences when paused */
	private boolean settingsChanged = false;
	
	/**
	 * Creates a new preference handler 
     * @param ctx The context
     * @param name The name for the shared preferences or null for the default name
	 * @param xmlId the ID of the XML of the preferences, from where the defaults will be got
	 * @param handler The handler for the preferences changes
	 */
	protected Preferences(Context ctx, String name, int xmlId, PreferenceChangeHandler handler)
	{
		settings = getSharedPreferences(ctx, name);
		
		// get the info about the versions
		createVersion = getInt(CREATE_VERSION);
		lastVersion = getInt(LAST_VERSION);
		int currentVersion = Misc.getAppVersionNumber(ctx);
		
		// check if no version stored in the preferences or version changed from last time
		if (createVersion == 0 || lastVersion != currentVersion) {
			
			// we need to update the stored versions
			Editor e = settings.edit();
			
			// if no create version stored, add it to the preferences
			if (createVersion == 0) {
				if (Misc.DEBUG) Log.i(TAG, "Setting the preferences create version to the current version: " + currentVersion);
				e.putInt(CREATE_VERSION, currentVersion);
				firstRun = true;
			}
			
			// update the last run version to the current one
			if (Misc.DEBUG) Log.i(TAG, "Updating the last run version to the current version: " + currentVersion);
			e.putInt(LAST_VERSION, currentVersion);
			firstVersionRun = true;
			
			// commit the changes
			e.apply();
		}
		
		// set the default values
		setDefaultValues(ctx, name, xmlId, true);

		// load the preferences
		reloadAll();
		
		// register the handlers
		settings.registerOnSharedPreferenceChangeListener(this);
		this.handler = handler;
	}
	
	/**
	 * Returns true if this is the first time that this app is run
	 * @return <code>true</code> if this is the first time the application is run
	 */
	public boolean isFirstRun()
	{
		return firstRun;
	}
	
	/**
	 * Returns true if this is the first time that this app is run with the current version
	 * @return <code>true</code> if the version changed from the last application run
	 */
	public boolean isFirstVersionRun()
	{
		return firstVersionRun;
	}
	
	/**
	 * Getter method for the create version
	 * @return The version which first created these preferences
	 */
	public int getCreateVersion()
	{
		return createVersion;
	}
	
	/**
	 * Start the preference changes monitoring
	 */
	public void onResume()
	{
		paused = false;
		if (settingsChanged) {
			reloadAll();
			if (handler != null) handler.preferencesChange();
			settingsChanged = false;
		}
	}
	
	/**
	 * Pause the preference changes monitoring
	 */
	public void onPause()
	{
		paused = true;
		settingsChanged = false;
	}

	/**
	 * Cleanup everything
	 */
	public void onDestroy()
	{
		settings.unregisterOnSharedPreferenceChangeListener(this);
	}
	
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
	{
		if (Misc.DEBUG) Log.d(TAG, "Preference changed: " + key);
		if (paused) settingsChanged = true;
		else {
			reloadAll();
			if (handler != null) handler.preferencesChange();
		}
	}

    /**
     * Returns an editor for the preferences
     * @return The Editor which can be used to change the application settings
     */
    public Editor edit()
    {
        return settings.edit();
    }


    /**
     * Reload all the preference values, eventually setting them to default values
     */
	abstract protected void reloadAll();

	/**
	 * Read a list value mapped to integers
	 * @param key The key
	 * @return The value associated with the key
	 */
	protected int getIntegerList(String key)
	{
		try {
			return Integer.parseInt(getString(key));
		}
		catch (Exception e) {
			return 0;
		}
	}

	/**
	 * Read a String value
	 * @return The value associated with the key
	 */
	protected String getString(String key)
	{
		String v = settings.getString(key, "");
		if (Misc.DEBUG) Log.i(TAG, "Value for preference: " + key + " = " + v);		
		return v;
	}

	/**
	 * Read a integer value
	 * @return The value associated with the key
	 */
	protected int getInt(String key)
	{
		int v = settings.getInt(key, 0);
		if (Misc.DEBUG) Log.i(TAG, "Value for preference: " + key + " = " + v);		
		return v;
	}

	/**
	 * Read a boolean value
	 * @param key The key
	 * @return The value associated with the key
	 */
	protected boolean getBoolean(String key)
	{
		boolean v = settings.getBoolean(key, false);
		if (Misc.DEBUG) Log.i(TAG, "Value for preference: " + key + " = " + v);		
		return v;
	}

    /**
     * Sets the default values. Can be overridden to use specific version of preferences.
     */
    protected void setDefaultValues(Context context, String name, int resId, boolean readAgain)
    {
        // set the default values
        if (name == null) PreferenceManager.setDefaultValues(context, resId, true);
        else PreferenceManager.setDefaultValues(context, name, Context.MODE_PRIVATE, resId, true);
    }

    /**
     * Gets the settings file from name, if any.
     * Can be overridden to use specific version of preferences.
     */
    protected SharedPreferences getSharedPreferences(Context context, String name)
    {
        return name == null ? PreferenceManager.getDefaultSharedPreferences(context) :
                context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public interface PreferenceChangeHandler {
		/** Called when a change in preferences need to be handled */
		void preferencesChange();
	}
}
