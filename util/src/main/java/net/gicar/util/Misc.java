package net.gicar.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.Secure;

import com.google.common.io.BaseEncoding;

import net.gicar.util.services.CrashReporting;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/** Miscellanea Android functions */
public class Misc {

    /** Hash for "net.gicar.util.Misc" FIXME TO BE RECALCULATED! */
    private static final String CLASS_HASH = null;

    /** Controls the debugging of the app */
    public static boolean DEBUG = false;

    /** Set to true if this is a valid app instance */
    public static boolean VALID = false;

    private Misc() {}

    /**
     * Initialize the DEBUG flag. The flag value before this call could be wrong.
     *
     * @param context The <code>Context</code> to be used for the operation
     * @param debug true if this is a debug build
     * @param package64 Base64-encoded package name
     * @param signature64 Base64-encoded apk signature
     */
    public static void init(Context context, boolean debug, String package64, String signature64)
    {
        // get hashes for package and class name
        String p64 = BaseEncoding.base64().encode(context.getPackageName().getBytes(StandardCharsets.UTF_8));
        String c64 = BaseEncoding.base64().encode(Misc.class.getName().getBytes(StandardCharsets.UTF_8));

        // check for valid APK package
        VALID = package64 == null || package64.isEmpty() || p64.equals(package64);

        // if there is the debug flag, check that we have a debug build with no obfuscation
        if (isDebuggable(context)) {
            VALID &= debug && (CLASS_HASH == null || CLASS_HASH.equals(c64));
            DEBUG = VALID;
        }
        else {

            // check the APK signature
            if (signature64 != null && !signature64.isEmpty()) {
                boolean found = false;
                try {
                    Signature[] sigs = context.getPackageManager().getPackageInfo(context.getPackageName(),
                            PackageManager.GET_SIGNATURES).signatures;
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    for (Signature sig : sigs) {
                        String s64 = BaseEncoding.base64().encode(md.digest(sig.toByteArray()));
                        md.reset();
                        found |= s64.equals(signature64);
//                        Log.e("XXX", "APK Signature64: " + s64);
                    }
                } catch (Exception e) {
                    // this should not happen
                    CrashReporting.logException(e);
                }
                VALID &= found;
            }
        }

        // uncomment for unchecked debug
//        Log.e("XXX", "Package name hash in Base64 is: " + p64);
//        Log.e("XXX", "Class name hash in Base64 is: " + c64);
//        Log.e("XXX", "VALID=" + VALID);
//        Log.e("XXX", "DEBUG=" + DEBUG);
//        VALID = true;
//        DEBUG = true;
    }

    /**
     * Load a text string from a file place in the assets directory
     *
     * @param context             The <code>Context</code> to be used for the operation
     * @param fileNameIdInStrings The ID of the string holding the file name
     * @return The string loaded from the file
     */
    public static String loadStringFromAsset(Context context, int fileNameIdInStrings) {
        InputStream is = null;
        byte[] b;
        try {
            is = context.getAssets().open(context.getResources().getString(fileNameIdInStrings));
            b = new byte[is.available()];
            is.read(b);
        } catch (IOException e) {
            return null;
        } finally {
            if (is != null) try {
                is.close();
            } catch (IOException ignored) {}
        }

        return new String(b);
    }

    /**
     * Returns the version name of the application
     *
     * @param context The <code>Context</code> to be used for the operation
     * @return The version name or the empty string in case of error
     */
    public static String getAppVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            return "";
        }
    }

    /**
     * Returns the version number of the application
     *
     * @param context The <code>Context</code> to be used for the operation
     * @return The version number or 0 in case of error
     */
    public static int getAppVersionNumber(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            return 0;
        }
    }

    /**
     * Returns the name in the application label
     *
     * @param context The <code>Context</code> to be used for the operation
     * @return The application label
     */
    public static String getApplicationLabel(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    /**
     * Returns the version name of the OS
     *
     * @return The version name
     */
    public static String getOsVersionName() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * Returns the version number of the OS
     *
     * @return The version number
     */
    public static int getOsVersionNumber() {
        return android.os.Build.VERSION.SDK_INT;
    }

    /**
     * Return a list of ABIs supported by the system
     *
     * @return The list of ABIs supported by the system
     */
    @SuppressWarnings("deprecation")
    public static String[] getSupportedAbis() {
        if (Build.VERSION.SDK_INT >= 21) return Build.SUPPORTED_ABIS;

        // up to lollipop there is no array of ABIs
        if (Build.CPU_ABI2 == null) return new String[]{Build.CPU_ABI};
        return new String[]{Build.CPU_ABI, Build.CPU_ABI2};
    }

    /**
     * Returns the device id
     *
     * @param context The <code>Context</code> to be used for the operation
     * @return The unique device id
     */
    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    /**
     * Returns the debuggable status of the application
     *
     * @param context The <code>Context</code> to be used for the operation
     * @return <code>true</code> if the application is in debug mode
     */
    public static boolean isDebuggable(Context context) {
        return (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
    }

    /**
     * Runs the specified Runnable on the UI thread, immediately if we are on it
     *
     * @param runnable The runnable to be run on the UI thread
     */
    public static void runOnUiThread(Runnable runnable) {
        Looper mainLooper = Looper.getMainLooper();
        if (Looper.myLooper() == mainLooper) runnable.run();
        else new Handler(Looper.getMainLooper()).post(runnable);
    }

    /**
     * Gets the metadata present in the manifest
     * @param context The context that will be used
     * @return The bundle holding the metadata
     */
    public static Bundle getApplicationMetadata(Context context)
    {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData;
        }
        catch(Exception e) {
            return null;
        }
    }
}
