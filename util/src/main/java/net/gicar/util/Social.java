package net.gicar.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/** This class implements "social" interactions */
public class Social {
	
	/**
	 * Opens a profile/page on Facebook
	 * @param context The context to be used
	 * @param profileId The Facebook profile identifier
	 * @return <code>true</code> if it was successfully executed, <code>false</code> on error
	 */
	public static boolean facebookOpenProfile(Context context, String profileId)
	{
		if (IntentUtils.viewIntent(context, "fb://page/" + profileId)) return true;
		return IntentUtils.viewIntent(context, "http://www.facebook.com/" + profileId);
	}
	
	/**
	 * Opens a profile on Twitter
	 * @param context The context to be used
	 * @param screenName The Twitter @ screen name
	 * @return <code>true</code> if it was successfully executed, <code>false</code> on error
	 */
	public static boolean twitterOpenProfile(Context context, String screenName)
	{
		if (IntentUtils.viewIntent(context, "twitter://user?screen_name=" + screenName)) return true;
		return IntentUtils.viewIntent(context, "http://twitter.com/" + screenName);
	}

	/**
	 * Opens a page on Google+
	 * @param context The context to be used
	 * @param pageId The Google+ page identifier
	 * @return <code>true</code> if it was successfully executed, <code>false</code> on error
	 */
	public static boolean googlePlusOpenPage(Context context, String pageId)
	{
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setClassName("com.google.android.apps.plus", "com.google.android.apps.plus.phone.UrlGatewayActivity");
			intent.putExtra("customAppUri", pageId);
			context.startActivity(intent);
			return true;
		} catch(Exception e) {
			return IntentUtils.viewIntent(context, "https://plus.google.com/" + pageId + "/posts");
		}
	}

	/**
	 * Sends an email
	 * @param context The context to be used
	 * @param emailAddress The destination email address
	 * @param subject The email subject or <code>null</code> if none
	 * @param body The email body or <code>null</code> if none
	 * @return <code>true</code> if it was successfully executed, <code>false</code> on error
	 */
	public static boolean sendEmail(Context context, String emailAddress, String subject, String body)
	{
		try {
			String uriText = "mailto:" + Uri.encode(emailAddress, "UTF-8");
			if (subject != null) {
				uriText += "?subject=" + Uri.encode(subject, "UTF-8");
				if (body != null) uriText += "&body=" + Uri.encode(body, "UTF-8");
			}
	
			Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
			sendIntent.setData( Uri.parse(uriText));
			context.startActivity(Intent.createChooser(sendIntent, "Send email"));

			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
}
