package net.gicar.util.web;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.ClientCertRequest;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.gicar.util.Misc;

/** Logs all the WebViewClient calls */
public class WebViewClientLog extends WebViewClient {

    /** The log tag */
    static protected String TAG = "WebViewClient";

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (Misc.DEBUG) Log.v(TAG, "onLoadResource: " + url);
        return super.shouldOverrideUrlLoading(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        if (Misc.DEBUG) Log.v(TAG, "shouldOverrideUrlLoading: " + request);
        return super.shouldOverrideUrlLoading(view, request);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        if (Misc.DEBUG) Log.v(TAG, "onPageStarted: " + url);
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        if (Misc.DEBUG) Log.v(TAG, "onPageFinished: " + url);
        super.onPageFinished(view, url);
    }

    @Override
    public void onLoadResource(WebView view, String url) {
        if (Misc.DEBUG) Log.v(TAG, "onLoadResource: " + url);
        super.onLoadResource(view, url);
    }

    @Override
    public void onPageCommitVisible(WebView view, String url) {
        if (Misc.DEBUG) Log.v(TAG, "onPageCommitVisible: " + url);
        super.onPageCommitVisible(view, url);
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        if (Misc.DEBUG) Log.v(TAG, "shouldInterceptRequest: " + url);
        return super.shouldInterceptRequest(view, url);
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        if (Misc.DEBUG) Log.v(TAG, "shouldInterceptRequest: " + request);
        return super.shouldInterceptRequest(view, request);
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        if (Misc.DEBUG) Log.v(TAG, "onReceivedError: " + description + ", " + failingUrl);
        super.onReceivedError(view, errorCode, description, failingUrl);
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        if (Misc.DEBUG) Log.v(TAG, "onReceivedError: " + request + ", " + error);
        super.onReceivedError(view, request, error);
    }

    @Override
    public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
        if (Misc.DEBUG) Log.v(TAG, "onReceivedHttpError: " + request + ", " + errorResponse);
        super.onReceivedHttpError(view, request, errorResponse);
    }

    @Override
    public void onFormResubmission(WebView view, Message dontResend, Message resend) {
        if (Misc.DEBUG) Log.v(TAG, "onFormResubmission: " + dontResend + ", " + resend);
        super.onFormResubmission(view, dontResend, resend);
    }

    @Override
    public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
        if (Misc.DEBUG) Log.v(TAG, "doUpdateVisitedHistory: " + url + ", " + isReload);
        super.doUpdateVisitedHistory(view, url, isReload);
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        if (Misc.DEBUG) Log.v(TAG, "onReceivedSslError: " + handler + ", " + error);
        super.onReceivedSslError(view, handler, error);
    }

    @Override
    public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
        if (Misc.DEBUG) Log.v(TAG, "onReceivedClientCertRequest: " + request);
        super.onReceivedClientCertRequest(view, request);
    }

    @Override
    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
        if (Misc.DEBUG) Log.v(TAG, "onReceivedHttpAuthRequest: " + handler + ", " + host + ", " + realm);
        super.onReceivedHttpAuthRequest(view, handler, host, realm);
    }

    @Override
    public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
        if (Misc.DEBUG) Log.v(TAG, "shouldOverrideKeyEvent: " + event);
        return super.shouldOverrideKeyEvent(view, event);
    }

    @Override
    public void onUnhandledKeyEvent(WebView view, KeyEvent event) {
        if (Misc.DEBUG) Log.v(TAG, "onUnhandledKeyEvent: " + event);
        super.onUnhandledKeyEvent(view, event);
    }

    @Override
    public void onScaleChanged(WebView view, float oldScale, float newScale) {
        if (Misc.DEBUG) Log.v(TAG, "onScaleChanged: " + oldScale + ", " + newScale);
        super.onScaleChanged(view, oldScale, newScale);
    }

    @Override
    public void onReceivedLoginRequest(WebView view, String realm, String account, String args) {
        if (Misc.DEBUG) Log.v(TAG, "onReceivedLoginRequest: " + realm + ", " + account + ", " + args);
        super.onReceivedLoginRequest(view, realm, account, args);
    }
}
