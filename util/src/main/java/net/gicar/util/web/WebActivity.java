package net.gicar.util.web;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import net.gicar.util.Misc;
import net.gicar.util.Network;
import net.gicar.util.R;
import net.gicar.util.appcompat.XAppCompatActivity;

import androidx.annotation.NonNull;

/** Shows pages from the web */
public class WebActivity extends XAppCompatActivity {

    /** The activity tag */
    static public String TAG = "WebActivity";

    /** Tag in the intent extra for the URL */
    static private final String
            INTENT_EXTRA_URL = "net.gicar.web.url",
            INTENT_EXTRA_TITLE = "net.gicar.web.title";

    /** Instance state keys */
    static private final String URL_KEY = "URL_KEY";

    /** The first url */
    private String firstUrl;

    /** The current url */
    private String url = "about:blank";

    /** The WebView */
    private WebView webView;

    /** The progress bar */
    private ProgressBar progressBar;

    /** The error view */
    private View errorView;

    /** The error reason text */
    private TextView errorReasonText;

    /** The progress bar fade animation */
    private Animation animation;

    /** If true a loading is in progress */
    private boolean started, loading;

    private class WebActivityViewClient extends WebViewClientLog {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (Misc.DEBUG) Log.d(TAG, "onPageStarted: " + url);
            if (!started) {
                started = true;
                WebActivity.this.url = url;
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (Misc.DEBUG) Log.d(TAG, "onPageFinished: " + url);
            started = false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            if (Misc.DEBUG) Log.w(TAG, "Received error: " + errorCode + ", " + description);

            // set the correct state and visibility
            started = loading = false;
            progressBar.setVisibility(View.INVISIBLE);
            webView.setVisibility(View.INVISIBLE);
            errorView.setVisibility(View.VISIBLE);

            // avoid flashing the standard error page on reload
            webView.loadUrl("about:blank");

            // set a reasonable error text
            if (Network.isAvailable(WebActivity.this)) errorReasonText.setText(description);
            else errorReasonText.setText(R.string.web_error_no_internet);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            if (Misc.DEBUG) Log.v(TAG, "shouldOverrideUrlLoading: " + url);

            // check if we are still in the host of the first url
            Uri uri = Uri.parse(url);
            if (firstUrl != null) {
                Uri u = Uri.parse(firstUrl);
                String h = u.getHost();
                if (h != null && h.equals(uri.getHost())) return false;
            }

            // load the new URI outside our app
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(intent);
            }
            catch (Exception e) {
                Log.w(TAG, "Unable to handle intent " + intent, e);
                Toast.makeText(WebActivity.this, getString(R.string.web_unable_to_load_URL, url), Toast.LENGTH_LONG).show();
            }
            return true;
        }
    }

    private class WebActivityChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            if (Misc.DEBUG) Log.v(TAG, "Progress: " + newProgress);

            if (started && !loading) {
                loading = true;
                if (animation.hasStarted()) animation.cancel();
                progressBar.setVisibility(View.VISIBLE);
                webView.setVisibility(View.VISIBLE);
                errorView.setVisibility(View.INVISIBLE);
            }

            progressBar.setProgress(newProgress);
            if (newProgress == 100) {
                loading = false;
                progressBar.startAnimation(animation);
            }
        }
    }

    /**
     * Launches the web browser activity
     * @param activity The caller activity
     * @param url The url to be shown
     * @param title The title to be set
     */
    public static void startActivity(Activity activity, String url, String title)
    {
        Intent intent = new Intent(activity, WebActivity.class);
        intent.putExtra(INTENT_EXTRA_URL, url);
        intent.putExtra(INTENT_EXTRA_TITLE, title);
        activity.startActivity(intent);
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        if (Misc.DEBUG) Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        // get the current url
        Intent intent = getIntent();
        if (Misc.DEBUG) Log.d(TAG, "Intent: " + intent);
        CharSequence c = intent.getCharSequenceExtra(INTENT_EXTRA_URL);
        if (c != null) firstUrl = c.toString();
        String lastUrl = null;
        if (savedInstanceState != null) lastUrl = savedInstanceState.getString(URL_KEY);
        if (lastUrl != null) url = lastUrl;
        else if (firstUrl != null) url = firstUrl;

        // set content and get reference to views
        setContentView(R.layout.web_main);
        webView = findViewById(R.id.webview);
        progressBar = findViewById(R.id.web_progress_bar);
        errorView = findViewById(R.id.web_error);
        errorReasonText = findViewById(R.id.web_error_reason);

        // set the retry action
        findViewById(R.id.web_retry).setOnClickListener(v -> webView.loadUrl(url));

        // configure the web view
        webView.setWebViewClient(new WebActivityViewClient());
        webView.setWebChromeClient(new WebActivityChromeClient());

        // this is needed to restart timers if someone had stopped them (e.g. ads handling)
        webView.resumeTimers();

        // start loading our URL
        webView.loadUrl(url);

        // configure the animation
        animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration(500);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!loading) progressBar.setVisibility(View.INVISIBLE);
                animation.reset();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        // set the back icon in the action bar
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // set the title if provided
        CharSequence title = intent.getCharSequenceExtra(INTENT_EXTRA_TITLE);
        if (title != null) setTitle(title.toString());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (Misc.DEBUG) Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);

        // save the initialized state
        outState.putString(URL_KEY, url);
    }
}
