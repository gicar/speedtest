package net.gicar.util;

import android.util.Log;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public class GestureManager {

	/** The log tag */
	private final static String TAG = "Gesture";

	/** Min distance to consider the gesture a swipe */
	private final int swipeMinDistance;

	/** Max distance on the other axis to consider the gesture a swipe */
	private final int swipeMaxOffPath;

	/** Min and max velocity to consider the gesture a swipe */
	private final int swipeMinVelocity;

	/** The gesture detector */
	private final GestureDetector gestureDetector;

	/** The gesture handler */
	private final GestureHandler gestureHandler;

	/** No gesture */
	public static int GESTURE_NONE = 0;

	/** Swipe gesture towards left */
	public static int GESTURE_SWIPE_LEFT = 1, GESTURE_SWIPE_LEFT2 = 16;

	/** Swipe gesture towards right */
	public static int GESTURE_SWIPE_RIGHT = 2, GESTURE_SWIPE_RIGHT2 = 32;

	/** Swipe gesture towards top */
	public static int GESTURE_SWIPE_TOP = 4, GESTURE_SWIPE_TOP2 = 64;

	/** Swipe gesture towards bottom */
	public static int GESTURE_SWIPE_BOTTOM = 8, GESTURE_SWIPE_BOTTOM2 = 128;

    /** Long press */
    public static int GESTURE_LONG_PRESS = 256;

    /** Double tap */
    public static int GESTURE_DOUBLE_TAP = 512;

    /** The current gesture filter */
    private int filter;

    /** Position of down events */
    private final SparseArray<MotionEvent.PointerCoords> downCoords = new SparseArray<>();

    /** Position of up events */
    private final SparseArray<MotionEvent.PointerCoords> upCoords = new SparseArray<>();

    /**
	 * Create a new gesture manager
	 * @param v The view on which it will run
	 */
	public GestureManager(View v, GestureHandler gestureHandler)
	{
	    // save the parameters
        this.gestureHandler = gestureHandler;

		// fetch information which could be useful later on
		ViewConfiguration vc = ViewConfiguration.get(v.getContext());
		swipeMinDistance = vc.getScaledPagingTouchSlop(); // was 120
		swipeMaxOffPath = vc.getScaledPagingTouchSlop() * 4; // was 250;
        swipeMinVelocity = vc.getScaledMinimumFlingVelocity(); // was 200

		if (Misc.DEBUG) Log.d(TAG, "swipeMinDistance=" + swipeMinDistance + ", swipeMaxOffPath=" + swipeMaxOffPath +
				", swipeMinVelocity=" + swipeMinVelocity);

        // create the gesture detector
		gestureDetector = new GestureDetector(v.getContext(), new MyGestureListener());
	}

    /**
     * Configures the filter for the events on which we are interested
     * @param filter The mask of events that will be dispatched
     */
	public void setFilter(int filter)
    {
	    this.filter = filter;
	    gestureDetector.setIsLongpressEnabled((filter & GESTURE_LONG_PRESS) != 0);
    }
	
	/**
	 * Call this method before other actions in the <code>onTouch</code> method of your view
	 * @param event The event to be handled
     * @return true if the gesture handler consumed the event, else false.
	 */
    public boolean onTouch(MotionEvent event)
    {
        int action = event.getActionMasked();
        int index, id;
        MotionEvent.PointerCoords coords;

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                // start a new gesture tracking
                downCoords.clear();
                upCoords.clear();

            case MotionEvent.ACTION_POINTER_DOWN:
                // save down position of the pointer
                index = event.getActionIndex();
                id = event.getPointerId(index);
                coords = new MotionEvent.PointerCoords();
                event.getPointerCoords(index, coords);
                downCoords.put(id, coords);
                break;

            case MotionEvent.ACTION_CANCEL:
                // clear gesture tracking
                downCoords.clear();
                upCoords.clear();
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                // save up position of the pointer
                index = event.getActionIndex();
                id = event.getPointerId(index);
                coords = new MotionEvent.PointerCoords();
                event.getPointerCoords(index, coords);
                upCoords.put(id, coords);
                break;

            case MotionEvent.ACTION_MOVE:
                break;
        }

        return gestureDetector.onTouchEvent(event);
    }

    /** Handler of gestures */
    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
    	
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            // this should not happen, but better safe than sorry
            if (downCoords.size() < 1) return false;

            // calculate deltas from the same pointer
            MotionEvent.PointerCoords p1 = downCoords.get(downCoords.keyAt(0));
            MotionEvent.PointerCoords p2 = upCoords.get(downCoords.keyAt(0));
            float dX = p1.x - p2.x;
            float dY = p1.y - p2.y;

            // select the swipe direction
        	boolean horSwipe = (Math.abs(velocityX) >= Math.abs(velocityY));
        	boolean verSwipe = (Math.abs(velocityX) < Math.abs(velocityY));
        	
    		if (Misc.DEBUG) {
    			Log.d(TAG, "e1=" + e1);
    			Log.d(TAG, "e2=" + e2);
    			Log.d(TAG, "dX=" + dX + ", dY=" + dY + ", vX=" + velocityX + ", vY=" + velocityY +
                        ", horSwipe=" + horSwipe + ", verSwipe=" + verSwipe);
                for (int i = 0; i < downCoords.size(); i++)
                    Log.d(TAG, "Down P" + downCoords.keyAt(i) + ": " + print(downCoords.get(downCoords.keyAt(i))));
                for (int i = 0; i < upCoords.size(); i++)
                    Log.d(TAG, "Up   P" + upCoords.keyAt(i) + ": " + print(upCoords.get(upCoords.keyAt(i))));
    		}

            boolean pointer2 = downCoords.size() > 1;
    		int gesture = GESTURE_NONE;

        	// check if horizontal velocity and max distance are compatible with a swipe
            if (horSwipe && Math.abs(velocityX) >= swipeMinVelocity && Math.abs(dY) <= swipeMaxOffPath) {
           	
            	if (dX > swipeMinDistance) gesture = pointer2 ? GESTURE_SWIPE_LEFT2 : GESTURE_SWIPE_LEFT;
            	else if (-dX > swipeMinDistance) gesture = pointer2 ? GESTURE_SWIPE_RIGHT2 : GESTURE_SWIPE_RIGHT;
            }
            // check if vertical velocity and max distance are compatible with a swipe
            else if (verSwipe && Math.abs(velocityY) >= swipeMinVelocity && Math.abs(dX) <= swipeMaxOffPath) {
            	
            	if (dY > swipeMinDistance) gesture = pointer2 ? GESTURE_SWIPE_TOP2 : GESTURE_SWIPE_TOP;
            	else if (-dY > swipeMinDistance) gesture = pointer2 ? GESTURE_SWIPE_BOTTOM2 : GESTURE_SWIPE_BOTTOM;
            }

            return onGesture(gesture, e1);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            onGesture(GESTURE_LONG_PRESS, e);
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return onGesture(GESTURE_DOUBLE_TAP, e);
        }
    }

    private boolean onGesture(int gesture, MotionEvent event)
    {
        if (Misc.DEBUG) Log.d(TAG, "Detected gesture: " + gesture + ", event:" + event);

        // filter the detected gesture
        gesture &= filter;

        // call the handler
        if (gesture != GESTURE_NONE) gestureHandler.onGesture(gesture, event);

        // return true if we detected some gesture
        return gesture != GESTURE_NONE;
    }

    /** Handles the detected gestures */
    public interface GestureHandler {

        /**
         * Called when a gesture has been detected
         * @param gesture The detected gesture
         * @param event The event related to the gestures
         */
        void onGesture(int gesture, MotionEvent event);
    }

    private String print(MotionEvent.PointerCoords p) {
        return "x=" + p.x + ", y=" + p.y;
    }
}
