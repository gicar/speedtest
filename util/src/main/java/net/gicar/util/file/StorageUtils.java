//package net.gicar.util.file;
//
//import android.content.Context;
//import android.net.Uri;
//import android.util.Log;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.nio.ByteBuffer;
//import java.nio.channels.Channels;
//import java.nio.channels.ReadableByteChannel;
//
//import net.gicar.util.Misc;
//import net.gicar.util.http.HttpAnswer;
//import net.gicar.util.http.HttpAnswerHeader;
//import net.gicar.util.http.HttpContentDisposition;
//import net.gicar.util.http.HttpFetcher;
//import net.gicar.util.http.HttpParser;
//
//public class StorageUtils {
//
//    /** The log tag */
//    private final static String TAG = StorageUtils.class.getSimpleName();
//
//    /**
//     * Loads the file in a ByteBuffer.
//     * Check the size of the file before to avoid out of memory situations.
//     * @param context The app context
//     * @param storageFile The file wthat shall be read
//     * @param maxLimit The maximum value of limit that shall be supported
//     * @return The ByteBuffer holding the file contents or null if reading has been aborted
//     * @throws net.gicar.util.file.StorageUtils.TooBigException The file was too big to be loaded
//     */
//    public static ByteBuffer load(Context context, StorageFile storageFile, int maxLimit) throws IOException, TooBigException
//    {
//        // unable to read HTTP files
//        if (storageFile.getType() == StorageFile.UriType.HTTP) throw new IOException("HTTP URI not supported here");
//
//        // check the limits
//        Long size = storageFile.getSize();
//        if (size != null) {
//            if (size > maxLimit) throw new TooBigException();
//            if (size < maxLimit) maxLimit = size.intValue();
//        }
//
//        int n;
//
//        // if we have a file try to access it directly
//        if (storageFile.getType() == StorageFile.UriType.FILE) {
//            File file = storageFile.getFile();
//            if (Misc.DEBUG) Log.v(TAG, "Trying to load directly from file '" + file + "'");
//
//            try (ReadableByteChannel readableChannel = Channels.newChannel(new FileInputStream(file))) {
//                // read into the byte buffer until no more data available or it is filled
//                ByteBuffer b = ByteBuffer.allocate(maxLimit + 1);
//                do n = readableChannel.read(b); while (n != -1 && b.remaining() > 0);
//                if (n != -1) throw new TooBigException();
//
//                return b;
//            } catch (Exception e) {
//                if (Misc.DEBUG) Log.v(TAG, "Unable to load directly from file", e);
//            }
//        }
//
//        // go the standard way
//        Uri uri = storageFile.getUri();
//        if (Misc.DEBUG) Log.v(TAG, "Loading content from " + uri);
//        try (ReadableByteChannel readableChannel = Channels.newChannel(context.getContentResolver().openInputStream(uri)))
//        {
//            // read into the byte buffer until no more data available or it is filled
//            ByteBuffer b = ByteBuffer.allocate(maxLimit + 1);
//            do n = readableChannel.read(b); while (n != -1 && b.remaining() > 0);
//            if (n != -1) throw new TooBigException();
//
//            return b;
//        }
//    }
//
//    /**
//     * Loads the file in a ByteBuffer, supports HTTP URIs
//     * Check the size of the file before to avoid out of memory situations.
//     * @param context The app context
//     * @param storageFile The file wthat shall be read
//     * @param maxLimit The maximum value of limit that shall be supported
//     * @param httpFetcher The HttpFetcher to be used for HTTP
//     * @return The ByteBuffer holding the file contents or null if reading has been aborted
//     * @throws net.gicar.util.file.StorageUtils.TooBigException The file was too big to be loaded
//     * @throws net.gicar.util.file.StorageUtils.WrongHttpStatusCode Bad HTTP answer
//     */
//    public static ByteBuffer load(Context context, StorageFile storageFile, final int maxLimit, HttpFetcher httpFetcher)
//            throws IOException, TooBigException, WrongHttpStatusCode
//    {
//        // check for non-HTTP files
//        if (storageFile.getType() != StorageFile.UriType.HTTP) return load(context, storageFile, maxLimit);
//
//        // fetch the URL
//        HttpAnswer httpAnswer = httpFetcher.getUri(storageFile.getUri().toString(), new HttpParser() {
//            @Override
//            public boolean prefersGzip() {
//                return true;
//            }
//
//            @Override
//            public Object parse(HttpAnswerHeader header, InputStream stream) throws IOException {
//                if (stream == null) return null;
//
//                if (header.getContentLength() > maxLimit) return null;
//
//                int i = (int) header.getContentLength();
//                if (i < 0) i = 4096;
//
//                ByteArrayOutputStream buffer = new ByteArrayOutputStream(i);
//                byte[] tmp = new byte[4096];
//                int l;
//                while ((l = stream.read(tmp)) != -1)
//                    buffer.write(tmp, 0, l);
//
//                return ByteBuffer.wrap(buffer.toByteArray());
//            }
//        }, null);
//
//        // check for aborts
//        if (httpAnswer == null) return null;
//
//        // check that we got valid data
//        HttpAnswerHeader header = httpAnswer.getHeader();
//        if (header.getStatusCode() != 200) throw new WrongHttpStatusCode(header);
//
//        // update the file name and size
//        HttpContentDisposition contentDisposition = header.getContentDisposition();
//        if (contentDisposition != null) {
//            String name = contentDisposition.getFileName();
//            if (name != null) storageFile.name = name;
//        }
//        long size = header.getContentLength();
//        if (size >= 0) storageFile.size = size;
//
//        // check for too big file
//        ByteBuffer data = (ByteBuffer)httpAnswer.getData();
//        if (data == null) throw new TooBigException();
//
//        return data;
//    }
//
//    /** Thrown if the file was too big to be loaded */
//    public static class TooBigException extends Exception {}
//
//    /** Thrown if the HTTP status code is not correct */
//    public static class WrongHttpStatusCode extends Exception {
//
//        private final HttpAnswerHeader header;
//
//        WrongHttpStatusCode(HttpAnswerHeader header)
//        {
//            this.header = header;
//        }
//
//        public HttpAnswerHeader getHeader() {
//            return header;
//        }
//    }
//}
