package net.gicar.util.file;

import java.io.File;

public class FileVolume {

    /** Directory where the storage is located */
    public final File directory;

    /** UUID of the storage or null if not available */
    public final String uuid;

    /** Description of the storage or null if not available */
    public final String description;

    /** If true this is the primary storage */
    public final boolean primary;

    FileVolume(File directory, String uuid, String description, boolean primary) {
        this.directory = directory;
        this.uuid = uuid;
        this.description = description;
        this.primary = primary;
    }
}
