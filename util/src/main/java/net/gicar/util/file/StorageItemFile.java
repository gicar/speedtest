package net.gicar.util.file;

import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import net.gicar.util.Misc;

public class StorageItemFile extends StorageItem implements Serializable {

    /* The associated Uri scheme */
    static final String SCHEME = "file";

    /** The File */
    private final File file;

    public StorageItemFile(Uri uri) throws StorageItemException {

        super(uri);

        // check for plain files
        if (!SCHEME.equals(uri.getScheme())) throw new StorageItemException("Wrong file scheme for " + uri);

        // check for valid path
        String path = uri.getPath();
        if (path == null) throw new StorageItemException("Invalid path for " + uri);

        // store info
        file = new File(path);

        if (Misc.DEBUG) Log.v(TAG, "Created " + this);
    }

    public StorageItemFile(File file)
    {
        super(Uri.fromFile(file));
        this.file = file;

        if (Misc.DEBUG) Log.v(TAG, "Created " + this);
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public String getName() {
        return file.getName();
    }

    @Override
    public Long getSize() {
        if (file.isFile()) return file.length();
        return null;
    }

    @Override
    public boolean isDirectory() {
        return file.isDirectory();
    }

    @Override
    public String getMimeType() {
        return FileUtils.getMimeType(file);
    }

    @Override
    public List<StorageItem> listDirectory()
    {
        File[] files = file.listFiles();
        if (files == null) return null;
        List<StorageItem> items = new ArrayList<>(files.length);
        for (File f : files) items.add(new StorageItemFile(f));
        return items;
    }

    @Override
    public StorageItem getParent() {
        File parent = file.getParentFile();
        return parent != null ? new StorageItemFile(parent) : null;
    }

    @Override
    public String getDisplayPath() {
        return file.getAbsolutePath();
    }

    @Override
    public boolean delete() {
        try {
            return file.delete();
        }
        catch (Exception e) {
            if (Misc.DEBUG) Log.w(TAG, "Unable to delete " + uri, e);
            return false;
        }
    }

    @Override
    public StorageItemFile rename(String name) {
        try {
            File newFile = new File(file.getParent(), name);
            if (!file.renameTo(newFile)) return null;
            return new StorageItemFile(newFile);
        }
        catch (Exception e) {
            if (Misc.DEBUG) Log.w(TAG, "Unable to rename " + uri + " to " + name, e);
            return null;
        }
    }

    @Override
    public InputStream openInputStream() throws IOException {
        return new FileInputStream(file);
    }

    /**
     * Loads the file in a ByteBuffer.
     * Check the size of the file before to avoid out of memory situations.
     * @param maxLimit The maximum value of limit that shall be supported
     * @return The ByteBuffer holding the file contents or null if reading has been aborted
     * @throws net.gicar.util.file.StorageItem.TooBigException The file was too big to be loaded
     */
    public ByteBuffer load(int maxLimit) throws IOException, TooBigException
    {
        return load(this::openInputStream, maxLimit);
    }
}
