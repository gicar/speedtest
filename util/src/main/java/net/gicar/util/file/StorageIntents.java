package net.gicar.util.file;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.UriPermission;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import java.util.List;

import net.gicar.util.Misc;

import static android.provider.DocumentsContract.EXTRA_INITIAL_URI;

/** Utility class to access Storage Access Framework files using external intents */
public class StorageIntents {

    /** Log tag */
    private final static String TAG = StorageIntents.class.getSimpleName();

    /**
     * Fires an intent to spin up the "file chooser" UI and select a file with the specified MIME type
     * @param activity The activity starting this request.
     * @param requestCode The request code for the reply
     * @param mimeType The MIME type of the file that can be selected
     * @param initialUri The starting URI in the file open dialog
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static boolean launchOpenDocument(Activity activity, int requestCode, String mimeType, String initialUri)
    {
        return launchOpenDocument(activity, requestCode, new String[] {mimeType}, initialUri);
    }

    /**
     * Fires an intent to spin up the "file chooser" UI and select a file with one of the specified
     * MIME types
     * @param activity The activity starting this request.
     * @param requestCode The request code for the reply
     * @param mimeTypes The MIME types of the file that can be selected
     * @param initialUri The starting URI in the file open dialog, could be null
     * @return true if selection successfully started, false otherwise
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static boolean launchOpenDocument(Activity activity, int requestCode, String[] mimeTypes, String initialUri)
    {
        // don't even try if the Android version is not the right one
        if (Misc.getOsVersionNumber() < Build.VERSION_CODES.KITKAT) return false;

        try {
            // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file browser.
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

            // filter to only show results that can be "opened", such as a file (as opposed to a list
            // of contacts or timezones)
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            // set the MIME types we are interested in
            if (mimeTypes.length == 1) intent.setType(mimeTypes[0]);
            else {
                intent.setType("*/*");
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }

            // set the initial URI if passed (and can be used)
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                    initialUri != null && !initialUri.isEmpty())
                intent.putExtra(EXTRA_INITIAL_URI, Uri.encode(initialUri));

            // launch the intent
            if (Misc.DEBUG) Log.v(TAG, "Launching open document intent: "+ intent);
            activity.startActivityForResult(intent, requestCode);
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Processes the open document selection result
     * @param resultCode The intent result code
     * @param resultData The data of the result intent
     * @return The URI to the selected file or null if none
     */
    public static Uri resultOpenDocument(int resultCode, Intent resultData)
    {
        return resultCode == Activity.RESULT_OK && resultData != null ? resultData.getData() : null;
    }

    /**
     * Fires an intent to spin up the "file chooser" UI and select a directory to be opened for read access
     * @param activity The activity starting this request.
     * @param requestCode The request code for the reply
     * @param initialUri The starting URI in the file open dialog, could be null
     * @param requestWrite If true also write access will be requested
     * @return true if selection successfully started, false otherwise
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static boolean lauchOpenTree(Activity activity, int requestCode, String initialUri, boolean requestWrite)
    {
        // don't even try if the Android version is not the right one
        if (Misc.getOsVersionNumber() < Build.VERSION_CODES.LOLLIPOP) return false;

        try {
            // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file browser.
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);

            // Provide read access to files and sub-directories in the user-selected directory.

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION |
                    (requestWrite ? Intent.FLAG_GRANT_WRITE_URI_PERMISSION : 0));

            // set the initial URI if passed (and can be used)
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                    initialUri != null && !initialUri.isEmpty())
                intent.putExtra(EXTRA_INITIAL_URI, Uri.encode(initialUri));

            // launch the intent
            if (Misc.DEBUG) Log.v(TAG, "Launching open tree intent: "+ intent);
            activity.startActivityForResult(intent, requestCode);
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Processes the open document tree selection result
     * @param resultCode The intent result code
     * @param resultData The data of the result intent
     * @return The URI to the selected directory or null if none
     */
    public static Uri resultOpenTree(int resultCode, Intent resultData)
    {
        return resultCode == Activity.RESULT_OK && resultData != null ? resultData.getData() : null;
    }

    /**
     * Persists an Uri access
     * @param context The Context
     * @param uri The URI on which the operation shall be performed
     * @param requestWrite If true also write access will be requested
     * @return true if the action was successful, false if some error occurred
     */
    public static boolean takePersistableUriPermission(Context context, Uri uri, boolean requestWrite)
    {
        if (Misc.DEBUG) Log.v(TAG, "Persisting access to " + uri + ", write=" + requestWrite);
        if (uri == null || !"content".equals(uri.getScheme())) return false;
        ContentResolver resolver = context.getContentResolver();
        try {
            resolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION |
                    (requestWrite ? Intent.FLAG_GRANT_WRITE_URI_PERMISSION : 0));
            return true;
        }
        catch (Exception e) {
                if (Misc.DEBUG) Log.w(TAG, "Persist access failed", e);
                return false;
            }
    }

    /**
     * Releases an Uri access
     * @param context The Context
     * @param uri The URI on which the operation shall be performed
     * @param requestWrite If true also write access will be requested
     * @return true if the action was successful, false if some error occurred
     */
    public static boolean releasePersistableUriPermission(Context context, Uri uri, boolean requestWrite)
    {
        if (Misc.DEBUG) Log.v(TAG, "Releasing access to " + uri + ", write=" + requestWrite);
        if (uri == null || !"content".equals(uri.getScheme())) return false;
        ContentResolver resolver = context.getContentResolver();
        try {
            resolver.releasePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION |
                    (requestWrite ? Intent.FLAG_GRANT_WRITE_URI_PERMISSION : 0));
            return true;
        }
        catch (Exception e) {
            if (Misc.DEBUG) Log.w(TAG, "Release access failed", e);
            return false;
        }
    }

    /**
     * Check an Uri read access
     * @param application The application requesting it
     * @param uri The URI on which the operation shall be performed
     * @return true if the read access is granted to that uri
     */
    public static boolean checkReadPersistableUriPermission(Application application, Uri uri)
    {
        ContentResolver resolver = application.getContentResolver();
        List<UriPermission> uriPermissions = resolver.getPersistedUriPermissions();
        if (Misc.DEBUG)
            for (int i = 0; i < uriPermissions.size(); i++)
                Log.v(TAG, "Persisted permission [" + i + "]: " + uriPermissions);
        for (UriPermission p : uriPermissions)
            if (uri.equals(p.getUri()) && p.isReadPermission()) return true;
        return false;
    }
}
