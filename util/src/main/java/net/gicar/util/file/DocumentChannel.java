package net.gicar.util.file;

import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;

import net.gicar.util.Misc;
import net.gicar.util.appcompat.XApplication;

/** Provides access to a StorageItemDocument backed by a local file */
public class DocumentChannel implements Closeable {

    /** The log tag */
    private final static String TAG = DocumentChannel.class.getSimpleName();

    /** The document */
    private final StorageItemDocument document;

    /** The ParcelFileDescriptor associated with this document */
    private final ParcelFileDescriptor parcelFileDescriptor;

    /** The FileDescriptor associated with this document */
    private final FileDescriptor fileDescriptor;

    /** The Channel */
    private final FileChannel channel;

    /**
     * Utility method that returns the ParcelFileDescriptor associated with a StorageItemDocument.
     * Note that both read and write access shall be present on the document
     * @param document The StorageItemDocument
     * @return The associated ParcelFileDescriptor
     * @throws FileNotFoundException The file does not exist
     */
    public static ParcelFileDescriptor getParcelFileDescriptor(StorageItemDocument document) throws FileNotFoundException {
        return XApplication.getInstance().getContentResolver().openFileDescriptor(document.getUri(), "rw");
    }

    /**
     * Creates the DocumentChannel
     * Note that for this to work, both read and write access shall be present on the document
     * @throws FileNotFoundException The operation is not possible
     */
    public DocumentChannel(StorageItemDocument document) throws FileNotFoundException
    {
        // save parameters
        this.document = document;

        // create the object to access the file
        // WARNING: the file descriptor must be kept otherwise the garbage collection will close it
        parcelFileDescriptor = XApplication.getInstance().getContentResolver().openFileDescriptor(document.getUri(), "rw");
        fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        channel = new FileInputStream(fileDescriptor).getChannel();

        if (Misc.DEBUG) Log.d(TAG, "Opened fd " + parcelFileDescriptor.getFd() + " for " + document);
    }

    @Override
    public void close() {
        try {
            channel.close();
            parcelFileDescriptor.close();
        } catch (IOException e) {
            if (Misc.DEBUG) Log.w(TAG, "Error closing fd " + parcelFileDescriptor.getFd(), e);
        }
    }

    public int read(ByteBuffer dst) throws IOException {
        return channel.read(dst);
    }

    public long position() throws IOException {
        return channel.position();
    }

    public SeekableByteChannel position(long newPosition) throws IOException {
        return channel.position(newPosition);
    }

    public long size() throws IOException {
        return channel.size();
    }
}
