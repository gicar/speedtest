package net.gicar.util.file;

import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.List;

import net.gicar.util.Misc;
import net.gicar.util.http.HttpAnswer;
import net.gicar.util.http.HttpAnswerHeader;
import net.gicar.util.http.HttpContentDisposition;
import net.gicar.util.http.HttpFetcher;
import net.gicar.util.http.HttpParser;

public class StorageItemHttp extends StorageItem implements Serializable {

    /* The associated Uri scheme */
    static final String SCHEME1 = "http", SCHEME2 = "https";

    /** The known name */
    private String name;

    /** The item size */
    private Long size;

    /** The known MIME type */
    private String mimeType;

    public StorageItemHttp(Uri uri) throws StorageItemException {

        super(uri);

        // check for plain files
        String scheme = uri.getScheme();
        if (!SCHEME1.equals(scheme) && !SCHEME2.equals(scheme))
            throw new StorageItemException("Wrong http scheme for " + uri);

        // check for valid path
        String path = uri.getPath();
        if (path == null) throw new StorageItemException("Invalid path for " + uri);

        // store info
        name = uri.getLastPathSegment();

        if (Misc.DEBUG) Log.v(TAG, "Created " + this);
    }

    @Override
    public File getFile() {
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Long getSize() {
        return size;
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    @Override
    public List<StorageItem> listDirectory() {
        return null;
    }

    @Override
    public StorageItem getParent() {
        return null;
    }

    @Override
    public String getDisplayPath() {
        return null;
    }

    @Override
    public boolean delete() {
        return false;
    }

    @Override
    public StorageItem rename(String name) {
        return null;
    }

    @Override
    public InputStream openInputStream() throws IOException {
        throw new IOException("Unsupported operation");
    }

    /**
     * Loads the file in a ByteBuffer, supports HTTP URIs
     * Check the size of the file before to avoid out of memory situations.
     * @param maxLimit The maximum value of limit that shall be supported
     * @param httpFetcher The HttpFetcher to be used for HTTP
     * @return The ByteBuffer holding the file contents or null if reading has been aborted
     * @throws net.gicar.util.file.StorageItem.TooBigException The file was too big to be loaded
     * @throws net.gicar.util.file.StorageItemHttp.WrongHttpStatusCode Bad HTTP answer
     */
    public ByteBuffer load(final int maxLimit, HttpFetcher httpFetcher)
            throws IOException, TooBigException, WrongHttpStatusCode
    {
        // fetch the URL
        HttpAnswer httpAnswer = httpFetcher.getUri(uri.toString(), new HttpParser() {
            @Override
            public boolean prefersGzip() {
                return true;
            }

            @Override
            public Object parse(HttpAnswerHeader header, InputStream stream) throws IOException {
                if (stream == null) return null;

                if (header.getContentLength() > maxLimit) return null;

                int i = (int) header.getContentLength();
                if (i < 0) i = 4096;

                ByteArrayOutputStream buffer = new ByteArrayOutputStream(i);
                byte[] tmp = new byte[4096];
                int l;
                while ((l = stream.read(tmp)) != -1)
                    buffer.write(tmp, 0, l);

                return ByteBuffer.wrap(buffer.toByteArray());
            }
        }, null);

        // check for aborts
        if (httpAnswer == null) return null;

        // check that we got valid data
        HttpAnswerHeader header = httpAnswer.getHeader();
        if (header.getStatusCode() != 200) throw new WrongHttpStatusCode(header);

        // update the file name, size and MIME type
        HttpContentDisposition contentDisposition = header.getContentDisposition();
        if (contentDisposition != null) {
            String name = contentDisposition.getFileName();
            if (name != null) this.name = name;
        }
        long size = header.getContentLength();
        if (size >= 0) this.size = size;
        mimeType = header.getMimeType();

        // check for too big file
        ByteBuffer data = (ByteBuffer)httpAnswer.getData();
        if (data == null) throw new TooBigException();

        return data;
    }

    /** Thrown if the HTTP status code is not correct */
    public static class WrongHttpStatusCode extends Exception {

        private final HttpAnswerHeader header;

        WrongHttpStatusCode(HttpAnswerHeader header)
        {
            this.header = header;
        }

        public HttpAnswerHeader getHeader() {
            return header;
        }
    }
}
