package net.gicar.util.file;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.gicar.util.Misc;

/** A StorageItem using SAF documents. Note that for SDK < 26 there are limitations */
public class StorageItemDocument extends StorageItem implements Serializable {

    /** The associated Uri scheme */
    static final String SCHEME = "content";

    /** External storage authority */
    static final private String EXTERNALSTORAGE_AUTHORITY = "com.android.externalstorage.documents";

    /** Primary volume label */
    static final private String PRIMARY_VOLUME_LABEL = "primary";

    /** Projection used in queries for item info */
    static private final String[] ITEM_INFO_PROJECTION = new String[] {
            DocumentsContract.Document.COLUMN_MIME_TYPE,
            OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE,
            MediaStore.Images.Media.DATA };

    /** Projection used in queries for list directory */
    static private final String[] LIST_DIRECTORY_PROJECTION = new String[] { DocumentsContract.Document.COLUMN_DOCUMENT_ID };

    /** The document ID associated with the URI */
    private final String id;

    /** The parent, if known */
    private final StorageItemDocument parent;

    /** The File */
    private File file;

    /** The known name */
    private String name;

    /** The item size */
    private Long size;

    /** The MIME type of the file */
    private String mimeType;

    public StorageItemDocument(Uri uri) throws StorageItemException {
        this(uri, null);
    }

    public StorageItemDocument(Uri uri, StorageItemDocument parent) throws StorageItemException
    {
        super(uri);
        this.parent = parent;

        // check for "content" scheme
        if (!SCHEME.equals(uri.getScheme())) throw new StorageItemException("Wrong document scheme for " + uri);

        // get the context from the application
        Context context = getContext();

        try {

            Log.w(TAG, "isDocumentUri: " + DocumentsContract.isDocumentUri(context, uri));

            // get the document id
            id = DocumentsContract.isDocumentUri(context, uri) ? DocumentsContract.getDocumentId(uri) : null;

            // we need to inspect the content resolver results
            ContentResolver resolver = context.getContentResolver();
            try (Cursor cursor = resolver.query(uri, ITEM_INFO_PROJECTION, null, null, null)) {

                // give up in these failure conditions
                if (cursor == null || !cursor.moveToFirst()) return;

                // log the available columns
                if (Misc.DEBUG)
                    Log.v(TAG, "Available columns: " + Arrays.deepToString(cursor.getColumnNames()));

                // Note it's called "Display Name".  This is
                // provider-specific, and might not necessarily be the file name.
                int i = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                if (i != -1 && !cursor.isNull(i)) name = cursor.getString(i);

                // If the size is unknown, the value stored is null.  But since an
                // int can't be null in Java, the behavior is implementation-specific,
                // which is just a fancy term for "unpredictable".  So as
                // a rule, check if it's null before assigning to an int.  This will
                // happen often:  The storage API allows for remote files, whose
                // size might not be locally known.
                i = cursor.getColumnIndex(OpenableColumns.SIZE);
                if (i != -1 && !cursor.isNull(i)) size = cursor.getLong(i);

                // get the MIME type
                i = cursor.getColumnIndex(DocumentsContract.Document.COLUMN_MIME_TYPE);
                if (i != -1 && !cursor.isNull(i)) mimeType = cursor.getString(i);
            }

            if (Misc.DEBUG) Log.v(TAG, "Created " + this);
        }
        catch (Exception e) {
            throw new StorageItemException("Unable to create StorageItemDocument for " + uri, e);
        }
    }

    @Override
    public File getFile()
    {
        // check if it is already available
        if (file == null) {

            // check if it is an external path
            String authority = uri.getAuthority();
            if (EXTERNALSTORAGE_AUTHORITY.equals(authority)) {

                // get the file and name
                file = decodeExternalStorageId(id);
                if (file != null) name = file.getName();
            }
        }

        return file;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Long getSize() {
        return size;
    }

    @Override
    public boolean isDirectory() {
        return DocumentsContract.Document.MIME_TYPE_DIR.equals(mimeType);
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    @Override
    public List<StorageItem> listDirectory()
    {
        // return null if not directory or cannot access
        if (!isDirectory() || Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return null;

        // get the context from the application
        Context context = getContext();

        // get the uri for the child
        Uri childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri, id);

        // query for the uris of the child
        ArrayList<Uri> results = new ArrayList<>();
        try (Cursor cursor = context.getContentResolver().query(childrenUri, LIST_DIRECTORY_PROJECTION,
                null, null, null)) {
            while (cursor.moveToNext())
                results.add(DocumentsContract.buildDocumentUriUsingTree(uri, cursor.getString(0)));
        } catch (Exception e) {
            if (Misc.DEBUG) Log.w(TAG, "Failed query: " + e);
        }

        // create the list of files from the uris
        List<StorageItem> items = new ArrayList<>(results.size());
        for (Uri u : results) {
            try {
                items.add(new StorageItemDocument(u, this));
            } catch (Exception e) {
                if (Misc.DEBUG) Log.w(TAG, "Failed URI parsing: " + e);
            }
        }
        return items;
    }

    @Override
    public StorageItem getParent()
    {
        // unfortunately, we are unable to find out the parent directory in a reliable way
        return parent;
    }

    @Override
    public String getDisplayPath()
    {
        // loop for all the parent items
        StringBuilder s = new StringBuilder(name != null ? name : "?");
        for (StorageItemDocument i = parent; i != null; i = i.parent)
            s.insert(0, (i.name != null ? i.name : "?") + "/");
        return s.toString();
    }

    @Override
    public boolean delete()
    {
        try {
            return DocumentsContract.deleteDocument(getContext().getContentResolver(), uri);
        }
        catch (Exception e) {
            if (Misc.DEBUG) Log.w(TAG, "Unable to delete " + uri, e);
            return false;
        }
    }

    @Override
    public StorageItemDocument rename(String name) {
        try {
            // SDK version 21 is needed for this feature
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return null;

            // try to rename the file
            ContentResolver resolver = getContext().getContentResolver();
            Uri newUri = DocumentsContract.renameDocument(resolver, uri, name);
            if (newUri == null) return null;

            // return the new item
            return new StorageItemDocument(newUri, parent);
        }
        catch (Exception e) {
            if (Misc.DEBUG) Log.w(TAG, "Unable to rename " + uri + " to " + name, e);
            return null;
        }
    }

    @Override
    public InputStream openInputStream() throws IOException {
        return getContext().getContentResolver().openInputStream(uri);
    }

    /**
     * Checks if the underlying file (if present) is actually readable
     * @return true if the File read access is possible
     */
    public boolean isFileReadable()
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return false;
        File file = getFile();
        return file != null && Files.isReadable(file.toPath());
    }

    /**
     * Copies the storage item to a file
     * @param target The target file
     * @return The number of copied bytes
     * @throws IOException Something went wrong
     */
    public long copy(File target, boolean replaceExisting) throws IOException
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // perform the copy
            try (InputStream inputStream = openInputStream()) {
                if (replaceExisting)
                    return Files.copy(inputStream, target.toPath(), StandardCopyOption.REPLACE_EXISTING);
                return Files.copy(inputStream, target.toPath());
            }
        }
        else throw new IOException("Not supported on Android " + Build.VERSION.SDK_INT);
    }

    /**
     * Loads the file in a ByteBuffer.
     * Check the size of the file before to avoid out of memory situations.
     * @param maxLimit The maximum value of limit that shall be supported
     * @return The ByteBuffer holding the file contents or null if reading has been aborted
     * @throws net.gicar.util.file.StorageItem.TooBigException The file was too big to be loaded
     */
    public ByteBuffer load(int maxLimit) throws IOException, TooBigException
    {
        return load(this::openInputStream, maxLimit);
    }

    /**
     * Builds an URI for this document, removing the reference to the tree if present
     * @return The URI for the document
     */
    public Uri buildDocumentUri() {
        return DocumentsContract.buildDocumentUri(uri.getAuthority(), id);
    }

    /**
     * Returns the document URI corresponding to the specified tree URI
     * @param treeUri The tree URI
     * @return The document URI from the specified tree
     */
    public static Uri buildDocumentUriUsingTree(Uri treeUri)
    {
        // return null if unable to perform the requested operation
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return null;

        return DocumentsContract.buildDocumentUriUsingTree(treeUri, DocumentsContract.getTreeDocumentId(treeUri));
    }

    /**
     * Creates a document URI by using the specified tree URI
     * @param treeUri The tree URI which will be used
     * @param documentUri The document URI that will be used to get the document id
     * @return The new document URI using the specified tree uri
     */
    public static Uri buildDocumentUriUsingTree(Uri treeUri, Uri documentUri)
    {
        // return null if unable to perform the requested operation
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return null;

        return DocumentsContract.buildDocumentUriUsingTree(treeUri, DocumentsContract.getDocumentId(documentUri));
    }

    /**
     * Decodes the id of external storage content into a file
     * @param id The id
     * @return The corresponding file or null if unable to decode
     */
    private File decodeExternalStorageId(String id)
    {
        // check for valid id
        if (id == null) return null;

        // try to split the id
        int split = id.indexOf(':');
        if (split == -1) return null;

        // fetch root and path
        String root = id.substring(0, split);
        String path = id.substring(split + 1);

        // try to convert the root into a File
        boolean isPrimary = PRIMARY_VOLUME_LABEL.equals(root);
        List<FileVolume> volumes = FileUtils.getFileVolumes(getContext());
        File rootFile = null;
        for (FileVolume v : volumes) if ((isPrimary && v.primary) || root.equals(v.uuid)) {
            rootFile = v.directory;
            break;
        }

        // if everything went well we now have the file full path
        if (rootFile == null) return null;

        return new File(rootFile, path);
    }
}
