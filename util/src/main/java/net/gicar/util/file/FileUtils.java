package net.gicar.util.file;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

import androidx.core.content.ContextCompat;
import net.gicar.util.Misc;
import net.gicar.util.appcompat.XApplication;

/** File utilities */
public class FileUtils {

    private FileUtils() {} //private constructor to enforce Singleton pattern

    /** Log tag */
    private final static String TAG = "FileUtils";

    /** Prefix of hidden files/folders */
    public static final String HIDDEN_PREFIX = ".";

    /**  File and folder comparator */
    public static final Comparator<File> FILE_COMPARATOR = (f1, f2) -> {
        // sort alphabetically by lower case, which is much cleaner
        return f1.getName().toLowerCase().compareTo(f2.getName().toLowerCase());
    };

    /**
     * Gets the extension of a file name, like "png" or "jpg".
     * @param file The file
     * @return Extension excluding the dot("."); "" if there is no extension;
     *         null if file was null. Extensions are always returned in lowercase
     */
    public static String getExtension(File file)
    {
        if (file == null) return null;
        return getExtension(file.getName());
    }

    /**
     * Gets the extension of a file name, like "png" or "jpg".
     * @param name The file name
     * @return Extension excluding the dot("."); "" if there is no extension;
     *         null if file was null. Extensions are always returned in lowercase
     */
    public static String getExtension(String name)
    {
        if (name == null) return null;
        int dot = name.lastIndexOf(".");
        if (dot >= 0) return name.substring(dot + 1).toLowerCase();
        return "";
    }

    /**
	 * Gets the file name removing its extension
	 * @param file The file
	 * @return The file name without the extension and the dot before it
	 */
	public static String getNameWithoutExtension(File file)
	{
		if (file == null) return null;
        return getNameWithoutExtension(file.getName());
	}

    /**
     * Gets the file name removing its extension
     * @param name The file name
     * @return The file name without the extension and the dot before it
     */
    public static String getNameWithoutExtension(String name)
    {
        if (name == null) return null;
        int dot = name.lastIndexOf(".");
        if (dot >= 0) return name.substring(0, dot);
        return name;
    }

    /**
     * Returns the path only (without file name).
     */
    public static File getPathWithoutFilename(File file) {
        if (file != null) {
            if (file.isDirectory()) {
                // no file to be split off. Return everything
                return file;
            } else {
                String filename = file.getName();
                String filepath = file.getAbsolutePath();

                // Construct path without file name.
                String pathwithoutname = filepath.substring(0,
                        filepath.length() - filename.length());
                if (pathwithoutname.endsWith("/")) {
                    pathwithoutname = pathwithoutname.substring(0, pathwithoutname.length() - 1);
                }
                return new File(pathwithoutname);
            }
        }
        return null;
    }

    /**
     * Provides the MIME type of a file
     * @return The MIME type for the given file.
     */
    public static String getMimeType(File file)
    {
        String extension = getExtension(file);

        if (extension.length() > 0)
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.substring(1));

        return "application/octet-stream";
    }

    /**
     * Checks is a File is comtained in the subtree of another
     * @param parent The base File which shall contain the other
     * @param child The File to be checked
     * @return true if toBeChecked is in a subtree of base
     */
    public static boolean isContained(File parent, File child)
    {
        try {
            return parent != null && child != null &&
                    child.getCanonicalFile().getAbsolutePath().startsWith(parent.getCanonicalFile().getAbsolutePath());
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Get the file size in a human-readable string.
     *
     * @param size Size in bytes
     * @return The string representing it
     */
    public static String getReadableSize(int size) {
        final int BYTES_IN_KILOBYTES = 1024;
        final DecimalFormat dec = new DecimalFormat("###.#");
        final String KILOBYTES = " KB";
        final String MEGABYTES = " MB";
        final String GIGABYTES = " GB";
        float fileSize = 0;
        String suffix = KILOBYTES;

        if (size > BYTES_IN_KILOBYTES) {
            //noinspection IntegerDivisionInFloatingPointContext
            fileSize = size / BYTES_IN_KILOBYTES;
            if (fileSize > BYTES_IN_KILOBYTES) {
                fileSize = fileSize / BYTES_IN_KILOBYTES;
                if (fileSize > BYTES_IN_KILOBYTES) {
                    fileSize = fileSize / BYTES_IN_KILOBYTES;
                    suffix = GIGABYTES;
                } else {
                    suffix = MEGABYTES;
                }
            }
        }
        return dec.format(fileSize) + suffix;
    }

    public enum DirPreference {
        /** Return internal directory only */
        INTERNAL,
        /** Prefer external, fallback to internal if not available */
        EXTERNAL,
        /** Use last external, no fallback to internal if not available */
        LAST_EXTERNALS_ONLY,
    }

    /**
     * Select the directory starting from the specified preference
     * @param context The <code>Context</code> to be used for the operation
     * @param type Name of the directory or null if none
     * @param dirPreference The preference about which directory type to choose
     * @return The directory or null if unavailable
     */
    public static File getFilesDir(Context context, String type, DirPreference dirPreference)
    {
        File f = null;
        switch (dirPreference) {
            case EXTERNAL:
                // on Huawei devices it seems that context.getExternalFilesDir does return the
                // path to the external SD card; hopefully ContextCompat.getExternalFilesDirs[0]
                // will return the right one
                //   f = context.getExternalFilesDir(type);
                //   if (f != null) break;
                File[] ff1 = ContextCompat.getExternalFilesDirs(context, type);
                if (ff1.length > 0) f = ff1[0];
                if (f != null) break;
            case INTERNAL:
                f = context.getFilesDir();
                if (f != null && type != null) f = new File(f, type);
                break;
            case LAST_EXTERNALS_ONLY:
                File[] ff2 = ContextCompat.getExternalFilesDirs(context, type);
                for (File x : ff2) if (x != null) f = x;
        }

        if (f != null) {
            //noinspection ResultOfMethodCallIgnored
            f.mkdirs();
            if (!f.exists() || !f.isDirectory()) f = null;
        }
        return f;
    }

    /**
     * Select the cache directory starting from the specified preference
     * @param context The <code>Context</code> to be used for the operation
     * @param dirPreference The preference about which directory type to choose
     * @return The directory or null if unavailable
     */
    public static File getCacheDir(Context context, DirPreference dirPreference)
    {
        return getCacheDir(context, null, dirPreference);
    }

    /**
     * Select the cache directory starting from the specified preference
     * @param context The <code>Context</code> to be used for the operation
     * @param type Name of the directory or null if none
     * @param dirPreference The preference about which directory type to choose
     * @return The directory or null if unavailable
     */
    public static File getCacheDir(Context context, String type, DirPreference dirPreference)
    {
        File f = null;
        switch (dirPreference) {
            case EXTERNAL:
                // on Huawei devices it seems that context.getExternalCacheDirs does return the
                // path to the external SD card; hopefully ContextCompat.getExternalCacheDirs[0]
                // will return the right one
                //   f = context.getExternalCacheDirs(type);
                //   if (f != null) break;
                File[] ff1 = ContextCompat.getExternalCacheDirs(context);
                if (ff1.length > 0) f = ff1[0];
                if (f != null) break;
            case INTERNAL:
                f = context.getCacheDir();
                break;
            case LAST_EXTERNALS_ONLY:
                File[] ff2 = ContextCompat.getExternalCacheDirs(context);
                for (File x : ff2) if (x != null) f = x;
        }

        if (f != null) {
            if (type != null) f = new File(f, type);
            //noinspection ResultOfMethodCallIgnored
            f.mkdirs();
            if (!f.exists() || !f.isDirectory()) f = null;
        }
        return f;
    }

//    /**
//     * Creates the specified directory and returns it
//     * @param context The <code>Context</code> to be used for the operation
//     * @param name Name of the directory
//     * @return The path to the directory or null if it not available
//     */
//    public static File createDirectory(Context context, String name)
//    {
//        // get the path to the cache directory if available
//        File dir = null;
//        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
//            dir = context.getExternalFilesDir(null);
//            if (dir != null) {
//                dir = new File(dir, name);
//                dir.mkdirs();
//                if (!dir.exists() || !dir.isDirectory()) dir = null;
//            }
//        }
//        return dir;
//    }

    /**
     * Returns the path to the SD card memory or null if not available
     * @return SD card path
     */
    public static File getSdCard()
    {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
            return Environment.getExternalStorageDirectory();
        return null;
    }

    /**
     * Returns the path to the External (removable) SD card memory or null if not available
     * @return SD card path
     */
    public static File getExtSdCard()
    {
        // if we are at least on R, use the storage service
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            Context context = XApplication.getInstance();
            StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
            List<StorageVolume> vs = storageManager.getStorageVolumes();

            // we will return just the first not primary, not emulated, removable volume
            for (StorageVolume v : vs)
                if (!v.isPrimary() && !v.isEmulated() && v.isRemovable()) return v.getDirectory();

            return null;
        }

        try {
            // try to find some info from environment variables or use an hardcoded path if none
            String strSDCardPath = System.getenv("SECONDARY_STORAGE");
            if (strSDCardPath == null || strSDCardPath.length() == 0)
                strSDCardPath = System.getenv("EXTERNAL_SDCARD_STORAGE");
            if (strSDCardPath == null || strSDCardPath.length() == 0) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) strSDCardPath = "/mnt/extSdCard/";
                else {
                    // on Marshmallow things are even worse: we will need to find the UUID of the
                    // removable storage
                    File[] dirs = new File("/storage").listFiles();
                    if (dirs == null) return null;
                    Pattern p = Pattern.compile("\\w\\w\\w\\w-\\w\\w\\w\\w");   // should be hex
                    for (File dir : dirs) if (dir != null && dir.isDirectory() && p.matcher(dir.getName()).matches()) {
                        strSDCardPath = dir.getAbsolutePath();
                        break;
                    }
                    if (strSDCardPath == null) return null;
                }
            }

            // if multiple paths are present pick the first one
            if (strSDCardPath.contains(":"))
                strSDCardPath = strSDCardPath.substring(0, strSDCardPath.indexOf(":"));

            // if empty path for any reason just give up
            if (strSDCardPath.length() == 0) return null;

            // build the File object and check for its existence
            File externalFilePath = new File(strSDCardPath);
            if (externalFilePath.exists()) return externalFilePath;

            return null;
        }
        catch (Exception e) {
            return null;
        }
    }

    /**
     * Return the list of available FileVolumes
     * @param context The Context
     * @return The list of available FileVolumes
     */
    public static List<FileVolume> getFileVolumes(Context context)
    {
        List<FileVolume> fileVolumes = new ArrayList<>();

        // if we are at least on N, use the storage service;
        // note that in this case we don't have the getDirectory() method before R, but we will
        // rely on the fact that the storage path is "/storage/UUID"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            // get the list of storage volumes
            StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
            List<StorageVolume> storageVolumes = storageManager.getStorageVolumes();

            // loop through all of them to create the corresponding FileVolume
            for (StorageVolume v : storageVolumes) {

                // the directory is not so easy to retrieve before R
                File directory = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) directory = v.getDirectory();
                else {
                    if (v.isPrimary()) directory = Environment.getExternalStorageDirectory();
                    else {
                        String uuid = v.getUuid();
                        if (uuid != null) {
                            directory = new File("/storage", uuid);
                            if (!directory.exists()) directory = null;
                        }
                    }
                }
                if (Misc.DEBUG) Log.d(TAG, "Volume " + directory + ", primary=" + v.isPrimary() +
                        ", emulated=" + v.isEmulated() + ", removable=" + v.isRemovable() +
                        ", description=" + v.getDescription(context) + ", state=" + v.getState() +
                        ", UUID=" + v.getUuid());

                // add it to the list if it has a directory
                if (directory != null) fileVolumes.add(new FileVolume(directory, v.getUuid(), v.getDescription(context), v.isPrimary()));
            }
        }
        else {
            // use the legacy method
            File d = getSdCard();
            if (d != null) fileVolumes.add(new FileVolume(d, null, null, true));
            d = getExtSdCard();
            if (d != null) fileVolumes.add(new FileVolume(d, d.getName(), null, false));
        }

        return fileVolumes;
    }

    /**
     * Returns a list of possible directories where application files can be stored.
     * The first element will always be present and is the directory in the application internal
     * private storage, which cannot be shared with other apps.
     * @param context The <code>Context</code> to be used for the operation
     * @return A list of directories which can be used to store files, no nulls
     */
    public static List<File> getFilesDirs(Context context)
    {
        // collect internal and external directories
        File intdir = context.getFilesDir();
        File[] extdirs = ContextCompat.getExternalFilesDirs(context, null);

        // add all the valid directories
        ArrayList<File> a = new ArrayList<>();
        a.add(intdir);
        for (File f : extdirs) if (f != null) a.add(f);

        return a;
    }

    /**
     * Returns a list of possible cache directories where application temporary files can be stored.
     * The first element will always be present and is the directory in the application internal
     * private storage, which cannot be shared with other apps.
     * @param context The <code>Context</code> to be used for the operation
     * @return A list of directories which can be used to store files, no nulls
     */
    public static List<File> getCacheDirs(Context context)
    {
        // collect internal and external directories
        File intdir = context.getCacheDir();
        File[] extdirs = ContextCompat.getExternalCacheDirs(context);

        // add all the valid directories
        ArrayList<File> a = new ArrayList<>();
        a.add(intdir);
        for (File f : extdirs) if (f != null) a.add(f);

        return a;
    }

    /**
     * Returns the directory on the file system with most space available on it
     * @param dirs The list of directories, elements could be null
     * @return The directory which has the most space on it
     */
    @SuppressWarnings("deprecation")
    @Deprecated
    public static File getMostSpaceDir(File[] dirs)
    {
        // just be sure that the array is not empty
        if (dirs == null || dirs.length == 0) return null;

        // initialize the selected directory and its size to invalid values
        File dir = null;
        long size = -1;

        // loop for all the directories looking for the one on the storage with the max available space
        for (File f : dirs) if (f != null) {
            StatFs fs = new StatFs(f.getPath());
            long s = ((long)fs.getBlockSize()) * fs.getAvailableBlocks();
            if (s > size) {
                dir = f;
                size = s;
            }
        }

        // return the selected directory
        return dir;
    }

    /**
     * Recursively delete a file/directory
     * @param element The file/directory to be deleted
     */
    public static void deleteFile(File element)
    {
        // be on the safe side
        if (element == null) return;

        // if it is a directory delete the contained files first
        if (element.isDirectory()) //noinspection ConstantConditions
            for (File sub : element.listFiles()) deleteFile(sub);

        // delete this file/directory
        //noinspection ResultOfMethodCallIgnored
        element.delete();
    }

    /**
     * Recursively delete a list of files/directories
     * @param elements The files/directories to be deleted
     */
    public static void deleteFile(File[] elements)
    {
        // delete the listed files/directories
        if (elements != null) for (File sub : elements) deleteFile(sub);
    }
}
