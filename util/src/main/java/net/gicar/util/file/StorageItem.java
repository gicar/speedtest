package net.gicar.util.file;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import net.gicar.util.Misc;
import net.gicar.util.appcompat.XApplication;
import net.gicar.util.services.CrashReporting;

public abstract class StorageItem implements Serializable {

    /** The log tag */
    protected static final String TAG = StorageItem.class.getSimpleName();

    /**
     * Creates the StorageItem corresponding to the specified URI
     * @param uri The URI
     * @return The StorageItem corresponding to that URI
     * @throws StorageItemException Some error occurred
     */
    public static StorageItem create(Uri uri) throws StorageItemException
    {
        try {
            switch (uri.getScheme()) {
                case StorageItemFile.SCHEME:
                    return new StorageItemFile((uri));
                case StorageItemHttp.SCHEME1:
                case StorageItemHttp.SCHEME2:
                    return new StorageItemHttp((uri));
                case StorageItemDocument.SCHEME:
                    return new StorageItemDocument((uri));
                default:
                    throw new StorageItemException("Unknown scheme for " + uri);
            }
        }
        catch (Exception unexpected) {
            CrashReporting.log("Unable to decode URI " + uri);
            CrashReporting.logException(unexpected);
            throw unexpected;
        }
    }

    /**  File and folder comparator */
    public static final Comparator<StorageItem> NAME_COMPARATOR = (f1, f2) -> {
        // sort alphabetically by lower case, which is much cleaner
        String n1 = f1.getName();
        String n2 = f2.getName();
        if (n1 == null && n2 == null) return 0;
        if (n1 == null) return -1;
        if (n2 == null) return 1;
        return n1.toLowerCase().compareTo(n2.toLowerCase());
    };

    /**
     * Creates a String representation of the given StorageItem
     * @param storageItem The StorageItem to be converted
     * @return A String that can be used to re-create it
     */
    public static String storageItemToString(StorageItem storageItem) {
        return storageItemToString(storageItem, -1);
    }

    /**
     * Creates a String representation of the given StorageItem
     * @param storageItem The StorageItem to be converted
     * @param maxDocumentParent Max number of parents to include for documents, -1 for all
     * @return A String that can be used to re-create it
     */
    public static String storageItemToString(StorageItem storageItem, int maxDocumentParent)
    {
        try {
            if (storageItem == null) return null;

            // create the list of URIs
            ArrayList<String> uris = new ArrayList<>();
            uris.add(storageItem.getUri().toString());

            // if we have a document, add the required parents
            if (storageItem instanceof StorageItemDocument) {
                int i = 0;
                for (storageItem = storageItem.getParent();
                     storageItem != null && (maxDocumentParent == -1 || i < maxDocumentParent);
                     storageItem = storageItem.getParent(), i++)
                    uris.add(storageItem.getUri().toString());
            }

            StringBuilder b = new StringBuilder();
            b.append(escapeUri(uris.get(0)));
            for (int i = 1; i < uris.size(); i++)
                b.append('|').append(escapeUri(uris.get(i)));

            return b.toString();
        } catch (Exception e) {
            // this should never happen
            CrashReporting.logException(e);
            return null;
        }
    }

    /**
     * Recover a StorageItem from its String representation
     * @param s The String
     * @param treeUri If not null, the tree URI that will overwrite the one previously included
     *                in the document URI
     * @return The rebuild StorageItem or null in case of failure
     */
    public static StorageItem storageItemFromString(String s, Uri treeUri)
    {
        try {
            // check for invalid input
            if (s == null || s.length() == 0) return null;

            // split the string and decode the strings
            String[] ss = s.split("\\|");
            for (int i = 0; i < ss.length; i++) ss[i] = unescapeUri(ss[i]);

            // with single parameter that isn't a content one, go with the default handling
            Uri uri0 = (Uri.parse(ss[0]));
            if (ss.length == 1 && !StorageItemDocument.SCHEME.equals(uri0.getScheme()))
                return create(uri0);

            // loop back recreating the chain of parents
            StorageItemDocument parent = null;
            for (int i = ss.length - 1; i >= 0; i--) {
                Uri uri = Uri.parse(ss[i]);
                if (treeUri != null) uri = StorageItemDocument.buildDocumentUriUsingTree(treeUri, uri);
                parent = new StorageItemDocument(uri, parent);
            }

            return parent;
        }
        catch (Exception e) {
            if (Misc.DEBUG) Log.w(TAG, "Unable to rebuild StorageItem from " + s, e);
            return null;
        }
    }

    private static String escapeUri(String uri) throws UnsupportedEncodingException {
        return URLEncoder.encode(uri, "UTF-8");
    }

    private static String unescapeUri(String uri) throws UnsupportedEncodingException {
        return URLDecoder.decode(uri, "UTF-8");
    }

    /** The URI to the content */
    protected transient Uri uri;

    protected StorageItem(Uri uri) {
        this.uri = uri;
    }

    /**
     * Returns the Uri associated to this storage item
     * @return The Uri
     */
    public Uri getUri() {
        return uri;
    }

    /**
     * Returns the File object corresponding to this storage item if it is local
     * @return The corresponding File object or null if it is a remote file
     */
    abstract public File getFile();

    /**
     * Returns the file name of this storage item
     * @return The name or null if unknown
     */
    abstract public String getName();

    /**
     * Returns the size of this storage item
     * @return The size or null if unknown
     */
    abstract public Long getSize();

    /**
     * Checks if this is a directory
     * @return true if the file item is a directory
     */
    abstract public boolean isDirectory();

    /**
     * The MIME type of the file
     * @return The MIME type or null if unknown
     */
    abstract public String getMimeType();

    /**
     * Lists the items present in the directory
     * @return The list of items in the directory or null if this is not a directory
     */
    abstract public List<StorageItem> listDirectory();

    /**
     * The parent of this directory
     * @return The parent or null if this is not a directory or a parent is not present
     */
    abstract public StorageItem getParent();

    /**
     * Returns a path for this storage item. Note that the path can be used only for display
     * purposes, as it cannot be used to addess a specific subpath
     * @return The path of this storage item or null if unknown
     */
    abstract public String getDisplayPath();

    /**
     * Deletes this storage item
     * @return true if it has been successfully deleted, false otherwise
     */
    abstract public boolean delete();

    /**
     * Renames this storage item
     * @param name The new name
     * @return The new StorageItem if it has been successfully renamed, null otherwise
     */
    abstract public StorageItem rename(String name);

    /**
     * Returns an input stream that can be used to read from the provided item
     * @return The InputStream
     * @throws IOException An error occurred
     */
    abstract public InputStream openInputStream() throws IOException;

    /**
     * Getter method for the application context
     * @return The application context
     */
    protected static Context getContext() {
        // get the context from the application
        Context context = XApplication.getInstance();
        if (context == null)
            throw new RuntimeException("Application class must extent XApplication");
        return context;
    }

    /**
     * Loads the file in a ByteBuffer.
     * Check the size of the file before to avoid out of memory situations.
     * @param streamProvider Provides the stream from where the data will be read
     * @param maxLimit The maximum value of limit that shall be supported
     * @return The ByteBuffer holding the file contents or null if reading has been aborted
     * @throws net.gicar.util.file.StorageItem.TooBigException The file was too big to be loaded
     */
    protected ByteBuffer load(InputStreamProvider streamProvider, int maxLimit) throws IOException, TooBigException
    {
        // check the limits
        Long size = getSize();
        if (size != null) {
            if (size > maxLimit) throw new TooBigException();
            if (size < maxLimit) maxLimit = size.intValue();
        }

        // copy all the data to a buffer
        int n;
        try (ReadableByteChannel readableChannel = Channels.newChannel(streamProvider.getInstance())) {
            // read into the byte buffer until no more data available or it is filled
            ByteBuffer b = ByteBuffer.allocate(maxLimit + 1);
            do n = readableChannel.read(b); while (n != -1 && b.remaining() > 0);
            if (n != -1) throw new TooBigException();

            return b;
        }
    }

    protected interface InputStreamProvider {
        InputStream getInstance() throws IOException;
    }

    /** Generic StorageItem exception */
    public static class StorageItemException extends Exception {
        StorageItemException(String message) { super(message); }
        StorageItemException(Throwable cause) { super(cause); }
        StorageItemException(String message, Throwable cause) { super(message, cause); }
    }

    /** Thrown if the file was too big to be loaded */
    public static class TooBigException extends Exception {}

    private void writeObject(java.io.ObjectOutputStream out) throws IOException
    {
        out.defaultWriteObject();
        out.writeObject(uri.toString());
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException
    {
        in.defaultReadObject();
        uri = Uri.parse((String) in.readObject());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StorageItem that = (StorageItem) o;

        return uri.equals(that.uri);
    }

    @Override
    public int hashCode() {
        return uri.hashCode();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "uri=" + uri +
                ", file=" + getFile() +
                ", name=" + getName() +
                ", size=" + getSize() +
                ", isDirectory=" + isDirectory() +
                ", mimeType=" + getMimeType() +
                '}';
    }
}
