package net.gicar.util.appcompat;

import android.content.Context;
import android.view.View;

import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.view.MenuCompat;

/**
 * PopupMenu with some additional feature.
 *
 *  Current features:
 * - Possibility to force the icons to be shown
 */
public class XPopupMenu extends PopupMenu {

    public XPopupMenu(@NonNull Context context, @NonNull View anchor) {
        super(context, anchor);
    }

    public XPopupMenu(@NonNull Context context, @NonNull View anchor, int gravity) {
        super(context, anchor, gravity);
    }

    public XPopupMenu(@NonNull Context context, @NonNull View anchor, int gravity, int popupStyleAttr, int popupStyleRes) {
        super(context, anchor, gravity, popupStyleAttr, popupStyleRes);
    }

    /**
     * Sets whether the popup menu's adapter is forced to show icons in the
     * menu item views.
     * <p>
     * Changes take effect on the next call to show().
     *
     * @param forceShowIcon {@code true} to force icons to be shown, or
     *                  {@code false} for icons to be optionally shown
     */
    public void setForceShowIcon(boolean forceShowIcon)
    {
        // access package private field
        Object menuHelper;
        Class[] argTypes;
        try {
            Field fMenuHelper = PopupMenu.class.getDeclaredField("mPopup");
            fMenuHelper.setAccessible(true);
            menuHelper = fMenuHelper.get(this);
            argTypes = new Class[] { boolean.class };
            menuHelper.getClass().getDeclaredMethod("setForceShowIcon",
                    argTypes).invoke(menuHelper, forceShowIcon);
        } catch (Exception e) {
            // Possible exceptions are NoSuchMethodError and
            // NoSuchFieldError
            //
            // In either case, an exception indicates something is wrong
            // with the reflection code, or the
            // structure of the PopupMenu class or its dependencies has
            // changed.
            //
            // These exceptions should never happen since we’re shipping the
            // AppCompat library in our own apk,
            // but in the case that they do, we simply can’t force icons to
            // display, so log the error and
            // show the menu normally.
//            Log.e("XXX", "Error", e);
        }
    }

    /**
     * Enable or disable the group dividers.
     * @param enabled True if enabled
     */
    public void setGroupDividerEnabled(boolean enabled)
    {
        MenuCompat.setGroupDividerEnabled(getMenu(), enabled);
    }
}
