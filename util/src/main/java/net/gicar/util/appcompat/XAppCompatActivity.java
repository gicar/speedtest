package net.gicar.util.appcompat;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;

import java.util.Arrays;
import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import net.gicar.util.Misc;

/**
 * AppCompatActivity implementing additional compatibility checks and functionalities.
 *
 * Current features:
 * - Maps the home button to the back button, which is the normal handling for activities without
 *   draw bars.
 * - Bug fix for action mode (see https://code.google.com/p/android/issues/detail?id=159527)
 * - Bug fix for appcompat 1.1.0 (see )
 * - Support for tasks which shall be performed by the activity when it is "fully operational",
 *   which in our terms mean that its state has not been saved due to an ongoing destroy.
 * - Permission request dispatching.
 * - Allows the storage of custom tags.
 */
public abstract class XAppCompatActivity extends AppCompatActivity {

    protected String TAG = this.getClass().getSimpleName();

    /*--------------------------------------------------------------------------------------------*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        //noinspection SwitchStatementWithTooFewBranches
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*--------------------------------------------------------------------------------------------*/

    @Override
    public ActionMode startSupportActionMode(@NonNull final ActionMode.Callback callback) {
        // Fix for bug https://code.google.com/p/android/issues/detail?id=159527
        final ActionMode mode = super.startSupportActionMode(callback);
        if (mode != null) {
            mode.invalidate();
        }
        return mode;
    }

    /*--------------------------------------------------------------------------------------------*/

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        // Fix for bug https://issuetracker.google.com/issues/141132133
        // Note that devices up tp SDK version 25 could be affected but only if Google Play is
        // not present, so we limit ourselves to avoid problems in the future
        // https://stackoverflow.com/questions/41025200/android-view-inflateexception-error-inflating-class-android-webkit-webview/58131421
        if (Build.VERSION.SDK_INT >= 21 && Build.VERSION.SDK_INT <= 22) return;
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    /*--------------------------------------------------------------------------------------------*/
    /* The following implementation is needed to properly handle dialogs */

    /** The ViewModel holding data to be retained for this activity */
    private ActivityViewModel activityViewModel;

    /** If true the instance state has been saved and we are going to die... */
    private boolean instanceStateSaved;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // get hold of the ViewModel we'll be using
        activityViewModel = new ViewModelProvider(this).get(ActivityViewModel.class);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        // reset instance saved state
        instanceStateSaved = false;

        // execute all the posted tasks
        for (ActivityTask task : activityViewModel.tasks) task.exec(this);
        activityViewModel.tasks.clear();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        instanceStateSaved = true;
    }

    /**
     * Checks if the activity state has been already saved.
     * After that event we are no longer allowed to commit fragment transactions.
     * @return true if the instance state has been saved
     */
    public boolean isInstanceStateSaved() {
        return instanceStateSaved;
    }

    /**
     * Posts a task to be executed when the activity state has not yet been saved
     * @param task The task to be executed
     * @return true if the task executed immediately, false if it has been queued
     */
    public final boolean post(ActivityTask task)
    {
        // execute it immediately if we have not been saved
        if (!isInstanceStateSaved()) {
            task.exec(this);
            return true;
        }

        // save it for better times
        activityViewModel.tasks.add(task);
        return false;
    }

    /** ViewModel used to retain activity data among re-instantiations */
    public static class ActivityViewModel extends ViewModel {

        /** The queued tasks */
        private final LinkedList<ActivityTask> tasks = new LinkedList<>();
    }

    /** A task which needs to be performed by the activity when it is "fully operational" */
    public interface ActivityTask {

        /**
         * Executed this task on the specified activity
         * @param activity The activity
         */
        void exec(XAppCompatActivity activity);
    }

    /*--------------------------------------------------------------------------------------------*/
    /* The following implementation is needed to support permission handling */

    /** Maps permission request codes to handlers */
    private final SparseArray<PermissionRequester> requesterMap = new SparseArray<>();

    /**
     * Registers a Permission requester
     * @param requestCode The associated request code
     * @param permissionRequester The PermissionRequester to be invoked
     */
    void registerPermissionRequester(int requestCode, PermissionRequester permissionRequester)
    {
        if (Misc.DEBUG) Log.d(TAG, "Registering permission requester on code " + requestCode + " for " +
                Arrays.toString(permissionRequester.getPermissions()));
        requesterMap.put(requestCode, permissionRequester);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        PermissionRequester permissionRequester = requesterMap.get(requestCode);
        if (permissionRequester != null) permissionRequester.result(permissions, grantResults);
        else super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /*--------------------------------------------------------------------------------------------*/
    /* The following implementation is needed to support extended activity result handling */

    /** Maps permission request codes to handlers */
    private final SparseArray<ActivityResultHandler> resultHandlerMap = new SparseArray<>();

    /**
     * Registers an activity result handler
     * @param requestCode The associated request code
     * @param handler The ActivityResultHandler to be invoked
     */
    public void registerActivityResultHandler(int requestCode, ActivityResultHandler handler)
    {
        resultHandlerMap.put(requestCode, handler);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData)
    {
        ActivityResultHandler handler = resultHandlerMap.get(requestCode);
        if (handler != null) handler.handleActivityResult(this, requestCode, resultCode, resultData);
        else super.onActivityResult(requestCode, resultCode, resultData);
    }

    /** Called to handle activity results */
    public interface ActivityResultHandler {

        /** Handle the activity result, similar to onActivityResult() */
        void handleActivityResult(XAppCompatActivity activity, int requestCode, int resultCode, Intent resultData);
    }

    /*--------------------------------------------------------------------------------------------*/

    /** Maps ids to tags */
    private final SparseArray<Object> customTagMap = new SparseArray<>();

    /**
     * Registers a tag
     * @param id The id
     * @param o The associated object
     */
    public void setCustomTag(int id, Object o) {
        customTagMap.put(id, o);
    }

    /**
     * Returns the object registered with the specified id, if any
     * @param id The unique identified
     * @return The Object or null if none
     */
    public Object getCustomTag(int id) {
        return customTagMap.get(id);
    }
}
