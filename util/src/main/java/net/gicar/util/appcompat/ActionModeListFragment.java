package net.gicar.util.appcompat;

import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.ListFragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/** A listFragment which supports ActionMode for AppCompat v7 */
abstract public class ActionModeListFragment extends ListFragment implements
		AdapterView.OnItemLongClickListener, androidx.appcompat.view.ActionMode.Callback {

	/** True if long clicks must be enabled */
	private boolean longClickEnabled = true;

	/** The ActionMode or null if not active */
	private ActionMode actionMode;

	/**
	 * Use this to change some configurations
	 * @param longClickEnabled true if long clicks must be enabled (default configuration)
	 */
	protected void setConfiguration(boolean longClickEnabled)
	{
		this.longClickEnabled = longClickEnabled;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// declare that we have menus (this is default behavior for classes extending this)
		setHasOptionsMenu(true);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		// register ourselves as long click listener
		ListView list = getListView();
		list.setLongClickable(longClickEnabled);
		if (longClickEnabled) list.setOnItemLongClickListener(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		// save if in ActionMode value
		outState.putBoolean("ActionMode", actionMode != null);

		super.onSaveInstanceState(outState);
	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		super.onViewStateRestored(savedInstanceState);

		// start the ActionMode if it was there before
		if (savedInstanceState != null && savedInstanceState.getBoolean("ActionMode", false))
			startActionMode();
	}

	@Override
	public void onListItemClick(ListView l, View view, int position, long id)
	{
		// if we are not with action mode report the selection and exit here
		if (actionMode == null) {
			onItemSelected(l, view, position, id);
			return;
		}

		// update the view: this assumes that getView will reuse the existing view
		l.getAdapter().getView(position, view, l);

		// notify the checked status of the list item
		onItemCheckedStateChanged(actionMode, view, position, id, l.isItemChecked(position));
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
	{
		// start the ActionMode
		startActionMode();

		// start selecting this item
		ListView list = getListView();
		list.setItemChecked(position, true);
		onItemCheckedStateChanged(actionMode, view, position, id, true);

		return true;
	}

	/**
	 * Call this method to start the ActionMode
	 * @return The created ActionMode
	 */
	public ActionMode startActionMode()
	{
		ListView list = getListView();

		// enable multiple selection and disable long clicks
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		list.setLongClickable(false);
		list.invalidateViews();

		// create the ActionMode
		actionMode = ((AppCompatActivity)getActivity()).startSupportActionMode(this);
		return actionMode;
	}

	/**
	 * Call this method to end the ActionMode
	 */
	protected void finishActionMode()
	{
		if (actionMode != null) actionMode.finish();
	}

	/**
	 * Returns the status of the action mode
	 * @return true is the ActionMode is active
	 */
	protected boolean isInActionMode()
	{
		return actionMode != null;
	}

	/**
	 * Calculates the number of checked items
	 * @return The number of checked items in the list
	 */
	protected int getCheckedItemCount()
	{
		// use built-in method if available
		if (Build.VERSION.SDK_INT >= 11) return getListView().getCheckedItemCount();

		// unfortunately we need to count them one by one :-(
		SparseBooleanArray checkedItems = getListView().getCheckedItemPositions();
		if (checkedItems == null) return 0;
		int cnt = 0;
		for (int i = 0, lim = checkedItems.size(); i < lim; ++i)
			if (checkedItems.valueAt(i)) cnt++;
		return cnt;
	}

	/**
	 * Returns the list of all the checked items. Not very efficient but easier to use.
	 * The returned list is sorted.
	 * @return List of positions of checked items, empty if none
	 */
	protected List<Integer> getCheckedItems()
	{
		List<Integer> l = new ArrayList<>();

		// get the sparse array
		SparseBooleanArray checkedItems = getListView().getCheckedItemPositions();
		if (checkedItems == null) return l;

		// add all the checked items one by one
		for (int i = 0, lim = checkedItems.size(); i < lim; ++i) {
			int k = checkedItems.keyAt(i);
			if (checkedItems.get(k)) l.add(k);
		}

		return l;
	}

	/**
	 * Returns the first checked item
	 * @return position of the first checked item, -1 if none
	 */
	protected int getFirstCheckedItem()
	{
		// get the sparse array
		SparseBooleanArray checkedItems = getListView().getCheckedItemPositions();
		if (checkedItems == null) return -1;

		// find the first checked item
		for (int i = 0, lim = checkedItems.size(); i < lim; ++i) {
			int k = checkedItems.keyAt(i);
			if (checkedItems.get(k)) return k;
		}

		return -1;
	}

	/**
	 * Utility function to build the string with the count of items
	 * @param n The number of items
	 * @param id_selected_0 The id of the string for 0 items
	 * @param id_selected_1 The id of the string for 1 item
	 * @param id_selected_n The id of the string for n items
	 * @return The string describing the number of items
	 */
	protected String buildCountString(int n, int id_selected_0, int id_selected_1, int id_selected_n)
	{
		Resources r = getActivity().getResources();
		int id;
		switch (n) {
			case 0:
				id = id_selected_0;
				break;
			case 1:
				id = id_selected_1;
				break;
			default:
				id = id_selected_n;
		}
		if (id == 0) return Integer.toString(n);
		return String.format(r.getString(id), n);
	}

	@Override
	public void onDestroyActionMode(androidx.appcompat.view.ActionMode mode)
	{
		// restore normal view status
		ListView list = getListView();
		list.setLongClickable(longClickEnabled);
		list.clearChoices();
		list.setChoiceMode(ListView.CHOICE_MODE_NONE);
		list.invalidateViews();

		// forget about the ActionMode
		actionMode = null;
	}

	/**
	 * This method will be called when an item in the list is selected.
	 * Subclasses should override. Subclasses can call
	 * getListView().getItemAtPosition(position) if they need to access the
	 * data associated with the selected item.
	 *
	 * @param l The ListView where the click happened
	 * @param v The view that was clicked within the ListView
	 * @param position The position of the view in the list
	 * @param id The row id of the item that was clicked
	 */
	abstract public void onItemSelected(ListView l, View v, int position, long id);

	/**
	 * Called when an item is checked or unchecked during selection mode.
	 *
	 * @param mode The {@link ActionMode} providing the selection mode
	 * @param v The view that was clicked within the ListView
	 * @param position Adapter position of the item that was checked or unchecked
	 * @param id Adapter ID of the item that was checked or unchecked
	 * @param checked <code>true</code> if the item is now checked, <code>false</code>
	 *                if the item is now unchecked.
	 */
	public void onItemCheckedStateChanged(ActionMode mode, View v, int position, long id, boolean checked)
	{
		// default behaviour is updating the ActionMode
		onPrepareActionMode(mode, mode.getMenu());
	}

}
