package net.gicar.util.appcompat;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.services.CrashReporting;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static net.gicar.util.appcompat.PermissionRequestBaseDialog.REQUESTCODE_KEY;

/** Handles permission requests */
public class PermissionRequester {

    /** The log tag */
    final private static String TAG = "PermissionRequester";

    /** Our Activity */
    final private XAppCompatActivity activity;

    /** The permission */
    final private String[] permissions;

    /** The request code associated with these permissions */
    final private int requestCode;

    /** Handles permission results */
    final private PermissionRequestDialogHandler requestDialogHandler;

    /** Handles permission results */
    final private PermissionHandler handler;

    /** Internal fake permission name used to get result from the request dialog */
    final static String DIALOG_PERMISSION = "net.gicar.dialogpermission";

    /**
     * Creates a PermissionRequester for the specified permissions
     * @param activity Our Activity
     * @param permission The single permission which shall be handled
     * @param requestCode The requestCode which will be supplied to onRequestPermissionsResult()
     * @param handler Handles permission results
     */
    public PermissionRequester(XAppCompatActivity activity, String permission, int requestCode,
                               PermissionHandler handler)
    {
        this(activity, new String[] { permission }, requestCode, handler);
    }

    /**
     * Creates a PermissionRequester for the specified permissions
     * @param activity Our Activity
     * @param permissions The permissions which shall be handled
     * @param requestCode The requestCode which will be supplied to onRequestPermissionsResult()
     * @param handler Handles permission results
     */
    public PermissionRequester(XAppCompatActivity activity, String[] permissions, int requestCode,
                               PermissionHandler handler)
    {
        this(activity, permissions, requestCode, handler, null);
    }

    /**
     * Creates a PermissionRequester for the specified permissions
     * @param activity Our Activity
     * @param permission The single permission which shall be handled
     * @param requestCode The requestCode which will be supplied to onRequestPermissionsResult()
     * @param handler Handles permission results
     * @param askTheme The theme id for the ask dialog, 0 if none
     * @param askIcon The icon id for the ask dialog, 0 if none
     * @param askTitle The title text id for the ask dialog, 0 if none
     * @param askMessage The message text id for the ask dialog, 0 if request if not supported
     */
    public PermissionRequester(XAppCompatActivity activity, String permission, int requestCode,
                               PermissionHandler handler,
                               int askTheme, int askIcon, int askTitle, int askMessage)
    {
        this(activity, new String[] { permission }, requestCode, handler, askTheme, askIcon, askTitle, askMessage);
    }

    /**
     * Creates a PermissionRequester for the specified permissions
     * @param activity Our Activity
     * @param permissions The permissions which shall be handled
     * @param requestCode The requestCode which will be supplied to onRequestPermissionsResult()
     * @param handler Handles permission results
     * @param askTheme The theme id for the ask dialog, 0 if none
     * @param askIcon The icon id for the ask dialog, 0 if none
     * @param askTitle The title text id for the ask dialog, 0 if none
     * @param askMessage The message text id for the ask dialog, 0 if request if not supported
     */
    public PermissionRequester(XAppCompatActivity activity, String[] permissions, int requestCode,
                               PermissionHandler handler,
                               int askTheme, int askIcon, int askTitle, int askMessage)
    {
        this(activity, permissions, requestCode, handler,
                (permissionRequester, missing, args) -> PermissionRequestDialog.show(activity, args, askTheme, askIcon, askTitle, askMessage));
    }

    /**
     * Creates a PermissionRequester for the specified permissions
     * @param activity Our Activity
     * @param permission The single permission which shall be handled
     * @param requestCode The requestCode which will be supplied to onRequestPermissionsResult()
     * @param handler Handles permission results
     * @param requestDialogHandler The provider for the request dialog or null if none
     */
    public PermissionRequester(XAppCompatActivity activity, String permission, int requestCode,
                               PermissionHandler handler, PermissionRequestDialogHandler requestDialogHandler)
    {
        this(activity, new String[] { permission }, requestCode, handler, requestDialogHandler);
    }

    /**
     * Creates a PermissionRequester for the specified permissions
     * @param activity Our Activity
     * @param permissions The permissions which shall be handled
     * @param requestCode The requestCode which will be supplied to onRequestPermissionsResult()
     * @param handler Handles permission results
     * @param requestDialogHandler The provider for the request dialog or null if none
     */
    public PermissionRequester(XAppCompatActivity activity, String[] permissions, int requestCode,
                               PermissionHandler handler, PermissionRequestDialogHandler requestDialogHandler)
    {
        this.activity = activity;
        this.permissions = permissions;
        this.requestCode = requestCode;
        this.requestDialogHandler = requestDialogHandler;
        this.handler = handler;

        activity.registerPermissionRequester(requestCode, this);
    }

    /**
     * Permissions associated with this requester
     * @return List of permissions needed by this requester
     */
    public String[] getPermissions() {
        return permissions;
    }

    /**
     * Returns the set of the permissions which are missing
     * @return A Set holding the missing permissions
     */
    public Set<String> missingPermissions()
    {
        HashSet<String> m = new HashSet<>(permissions.length);
        for (String p : permissions)
            if (ContextCompat.checkSelfPermission(activity, p) != PackageManager.PERMISSION_GRANTED)
                m.add(p);
        return m;
    }

    /**
     * Returns the set of the permissions which are missing and that can show a request
     * @return A Set holding the missing permissions
     */
    public Set<String> missingPermissionsCanShowRequest()
    {
        HashSet<String> m = new HashSet<>(permissions.length);
        for (String p : permissions)
            if (ContextCompat.checkSelfPermission(activity, p) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.shouldShowRequestPermissionRationale(activity, p))
                m.add(p);
        return m;
    }

    /**
     * Returns the set of the permissions which are missing but that cannot be requested to the user
     * as he denied them
     * @return A Set holding the missing permissions that cannot be requested
     */
    public Set<String> missingPermissionsCantRequest()
    {
        HashSet<String> m = new HashSet<>(permissions.length);
        for (String p : permissions)
            if (ContextCompat.checkSelfPermission(activity, p) != PackageManager.PERMISSION_GRANTED &&
                    !ActivityCompat.shouldShowRequestPermissionRationale(activity, p))
                m.add(p);
        return m;
    }

    /**
     * Shows that system setting of the app where the user can modify the permissions
     * @param context The context
     * @return true if the settings were shown, false if some error occurred
     */
    public static boolean showAppSystemSettings(Context context)
    {
        try {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", context.getPackageName(), null);
            intent.setData(uri);
            context.startActivity(intent);
            return true;
        }
        catch (Exception e) {
            CrashReporting.logException(e);
            return false;
        }
    }

    /** How to handle the permission dialog */
    public enum AskStrategy {
        DO_NO_ASK,
        ASK_DEFAULT,
        FORCE_ASK
    }

    /** Requests this permission set using the default ask strategy */
    public void request()
    {
        request(AskStrategy.ASK_DEFAULT);
    }

    /**
     * Requests this permission set
     * @param ask The ask strategy
     */
    public void request(AskStrategy ask)
    {
        if (Misc.DEBUG) Log.d(TAG, "Requesting permissions " + Arrays.toString(permissions) +
                " ask:" + ask);

        // if all already granted just proceed
        Set<String> m = missingPermissions();
        if (m.size() == 0) {
            if (Misc.DEBUG) Log.d(TAG, "All permissions are granted");
            handler.permissionResult(this, m);
            return;
        }
        if (Misc.DEBUG) Log.d(TAG, "Missing permissions " + m);

        // check if asking is allowed
        if (ask != AskStrategy.DO_NO_ASK && requestDialogHandler != null) {

            // check which of the missing permissions should show a request dialog
            Set<String> x = ask == AskStrategy.ASK_DEFAULT ? missingPermissionsCanShowRequest() : missingPermissions();
            if (x.size() != 0) {
                if (Misc.DEBUG) Log.d(TAG, "Asking for permissions " + x);
                Bundle args = new Bundle();
                args.putInt(REQUESTCODE_KEY, requestCode);
                requestDialogHandler.showPermissionRequestDialog(this, x, args);
                return;
            }
        }

        // No explanation needed, we can request the permission.
        if (Misc.DEBUG) Log.d(TAG, "Requesting missing permissions");
        ActivityCompat.requestPermissions(activity, m.toArray(new String[0]), requestCode);
    }

    /**
     * Processes permission request results
     * @param permissions The requested permissions, empty if cancelled
     * @param grantResults The grant result
     */
    public void result(@NonNull String[] permissions, @NonNull int[] grantResults)
    {
        // on screen rotation in some unclear conditions we could get an empty list;
        // documentations tells to treat it as cancellation, but it happens on an uninitialized
        // activity so we will just ignore it
        if (permissions.length == 0) return;

        // check if we have got the result from our dialog
        if (permissions.length == 1 && DIALOG_PERMISSION.equals(permissions[0])) {

            // proceed to request permissions again only if user accepted the dialog
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (Misc.DEBUG) Log.d(TAG, "Dialog request acknowledged");
                request(AskStrategy.DO_NO_ASK);
                return;
            }

            if (Misc.DEBUG) Log.d(TAG, "Dialog request dismissed");
        }

        // ignore the parameters and just query the current permissions
        Set<String> m = missingPermissions();
        if (Misc.DEBUG) Log.d(TAG, "Request result reached: needed " + Arrays.toString(permissions) +
                ", missing " + m);
        handler.permissionResult(this, m);
    }

    /** Called to handle permission results */
    public interface PermissionRequestDialogHandler {

        /**
         * Shows the permission request dialog
         * @param permissionRequester The source PermissionRequester
         * @param missing The missing permissions
         * @param args The base arguments
         */
        void showPermissionRequestDialog(PermissionRequester permissionRequester, Set<String> missing, Bundle args);
    }

    /** Called to handle permission results */
    public interface PermissionHandler {

        /**
         * Handle the permission result
         * @param permissionRequester The source PermissionRequester
         * @param missing The missing permissions
         */
        void permissionResult(PermissionRequester permissionRequester, Set<String> missing);
    }
}
