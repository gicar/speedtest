package net.gicar.util.appcompat;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import net.gicar.util.Misc;

/** Manages the Fragment transitions */
public class FragmentHandler {

    /** The log tag */
    private final static String TAG = FragmentHandler.class.getSimpleName();

    /** Saved instance configuration */
    static private final String
            CURRENT_CONFIGURATION_CONTAINER_VIEW_ID = "CURRENT_CONFIGURATION_CONTAINER_VIEW_ID",
            CURRENT_CONFIGURATION_FRAGMENT_VIEW_IDS = "CURRENT_CONFIGURATION_FRAGMENT_VIEW_IDS";

    /** Our Activity */
    private final XAppCompatActivity activity;

    /** Factory class for Fragments */
    private final FragmentFactory fragmentFactory;

    /** The activity's base view */
    private final ViewGroup baseView;

    /** The container views */
    private final HashMap<Integer, ViewGroup> containerViewMap = new HashMap<>();

    /** The current Fragment configuration or null if none */
    private FragmentConfiguration currentConfiguration;

    /** The Fragment configuration pending to be applied or null if none */
    private FragmentConfiguration pendingConfiguration;

    /**
     * Creates a the FragmentHandler for the Activity. Call it onCreate() after adding the base
     * activity view.
     * @param activity The Activity wich will hold the section
     * @param savedInstanceState The saved instance state
     * @param fragmentFactory Factory class for Fragments
     * @param baseViewId The activity's base view id
     */
    public FragmentHandler(XAppCompatActivity activity, Bundle savedInstanceState,
                           FragmentFactory fragmentFactory, int baseViewId)
    {
        // save the parameters
        this.activity = activity;
        this.fragmentFactory = fragmentFactory;
        this.baseView = (ViewGroup) activity.findViewById(baseViewId);

        // retrieve the previous configuration if any and restore its container view
        // while the fragments will be automatically put back in place by the framework
        if (savedInstanceState != null && savedInstanceState.containsKey(CURRENT_CONFIGURATION_CONTAINER_VIEW_ID)) {
            currentConfiguration = new FragmentConfiguration(
                    savedInstanceState.getInt(CURRENT_CONFIGURATION_CONTAINER_VIEW_ID),
                    savedInstanceState.getIntegerArrayList(CURRENT_CONFIGURATION_FRAGMENT_VIEW_IDS));
            baseView.addView(getContainerView(currentConfiguration.containerViewId));
        }
    }

    /**
     * Call this in the onSaveInstanceState of the activity
     * @param outState The Bundle where the fragment configuration will be saved
     */
    public void onSaveInstanceState(Bundle outState)
    {
        if (currentConfiguration != null) {
            outState.putInt(CURRENT_CONFIGURATION_CONTAINER_VIEW_ID,
                    currentConfiguration.containerViewId);
            outState.putIntegerArrayList(CURRENT_CONFIGURATION_FRAGMENT_VIEW_IDS,
                    new ArrayList<>(currentConfiguration.fragmentViewIds));
        }
    }

    private ViewGroup getContainerView(int viewId)
    {
        // check if we already have it inflated, if not create and store it
        ViewGroup view = containerViewMap.get(viewId);
        if (view == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            view = (ViewGroup)inflater.inflate(viewId, null);
            containerViewMap.put(viewId, view);
        }

        return view;
    }

    /**
     * Set the displayed currentFragments
     * @param c The configuration which will be shown
     */
    public void show(FragmentConfiguration c)
    {
        // return if it is the current one and there are no pending ones
        if (c.equals(currentConfiguration) && pendingConfiguration == null) return;

        // if instance saved save for next time
        if (activity.isInstanceStateSaved()) {
            pendingConfiguration = new FragmentConfiguration(c.containerViewId, c.fragmentViewIds);
            return;
        }
        pendingConfiguration = null;

        if (Misc.DEBUG) Log.d(TAG, "Switching to " + c);

        // get the FragmentManager and
        // make sure that any previous fragment operations have been completed
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.executePendingTransactions();

        // build sets of ids to remove and add
        Set<Integer> removeIds = null;
        Set<Integer> addIds = new HashSet<>(c.fragmentViewIds);
        boolean changeContainer = true;
        if (currentConfiguration != null) {
            removeIds = new HashSet<>(currentConfiguration.fragmentViewIds);
            if (c.containerViewId == currentConfiguration.containerViewId) {
                changeContainer = false;
                removeIds.removeAll(c.fragmentViewIds);
                addIds.removeAll(currentConfiguration.fragmentViewIds);
            }
        }

        // remove previous fragments from the section view
        FragmentTransaction t = fragmentManager.beginTransaction();
        if (removeIds != null) {
            for (Integer id : removeIds) {
                Fragment f = fragmentManager.findFragmentById(id);
                if (f != null) t.remove(f);
            }
        }

        // if changing container view, remove the prev one if present and add the new one
        ViewGroup containerView = getContainerView(c.containerViewId);
        if (changeContainer) {
            t.commitNow();
            t = fragmentManager.beginTransaction();
            if (currentConfiguration != null)
                baseView.removeView(containerViewMap.get(currentConfiguration.containerViewId));
            baseView.addView(containerView);
        }

        // add the fragments in it
        for (Integer id : addIds) {

            // skip currentFragments without a view in the current section
            if (containerView.findViewById(id) == null) continue;

            // try to find the Fragment if already existing
            Fragment f = fragmentManager.findFragmentById(id);

            // if not found create it for the first time
            if (f == null) f = fragmentFactory.getFragmentInstance(id);

            // if not already added just add it
            if (!f.isAdded()) t.add(id, f);
        }

        // commit the changes
        t.commitNow();

        // save this configuration as the current one, making a copy
        currentConfiguration = new FragmentConfiguration(c.containerViewId, c.fragmentViewIds);
    }

    /**
     * Fixes the fragments currently displayed: after screen rotation the container view could
     * have different fragments and we need to check that the ones that we have are still the right
     * ones.
     */
    public void onPostCreate()
    {
        // if no current configuration just exit
        if (currentConfiguration == null) return;

        // loop for all the fragment ids to check if they need to be fixed
        ViewGroup containerView = getContainerView(currentConfiguration.containerViewId);
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction t = fragmentManager.beginTransaction();
        for (int id : currentConfiguration.fragmentViewIds) {

            // try to find the Fragment if already existing
            Fragment f = fragmentManager.findFragmentById(id);

            // skip currentFragments without a view in the current section
            if (containerView.findViewById(id) == null) {
                if (f != null) t.remove(f);
            }
            else {
                if (f == null) t.add(id, fragmentFactory.getFragmentInstance(id));
            }
        }
        t.commitNow();
    }

    /**
     * Set the displayed currentFragments to the pending configuration, if any.
     * Must be called onPostResume to handle cases where the configuration could not be applied
     * because the state was saved during suspension.
     */
    public void onPostResume()
    {
        // get and clear the pending configuration
        FragmentConfiguration c = pendingConfiguration;
        pendingConfiguration = null;

        // show it if any
        if (c != null) show(c);
    }

    /**
     * Look for the specified view in the current configuration, returning it if found
     * @param viewId The id of the view
     * @return The View or null if not found/present
     */
    public View getView(int viewId)
    {
        if (currentConfiguration == null) return null;

        ViewGroup c = getContainerView(currentConfiguration.containerViewId);
        if (c == null) return null;

        return c.findViewById(viewId);
    }

    /** Interface which shall provide Fragment instances */
    public interface FragmentFactory {
        /**
         * Creates a Fragment instance which shall be contained in the specified container view
         * @param fragmentViewId The Id of the view which will contain the Fragment
         * @return The created Fragment or null if not available
         */
        Fragment getFragmentInstance(int fragmentViewId);
    }

    /** A particular configuration of Fragments */
    public static class FragmentConfiguration {

        /** The view ID which will contain these Fragments */
        private final int containerViewId;

        /** The Ids of the views which will contain the Fragments */
        private final Set<Integer> fragmentViewIds;

        /**
         * Creates a FragmentConfiguration instance
         * @param containerViewId The view ID which will contain these Fragments
         * @param fragmentViewIds The Ids of the views which will contain the Fragments
         */
        public FragmentConfiguration(int containerViewId, Collection<Integer> fragmentViewIds) {
            this.containerViewId = containerViewId;
            this.fragmentViewIds = new HashSet<>(fragmentViewIds);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            FragmentConfiguration that = (FragmentConfiguration) o;

            if (containerViewId != that.containerViewId) return false;
            return fragmentViewIds.equals(that.fragmentViewIds);
        }

        @Override
        public int hashCode() {
            int result = containerViewId;
            result = 31 * result + fragmentViewIds.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "FragmentConfiguration{" +
                    "containerViewId=" + containerViewId +
                    ", fragmentViewIds=" + fragmentViewIds +
                    '}';
        }
    }
}
