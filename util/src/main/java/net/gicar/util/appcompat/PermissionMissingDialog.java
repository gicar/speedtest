package net.gicar.util.appcompat;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

/** Notifies a missing Android permission */
public class PermissionMissingDialog extends XAppCompatDialogFragment {

	/** The name we will use for our preferences file, fragment name and logs */
	private final static String PERMISSION_MISSING_DIALOG = "PERMISSION_MISSING_DIALOG";

    /** The keys for the fields in the bundles */
    private final static String
            THEME_KEY = "net.gicar.permissionmissing.theme",
            ICON_KEY = "net.gicar.permissionmissing.icon",
            TITLE_KEY = "net.gicar.permissionmissing.title",
            MESSAGE_KEY = "net.gicar.permissionmissing.message";

    /**
     * Starts a dialog showing the specified permission request message
     * @param activity The activity requesting the dialog
     * @param theme The theme id
     * @param icon The icon id
     * @param title The title text
     * @param message The message text
     */
    public static void show(XAppCompatActivity activity,
                            int theme, int icon, int title, int message)
    {
        // create our fragment, save the parameters and show it
        Bundle args = new Bundle();
        args.putInt(THEME_KEY, theme);
        args.putInt(ICON_KEY, icon);
        args.putInt(TITLE_KEY, title);
        args.putInt(MESSAGE_KEY, message);
        new PermissionMissingDialog().showRequest(activity, PERMISSION_MISSING_DIALOG, args);
    }

    public PermissionMissingDialog() {}

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle arg = getArguments();
        return new AlertDialog.Builder(getActivity(), arg.getInt(THEME_KEY))
                .setTitle(arg.getInt(TITLE_KEY))
                .setMessage(arg.getInt(MESSAGE_KEY))
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(arg.getInt(ICON_KEY))
                .create();
    }
}
