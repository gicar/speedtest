package net.gicar.util.appcompat;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

/** Requests an Android permission */
public class PermissionRequestDialog extends PermissionRequestBaseDialog {

	/** The name we will use for our preferences file, fragment name and logs */
	private final static String PERMISSION_REQUEST_DIALOG = "PERMISSION_REQUEST_DIALOG";

    /** The keys for the fields in the bundles */
    private final static String
            THEME_KEY = "net.gicar.permissionrequest.theme",
            ICON_KEY = "net.gicar.permissionrequest.icon",
            TITLE_KEY = "net.gicar.permissionrequest.title",
            MESSAGE_KEY = "net.gicar.permissionrequest.message";

    /**
     * Starts a dialog showing the specified permission request message
     * @param activity The activity requesting the dialog
     * @param args The base args
     * @param theme The theme id
     * @param icon The icon id
     * @param title The title text
     * @param message The message text
     */
    public static void show(XAppCompatActivity activity, Bundle args,
                            int theme, int icon, int title, int message)
    {
        // create our fragment, save the parameters and show it
        args.putInt(THEME_KEY, theme);
        args.putInt(ICON_KEY, icon);
        args.putInt(TITLE_KEY, title);
        args.putInt(MESSAGE_KEY, message);
        new PermissionRequestDialog().showRequest(activity, PERMISSION_REQUEST_DIALOG, args);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle arg = requireArguments();
        return new AlertDialog.Builder(requireActivity(), arg.getInt(THEME_KEY))
                .setTitle(arg.getInt(TITLE_KEY))
                .setMessage(arg.getInt(MESSAGE_KEY))
                .setPositiveButton(android.R.string.ok, (dialog, which) -> notifyRequestDialogResult(true))
                .setIcon(arg.getInt(ICON_KEY))
                .create();
    }
}
