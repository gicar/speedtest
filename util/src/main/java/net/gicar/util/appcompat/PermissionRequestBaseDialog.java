package net.gicar.util.appcompat;

import android.content.DialogInterface;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;

public abstract class PermissionRequestBaseDialog extends XAppCompatDialogFragment {

    /** The keys for the fields in the bundles */
    final static String REQUESTCODE_KEY = "net.gicar.permissionrequest.requestcode";

    /**
     * Notifies the result of the dialog
     * @param accepted If true the user accepted to proceed with the permission request
     */
    protected void notifyRequestDialogResult(boolean accepted)
    {
        requireActivity().onRequestPermissionsResult(requireArguments().getInt(REQUESTCODE_KEY),
                new String[] { PermissionRequester.DIALOG_PERMISSION },
                new int[] { accepted ? PackageManager.PERMISSION_GRANTED : PackageManager.PERMISSION_DENIED });
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        notifyRequestDialogResult(false);
    }
}
