package net.gicar.util.appcompat;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import net.gicar.util.Misc;

/** AppCompatActivity providing interactions with the navigation drawer(s) */
public abstract class NavigationDrawerActivity extends XAppCompatActivity {

    /** The drawer layout view */
    private DrawerLayout drawerLayout;

    /** The navigation drawers */
    private NavigationDrawer leftNavigationDrawer, rightNavigationDrawer;

    /** The selected item from the menu */
    private MenuItem selectedMenuItem;

    /** The positions of the navigation drawers */
    public enum NavigationDrawerPosition {
        LEFT,
        RIGHT
    }

    /** Handler associated to a navigation drawer */
    public class NavigationDrawer implements NavigationView.OnNavigationItemSelectedListener {

        /** The view */
        private final NavigationView navigationView;

        /** Its gravity in the window */
        private final int gravity;

        private NavigationDrawer(NavigationView navigationView, int gravity) {
            this.navigationView = navigationView;
            this.gravity = gravity;
            navigationView.setNavigationItemSelectedListener(this);
        }

        /** Opens this navigation drawer */
        public void open() {
            drawerLayout.openDrawer(gravity);
        }

        /** Closes this navigation drawer */
        public void close() {
            close(true);
        }

        /** Closes this navigation drawer */
        public void close(boolean animate) {
            drawerLayout.closeDrawer(gravity, animate);
        }

        /**
         * Check if the drawer view is currently in an open state.
         * To be considered "open" the drawer must have settled into its fully visible state.
         * To check for partial visibility use isNavigationDrawerVisible().
         * @return true if the given drawer view is in an open state
         */
        public boolean isOpen() {
            return drawerLayout.isDrawerOpen(gravity);
        }

        /**
         * Check if the drawer view is currently visible on-screen.
         * The drawer may be only peeking onto the screen, fully extended, or anywhere in between.
         * @return true if the drawer is visible on-screen
         */
        public boolean isVisible() {
            return drawerLayout.isDrawerVisible(gravity);

        }

        /** Unlocks this navigation drawer */
        public void setUnlocked() {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, gravity);
        }

        /** Closes this navigation drawer and locks it */
        public void setLockedClosed() {
            close();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, gravity);
        }

        /**
         * Returns the Navigation menu
         * @return The Menu
         */
        public Menu getMenu()
        {
            return navigationView.getMenu();
        }

        /**
         * Finds a specific navigation menu item
         * @param menuId The id of the menu entry
         * @return The corresponding MenuItem
         */
        public MenuItem getMenuItem(int menuId) {
            return navigationView.getMenu().findItem(menuId);
        }

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            selectedMenuItem = menuItem;
            drawerLayout.closeDrawers();
            return false;
        }
    }

    /**
     * Configures the navigation drawer. Shall be called in onCreate
     * @param drawerLayoutId The view id of the drawer
     * @param navigationViewId The view id of the navigation
     */
    protected void configureNavigationDrawer(int drawerLayoutId, int navigationViewId) {
        configureNavigationDrawer(drawerLayoutId, navigationViewId, 0);
    }

    /**
     * Configures the navigation drawer. Shall be called in onCreate
     * @param drawerLayoutId The view id of the drawer
     * @param navigationViewId The view id of the left navigation
     * @param rightNavigationViewId The view id of the optional right navigation, use 0 for none
     */
    protected void configureNavigationDrawer(int drawerLayoutId, int navigationViewId, int rightNavigationViewId)
    {
        // configure the drawer
        drawerLayout = findViewById(drawerLayoutId);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerStateChanged(int newState) {
                NavigationDrawerActivity.this.onDrawerStateChanged(newState != 0);
            }

            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                if (drawerView == leftNavigationDrawer.navigationView)
                    NavigationDrawerActivity.this.onDrawerSlide(NavigationDrawerPosition.LEFT, slideOffset);
                else if (drawerView == rightNavigationDrawer.navigationView)
                    NavigationDrawerActivity.this.onDrawerSlide(NavigationDrawerPosition.RIGHT, slideOffset);
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                if (drawerView == leftNavigationDrawer.navigationView)
                    NavigationDrawerActivity.this.onDrawerOpened(NavigationDrawerPosition.LEFT);
                else if (drawerView == rightNavigationDrawer.navigationView)
                    NavigationDrawerActivity.this.onDrawerOpened(NavigationDrawerPosition.RIGHT);
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                if (selectedMenuItem != null) {
                    int id = selectedMenuItem.getItemId();
                    selectedMenuItem = null;
                    executeNavigationItem(id);
                }
                if (drawerView == leftNavigationDrawer.navigationView)
                    NavigationDrawerActivity.this.onDrawerClosed(NavigationDrawerPosition.LEFT);
                else if (drawerView == rightNavigationDrawer.navigationView)
                    NavigationDrawerActivity.this.onDrawerClosed(NavigationDrawerPosition.RIGHT);
            }
        });

        // configure the navigation drawer
        leftNavigationDrawer = new NavigationDrawer(findViewById(navigationViewId), GravityCompat.START);

        // configure the right navigation drawer if present
        if (rightNavigationViewId != 0)
            rightNavigationDrawer = new NavigationDrawer(findViewById(rightNavigationViewId), GravityCompat.END);
    }

    /**
     * Getter method for the navigation drawer
     * @return The navigation drawer
     */
    public NavigationDrawer getNavigationDrawer(NavigationDrawerPosition position) {
        if (position == NavigationDrawerPosition.LEFT) return leftNavigationDrawer;
        else if (position == NavigationDrawerPosition.RIGHT) return rightNavigationDrawer;
        return null;
    }

    /**
     * Getter method for the left navigation drawer
     * @return The left navigation drawer
     */
    public NavigationDrawer getLeftNavigationDrawer() {
        return leftNavigationDrawer;
    }

    /**
     * Getter method for the right navigation drawer (if any)
     * @return The right navigation drawer or null if none
     */
    public NavigationDrawer getRightNavigationDrawer() {
        return rightNavigationDrawer;
    }

    /**
     * Close open drawers
     * @return true if a close operation has been performed, false if there is no drawer to close
     */
    public boolean closeDrawers()
    {
        boolean closed = false;
        if (leftNavigationDrawer != null && leftNavigationDrawer.isVisible()) {
            leftNavigationDrawer.close();
            closed = true;
        }
        if (rightNavigationDrawer != null && rightNavigationDrawer.isVisible()) {
            rightNavigationDrawer.close();
            closed = true;
        }
        return closed;
    }

    /**
     * Callback for the opening of a drawer
     * @param position The position for the drawer which has been opened
     */
    protected void onDrawerOpened(@NonNull NavigationDrawerPosition position) {
        if (Misc.DEBUG) Log.v(TAG, "onDrawerOpened " + position);
    }

    /**
     * Callback for the closing of a drawer.
     * Could not be called if the corresponding open has not been called, for example if the
     * drawer was not fully opened.
     * @param position The position for the drawer which has been closed
     */
    protected void onDrawerClosed(@NonNull NavigationDrawerPosition position) {
        if (Misc.DEBUG) Log.v(TAG, "onDrawerClosed " + position);
    }

    /**
     * Callback for the position sliding of a drawer.
     * @param position The position for the drawer which has been closed
     * @param slideOffset The sliding offset, 0 = closed, 1 = open
     */
    protected void onDrawerSlide(@NonNull NavigationDrawerPosition position, float slideOffset) {
        if (Misc.DEBUG) Log.v(TAG, "onDrawerSlide " + position + " offset=" + slideOffset);
    }

    /**
     * Called when the drawer motion state changes.
     * @param moving If true the drawers are moving, if false no movement is active
     */
    protected void onDrawerStateChanged(boolean moving) {
        if (Misc.DEBUG) Log.v(TAG, "onDrawerStateChanged moving=" + moving);
    }


    @Override
    public void onBackPressed() {
        if (!closeDrawers()) super.onBackPressed();
    }

    /**
     * Execute the action corresponding to the specified menu item
     * @param menuItemId The selected menu item id
     */
    protected void executeNavigationItem(int menuItemId) {}
}
