package net.gicar.util.appcompat;

import androidx.multidex.MultiDexApplication;
import net.gicar.util.Misc;

/** Base class for all our Applications */
public class XApplication extends MultiDexApplication {

    /** The current active application instance */
    private static XApplication instance;

    /**
     * Getter method for the current XApplication instance
     * @return The created XApplication currently active
     */
    public static XApplication getInstance() {
        if (instance == null) throw new RuntimeException("Application class must extend XApplication");
        return instance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        // save the current application instance
        instance = this;
    }

    /**
     * Configures the application
     * @param debug true if this is a debug build
     * @param package64 Base64-encoded package name
     * @param signature64 Base64-encoded apk signature
     */
    protected void configure(boolean debug, String package64, String signature64)
    {
        // initialize the debug output flag
        Misc.init(this, debug, package64, signature64);
    }
}
