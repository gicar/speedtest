package net.gicar.util.appcompat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.appcompat.content.res.AppCompatResources;

/** Misc utilities for AppCompat */
public class CompatUtils {

    /**
     * Gets the reference to a drawable which could be tinted
     * @param context The Context which shall be used for the operation
     * @param drawableId The ID of the drawable resource
     * @param colorId The ID of the color which shall be used, or 0 for none
     */
    public static Drawable tintDrawable(Context context, int drawableId, int colorId)
    {
        // get the original drawable and return it if no color shall be applied
        Drawable d = AppCompatResources.getDrawable(context, drawableId);
        if (colorId == 0) return d;

        // tint it
        Drawable d2 = DrawableCompat.wrap(d).mutate();
        DrawableCompat.setTint(d2, ContextCompat.getColor(context, colorId));
        return d2;
    }

    /**
     * Gets the reference to a drawable which could be tinted
     * @param drawable The drawable
     * @param color The color which shall be used
     */
    public static Drawable tintDrawable(Drawable drawable, int color)
    {
        // tint it
        Drawable d = DrawableCompat.wrap(drawable).mutate();
        DrawableCompat.setTint(d, color);
        return d;
    }
}
