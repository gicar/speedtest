package net.gicar.util.appcompat;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ViewModelProvider;
import net.gicar.util.Misc;

/** Mimics the good old Fragment with setRetainInstance(true) */
abstract public class RetainedInstance extends AndroidViewModel {

    private final String TAG = "RetainedInstance";

    /**
     * Gets the RetainedInstance connected with the specified activity
     * @param activity The activity to which this instance must be tied
     * @param modelClass The class of the RetainedInstance to create an instance of it if it is not present
     * @param <T> The type parameter for the RetainedInstance
     * @return A RetainedInstance that is an instance of the given type {@code T}
     */
    public static <T extends RetainedInstance> T getInstance(@NonNull FragmentActivity activity,
                                                             @NonNull Class<T> modelClass) {
        return getInstance(activity, modelClass, null);
    }

    /**
     * Gets the RetainedInstance connected with the specified activity
     * @param activity The activity to which this instance must be tied
     * @param modelClass The class of the RetainedInstance to create an instance of it if it is not present
     * @param args Optional arguments
     * @param <T> The type parameter for the RetainedInstance
     * @return A RetainedInstance that is an instance of the given type {@code T}
     */
    public static <T extends RetainedInstance> T getInstance(@NonNull FragmentActivity activity,
                                                             @NonNull Class<T> modelClass,
                                                             Bundle args)
    {
        // get the ViewModel
        T r = new ViewModelProvider(activity).get(modelClass);

        // check if we are already connected to an activity
        if (r.lifecycleObserver != null) {
            // if the connected LifecycleObserver is still there for a different activity, something went wrong
            if (r.lifecycleObserver.activity != activity)
                throw new IllegalStateException("Wrong activity instance");
            return r;
        }

        // check if onCreate() has already been called
        if (!r.created) {
            r.created = true;
            r.args = args;
            r.onCreate();
        }

        // create the new LifecycleObserver and link it to this activity
        r.lifecycleObserver = r.new ActivityLifecycleObserver(activity);
        activity.getLifecycle().addObserver(r.lifecycleObserver);
        return r;
    }

    /** Observer for an Activity */
    class ActivityLifecycleObserver implements LifecycleObserver {

        @NonNull private final FragmentActivity activity;

        ActivityLifecycleObserver(FragmentActivity activity) { this.activity = activity; }

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        public void onCreate() { RetainedInstance.this.onActivityCreate(); }

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void onStart() { RetainedInstance.this.onStart(); }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        public void onResume() { RetainedInstance.this.onResume(); }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        public void onPause() { RetainedInstance.this.onPause(); }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        public void onStop() { RetainedInstance.this.onStop(); }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void onDestroy() {
            RetainedInstance.this.onActivityDestroy();
            lifecycleObserver = null;
        }
    }

    /** If true the onCreate() has been already called */
    boolean created;

    /** Arguments */
    Bundle args;

    /** Current LifecycleObserver for the activity or null if the activity has been destroyed */
    ActivityLifecycleObserver lifecycleObserver;

    protected RetainedInstance(@NonNull Application application) {
        super(application);
    }

    protected final void onCleared() { onDestroy(); }

    /** Return the arguments supplied when the RetainedInstance was instantiated, if any */
    @Nullable
    final public Bundle getArguments() {
        return args;
    }

    /**
     * Return the arguments supplied when the fragment was instantiated.
     *
     * @throws IllegalStateException if no arguments were supplied to the RetainedInstance.
     * @see #getArguments()
     */
    @NonNull
    public final Bundle requireArguments() {
        Bundle arguments = getArguments();
        if (arguments == null) {
            throw new IllegalStateException("RetainedInstance " + this + " does not have any arguments.");
        }
        return arguments;
    }

    /**
     * Return the activity this fragment is currently associated with.
     * May return {@code null} if not currently associated with an activity
     * instead.
     *
     * @see #requireActivity()
     */
    @Nullable
    final public FragmentActivity getActivity() {
        return lifecycleObserver == null ? null : lifecycleObserver.activity;
    }

    /**
     * Return the activity this RetainedInstance is currently associated with.
     * @throws IllegalStateException if not currently associated with an activity
     */
    @NonNull
    public final Activity requireActivity() {
        if (lifecycleObserver == null)
            throw new IllegalStateException("RetainedInstance " + this + " not attached to an activity.");
        return lifecycleObserver.activity;
    }

    /** This RetainedInstance is created */
    @CallSuper
    protected void onCreate() {
        if (Misc.DEBUG) Log.d(TAG, "onCreate() " + this);
    }

    /** An Activity instance is created */
    @CallSuper
    protected void onActivityCreate() {
        if (Misc.DEBUG) Log.d(TAG, "onActivityCreate() " + this);
    }

    /** Start event */
    @CallSuper
    protected void onStart() {
        if (Misc.DEBUG) Log.d(TAG, "onStart() " + this);
    }

    /** Resume event */
    @CallSuper
    protected void onResume() {
        if (Misc.DEBUG) Log.d(TAG, "onResume() " + this);
    }

    /** Pause event */
    @CallSuper
    protected void onPause() {
        if (Misc.DEBUG) Log.d(TAG, "onPause() " + this);
    }

    /** Stop event */
    @CallSuper
    protected void onStop() {
        if (Misc.DEBUG) Log.d(TAG, "onStop() " + this);
    }

    /** An Activity instance is destroyed */
    @CallSuper
    protected void onActivityDestroy() {
        if (Misc.DEBUG) Log.d(TAG, "onActivityDestroy() " + this);
    }

    /** This RetainedInstance is destroyed */
    @CallSuper
    protected void onDestroy() {
        if (Misc.DEBUG) Log.d(TAG, "onDestroy() " + this);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append(getClass().getSimpleName());
        sb.append("{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("}");
        if (lifecycleObserver != null) {
            sb.append(":(");
            sb.append(lifecycleObserver.activity.getClass().getSimpleName());
            sb.append("{");
            sb.append(Integer.toHexString(System.identityHashCode(lifecycleObserver.activity)));
            sb.append("}");
            sb.append(")");
        }
        return sb.toString();
    }
}
