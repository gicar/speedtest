package net.gicar.util;

import android.app.Application;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import net.gicar.util.services.CrashReporting;

/** ViewModel providing async background operations */
public class AsyncViewModel extends AndroidViewModel {

    /** Interfaces for tasks that will be executed */
    public interface Task {
        /**
         * Executes the operations of this task. Cancel operations will always generate interruptions.
         * @throws InterruptedException Could be optionally thrown if the interruption is not handled
         *                              within the task.
         */
        void run() throws InterruptedException;
    }

    /** A scheduled task */
    protected static class ScheduledTask {
        private final Future<Void> future;
        private ScheduledTask(Future<Void> future) {
            this.future = future;
        }
    }

    /** The executor */
    private final ExecutorService executor;

    public AsyncViewModel(@NonNull Application application) {
        this(application, 0);
    }

    public AsyncViewModel(@NonNull Application application, int nThreads) {
        super(application);
        if (nThreads <= 1) executor = Executors.newSingleThreadExecutor();
        else executor = Executors.newFixedThreadPool(nThreads);
    }

    /**
     * Queues the specified task for execution
     * @param task The task to be executed in background
     * @return The scheduled task
     */
    protected ScheduledTask execute(Task task) {
        return execute(task, null);
    }

    /**
     * Queues the specified task for execution
     * @param task The task to be executed in background
     * @param waitFor The scheduled task that we must wait for or null if none
     * @return The scheduled task
     */
    protected ScheduledTask execute(Task task, ScheduledTask waitFor)
    {
        return new ScheduledTask(executor.submit((Callable<Void>) () -> {
            try {
                if (waitFor != null) waitFor.future.get();
                task.run();
            }
            catch (InterruptedException ignored) {}
            catch (Exception e) {
                CrashReporting.logException(e);
            }
            return null;
        }));
    }

    /**
     * Cancels the execution of a scheduled task, if it has not been already executed
     * @param scheduledTask The scheduled task
     */
    protected void cancel(ScheduledTask scheduledTask) {
        scheduledTask.future.cancel(true);
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        // queue the stop request, pending tasks will be executed
        executor.shutdown();
    }
}
