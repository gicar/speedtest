package net.gicar.util.controls;

import android.view.MotionEvent;
import android.view.View;

import static android.view.MotionEvent.ACTION_DOWN;

/** Base abstract class for controls */
public abstract class BaseControl implements View.OnTouchListener {

    private final boolean requestDisallowInterceptTouchEvent;

    protected BaseControl(View v, boolean requestDisallowInterceptTouchEvent) {
        this.requestDisallowInterceptTouchEvent = requestDisallowInterceptTouchEvent;
        v.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        // on action down ask the parent to not steal touch events for this touch
        if (event.getAction() == ACTION_DOWN)
            v.getParent().requestDisallowInterceptTouchEvent(requestDisallowInterceptTouchEvent);

        // pass the event to the original view
        return v.onTouchEvent(event);
    }
}
