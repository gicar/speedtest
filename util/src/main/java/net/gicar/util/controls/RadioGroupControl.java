package net.gicar.util.controls;

import android.app.Activity;
import android.view.View;
import android.widget.RadioGroup;

/** RadioGroup control */
public class RadioGroupControl {

    /** Switch view */
    final private RadioGroup radioGroupView;

    /** The handler */
    final private RadioGroupControlHandler handler;

    /** The change listener which reports to the user */
    final private RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            handler.newValue(RadioGroupControl.this, checkedId);
        }
    };

    /**
     * Creates a RadioGroupControl
     * @param rootView The root view
     * @param groupId The id of the group
     * @param handler The handler for the events
     */
    public RadioGroupControl(View rootView, int groupId, RadioGroupControlHandler handler) {
        this(rootView.findViewById(groupId), handler);
    }

    /**
     * Creates a RadioGroupControl
     * @param activity The Activity
     * @param groupId The id of the group
     * @param handler The handler for the events
     */
    public RadioGroupControl(Activity activity, int groupId, RadioGroupControlHandler handler) {
        this(activity.findViewById(groupId), handler);
    }

    /**
     * Creates a RadioGroupControl
     * @param groupView The view of the group
     * @param handler The handler for the events
     */
    public RadioGroupControl(RadioGroup groupView, RadioGroupControlHandler handler)
    {
        // find the views
        this.radioGroupView = groupView;

        // save relevant parameters
        this.handler = handler;

        // set the listeners
        radioGroupView.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    /**
     * Enables or disables the control
     * @param enabled If true the control is enabled
     */
    public void setEnabled(boolean enabled)
    {
        radioGroupView.setEnabled(enabled);
        for (int i = 0; i < radioGroupView.getChildCount(); i++) {
            radioGroupView.getChildAt(i).setEnabled(enabled);
        }
    }

    /**
     * Sets the checked RadioButton
     * @param checkedId The new checked RadioButton id
     */
    public void check(int checkedId)
    {
        radioGroupView.setOnCheckedChangeListener(null);
        radioGroupView.check(checkedId);
        radioGroupView.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    /** Handles events from the RadioGroupControl */
    public static abstract class RadioGroupControlHandler {
        /**
         * New control value
         * @param radioGroupControl the SwitchControl
         * @param checkedId The new checked RadioButton id
         */
        public abstract void newValue(RadioGroupControl radioGroupControl, int checkedId);
    }
}
