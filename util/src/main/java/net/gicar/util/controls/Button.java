package net.gicar.util.controls;

import android.widget.ImageView;

/** Button with enabled and disabled state */
public class Button extends BaseButton {

    /** The ImageButton */
    private final ImageButton imageButton;

    /** The ImageButton configurations */
    private final ImageButton.ButtonConfig enabledConfig, disabledConfig, highlightedConfig;

    /** The click listener */
    private OnClickListener listener;

    /** The current status */
    private boolean enabled, highlighted;

    /**
     * Creates a new button instance
     * @param buttonView The button view
     * @param drawableId The id of the drawable
     * @param enabledColorId The color to be used when the button is enabled
     * @param disabledColorId The color to be used when the button is disabled
     * @param highlightedColorId The color to be used when the button is highlighted
     * @param tooltipStringId The string to be used as tooltip on the button
     */
    public Button(ImageView buttonView, int drawableId,
                  int enabledColorId, int disabledColorId, int highlightedColorId,
                  int tooltipStringId)
    {
        // create the ImageButton
        imageButton = new ImageButton(buttonView, (button, config) -> {
            if (this.listener != null) this.listener.onClick(this);
        });

        // create the configurations
        enabledConfig = new ImageButton.ButtonConfig(true, drawableId, enabledColorId, tooltipStringId);
        disabledConfig = new ImageButton.ButtonConfig(false, drawableId, disabledColorId, 0);
        highlightedConfig = new ImageButton.ButtonConfig(true, drawableId, highlightedColorId, tooltipStringId);

        // set it enabled
        setEnabled(true);
    }

    @Override
    public Button setListener(OnClickListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        if (this.enabled == enabled) return;
        this.enabled = enabled;
        imageButton.setConfig(enabled ? (highlighted ? highlightedConfig : enabledConfig) : disabledConfig);
    }

    @Override
    public void setHighlighted(boolean highlighted)
    {
        if (this.highlighted == highlighted) return;
        this.highlighted = highlighted;
        imageButton.setConfig(enabled ? (highlighted ? highlightedConfig : enabledConfig) : disabledConfig);
    }

    /** Getter for the underlying ImageButton */
    public ImageButton getImageButton() {
        return imageButton;
    }
}
