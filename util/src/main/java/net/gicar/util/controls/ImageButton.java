package net.gicar.util.controls;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.util.HashMap;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.TooltipCompat;
import net.gicar.util.appcompat.CompatUtils;

/** Button with an image */
public class ImageButton {

    /** Configuration for a button */
    public static class ButtonConfig {

        /** If true the button is enabled in this config */
        private final boolean enabled;

        /** The id of the drawable */
        private final int drawableId;

        /** The color to be used for tinting the button */
        private final int colorId;

        /** The string to be used as tooltip on the button */
        private final int tooltipStringId;

        /**
         * Creates a new button configuration
         * @param enabled If true the button is enabled in this config
         * @param drawableId The id of the drawable
         * @param colorId The color to be used for tinting the button, 0 if none
         * @param tooltipStringId The string to be used as tooltip on the button, 0 if none
         */
        public ButtonConfig(boolean enabled, int drawableId, int colorId, int tooltipStringId)
        {
            this.enabled = enabled;
            this.drawableId = drawableId;
            this.colorId = colorId;
            this.tooltipStringId = tooltipStringId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ButtonConfig that = (ButtonConfig) o;

            if (enabled != that.enabled) return false;
            if (drawableId != that.drawableId) return false;
            if (colorId != that.colorId) return false;
            return tooltipStringId == that.tooltipStringId;
        }

        @Override
        public int hashCode() {
            int result = (enabled ? 1 : 0);
            result = 31 * result + drawableId;
            result = 31 * result + colorId;
            result = 31 * result + tooltipStringId;
            return result;
        }
    }

    /** Interface definition for a callback to be invoked when a button is clicked. */
    public interface OnClickListener {
        /**
         * Called when a button has been clicked.
         * @param button The button that was clicked.
         * @param config The config that was clicked.
         */
        void onClick(ImageButton button, ButtonConfig config);
    }

    /** Button view */
    private final ImageView buttonView;

    /** Current configuration */
    private ButtonConfig config;

    /** Cached drawables */
    private final HashMap<ButtonConfig, Drawable> imageMap = new HashMap<>();

    /**
     * Creates a new button instance
     * @param buttonView The button view
     */
    public ImageButton(ImageView buttonView)
    {
        this(buttonView, null);
    }

    /**
     * Creates a new button instance
     * @param buttonView The button view
     * @param listener The listener which will be called on click
     */
    public ImageButton(ImageView buttonView, OnClickListener listener)
    {
        this.buttonView = buttonView;
        if (listener != null) setListener(listener);
    }

    /**
     * Replaces the listener for clicks
     * @param listener The new listener
     * @return self
     */
    public ImageButton setListener(OnClickListener listener) {
        buttonView.setOnClickListener(v -> {
            if (listener != null) listener.onClick(ImageButton.this, config);
        });
        return this;
    }

    /**
     * Sets the current config for this button
     * @param config The config
     * @return self
     */
    public ImageButton setConfig(ButtonConfig config)
    {
        this.config = config;

        // get the image caching it if needed
        Drawable image = imageMap.get(config);
        if (image == null) {
            image = config.colorId == 0 ? AppCompatResources.getDrawable(buttonView.getContext(), config.drawableId) :
                    CompatUtils.tintDrawable(buttonView.getContext(), config.drawableId, config.colorId);
            imageMap.put(config, image);
        }
        buttonView.setImageDrawable(image);

        // set the tooltip text if needed
        TooltipCompat.setTooltipText(buttonView,
                config.tooltipStringId != 0 ? buttonView.getResources().getText(config.tooltipStringId) : null);

        // set the enabled state
        buttonView.setEnabled(config.enabled);

        return this;
    }

    /** Getter fot the underlying ImageView */
    public ImageView getImageView() {
        return buttonView;
    }
}
