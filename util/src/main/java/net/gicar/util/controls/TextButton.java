package net.gicar.util.controls;

import com.google.android.material.button.MaterialButton;

public class TextButton extends BaseButton {

    /** The actual button */
    MaterialButton button;

    /** The click listener */
    private OnClickListener listener;

    public TextButton(MaterialButton button, String textForWidth) {
        this.button = button;
        button.setOnClickListener(v -> {
            if (listener != null) listener.onClick(TextButton.this);
        });

        // set the width of the text
        if (textForWidth != null) {
            float w = button.getPaint().measureText(textForWidth) +
                    button.getPaddingLeft() + button.getPaddingRight();
            button.setWidth((int) (w + 2));
        }
    }

    @Override
    public TextButton setListener(OnClickListener listener) {
        this.listener = listener;
        return this;
    }

    @Override
    public void setEnabled(boolean enabled) {
        button.setEnabled(enabled);
    }

    @Override
    public void setHighlighted(boolean highlighted) {
        button.setChecked(highlighted);
    }
}
