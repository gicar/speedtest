package net.gicar.util.controls;

import android.app.Activity;
import android.view.View;
import android.widget.CompoundButton;

/** Switch control */
public class SwitchControl extends BaseControl {

    /** Switch view */
    final private CompoundButton switchView;

    /** The handler */
    final private SwitchControlHandler handler;

    /** The change listener which reports to the user */
    final private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            handler.newValue(SwitchControl.this, b);
        }
    };

    /**
     * Creates a SwitchControl
     * @param rootView The root view
     * @param switchId The id of the switch
     * @param requestDisallowInterceptTouchEvent If true parent and its ancestors are not allowed to
     *                                           intercept touch events
     * @param handler The handler for the events
     */
    public SwitchControl(View rootView, int switchId, boolean requestDisallowInterceptTouchEvent,
                         SwitchControlHandler handler) {
        this(rootView.findViewById(switchId), requestDisallowInterceptTouchEvent, handler);
    }

    /**
     * Creates a SwitchControl
     * @param activity The Activity
     * @param switchId The id of the switch
     * @param requestDisallowInterceptTouchEvent If true parent and its ancestors are not allowed to
     *                                           intercept touch events
     * @param handler The handler for the events
     */
    public SwitchControl(Activity activity, int switchId,  boolean requestDisallowInterceptTouchEvent,
                         SwitchControlHandler handler) {
        this(activity.findViewById(switchId), requestDisallowInterceptTouchEvent, handler);
    }

    /**
     * Creates a SwitchControl
     * @param switchView The view of the switch
     * @param requestDisallowInterceptTouchEvent If true parent and its ancestors are not allowed to
     *                                           intercept touch events
     * @param handler The handler for the events
     */
    public SwitchControl(CompoundButton switchView,  boolean requestDisallowInterceptTouchEvent,
                         SwitchControlHandler handler)
    {
        super(switchView, requestDisallowInterceptTouchEvent);

        // set the views
        this.switchView = switchView;

        // save relevant parameters
        this.handler = handler;

        // set the listeners
        switchView.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    /**
     * Enables or disables the control
     * @param enabled If true the control is enabled
     */
    public void setEnabled(boolean enabled)
    {
        switchView.setEnabled(enabled);
    }

    /**
     * Sets the checked value
     * @param v The new checked value
     */
    public void setChecked(boolean v)
    {
        switchView.setOnCheckedChangeListener(null);
        switchView.setChecked(v);
        switchView.jumpDrawablesToCurrentState();
        switchView.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    /**
     * Getter method for the switch setting
     * @return If true the switch is checked
     */
    public boolean isChecked()
    {
        return switchView.isChecked();
    }

    /**
     * Sets the text of te control formatting with arguments
     * @param formatResId The string format resource id
     * @param args The arguments
     */
    public void setText(int formatResId, String... args)
    {
        switchView.setText(String.format(switchView.getContext().getString(formatResId), (Object[]) args));
    }

    /**
     * Sets the text of the control form the resource
     * @param resId The string resource id
     */
    public void setText(int resId)
    {
        switchView.setText(resId);
    }


    /** Handles events from the SwitchControl */
    public interface SwitchControlHandler {
        /**
         * New control value
         * @param switchControl the SwitchControl
         * @param v The new value
         */
        void newValue(SwitchControl switchControl, boolean v);
    }
}
