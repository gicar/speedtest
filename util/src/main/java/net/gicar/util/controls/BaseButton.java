package net.gicar.util.controls;

/** Base abstract class for buttons */
public abstract class BaseButton {

    /** Interface definition for a callback to be invoked when a button is clicked. */
    public interface OnClickListener {
        /**
         * Called when a button has been clicked.
         * @param button The button that was clicked.
         */
        void onClick(BaseButton button);
    }

    /**
     * Sets the listener
     * @param listener The new listener
     * @return self
     */
    public abstract BaseButton setListener(OnClickListener listener);

    /**
     * Sets the button in enabled or disabled state
     * @param enabled If true the button is enabled, if false is disabled
     */
    public abstract void setEnabled(boolean enabled);

    /**
     * Sets the button in highlighted or normal state. Note that highlighting is applied only if enabled.
     * @param highlighted If true the button is highlighted, if false is normal
     */
    public abstract void setHighlighted(boolean highlighted);

}
