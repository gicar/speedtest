package net.gicar.util.controls;

import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

/** Seek bar control */
public class SeekControl extends BaseControl {

    /** Control button */
    final private BaseButton button;

    /** Text view */
    final private TextView textView;

    /** Seek bar */
    final private SeekBar seekBar;

    /** The min/offset value */
    final private int min;

    /** The increment value */
    final private int increment;

    /** The reset value */
    private int resetValue;

    /** The handler */
    final private SeekControlHandler handler;

    /** The text formatter */
    final private TextFormatter textFormatter;

    /**
     * Creates a SeekControl
     * @param imageView The optional image button view. Set to null if not present
     * @param textView The view of the text value
     * @param seekBar The view of the seekbar
     * @param enabledImageId The id of the drawable for the image button
     * @param disabledImageColorId The color to be used when the button is disabled
     * @param highlightedColorId The color to be used on the button when the value is different from default one
     * @param imageTooltipStringId The string to be used as tooltip on the button
     * @param min The min value
     * @param max The max value
     * @param increment The increment
     * @param resetValue The reset value
     * @param textForWidth The text which will be used to calculate its max width
     * @param textFormat The format which will be used to set the text from the value, or null if none
     * @param requestDisallowInterceptTouchEvent If true parent and its ancestors are not allowed to
     *                                           intercept touch events
     * @param handler The handler for the events
     */
    public SeekControl(ImageView imageView, TextView textView, SeekBar seekBar,
                       int enabledImageId, int disabledImageColorId, int highlightedColorId,
                       int imageTooltipStringId,
                       int min, int max, int increment, int resetValue,
                       String textForWidth, String textFormat,
                       boolean requestDisallowInterceptTouchEvent,
                       final SeekControlHandler handler) {
        this(
                imageView == null ? null : new Button(imageView, enabledImageId,
                        0, disabledImageColorId, highlightedColorId,
                        imageTooltipStringId),
                textView, seekBar,
                min, max, increment, resetValue,
                textForWidth, new StringFormatter(textFormat),
                requestDisallowInterceptTouchEvent, handler);
    }

    /**
     * Creates a SeekControl
     * @param button The optional button view. Set to null if not present
     * @param textView The view of the text value
     * @param seekBar The view of the seekbar
     * @param min The min value
     * @param max The max value
     * @param increment The increment
     * @param resetValue The reset value
     * @param textForWidth The text which will be used to calculate its max width
     * @param textFormatter The formatter which will be used to set the text from the value, or null if none
     * @param requestDisallowInterceptTouchEvent If true parent and its ancestors are not allowed to
     *                                           intercept touch events
     * @param handler The handler for the events
     */
    public SeekControl(BaseButton button, TextView textView, SeekBar seekBar,
                       int min, int max, int increment, int resetValue,
                       String textForWidth, TextFormatter textFormatter,
                       boolean requestDisallowInterceptTouchEvent,
                       final SeekControlHandler handler)
    {
        super(seekBar, requestDisallowInterceptTouchEvent);

        // set the views
        this.button = button;
        this.textView = textView;
        this.seekBar = seekBar;

        // save relevant parameters
        this.min = min;
        this.increment = increment;
        this.resetValue = resetValue;
        this.handler = handler;
        this.textFormatter = textFormatter;

        // configure the SeekBar
        seekBar.setMax((max - min) / increment);
        seekBar.setProgress((resetValue - min) / increment);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
               private boolean tracking = false;
               @Override
               public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                   if (fromUser) newValue(SeekControl.this, min + progress * increment, true, tracking);
               }
               @Override
               public void onStartTrackingTouch(SeekBar seekBar) {
                   tracking = true;
               }
               @Override
               public void onStopTrackingTouch(SeekBar seekBar) {
                   tracking = false;
                   handler.onUserValueCommit(SeekControl.this, min + seekBar.getProgress() * increment);
               }
           }
        );

        // configure the button
        if (button != null) button.setListener(v -> {
            seekBar.setProgress((SeekControl.this.resetValue - min) / increment);
            newValue(SeekControl.this, SeekControl.this.resetValue, true, false);
            handler.onUserValueCommit(SeekControl.this, SeekControl.this.resetValue);
        });

        // set the width of the text
        float w = textView.getPaint().measureText(textForWidth) +
                textView.getPaddingLeft() + textView.getPaddingRight();
        textView.setWidth((int) (w + 2));

        // start enabled
        setEnabled(true);

        // initialize the value
        newValue(this, resetValue, false, false);
    }

    /**
     * Enables or disables the control
     * @param enabled If true the control is enabled
     */
    public void setEnabled(boolean enabled)
    {
        if (button != null) button.setEnabled(enabled);
        seekBar.setEnabled(enabled);
    }

    /**
     * Sets the value
     * @param v The new value
     */
    public void setValue(int v)
    {
        seekBar.setProgress((v - min) / increment);
        newValue(this, v, false, false);
    }

    /**
     * Gets the current value
     * @return the current value
     */
    public int getValue()
    {
        return min + seekBar.getProgress() * increment;
    }

    /**
     * Sets the reset value
     * @param resetValue The new reset value
     */
    public void setResetValue(int resetValue)
    {
        this.resetValue = resetValue;
        if (button != null) button.setHighlighted(getValue() != resetValue);
    }

    public SeekBar getSeekBar() {
        return seekBar;
    }

    public TextView getTextView() {
        return textView;
    }

    private void newValue(SeekControl seekControl, int v, boolean fromUser, boolean isTracking)
    {
        if (textFormatter != null) textView.setText(textFormatter.format(v));
        if (button != null) button.setHighlighted(v != resetValue);
        if (fromUser) handler.onUserValueChange(seekControl, v, isTracking);
    }

    /** Handles events from the SeekControl */
    public interface SeekControlHandler {
        /**
         * There is a value change in the control triggered by an user action.
         * It could either be a direct change by tap, or a change due to tracking.
         * @param seekControl The SeekControl the event refers to
         * @param v The new value
         * @param isTracking Signals if there is a tracking ongoing
         */
        void onUserValueChange(SeekControl seekControl, int v, boolean isTracking);

        /**
         * There is a final value change in the control triggered by an user action.
         * @param seekControl The SeekControl the event refers to
         * @param v The new value
         */
        void onUserValueCommit(SeekControl seekControl, int v);
    }

    /** Provides formatting for the value text */
    public interface TextFormatter {
        String format(int v);
    }

    /** Simple text formatter using the String.format method */
    public static class StringFormatter implements TextFormatter {

        final String textFormat;

        public StringFormatter(String textFormat) {
            this.textFormat = textFormat;
        }

        @Override
        public String format(int v) {
            return String.format(textFormat, v);
        }
    }
}

