package net.gicar.util.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/** Parser class for binary data */
public class HttpParserBinary implements HttpParser {

    private final boolean preferGzip;

	private final int maxLength;

    public HttpParserBinary(boolean preferGzip, int maxLength)
    {
		this.preferGzip = preferGzip;
		this.maxLength = maxLength;
    }

	public HttpParserBinary(boolean preferGzip)
	{
		this(preferGzip, Integer.MAX_VALUE);
	}

	public HttpParserBinary()
    {
        this(false, Integer.MAX_VALUE);
    }

    public Object parse(HttpAnswerHeader header, InputStream stream) throws IOException
    {
        if (stream == null) return null;

        if (header.getContentLength() > maxLength)
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");

        int i = (int)header.getContentLength();
        if (i < 0) i = 4096;

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(i);
        byte[] tmp = new byte[4096];
        int l;
        while((l = stream.read(tmp)) != -1)
            buffer.write(tmp, 0, l);

        return buffer.toByteArray();
    }

    public boolean prefersGzip() {
        return preferGzip;
    }
}
