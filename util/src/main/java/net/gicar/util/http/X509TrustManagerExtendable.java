package net.gicar.util.http;

import android.net.http.X509TrustManagerExtensions;
import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.services.CrashReporting;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/** An X.509 trust manager which can be extended */
class X509TrustManagerExtendable implements X509TrustManager {

    /** The log tag */
    private final static String TAG = "X509TrustManager";

    /** The certificate checker */
    private X509ServerCertificateChecker certificateChecker;

    /** The default trust manager */
    private X509TrustManager defaultTrustMgr;

    /** The default trust manager extensions */
    private X509TrustManagerExtensions defaultTrustMgrExtension;

    /** The host that will be used for next requests */
    private final ThreadLocal<String> host = new ThreadLocal<>();

    X509TrustManagerExtendable()
    {
        TrustManagerFactory tmf;
        try {
            // Using null here initialises the TMF with the default trust store.
            tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init((KeyStore) null);
        } catch (NoSuchAlgorithmException | KeyStoreException e) {
            CrashReporting.logException(e);
            return;
        }

        // Get hold of the default trust manager
        TrustManager[] trustManagers = tmf.getTrustManagers();
        if (trustManagers != null) for (TrustManager tm : trustManagers) {
            if (tm instanceof X509TrustManager) {
                defaultTrustMgr = (X509TrustManager) tm;
                defaultTrustMgrExtension = new X509TrustManagerExtensions(defaultTrustMgr);
                break;
            }
        }
        if (defaultTrustMgr == null) if (Misc.DEBUG) Log.e(TAG, "Unable to get X509TrustManager");
    }

    /**
     * Sets the certificate checker that will be used to extends base on
     * @param certificateChecker The user's certificate checker
     */
    void setCertificateChecker(X509ServerCertificateChecker certificateChecker) {
        if (Misc.DEBUG) Log.v(TAG, "setCertificateChecker() called with: certificateChecker = [" + certificateChecker + "]");
        this.certificateChecker = certificateChecker;
    }

    /**
     * Sets the host that will be used for next requests
     * @param host The host
     */
    void setHost(String host) {
        if (Misc.DEBUG) Log.v(TAG, "setHost() called with: host = [" + host + "]");
        this.host.set(host);
    }


    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        if (Misc.DEBUG) Log.v(TAG, "checkClientTrusted() called with: chain = [" + Arrays.toString(chain) + "], authType = [" + authType + "]");
        if (defaultTrustMgr == null) throw new CertificateException("Unable to get default trust manager");
        defaultTrustMgr.checkClientTrusted(chain, authType);
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
    {
        if (Misc.DEBUG) Log.v(TAG, "checkServerTrusted() called with: chain = [" + Arrays.toString(chain) + "], authType = [" + authType + "]");
        try {
            if (defaultTrustMgr == null) throw new CertificateException("Unable to get default trust manager");
            String h = host.get();
            if (h == null) defaultTrustMgr.checkServerTrusted(chain, authType);
            else defaultTrustMgrExtension.checkServerTrusted(chain, authType, h);
        }
        catch (CertificateException e) {
            if (certificateChecker == null || !certificateChecker.isServerTrusted(chain, authType))
                throw e;
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        if (Misc.DEBUG) Log.v(TAG, "getAcceptedIssuers() called");
        return defaultTrustMgr.getAcceptedIssuers();
    }
}
