package net.gicar.util.http;

/**
 * Generic HTTP Get answer
 */
public class HttpAnswer {
	
	/** The answer header */
	final private HttpAnswerHeader header;
	
	/** Data contents */
	final private Object data;

	public HttpAnswer(HttpAnswerHeader header, Object data) {
		this.header = header;
		this.data = data;
	}

	public HttpAnswerHeader getHeader() {
		return header;
	}

	public Object getData() {
		return data;
	}

	public String toString()
	{
		return header + ", " + data;
	}
}
