package net.gicar.util.http;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** This class holds information from the Content-Disposition header */
public class HttpContentDisposition {

    /** Pattern matching filename */
    public final static Pattern FILENAME_PATTERN1 = Pattern.compile("(?i)^.*filename=\"([^\"]+)\".*$"),
            FILENAME_PATTERN2 = Pattern.compile("(?i)^.*filename=(.+)$");

    /** The specified file name */
    private final String fileName;

    public HttpContentDisposition(String contentDisposition)
    {
        // WARNING: this code is far from being perfect, but could do the job in most cases
        // Don't use it if conformance is needed

        String filename = null;

        try {
            // abort if no parameter
            if (contentDisposition == null) return;

            // find the file name int the header field
            Matcher m = FILENAME_PATTERN1.matcher(contentDisposition);
            if (m.find()) filename = new File(m.group(1)).getName();
            else {
                m = FILENAME_PATTERN2.matcher(contentDisposition);
                if (m.find()) filename = new File(m.group(1)).getName();
            }
        }
        finally {
            this.fileName = filename;
        }
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public String toString() {
        return "HttpContentDisposition{" +
                "fileName='" + fileName + '\'' +
                '}';
    }
}
