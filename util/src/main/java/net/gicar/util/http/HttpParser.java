package net.gicar.util.http;

import java.io.IOException;
import java.io.InputStream;

public interface HttpParser {

	/**
	 * Used to specify if the contents of the request shall be eventually compressed.
	 * @return <code>true</code> if the contents are compressible
	 */
	boolean prefersGzip();
	
	/**
	 * Parse the response content. The stream is already uncompressed.
	 * @param header The header of the answer
	 * @param stream The input stream to be parsed
	 * @return The parsed data or <code>null</code> if none
	 */
	Object parse(HttpAnswerHeader header, InputStream stream) throws IOException;

    /**
     * Specifies the size of the buffering
     * @return The size of the buffering for buffered stream, or 0 to not use it and -1 to use default size
     */
    default int streamBufferSize() {
        return -1;
    }
}
