package net.gicar.util.http;

import java.io.IOException;
import java.io.InputStream;

/**
 * Utility class to perform connections to an HTTP server.
 * There shall be a single background thread assigned to each HttpFetcher object.
 */
public abstract class HttpFetcher {

    /**
     * Retrieve the specified URI using default parsers.
     * @param uri The URI to be retrieved
     * @param preferGzip Set to <code>true</code> if the stream could be zipped
     * @param requestHeader Additional request headers or <code>null</code> if not present
     * @return The object containing the retrieved data. It is a <code>String</code> in case of MIME text type or a
     *         <code>byte[]</code> in all the other cases. Returns <code>null</code> if the request has been aborted.
     */
    public HttpAnswer getUri(String uri, final boolean preferGzip, HttpRequestHeader requestHeader)
            throws IOException
    {
        return getUri(uri, new HttpParser() {
            public Object parse(HttpAnswerHeader header, InputStream stream) throws IOException
            {
                String mimeType = header.getMimeType();
                if (mimeType.startsWith("text/") ||
                        mimeType.equals("application/javascript") ||
                        mimeType.equals("application/json") ||
                        mimeType.equals("application/xml"))
                    return new HttpParserString().parse(header, stream);
                return new HttpParserBinary().parse(header, stream);
            }

            public boolean prefersGzip() {
                return preferGzip;
            }
        }, requestHeader);
    }

    /**
     * Retrieve the specified URI using an user specified parser. Do not call this from UI thread.
     * GET or HEAD will be used to fetch it depending if the content is requested or not.
     * @param uri The URI to be retrieved
     * @param parser The parse that will be used for parsing the answer, pass <code>null</code> to fetch only the header
     * @param requestHeader Additional request headers or <code>null</code> if not present
     * @return The object containing the retrieved data. Returns <code>null</code> if the request has been aborted.
     */
    public abstract HttpAnswer getUri(String uri, HttpParser parser, HttpRequestHeader requestHeader)
                    throws IOException;

    /**
     * Performs an URI request. Do not call this from UI thread.
     * POST will be used if a producer is specified, else
     * GET or HEAD will be used to fetch it depending if the content is requested or not.
     * @param uri The URI to be retrieved
     * @param producer The producer that will be used for sending the data, pass <code>null</code> to perform a GET instead
     * @param parser The parse that will be used for parsing the answer, pass <code>null</code> to fetch only the header
     * @param requestHeader Additional request headers or <code>null</code> if not present
     * @return The object containing the retrieved data. Returns <code>null</code> if the request has been aborted.
     */
    public abstract HttpAnswer requestUri(String uri, HttpProducer producer, HttpParser parser, HttpRequestHeader requestHeader)
            throws IOException;

    /**
     * Abort the get request currently in execution, if any.
     * Could be called from another/UI thread.
     */
    public abstract void abort();

    /**
     * Releases all the used resources. After this method call the fetcher cannot be used any longer.
     * Could be called from another/UI thread.
     */
    public abstract void shutdown();
}
