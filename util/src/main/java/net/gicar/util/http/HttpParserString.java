package net.gicar.util.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/** Parser class for text data */
public class HttpParserString implements HttpParser {

    public Object parse(HttpAnswerHeader header, InputStream stream) throws IOException
    {
        if (stream == null) return null;

        if (header.getContentLength() > Integer.MAX_VALUE)
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");

        int i = (int)header.getContentLength();
        if (i < 0) i = 4096;

        Reader reader = new InputStreamReader(stream, header.getCharSet());
        StringBuffer buffer = new StringBuffer(i);

        char[] tmp = new char[1024];
        int l;
        while((l = reader.read(tmp)) != -1)
            buffer.append(tmp, 0, l);

        return buffer.toString();
    }

    public boolean prefersGzip() {
        return true;
    }
}

