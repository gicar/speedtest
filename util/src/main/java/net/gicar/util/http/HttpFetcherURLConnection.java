package net.gicar.util.http;

import android.util.Log;

import net.gicar.util.Misc;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.TrustManager;

/**
 * Utility class to perform connections to an HTTP server.
 * There shall be a single background thread assigned to each HttpFetcherURLConnection object.
 */
public class HttpFetcherURLConnection extends HttpFetcher {

	/** The log tag */
	private final static String TAG = "URLConnection";

    /** HTTP default content charset */
    public static final String DEFAULT_CONTENT_CHARSET = "ISO-8859-1";

    /** Used to generate unique indexes for fetchers */
	private final static AtomicInteger fetcherIndex = new AtomicInteger(1);

	/** Used to generate unique indexes for requests */
	private final static AtomicInteger requestIndex = new AtomicInteger(1);

    /** The executor which will be used for scheduling requests */
    private final static ExecutorService executor = Executors.newCachedThreadPool();

	/** Get an unique index for this fetcher */
	private final int httpFetcherIndex = fetcherIndex.getAndIncrement();

	/** Lock used to allow only one request at a time for fetcher instance */
	private final Object requestLock = new Object();

	/** If true the socket will disconnected after each request */
	private final boolean disconnect;

    /** SSL socket factory */
    private final SSLSocketFactoryCompat socketFactory;

    /** The certificate trust manager */
    private final X509TrustManagerExtendable x509TrustManager;

    /** The future holding the request currently in progress */
    private Future<HttpAnswer> future;

    /** If true we are in shutdown and new requests shall be declined */
    private boolean shutdown = false;

    /**
     * Create a URL fetcher which does not force disconnections after each request
     */
    public HttpFetcherURLConnection()
    {
        this(false);
    }

    /**
     * Create a URL fetcher with the specified disconnect behaviour
     * @param disconnect If true the socket will always closed after each request
     */
	public HttpFetcherURLConnection(boolean disconnect)
	{
	    this(disconnect, null);
	}

    /**
     * Create a URL fetcher with the specified disconnect behaviour
     * @param disconnect If true the socket will always closed after each request
     * @param socketCreationObserver Optional observer for created sockets
     */
    public HttpFetcherURLConnection(boolean disconnect, SocketCreationObserver socketCreationObserver)
    {
        this.disconnect = disconnect;
        x509TrustManager = new X509TrustManagerExtendable();
        socketFactory = new SSLSocketFactoryCompat(null, new TrustManager[] { x509TrustManager }, null, socketCreationObserver);
        if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + ">* HTTP fetcher created, disconnect=" + disconnect);
    }

    /**
     * Sets the fallback certificate checker in case authentication fails
     * @param certificateChecker The certificate checker code
     */
	public void setCertificateChecker(X509ServerCertificateChecker certificateChecker) {
        x509TrustManager.setCertificateChecker(certificateChecker);
    }

    @Override
    public HttpAnswer getUri(String uri, HttpParser parser,
                             HttpRequestHeader requestHeader)
            throws IOException
    {
        return requestUri(uri, null, parser, requestHeader);
    }

    @Override
    public HttpAnswer requestUri(String uri, HttpProducer producer, HttpParser parser,
                             HttpRequestHeader requestHeader)
			throws IOException
	{
		// only one request at a time is allowed,
		// this lock is needed to avoid a new request tries to begin before a previous one is aborted
		synchronized (requestLock) {

            // get an unique index for this request
            final int index = requestIndex.getAndIncrement();

            // create the request object instance
            Callable<HttpAnswer> callable = () -> {

                if (Misc.DEBUG)
                    Log.d(TAG, httpFetcherIndex + ">" + index + " Requesting URL: " + uri + ", " + requestHeader);

                // the relevant information which shall be cleaned up
                HttpURLConnection urlConnection = null;
                InputStream instream = null;

                try {
                    // convert the URI to an URL
                    URL url = new URL(uri);

                    // create the request
                    urlConnection = (HttpURLConnection) url.openConnection();

                    // if we have a HTTPS request, connect the SSLSocketFactoryCompat to it
                    if (urlConnection instanceof HttpsURLConnection) {
                        ((HttpsURLConnection) urlConnection).setSSLSocketFactory(socketFactory);
                        x509TrustManager.setHost(url.getHost());
                    }

                    // use POST if we will send data and if no parser has been specified just request the head
                    if (producer != null) {
                        urlConnection.setRequestMethod("POST");
                        urlConnection.setDoOutput(true);
                        long len = producer.getDataLength();
                        if (len == -1) urlConnection.setChunkedStreamingMode(0);
                        else urlConnection.setFixedLengthStreamingMode(len);
                        String contentType = producer.getContentType();
                        if (contentType != null) urlConnection.setRequestProperty("Content-Type", contentType);
                    } else if (parser == null) urlConnection.setRequestMethod("HEAD");

                    // disable gzip if it is better to
                    if ((producer != null && !producer.prefersGzip()) ||
                            (parser != null && !parser.prefersGzip()))
                        urlConnection.setRequestProperty("Accept-Encoding", "identity");

                    // set other request header parameters
                    if (requestHeader != null) {

                        // set user agent
                        String userAgent = requestHeader.getUserAgent();
                        if (userAgent != null)
                            urlConnection.setRequestProperty("User-Agent", userAgent);

                        // set cache controlling headers
                        HttpCacheControlInfo cacheControlInfo = requestHeader.getCacheControlInfo();
                        if (cacheControlInfo != null) {
                            String cacheControl = cacheControlInfo.getCacheControl();
                            if (cacheControl != null)
                                urlConnection.setRequestProperty("Cache-Control", cacheControl);
                            Date lastModified = cacheControlInfo.getLastModified();
                            if (lastModified != null)
                                urlConnection.setRequestProperty("If-Modified-Since",
                                        DateUtils.formatDate(lastModified));
                            String etag = cacheControlInfo.getEtag();
                            if (etag != null)
                                urlConnection.setRequestProperty("If-None-Match", etag);
                        }
                    }

                    // explicitly connect, avoid implicit connect on stream open for clearness
                    urlConnection.connect();

                    // send the data if needed
                    if (producer != null) {
                        try (OutputStream outstream = producer.streamBufferSize() != 0 ?
                                new BufferedOutputStream(urlConnection.getOutputStream(), producer.streamBufferSize()) :
                                urlConnection.getOutputStream()) {
                            producer.produce(outstream);
                        }
                    }

                    // requesting the streams will perform the connection and get at least
                    // the header part; this could fail if the server answers with an error code
                    try {
                        instream = urlConnection.getInputStream();
                        if (parser == null) instream = new BufferedInputStream(instream);
                        else {
                            int bufferSize = parser.streamBufferSize();
                            if (bufferSize != 0) {
                                if (bufferSize == -1) instream = new BufferedInputStream(instream);
                                else instream = new BufferedInputStream(instream, bufferSize);
                            }
                        }
                    } catch (IOException e) {
                        // error sent in reply to request, request the error stream
                        Log.d(TAG, httpFetcherIndex + "=" + index + " Switching input to error stream due to error", e);
                        InputStream errorStream = urlConnection.getErrorStream();
                        instream = errorStream != null ? new BufferedInputStream(errorStream) : null;
                    }

                    // retrieve the actual URL after redirections if any
                    String currentUrl = urlConnection.getURL().toExternalForm();

                    // get content type (MIME type and charset)
                    String mimeType = null;
                    String charSet = DEFAULT_CONTENT_CHARSET;
                    if (requestHeader != null && requestHeader.getDefaultCharset() != null)
                        charSet = requestHeader.getDefaultCharset();
                    String contentType = urlConnection.getContentType();
                    if (contentType != null) {
                        String[] values = contentType.split(";");
                        mimeType = values[0].trim();
                        for (int i = 1; i < values.length; i++) {
                            String value = values[i].trim();
                            if (value.toLowerCase().startsWith("charset=")) {
                                String s = value.substring("charset=".length());
                                // on some crazy result we have charsets with quotes...
                                if (s.length() >= 2 && s.charAt(0) == '"' && s.charAt(s.length() - 1) == '"')
                                    s = s.substring(1, s.length() - 1);
                                if (s.length() > 0) charSet = s;
                            }
                        }
                    }

                    // get content length
                    long contentLength = urlConnection.getContentLength();

                    // get the cache control, last modified date and time and the etag from the headers
                    String cacheControl = urlConnection.getHeaderField("Cache-Control");
                    Date lastModified = null;
                    String s = urlConnection.getHeaderField("Last-Modified");
                    if (s != null)
                        try {
                            lastModified = DateUtils.parseDate(s);
                        } catch (Exception e) { /* not really sure if this could  happen */ }
                    String etag = urlConnection.getHeaderField("Etag");

                    // get the content disposition
                    String contentDisposition = urlConnection.getHeaderField("Content-Disposition");
                    HttpContentDisposition httpContentDisposition =
                            contentDisposition == null ? null : new HttpContentDisposition(contentDisposition);

                    // build the header object
                    HttpAnswerHeader header = new HttpAnswerHeader(currentUrl, urlConnection.getResponseCode(),
                            urlConnection.getResponseMessage(), mimeType, charSet,
                            contentLength, new HttpCacheControlInfo(cacheControl, lastModified, etag),
                            httpContentDisposition);

                    if (Misc.DEBUG)
                        Log.d(TAG, httpFetcherIndex + "=" + index + " " + header);

                    // get the input stream and parse the contents
                    return new HttpAnswer(header, parser == null || instream == null ? null :
                            parser.parse(header, instream));

                } finally {

                    // if we got a stream just close it: in case a few (or no) data is still
                    // present, it will be flushed and the connection will be reused.
                    // In other cases try to disconnect
                    try {
                        if (instream != null) {
                            instream.close();
                            if (!disconnect) urlConnection = null;
                            if (Misc.DEBUG)
                                Log.d(TAG, httpFetcherIndex + "<" + index + " closed");
                        }
                    } finally {
                        if (urlConnection != null) {
                            urlConnection.disconnect();
                            if (Misc.DEBUG)
                                Log.d(TAG, httpFetcherIndex + "<" + index + " disconnected");
                        }
                    }
                }
            };

            // schedule the request for execution
            Future<HttpAnswer> future;
            try {
                synchronized (this) {

                    // check if we are in shutdown
                    if (shutdown) return null;

                    // submit the request to the executor
                    future = executor.submit(callable);

                    // publish the current request for cancellation
                    this.future = future;
                }
            }
            catch (Exception e) {
                // something went wrong, abort
                if (Misc.DEBUG) Log.w(TAG, httpFetcherIndex + "#" + index + " Error in Future submission", e);
                return null;
            }

            try {
                // submit the request to the executor
                return future.get();
            } catch (CancellationException e) {
                // request has been cancelled
                if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + "<" + index + " cancelled");
                return null;
            } catch (InterruptedException e) {
                // we have been interrupted
                throw new IOException(e);
            } catch (ExecutionException e) {
                // got error in fetch
                if (Misc.DEBUG) Log.i(TAG, httpFetcherIndex + "#" + index + " Error requesting URL: " + uri, e);
                throw new IOException(e);
            }
            finally {
                synchronized (this) {
                    // clear the current request
                    this.future = null;
                }
            }
        }
	}

	@Override
    synchronized public void abort()
	{
		if (future != null) {
            if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + ">* cancel");
            future.cancel(true);
            future = null;
        }
	}

	@Override
    synchronized public void shutdown()
	{
        if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + ">* shutdown");

        // abort pending task if any and avoid new ones to be executed
        abort();

        // set the shutdown flag
        shutdown = true;
	}
}
