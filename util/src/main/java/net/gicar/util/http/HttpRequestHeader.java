package net.gicar.util.http;

/** Holds additional parameters for HTTP gets */
public class HttpRequestHeader {

	/** The user agent */
	private final String userAgent;

	/** The cache control information */
	private final HttpCacheControlInfo cacheControlInfo;

	/** The default charset if different from the HTTP default of ISO-8859-1 (use null in this case) */
	private final String defaultCharset;

	public HttpRequestHeader(String userAgent, HttpCacheControlInfo cacheControlInfo, String defaultCharset)
	{
		this.userAgent = userAgent;
        this.cacheControlInfo = cacheControlInfo;
        this.defaultCharset = defaultCharset;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public HttpCacheControlInfo getCacheControlInfo() {
		return cacheControlInfo;
	}

	public String getDefaultCharset() {
        return defaultCharset;
    }

    @Override
    public String toString() {
        return "HttpRequestHeader{" +
                "userAgent='" + userAgent + '\'' +
                ", cacheControlInfo=" + cacheControlInfo +
                ", defaultCharset='" + defaultCharset + '\'' +
                '}';
    }
}
