package net.gicar.util.http;

import java.io.IOException;
import java.io.OutputStream;

/** Interface that sends data to an HTTP connection */
public interface HttpProducer {

	/**
	 * Used to specify if the contents of the request shall be eventually compressed.
	 * @return <code>true</code> if the contents are compressible
	 */
	boolean prefersGzip();

    /**
     * Provides the content type to be specified in the POST
     * @return The Content-Type of null if none
     */
	String getContentType();

    /**
     * Provides the length of the data that will be send or requests chunk mode
     * @return The length of the data that will be send, or -1 to use chunk mode
     */
	long getDataLength();

	/**
	 * Push the request content. The stream will be compressed if needed
	 * @param stream The output stream to be written
	 */
	void produce(OutputStream stream) throws IOException;

    /**
     * Specifies the size of the buffering
     * @return The size of the buffering for buffered stream, or 0 to not use it
     */
    default int streamBufferSize() {
        return 8192;
    }
}
