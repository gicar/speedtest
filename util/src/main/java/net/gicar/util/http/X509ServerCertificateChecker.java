package net.gicar.util.http;

import java.security.cert.X509Certificate;

/** Checks if the specified certificate chain can be trusted */
public interface X509ServerCertificateChecker {
    /**
     * Given the partial or complete certificate chain provided by the
     * peer, build a certificate path to a trusted root and return if
     * it can be validated and is trusted for server SSL
     * authentication based on the authentication type.
     * @param chain the peer certificate chain
     * @param authType the key exchange algorithm used
     * @return true if the check is OK, false if there is a CertificateException
     */
    boolean isServerTrusted(X509Certificate[] chain, String authType);
}
