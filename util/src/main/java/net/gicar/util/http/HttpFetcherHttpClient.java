//package net.gicar.util.http;
//
//import android.util.Log;
//
//import org.apache.http.Header;
//import org.apache.http.HeaderElement;
//import org.apache.http.HeaderIterator;
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpHost;
//import org.apache.http.HttpResponse;
//import org.apache.http.NameValuePair;
//import org.apache.http.ParseException;
//import org.apache.http.StatusLine;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpHead;
//import org.apache.http.client.methods.HttpRequestBase;
//import org.apache.http.client.methods.HttpUriRequest;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.impl.cookie.DateParseException;
//import org.apache.http.impl.cookie.DateUtils;
//import org.apache.http.message.BasicHeaderIterator;
//import org.apache.http.protocol.BasicHttpContext;
//import org.apache.http.protocol.ExecutionContext;
//import org.apache.http.protocol.HTTP;
//import org.apache.http.protocol.HttpContext;
//import org.apache.http.util.ByteArrayBuffer;
//import org.apache.http.util.CharArrayBuffer;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.Reader;
//import java.util.Date;
//import java.util.concurrent.atomic.AtomicInteger;
//import java.util.zip.GZIPInputStream;
//
//import net.gicar.util.Misc;
//
///**
// * Utility class to perform connections to an HTTP server.
// * There shall be a single background thread assigned to each HttpFetcherHttpClient object.
// */
//public class HttpFetcherHttpClient extends HttpFetcher {
//
//	/** The log tag */
//	private final static String TAG = "HttpClient";
//
//	/** Used to generate unique indexes for fetchers */
//	private final static AtomicInteger fetcherIndex = new AtomicInteger(1);
//
//	/** Used to generate unique indexes for requests */
//	private final static AtomicInteger requestIndex = new AtomicInteger(1);
//
//	/** Get an unique index for this fetcher */
//	private final int httpFetcherIndex = fetcherIndex.getAndIncrement();
//
//	/** Lock used to allow only one request at a time for instance */
//	private final Object requestLock = new Object();
//
//	/** The HTTP client object */
//    private HttpClient httpClient = new DefaultHttpClient();
//
//    /** The request currently in execution */
//    private HttpRequestBase httpRequest = null;
//
//    /** The index of the request currently in execution */
//    private int httpRequestIndex = 0;
//
//	/** Internal thread used to control abort and shutdown execution */
//	private ControlThread controlThread;
//
//	public HttpFetcherHttpClient()
//	{
//		if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + ">* HTTP fetcher created");
//
//		// create and start the control thread
//		controlThread = new ControlThread();
//		controlThread.start();
//	}
//
//	@Override
//    public HttpAnswer getUri(String uri, HttpParser parser, String defaultCharset, HttpRequestHeader requestHeader)
//			throws IOException
//	{
//		// fix the default charset if not specified
//		if (defaultCharset == null) defaultCharset = HTTP.DEFAULT_CONTENT_CHARSET;
//
//		// get an unique index for this request
//		int index = requestIndex.getAndIncrement();
//
//		if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + ">" + index + " Requesting URL: " + uri + ", " + requestHeader);
//
//		// only one request at a time is allowed,
//		// this lock is needed to avoid a new request tries to begin before a previous one is aborted
//		synchronized (requestLock) {
//
//			InputStream instream = null;
//			try {
//				// get the client and create the get request
//				HttpClient client;
//				HttpRequestBase request;
//		        synchronized (this) {
//
//		        	// if no httpClient available, shutdown has been called
//		        	if (httpClient == null) return null;
//
//		        	// get the httpClient and create the request
//		        	client = httpClient;
//		        	request = httpRequest = (parser != null ? new HttpGet(uri) : new HttpHead(uri));
//		        	httpRequestIndex = index;
//		        }
//
//		        // set request parameters
//		        if (parser != null && parser.prefersGzip()) request.addHeader("Accept-Encoding", "gzip");
//		        if (requestHeader != null) {
//
//		        	// set user agent
//		        	String userAgent = requestHeader.getUserAgent();
//		        	if (userAgent != null)
//		        		request.setHeader("User-Agent", userAgent);
//
//		        	// set cache controlling headers
//		        	HttpCacheControlInfo cacheControlInfo = requestHeader.getCacheControlInfo();
//			        if (cacheControlInfo != null) {
//			        	Date lastModified = cacheControlInfo.getLastModified();
//			        	if (lastModified != null) request.addHeader("If-Modified-Since", DateUtils.formatDate(lastModified));
//			        	String etag = cacheControlInfo.getEtag();
//			        	if (etag != null) request.addHeader("If-None-Match", etag);
//			        }
//		        }
//
//				// start its execution
//                HttpContext context = new BasicHttpContext();
//		        HttpResponse response = client.execute(request, context);
//
//                // retrieve the actual URL after redirections if any
//                HttpUriRequest currentReq = (HttpUriRequest) context.getAttribute(ExecutionContext.HTTP_REQUEST);
//                HttpHost currentHost = (HttpHost)  context.getAttribute(ExecutionContext.HTTP_TARGET_HOST);
//                String currentUrl = (currentReq.getURI().isAbsolute()) ? currentReq.getURI().toString() : (currentHost.toURI() + currentReq.getURI());
//
//		        // get hold of the response data
//		        StatusLine statusLine = response.getStatusLine();
//		        Header[] headers = response.getAllHeaders();
//		        HttpEntity entity = response.getEntity();
//		        instream = entity.getContent();
//
//		        // get relevant data from entity
//		        String mimeType = EntityUtils.getContentMimeType(entity);
//		        String charSet = EntityUtils.getContentCharSet(entity);
//		        if (charSet == null) charSet = defaultCharset;
//		        long contentLength = entity.getContentLength();
//
//		        // check for data compression
//		        if (instream != null) {
//	                Header ceheader = entity.getContentEncoding();
//	                if (ceheader != null) {
//	                    HeaderElement[] codecs = ceheader.getElements();
//	                    for (int i = 0; i < codecs.length; i++) {
//	                        if (codecs[i].getName().equalsIgnoreCase("gzip")) {
//	                        	instream = new GZIPInputStream(instream);
//	                        	contentLength = -1;
//	                        	break;
//	                        }
//	                    }
//	                }
//		        }
//
//                // get the last modified date and time and the etag from the headers
//                Date lastModified = null;
//                String etag = null;
//                HeaderIterator it = new BasicHeaderIterator(headers, "Last-Modified");
//                if (it.hasNext())
//                    try {
//                        lastModified = DateUtils.parseDate(((Header) it.next()).getValue());
//                    } catch (DateParseException e) {}
//                it = new BasicHeaderIterator(headers, "Etag");
//                if (it.hasNext()) etag = ((Header) it.next()).getValue();
//
//                // build the header object
//		        HttpAnswerHeader header = new HttpAnswerHeader(currentUrl, statusLine.getStatusCode(),
//                        statusLine.getReasonPhrase(), mimeType, charSet,
//                        contentLength, new HttpCacheControlInfo(lastModified, etag));
//
//		        if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + "<" + index + " " + header);
//
//                // get the input stream and parse the contents
//	        	return new HttpAnswer(header, parser == null || instream == null ? null : parser.parse(header, instream));
//			}
//			finally {
//
//				// close the stream if present
//				if (instream != null) instream.close();
//
//				synchronized (this) {
//
//					// if the client is null we are in shutdown
//					if (httpClient == null) return null;
//
//					// if the request is null, it has been aborted
//	            	if (httpRequest == null) {
//
//	            		// replace the client as the current one is no longer reliable
//	            		httpClient.getConnectionManager().shutdown();
//	            		httpClient = new DefaultHttpClient();
//
//	            		if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + "<" + index + " Aborted");
//	            		return null;
//	            	}
//
//	            	// mark the request as completed
//	            	httpRequest = null;
//		        	httpRequestIndex = 0;
//	            }
//			}
//		}
//	}
//
//	/** This class is used to control the execution of abort and shutdown requests */
//	private class ControlThread extends Thread {
//
//		/** Index of the request to be aborted or 0 if none */
//		int index = 0;
//
//		/** If true we have received a shutdown request */
//		boolean shutdown = false;
//
//		public ControlThread() {
//			super("HttpFetcherHttpClient#" + httpFetcherIndex);
//		}
//
//		@Override
//		public void run()
//		{
//			synchronized (HttpFetcherHttpClient.this) {
//
//				try {
//					// loop until there is a shutdown request
//					while (!shutdown) {
//
//						// wait for a notify to happen
//                        HttpFetcherHttpClient.this.wait();
//
//						// check again if our original request is still ongoing, or any if we are in shutdown mode
//						if (httpRequest != null && (shutdown || httpRequestIndex == index)) {
//							httpRequest.abort();
//							httpRequest = null;
//						}
//
//						// on shutdown delete the httpClient if any
//						if (shutdown && httpClient != null) {
//							httpClient.getConnectionManager().shutdown();
//							httpClient = null;
//						}
//					}
//				} catch (InterruptedException e) {}
//
//				// we shall be no longer accessible
//				controlThread = null;
//
//				if (Misc.DEBUG) Log.d(TAG, httpFetcherIndex + "<* HTTP fetcher shutdown");
//			}
//		}
//	}
//
//	@Override
//    synchronized public void abort()
//	{
//		if (controlThread != null && httpRequestIndex != 0) {
//			controlThread.index = httpRequestIndex;
//			notify();
//		}
//	}
//
//	@Override
//    synchronized public void shutdown()
//	{
//		if (controlThread != null) {
//			controlThread.index = httpRequestIndex;
//			controlThread.shutdown = true;
//			notify();
//		}
//	}
//
//    /**
//     * Static helpers for dealing with {@link HttpEntity}s.
//     *
//     * @since 4.0
//     */
//    private static final class EntityUtils {
//
//        private EntityUtils() {
//        }
//
//        /**
//         * Ensures that the entity content is fully consumed and the content stream, if exists,
//         * is closed.
//         *
//         * @param entity
//         * @throws IOException if an error occurs reading the input stream
//         *
//         * @since 4.1
//         */
//        public static void consume(final HttpEntity entity) throws IOException {
//            if (entity == null) {
//                return;
//            }
//            if (entity.isStreaming()) {
//                InputStream instream = entity.getContent();
//                if (instream != null) {
//                    instream.close();
//                }
//            }
//        }
//
//        /**
//         * Read the contents of an entity and return it as a byte array.
//         *
//         * @param entity
//         * @return byte array containing the entity content. May be null if
//         *   {@link HttpEntity#getContent()} is null.
//         * @throws IOException if an error occurs reading the input stream
//         * @throws IllegalArgumentException if entity is null or if content length > Integer.MAX_VALUE
//         */
//        public static byte[] toByteArray(final HttpEntity entity) throws IOException {
//            if (entity == null) {
//                throw new IllegalArgumentException("HTTP entity may not be null");
//            }
//            InputStream instream = entity.getContent();
//            if (instream == null) {
//                return null;
//            }
//            try {
//                if (entity.getContentLength() > Integer.MAX_VALUE) {
//                    throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
//                }
//                int i = (int)entity.getContentLength();
//                if (i < 0) {
//                    i = 4096;
//                }
//                ByteArrayBuffer buffer = new ByteArrayBuffer(i);
//                byte[] tmp = new byte[4096];
//                int l;
//                while((l = instream.read(tmp)) != -1) {
//                    buffer.append(tmp, 0, l);
//                }
//                return buffer.toByteArray();
//            } finally {
//                instream.close();
//            }
//        }
//
//        /**
//         * Obtains character set of the entity, if known.
//         *
//         * @param entity must not be null
//         * @return the character set, or null if not found
//         * @throws ParseException if header elements cannot be parsed
//         * @throws IllegalArgumentException if entity is null
//         */
//        public static String getContentCharSet(final HttpEntity entity) throws ParseException {
//            if (entity == null) {
//                throw new IllegalArgumentException("HTTP entity may not be null");
//            }
//            String charset = null;
//            if (entity.getContentType() != null) {
//                HeaderElement values[] = entity.getContentType().getElements();
//                if (values.length > 0) {
//                    NameValuePair param = values[0].getParameterByName("charset");
//                    if (param != null) {
//                        charset = param.getValue();
//                    }
//                }
//            }
//            return charset;
//        }
//
//        /**
//         * Obtains mime type of the entity, if known.
//         *
//         * @param entity must not be null
//         * @return the character set, or null if not found
//         * @throws ParseException if header elements cannot be parsed
//         * @throws IllegalArgumentException if entity is null
//         *
//         * @since 4.1
//         */
//        public static String getContentMimeType(final HttpEntity entity) throws ParseException {
//            if (entity == null) {
//                throw new IllegalArgumentException("HTTP entity may not be null");
//            }
//            String mimeType = null;
//            if (entity.getContentType() != null) {
//                HeaderElement values[] = entity.getContentType().getElements();
//                if (values.length > 0) {
//                    mimeType = values[0].getName();
//                }
//            }
//            return mimeType;
//        }
//
//        /**
//         * Get the entity content as a String, using the provided default character set
//         * if none is found in the entity.
//         * If defaultCharset is null, the default "ISO-8859-1" is used.
//         *
//         * @param entity must not be null
//         * @param defaultCharset character set to be applied if none found in the entity
//         * @return the entity content as a String. May be null if
//         *   {@link HttpEntity#getContent()} is null.
//         * @throws ParseException if header elements cannot be parsed
//         * @throws IllegalArgumentException if entity is null or if content length > Integer.MAX_VALUE
//         * @throws IOException if an error occurs reading the input stream
//         */
//        public static String toString(
//                final HttpEntity entity, final String defaultCharset) throws IOException, ParseException {
//            if (entity == null) {
//                throw new IllegalArgumentException("HTTP entity may not be null");
//            }
//            InputStream instream = entity.getContent();
//            if (instream == null) {
//                return null;
//            }
//            try {
//                if (entity.getContentLength() > Integer.MAX_VALUE) {
//                    throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
//                }
//                int i = (int)entity.getContentLength();
//                if (i < 0) {
//                    i = 4096;
//                }
//                String charset = getContentCharSet(entity);
//                if (charset == null) {
//                    charset = defaultCharset;
//                }
//                if (charset == null) {
//                    charset = HTTP.DEFAULT_CONTENT_CHARSET;
//                }
//                Reader reader = new InputStreamReader(instream, charset);
//                CharArrayBuffer buffer = new CharArrayBuffer(i);
//                char[] tmp = new char[1024];
//                int l;
//                while((l = reader.read(tmp)) != -1) {
//                    buffer.append(tmp, 0, l);
//                }
//                return buffer.toString();
//            } finally {
//                instream.close();
//            }
//        }
//
//        /**
//         * Read the contents of an entity and return it as a String.
//         * The content is converted using the character set from the entity (if any),
//         * failing that, "ISO-8859-1" is used.
//         *
//         * @param entity
//         * @return String containing the content.
//         * @throws ParseException if header elements cannot be parsed
//         * @throws IllegalArgumentException if entity is null or if content length > Integer.MAX_VALUE
//         * @throws IOException if an error occurs reading the input stream
//         */
//        public static String toString(final HttpEntity entity)
//                throws IOException, ParseException {
//            return toString(entity, null);
//        }
//    }
//}
