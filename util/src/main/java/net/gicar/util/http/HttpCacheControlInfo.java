package net.gicar.util.http;

import java.util.Date;

/** This class holds information needed for HTTP cache control mechanism */
public class HttpCacheControlInfo {

	/** The main cache control option */
	private final String cacheControl;

	/** The last modified date and time of this element or <code>null</code> if not available */
	private final Date lastModified;
	
	/** The ETag associated to the element or <code>null</code> if not available */
	private final String etag;

    public HttpCacheControlInfo(String cacheControl, Date lastModified, String etag) {
		this.cacheControl = cacheControl;
		this.lastModified = lastModified;
        this.etag = etag;
    }

	public String getCacheControl() {
		return cacheControl;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public String getEtag() {
		return etag;
	}
	
	public String toString()
	{
		return "LastModified: "+ lastModified + ", etag: " + etag;
	}
}
