package net.gicar.util.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.regex.Pattern;

/** Parser class for HTML substrings */
public class HttpParserHtmlSubstring implements HttpParser {

    private final Pattern p;

    public HttpParserHtmlSubstring(Pattern p) { this.p = p; }

    @SuppressWarnings("resource")
    public Object parse(HttpAnswerHeader header, InputStream stream) throws IOException
    {
        // check the header info
        String mimeType = header.getMimeType();
        if (header.getStatusCode() != 200 || !(mimeType.equals("text/html") || mimeType.equals("application/xhtml+xml")))
            return null;

        // find the specified pattern, returning null if no match
        return new Scanner(new BufferedReader(new InputStreamReader(stream, header.getCharSet()), 8192)).
                findWithinHorizon(p, 0);
    }

    public boolean prefersGzip() {
        return true;
    }
}
