package net.gicar.util.http;

public class HttpAnswerHeader {

	/** The original URI of the request */
	final private String uri;
	
	/** The answer status code */
	final private int statusCode;
	
	/** The answer reason phrase */
	final private String reasonPhrase;
	
	/** The MIME type of the entity */
	final private String mimeType;
	
	/** The encoding charset of the entity */
	final private String charSet;
	
	/** The length of the entity contents, -1 if not known (e.g. uses compression) */
	final private long length;

	/** Cache control info */
	final private HttpCacheControlInfo cacheControlInfo;

	/** The content disposition information if present */
	final private HttpContentDisposition contentDisposition;

	public HttpAnswerHeader(String uri, int statusCode, String reasonPhrase, String mimeType, String charSet,
                            long length, HttpCacheControlInfo cacheControlInfo, HttpContentDisposition contentDisposition)
    {
        this.uri = uri;
        this.statusCode = statusCode;
        this.reasonPhrase = reasonPhrase;
        this.mimeType = mimeType;
        this.charSet = charSet;
        this.length = length;
		this.cacheControlInfo = cacheControlInfo;
		this.contentDisposition = contentDisposition;
    }

	public String getUri() {
		return uri;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getReasonPhrase() {
		return reasonPhrase;
	}

	public String getMimeType() {
		return mimeType;
	}

	public String getCharSet() {
		return charSet;
	}

	public long getContentLength() {
		return length;
	}
	
	public HttpCacheControlInfo getCacheControlInfo() {
		return cacheControlInfo;
	}

	public HttpContentDisposition getContentDisposition() {
		return contentDisposition;
	}

	@Override
	public String toString() {
		return "HttpAnswerHeader{" +
				"uri='" + uri + '\'' +
				", statusCode=" + statusCode +
				", reasonPhrase='" + reasonPhrase + '\'' +
				", mimeType='" + mimeType + '\'' +
				", charSet='" + charSet + '\'' +
				", length=" + length +
				", cacheControlInfo=" + cacheControlInfo +
				", contentDisposition=" + contentDisposition +
				'}';
	}
}
