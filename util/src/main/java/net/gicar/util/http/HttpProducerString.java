package net.gicar.util.http;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class HttpProducerString implements HttpProducer {

    private final byte[] string;

    public HttpProducerString(String string) {
        this(string, Charset.forName(HttpFetcherURLConnection.DEFAULT_CONTENT_CHARSET));
    }

    public HttpProducerString(String string, Charset charset) {
        this.string = string.getBytes(charset);
    }

    @Override
    public boolean prefersGzip() { return true; }

    @Override
    public String getContentType() {
        return "text/plain";
    }

    @Override
    public long getDataLength() {
        return string.length;
    }

    @Override
    public void produce(OutputStream stream) throws IOException {
        stream.write(string);
    }
}
