package net.gicar.util.http;

import java.net.InetAddress;
import java.net.Socket;

/** Receives callbacks for created sockets */
public interface SocketCreationObserver {

    void onSocketCreated(Socket socket, String host, InetAddress address, int port, InetAddress localAddress, Integer localPort);
}
