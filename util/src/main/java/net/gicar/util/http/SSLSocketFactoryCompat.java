package net.gicar.util.http;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import net.gicar.util.Misc;
import net.gicar.util.services.CrashReporting;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

/** Fixes SSL/TSL options depending on Android version */
class SSLSocketFactoryCompat extends SSLSocketFactory {

    /** The log tag */
    private final static String TAG = "SSLSocketFactoryCompat";

    /** The default SSLSocketFactory */
    private final SSLSocketFactory defaultFactory;

    /** Observer for sockets creation */
    private final SocketCreationObserver observer;

    /** The protocols to be enabled, if not null */
    private static String[] protocols;

    static {
        // on pre-Lollipop we need to enable TLSv1.1 and TLSv1.2
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            protocols = new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"};
    }

    SSLSocketFactoryCompat() {
        this(null, null, null, null);
    }

    SSLSocketFactoryCompat(KeyManager[] km, TrustManager[] tm, SecureRandom random, SocketCreationObserver observer) {
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(km, tm, random);
            defaultFactory = sslContext.getSocketFactory();
        } catch (GeneralSecurityException e) {
            if (Misc.DEBUG) Log.e(TAG, "Seems that TLS is not available", e);
            CrashReporting.logException(e);
            throw new AssertionError(); // The system has no TLS. Just give up.
        }
        this.observer = observer;
    }

    private void upgradeTLS(SSLSocket ssl) {
        if (protocols != null) {
            if (Misc.DEBUG) Log.d(TAG, "Setting allowed TLS protocols: " + TextUtils.join(", ", protocols));
            ssl.setEnabledProtocols(protocols);
        }
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return defaultFactory.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return defaultFactory.getSupportedCipherSuites();
    }

    @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        if (Misc.DEBUG) Log.d(TAG, "createSocket() called with: s = [" + s + "], host = [" + host + "], port = [" + port + "], autoClose = [" + autoClose + "]");
        Socket ssl = defaultFactory.createSocket(s, host, port, autoClose);
        if (ssl instanceof SSLSocket)
            upgradeTLS((SSLSocket)ssl);
        if (observer != null && ssl != null)
            observer.onSocketCreated(ssl, host, null, port, null, null);
        return ssl;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        if (Misc.DEBUG) Log.d(TAG, "createSocket() called with: host = [" + host + "], port = [" + port + "]");
        Socket ssl = defaultFactory.createSocket(host, port);
        if (ssl instanceof SSLSocket)
            upgradeTLS((SSLSocket)ssl);
        if (observer != null && ssl != null)
            observer.onSocketCreated(ssl, host, null, port, null, null);
        return ssl;
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
        if (Misc.DEBUG) Log.d(TAG, "createSocket() called with: host = [" + host + "], port = [" + port + "], localHost = [" + localHost + "], localPort = [" + localPort + "]");
        Socket ssl = defaultFactory.createSocket(host, port, localHost, localPort);
        if (ssl instanceof SSLSocket)
            upgradeTLS((SSLSocket)ssl);
        if (observer != null && ssl != null)
            observer.onSocketCreated(ssl, host, null, port, localHost, localPort);
        return ssl;
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        if (Misc.DEBUG) Log.d(TAG, "createSocket() called with: host = [" + host + "], port = [" + port + "]");
        Socket ssl = defaultFactory.createSocket(host, port);
        if (ssl instanceof SSLSocket)
            upgradeTLS((SSLSocket)ssl);
        observer.onSocketCreated(ssl, null, host, port, null, null);
        return ssl;
    }

    @Override
    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        if (Misc.DEBUG) Log.d(TAG, "createSocket() called with: address = [" + address + "], port = [" + port + "], localAddress = [" + localAddress + "], localPort = [" + localPort + "]");
        Socket ssl = defaultFactory.createSocket(address, port, localAddress, localPort);
        if (ssl instanceof SSLSocket)
            upgradeTLS((SSLSocket)ssl);
        observer.onSocketCreated(ssl, null, address, port, localAddress, localPort);
        return ssl;
    }
}
